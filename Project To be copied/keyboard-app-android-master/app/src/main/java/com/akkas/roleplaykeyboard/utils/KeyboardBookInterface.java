package com.akkas.roleplaykeyboard.utils;

public interface KeyboardBookInterface {
    public void getLines(String id, String bookType, String bookName);
}

package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.db.Chapter;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.model.ChapterModel;
import com.akkas.roleplaykeyboard.model.RoleplayModel;
import com.akkas.roleplaykeyboard.network.VolleyLibrary;
import com.akkas.roleplaykeyboard.utils.AppConstants;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by usman on 7/10/2018.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHolder> {

    List<ChapterModel> chapterList;
    Context context;

    public BookAdapter(List<ChapterModel> chapterList, Context context) {
        this.chapterList = chapterList;
        this.context = context;
    }

    @Override
    public BookAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, parent, false);
        return new BookAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BookAdapter.MyViewHolder holder, int position) {

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(holder.viewGroup);

        final ChapterModel chapterModel = chapterList.get(position);

        if (chapterModel.isSaved()) {
            holder.checkedIv.setVisibility(View.VISIBLE);
        } else {
            holder.checkedIv.setVisibility(View.INVISIBLE);
        }

        try {
            Glide.with(context).load(chapterModel.getBookImgUrl()).apply(new RequestOptions().fitCenter().centerCrop().placeholder(R.drawable.book_placeholder)).into(holder.bookIv);
//            Picasso.get().load(chapterModel.getBookImgUrl()).fit().centerCrop().into(holder.bookIv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.bookNameTv.setText(chapterModel.getBookName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.checkedIv.isShown()){
                    Chapter.deleteAll(Chapter.class, "bookname = ?", chapterModel.getBookName());
                    holder.checkedIv.setVisibility(View.INVISIBLE);
                    chapterModel.setSaved(false);
                } else {
                    holder.checkedIv.setVisibility(View.VISIBLE);
                    chapterModel.setSaved(true);
                    getChapterOfTheBook(chapterModel, holder, chapterModel.getId(), chapterModel.getBookName(), chapterModel.getBookImgUrl(), chapterModel.getCategoryId(), chapterModel.getSubCategoryId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return chapterList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView bookIv, checkedIv;
        TextView bookNameTv;
        LinearLayout viewGroup;

        public MyViewHolder(View itemView) {
            super(itemView);
            viewGroup = itemView.findViewById(R.id.view_group);
            checkedIv = itemView.findViewById(R.id.download_checked_iv);
            bookNameTv = itemView.findViewById(R.id.book_name_tv);
            bookIv = itemView.findViewById(R.id.book_iv);
        }

    }

    private void getChapterOfTheBook(final ChapterModel chapterModel, final MyViewHolder holder, final int entryId, final String bookName, final String bookImgUrl, final int categoryId, final int subCategoryId) {
        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("start", 0);
            jsonObject.put("limit", 50);
            jsonObject.put("order", "asc");
            jsonObject.put("entry_id", entryId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstants.DATABASE_CHAPTERS, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray bodyArray = response.getJSONArray("body");

                    for (int i = 0; i < bodyArray.length(); i++) {
                        JSONObject jsonObject1 = bodyArray.getJSONObject(i);
                        JSONObject emotionObject = jsonObject1.getJSONObject("emotion");

                        Chapter chapter = new Chapter();

                        chapter.setBookexampletype(emotionObject.getString("name"));
                        chapter.setBookdesc(jsonObject1.getString("description"));
                        chapter.setBookimageurl(bookImgUrl);
                        chapter.setBookname(bookName);

                        chapter.setCategoryid(categoryId);
                        chapter.setSubcategoryid(subCategoryId);
                        chapter.setEntryid(entryId);
                        chapter.save();

                        if (holder.checkedIv.getVisibility() == View.INVISIBLE) {
                            holder.checkedIv.setVisibility(View.VISIBLE);
                            chapterModel.setSaved(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleyLibrary.getInstance(context).addToRequestQueue(jsonObjectRequest, "movies", false);
    }
}

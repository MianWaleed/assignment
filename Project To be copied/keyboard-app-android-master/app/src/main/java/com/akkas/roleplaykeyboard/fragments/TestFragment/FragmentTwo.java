package com.akkas.roleplaykeyboard.fragments.TestFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.receiver.AlarmReceiver;

public class FragmentTwo extends Fragment {
    View view;
    private BroadcastReceiver mBroadcastReceiver;
    TextView fromReceiverTv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.two_fragment, container, false);

        init();

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (AlarmReceiver.ACTION_CUSTOM.equals(intent.getAction())) {
                    fromReceiverTv.setText(intent.getStringExtra("enter"));
                }
            }
        };


        LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter(AlarmReceiver.ACTION_CUSTOM));

        return view;
    }

    private void init() {
        fromReceiverTv = view.findViewById(R.id.from_receiver_tv);
    }


}
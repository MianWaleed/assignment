package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.model.RoleplayModel;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by usman on 7/7/2018.
 */

public class RoleplayAdapter extends RecyclerView.Adapter<RoleplayAdapter.MyViewHolder> {

    List<RoleplayModel> roleList;
    Context context;

    public RoleplayAdapter(List<RoleplayModel> roleList, Context context) {
        this.roleList = roleList;
        this.context = context;
    }

    @Override
    public RoleplayAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.role_play_item, parent, false);
        return new RoleplayAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RoleplayAdapter.MyViewHolder holder, int position) {

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(holder.viewGroup);
        final RoleplayModel roleplayModel = roleList.get(position);

        try {
            Glide.with(context).load(roleplayModel.getMovieImageUr()).apply(new RequestOptions().fitCenter().centerCrop().placeholder(R.drawable.movie_placeholder)).into(holder.movieIv);
//            Picasso.get().load(roleplayModel.getMovieImageUr()).fit().centerCrop().into(holder.movieIv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.movieIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MIN",""+roleplayModel.getCastModelList().size());
            }
        });

        holder.movieNameTv.setText(roleplayModel.getMovieName());

        CastAdapter castAdapter = new CastAdapter(roleplayModel.getCastModelList(), context);
        holder.castRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.castRecycler.setAdapter(castAdapter);


    }

    @Override
    public int getItemCount() {
        return roleList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RecyclerView castRecycler;
        ImageView movieIv;
        LinearLayout viewGroup;
        TextView movieNameTv;

        public MyViewHolder(View itemView) {
            super(itemView);

            viewGroup = itemView.findViewById(R.id.view_group);
            movieNameTv = itemView.findViewById(R.id.movie_name_tv);
            castRecycler = itemView.findViewById(R.id.role_cast_recycler);
            movieIv = itemView.findViewById(R.id.role_play_iv);
        }
    }
}

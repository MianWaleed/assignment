package com.akkas.roleplaykeyboard.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by usman on 7/7/2018.
 */

public class CastModel implements Serializable {

    int id;
    int entryId;
    String castImageUrl, castName;
    boolean isSaved;

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getCastImageUrl() {
        return castImageUrl;
    }

    public void setCastImageUrl(String castImageUrl) {
        this.castImageUrl = castImageUrl;
    }

    public String getCastName() {
        return castName;
    }

    public void setCastName(String castName) {
        this.castName = castName;
    }
}

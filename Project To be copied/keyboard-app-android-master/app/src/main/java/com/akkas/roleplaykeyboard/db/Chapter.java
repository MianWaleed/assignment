package com.akkas.roleplaykeyboard.db;

import com.orm.SugarRecord;

/**
 * Created by usman on 7/17/2018.
 */

public class Chapter extends SugarRecord<Chapter> {
    String bookname, bookimageurl, bookexampletype, bookdesc;
    int categoryid, subcategoryid, entryid;

    public Chapter(String bookname, String bookimageurl, String bookexampletype, String bookdesc, int categoryid, int subcategoryid, int entryid) {
        this.bookname = bookname;
        this.bookimageurl = bookimageurl;
        this.bookexampletype = bookexampletype;
        this.bookdesc = bookdesc;
        this.categoryid = categoryid;
        this.subcategoryid = subcategoryid;
        this.entryid = entryid;
    }

    public Chapter() {
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookimageurl() {
        return bookimageurl;
    }

    public void setBookimageurl(String bookimageurl) {
        this.bookimageurl = bookimageurl;
    }

    public String getBookexampletype() {
        return bookexampletype;
    }

    public void setBookexampletype(String bookexampletype) {
        this.bookexampletype = bookexampletype;
    }

    public String getBookdesc() {
        return bookdesc;
    }

    public void setBookdesc(String bookdesc) {
        this.bookdesc = bookdesc;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public int getSubcategoryid() {
        return subcategoryid;
    }

    public void setSubcategoryid(int subcategoryid) {
        this.subcategoryid = subcategoryid;
    }

    public int getEntryid() {
        return entryid;
    }

    public void setEntryid(int entryid) {
        this.entryid = entryid;
    }
}

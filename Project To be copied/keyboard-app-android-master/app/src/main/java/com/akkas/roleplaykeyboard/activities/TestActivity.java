package com.akkas.roleplaykeyboard.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.fragments.TestFragment.FragmentOne;
import com.akkas.roleplaykeyboard.fragments.TestFragment.FragmentTwo;
import com.akkas.roleplaykeyboard.receiver.AlarmReceiver;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        loadOneFragment();
        loadSecondFragment();
    }

    private void loadSecondFragment() {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_two, new FragmentTwo());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void loadOneFragment() {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_one, new FragmentOne());
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.db.Chapter;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.utils.ChaptersLinesInterface;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.akkas.roleplaykeyboard.utils.LinesInterface;

import java.util.List;

public class ChaptersLinesAdapter extends RecyclerView.Adapter<ChaptersLinesAdapter.MyViewHolder> {
    List<Chapter> chapterList;
    Context context;
    boolean isFirst = true;
    int currentPosition = 0;
    ChaptersLinesInterface chaptersLinesInterface;
    RecyclerView linesRecycler;

    public ChaptersLinesAdapter(List<Chapter> chapterList, Context context, ChaptersLinesInterface chaptersLinesInterface, RecyclerView linesRecycler) {
        this.chapterList = chapterList;
        this.context = context;
        this.chaptersLinesInterface = chaptersLinesInterface;
        this.linesRecycler = linesRecycler;
    }

    public ChaptersLinesAdapter(List<Chapter> chapterList, Context context) {
        this.chapterList = chapterList;
        this.context = context;
    }

    @Override
    public ChaptersLinesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialogue_item, parent, false);
        return new ChaptersLinesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ChaptersLinesAdapter.MyViewHolder holder, final int position) {

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(holder.viewGroup);

        final Chapter chapter = chapterList.get(position);
        holder.dialogueTv.setText(chapter.getBookdesc());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (isFirst) {
                        currentPosition = holder.getAdapterPosition();
                        holder.isSelectedDialogueView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                        isFirst = false;
                        chaptersLinesInterface.getChapterLines(chapter.getBookdesc());
                    } else {
                        CardView genreTitle = linesRecycler.getLayoutManager().findViewByPosition(currentPosition).findViewById(R.id.is_selected_dialogue_view);
                        genreTitle.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                        currentPosition = holder.getAdapterPosition();

                        holder.isSelectedDialogueView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                        chaptersLinesInterface.getChapterLines(chapter.getBookdesc());
                    }
                } catch (Exception e) {
                    currentPosition = position;
                    chaptersLinesInterface.getChapterLines(chapter.getBookdesc());
                }

                chaptersLinesInterface.getChapterLines(chapter.getBookdesc());
            }
        });
    }

    @Override
    public int getItemCount() {
        return chapterList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dialogueTv;
        LinearLayout viewGroup;
        CardView isSelectedDialogueView;

        public MyViewHolder(View itemView) {
            super(itemView);

            isSelectedDialogueView = itemView.findViewById(R.id.is_selected_dialogue_view);
            viewGroup = itemView.findViewById(R.id.view_group);
            dialogueTv = itemView.findViewById(R.id.dialogue_tv);
        }
    }
}

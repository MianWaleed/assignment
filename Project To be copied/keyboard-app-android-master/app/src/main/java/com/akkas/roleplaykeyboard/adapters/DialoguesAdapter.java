package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.utils.ActorLinesInterface;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.akkas.roleplaykeyboard.utils.LinesInterface;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by usman on 7/18/2018.
 */

public class DialoguesAdapter extends RecyclerView.Adapter<DialoguesAdapter.MyViewHolder> {
    List<Roleplay> dialogueList;
    Context context;
    LinesInterface linesInterface;
    RecyclerView linesRecycler;
    int currentPosition = 0;
    boolean isFirst = true;

    public DialoguesAdapter(List<Roleplay> dialogueList, Context context, LinesInterface linesInterface, RecyclerView linesRecycler) {
        this.dialogueList = dialogueList;
        this.context = context;
        this.linesInterface = linesInterface;
        this.linesRecycler = linesRecycler;
    }

    @Override
    public DialoguesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialogue_item, parent, false);
        return new DialoguesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DialoguesAdapter.MyViewHolder holder, final int position) {

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(holder.viewGroup);

        final Roleplay roleplay = dialogueList.get(position);
        holder.dialogueTv.setText(roleplay.getDialogue());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (isFirst) {
                        currentPosition = holder.getAdapterPosition();
                        holder.isSelectedDialogueView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                        isFirst = false;
                        linesInterface.getLines(roleplay.getDialogue(), 0);
                    } else {
                        CardView genreTitle = linesRecycler.getLayoutManager().findViewByPosition(currentPosition).findViewById(R.id.is_selected_dialogue_view);
                        genreTitle.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                        currentPosition = holder.getAdapterPosition();

                        holder.isSelectedDialogueView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                        linesInterface.getLines(roleplay.getDialogue(), 0);
                    }
                } catch (Exception e) {
                    currentPosition = position;
                    linesInterface.getLines(roleplay.getDialogue(), 0);
                }

//                linesInterface.getLines(roleplay.getDialogue(), 0);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                linesInterface.getLines(roleplay.getDialogue(), 1);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return dialogueList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dialogueTv;
        LinearLayout viewGroup;
        CardView isSelectedDialogueView;

        public MyViewHolder(View itemView) {
            super(itemView);

            isSelectedDialogueView = itemView.findViewById(R.id.is_selected_dialogue_view);
            viewGroup = itemView.findViewById(R.id.view_group);
            dialogueTv = itemView.findViewById(R.id.dialogue_tv);
        }
    }
}

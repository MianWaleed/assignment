package com.akkas.roleplaykeyboard.utils;

/**
 * Created by usman on 7/18/2018.
 */

public interface ActorLinesInterface {
    public void getLines(String actor, String emotion);
}

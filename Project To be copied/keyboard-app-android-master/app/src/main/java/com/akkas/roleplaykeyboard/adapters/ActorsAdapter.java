package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.model.ActorModel;
import com.akkas.roleplaykeyboard.model.ChapterModel;
import com.akkas.roleplaykeyboard.utils.ActorLinesInterface;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by usman on 7/18/2018.
 */

public class ActorsAdapter extends RecyclerView.Adapter<ActorsAdapter.MyViewHolder> {
    List<Roleplay> actorModelList;
    Context context;
    ActorLinesInterface actorLinesInterface;
    boolean isFirst = true;
    RecyclerView actorRecycler;
    int currentPosition = 0, lastPosition = 0;

    public ActorsAdapter(List<Roleplay> actorModelList, Context context, ActorLinesInterface actorLinesInterface, RecyclerView actorRecycler) {
        this.actorModelList = actorModelList;
        this.context = context;
        this.actorLinesInterface = actorLinesInterface;
        this.actorRecycler = actorRecycler;
    }

    @Override
    public ActorsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.actor_item, parent, false);
        return new ActorsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ActorsAdapter.MyViewHolder holder, final int position) {

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(holder.viewGroup);

        final Roleplay actorModel = actorModelList.get(position);

        if (isFirst) {
            actorLinesInterface.getLines(actorModel.getActorname(), actorModel.getEmotion());
            isFirst = false;
            holder.isSelectedCardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }

        holder.actorTv.setText(actorModel.getActorname());
        try {
            Glide.with(context).load(actorModel.getActorimgurl())
                    .apply(new RequestOptions().fitCenter().centerCrop()).into(holder.actorIv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    CardView genreTitle = actorRecycler.getLayoutManager().findViewByPosition(currentPosition).findViewById(R.id.is_selected_layout);
                    genreTitle.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                    currentPosition = holder.getAdapterPosition();
                    holder.isSelectedCardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                actorLinesInterface.getLines(actorModel.getActorname(), actorModel.getEmotion());
            }
        });
    }

    @Override
    public int getItemCount() {
        return actorModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView actorIv;
        TextView actorTv;
        LinearLayout viewGroup;
        CardView isSelectedCardView;

        public MyViewHolder(View itemView) {
            super(itemView);


            isSelectedCardView = itemView.findViewById(R.id.is_selected_layout);
            viewGroup = itemView.findViewById(R.id.view_group);
            actorIv = itemView.findViewById(R.id.actor_iv);
            actorTv = itemView.findViewById(R.id.actor_tv);
        }
    }
}

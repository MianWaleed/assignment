package com.akkas.roleplaykeyboard.utils;

/**
 * Created by usman on 7/9/2018.
 */

public class AppConstants {

    //    public static String BASE_URL = "http://192.168.200.52:8070/";
    public static String BASE_URL = "http://vizapay.com/keyboard/public/";

    public static String IMG_PATH = BASE_URL + "storage/";

    //Movies
    public static String MOVIES_URL = BASE_URL + "api/get_movies";

    //Tv-Serials
    public static String TV_SERIES_URL = BASE_URL + "api/tv_serials";

    //Animation
    public static String ANIMATION_URL = BASE_URL + "api/anime";

    //Get actor dialog
    public static String GET_ACTOR_DIALOGUE = BASE_URL + "api/get_actor_dialouge";

    //Get Physics
    public static String PHYSICS_CHAPTER = BASE_URL + "api/chapters_physics";

    //Get Chemistry
    public static String CHEMISTRY_CHAPTER = BASE_URL + "api/chapters_chemest";

    //Get Bio
    public static String BIO_CHAPTER = BASE_URL + "api/chapters_bio";

    //Get Database Chapters
    public static String DATABASE_CHAPTERS = BASE_URL + "api/get_all_database_dialouges";

}

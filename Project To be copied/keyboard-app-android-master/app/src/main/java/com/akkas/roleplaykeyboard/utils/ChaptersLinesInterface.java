package com.akkas.roleplaykeyboard.utils;

public interface ChaptersLinesInterface {
    public void getChapterLines(String chapterLines);
}

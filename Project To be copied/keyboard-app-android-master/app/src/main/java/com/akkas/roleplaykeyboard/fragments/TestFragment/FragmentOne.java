package com.akkas.roleplaykeyboard.fragments.TestFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.receiver.AlarmReceiver;

public class FragmentOne extends Fragment implements View.OnClickListener {

    View view;
    EditText enterSomethingsEt;
    Button sendBroadcastBt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.one_fragment, container, false);

        init();

        return view;
    }

    private void init() {
        enterSomethingsEt = view.findViewById(R.id.enter_something_et);
        sendBroadcastBt = view.findViewById(R.id.send_broadcast_bt);

        sendBroadcastBt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_broadcast_bt: {
                if (!enterSomethingsEt.getText().toString().isEmpty()){
                    Intent intent = new Intent(AlarmReceiver.ACTION_CUSTOM).putExtra("enter", enterSomethingsEt.getText().toString());
                    LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getActivity());
                    mgr.sendBroadcast(intent);
                }

                break;
            }
        }
    }
}

package com.akkas.roleplaykeyboard.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.adapters.BookAdapter;
import com.akkas.roleplaykeyboard.db.Chapter;
import com.akkas.roleplaykeyboard.model.ChapterModel;
import com.akkas.roleplaykeyboard.network.VolleyLibrary;
import com.akkas.roleplaykeyboard.utils.AppConstants;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usman on 7/10/2018.
 */

public class BookFragment extends Fragment {
    View view;
    RecyclerView bookRecycler;
    BookAdapter bookAdapter;
    List<ChapterModel> chapterList;
    LinearLayout emptyLayout;
    LinearLayout errorLayout;
    ProgressBar progressDatabase;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.book_fragment, container, false);

        init();

        return view;
    }

    public static Fragment getInstance(String url) {
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        BookFragment bookFragment = new BookFragment();
        bookFragment.setArguments(bundle);
        return bookFragment;
    }

    public BookFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getContext().getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    private void init() {
        bookRecycler = view.findViewById(R.id.book_recycler);
        emptyLayout = view.findViewById(R.id.empty_layout);
        chapterList = new ArrayList<>();
        progressDatabase = view.findViewById(R.id.progress_database);
        errorLayout = view.findViewById(R.id.error_layout);
        loadDataFromApi(getArguments().getString("url", ""));
//        loadDummyData();
    }

    private void loadDummyData() {
        for (int i = 0; i < 10; i++) {
            ChapterModel chapterModel = new ChapterModel();
            chapterModel.setBookName("Chapter " + i);
            chapterModel.setBookImgUrl("http://imagens.publico.pt/imagens.aspx/606569?tp=KM&w=220");
            chapterModel.setCategoryId(i);
            chapterModel.setSubCategoryId(i);
            chapterModel.setId(i);
            chapterList.add(chapterModel);
        }

        bookAdapter = new BookAdapter(chapterList, getActivity());
        bookRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        bookRecycler.setAdapter(bookAdapter);
    }

    private void loadDataFromApi(String url) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("start", 0);
            jsonObject.put("limit", 50);
            jsonObject.put("order", "asc");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray bodyArray = response.getJSONArray("body");

                    if (bodyArray.length() == 0) {
                        emptyLayout.setVisibility(View.VISIBLE);
                    }

                    for (int i = 0; i < bodyArray.length(); i++) {
                        progressDatabase.setVisibility(View.GONE);
                        JSONObject jsonObject1 = bodyArray.getJSONObject(i);

                        ChapterModel chapterModel = new ChapterModel();
                        chapterModel.setBookName(jsonObject1.getString("name"));
                        chapterModel.setBookImgUrl(AppConstants.IMG_PATH + jsonObject1.getString("img_path"));
                        chapterModel.setCategoryId(jsonObject1.getInt("category_id"));
                        chapterModel.setSubCategoryId(jsonObject1.getInt("subcategory_id"));
                        chapterModel.setId(jsonObject1.getInt("id"));

                        List<Chapter> chapters = Chapter.find(Chapter.class, "entryid = ? and bookname = ?", jsonObject1.getString("id"), jsonObject1.getString("name"));

                        if (chapters.size() > 0) {
                            chapterModel.setSaved(true);
                        }

                        chapterList.add(chapterModel);
                    }

                    bookAdapter = new BookAdapter(chapterList, getActivity());
                    bookRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    bookRecycler.setAdapter(bookAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDatabase.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });

        VolleyLibrary.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest, "movies", false);
    }
}

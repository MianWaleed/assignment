package com.akkas.roleplaykeyboard.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by usman on 7/7/2018.
 */

public class RoleplayModel implements Serializable {

    int categoryId;
    int subCategoryId;
    String movieName;
    String movieImageUr;
    List<CastModel> castModelList;

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieImageUr() {
        return movieImageUr;
    }

    public void setMovieImageUr(String movieImageUr) {
        this.movieImageUr = movieImageUr;
    }

    public List<CastModel> getCastModelList() {
        return castModelList;
    }

    public void setCastModelList(List<CastModel> castModelList) {
        this.castModelList = castModelList;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        this.subCategoryId = subCategoryId;
    }
}

package com.akkas.roleplaykeyboard.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.adapters.RoleplayAdapter;
import com.akkas.roleplaykeyboard.model.CastModel;
import com.akkas.roleplaykeyboard.model.RoleplayModel;
import com.akkas.roleplaykeyboard.network.VolleyLibrary;
import com.akkas.roleplaykeyboard.utils.AppConstants;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usman on 7/9/2018.
 */

public class RoleplayFragment extends Fragment {
    RecyclerView rolePlayRecycler;
    View view;
    ProgressBar progressRolePlay;
    LinearLayout errorLayout;
    RoleplayAdapter roleplayAdapter;
    List<RoleplayModel> roleplayModels;
    LinearLayout emptyLayout;

    public static Fragment getInstance(String url) {
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        RoleplayFragment roleplayFragment = new RoleplayFragment();
        roleplayFragment.setArguments(bundle);
        return roleplayFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getContext().getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.role_play_fragment, container, false);

        init();

        return view;
    }

    public RoleplayFragment() {
    }

    private void init() {
        rolePlayRecycler = view.findViewById(R.id.role_play_recycler);
        progressRolePlay = view.findViewById(R.id.progress_role_play);
        roleplayModels = new ArrayList<>();
        emptyLayout = view.findViewById(R.id.empty_layout);
        errorLayout = view.findViewById(R.id.error_layout);
        loadDataFromApi(getArguments().getString("url", ""));
//        loadDummyData();
    }

    private void loadDummyData() {
        progressRolePlay.setVisibility(View.GONE);
        for (int i = 0; i < 10; i++) {

            RoleplayModel roleplayModel = new RoleplayModel();
            roleplayModel.setMovieName("Movie " + i);
            roleplayModel.setMovieImageUr("http://imagens.publico.pt/imagens.aspx/606569?tp=KM&w=220");
            roleplayModel.setCategoryId(i);
            roleplayModel.setSubCategoryId(i);

            List<CastModel> castModelList = new ArrayList<>();

            for (int f = 0; f < 5; f++) {
                CastModel castModel = new CastModel();
                castModel.setCastImageUrl("https://cdn.mos.cms.futurecdn.net/pWD8q5vpZFcE8smhzwArPY-480-80.jpg");
                castModel.setCastName("Actor");
                castModel.setEntryId(f);
                castModel.setId(f);

                castModelList.add(castModel);

                roleplayModel.setCastModelList(castModelList);
            }
            roleplayModels.add(roleplayModel);

            roleplayAdapter = new RoleplayAdapter(roleplayModels, getActivity());
            rolePlayRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            rolePlayRecycler.setAdapter(roleplayAdapter);
        }
    }

    private void loadDataFromApi(final String url) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("start", 0);
            jsonObject.put("limit", 50);
            jsonObject.put("order", "asc");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(url, response.toString());
                try {
                    JSONArray bodyArray = response.getJSONArray("body");

                    if (bodyArray.length() == 0) {
                        emptyLayout.setVisibility(View.VISIBLE);
                    }

                    for (int i = 0; i < bodyArray.length(); i++) {
                        progressRolePlay.setVisibility(View.GONE);
                        JSONObject jsonObject1 = bodyArray.getJSONObject(i);

                        RoleplayModel roleplayModel = new RoleplayModel();
                        roleplayModel.setMovieName(jsonObject1.getString("name"));
                        roleplayModel.setMovieImageUr(AppConstants.IMG_PATH + jsonObject1.getString("img_path"));
                        roleplayModel.setCategoryId(jsonObject1.getInt("category_id"));
                        roleplayModel.setSubCategoryId(jsonObject1.getInt("subcategory_id"));

                        JSONArray characterArray = jsonObject1.getJSONArray("characters");

                        List<CastModel> castModelList = new ArrayList<>();
                        for (int f = 0; f < characterArray.length(); f++) {

                            JSONObject jsonObject2 = characterArray.getJSONObject(f);

                            CastModel castModel = new CastModel();

                            castModel.setCastImageUrl(AppConstants.IMG_PATH + jsonObject2.getString("img_path"));
                            castModel.setCastName(jsonObject2.getString("name"));
                            castModel.setEntryId(jsonObject2.getInt("entry_id"));
                            castModel.setId(jsonObject2.getInt("id"));

                            castModelList.add(castModel);

                            roleplayModel.setCastModelList(castModelList);
                        }
                        roleplayModels.add(roleplayModel);
                    }

                    roleplayAdapter = new RoleplayAdapter(roleplayModels, getActivity());
                    rolePlayRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    rolePlayRecycler.setAdapter(roleplayAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressRolePlay.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });

        VolleyLibrary.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest, "movies", false);
    }
}

package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.db.Chapter;
import com.akkas.roleplaykeyboard.utils.KeyboardBookInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class KeyboardBookAdapter extends RecyclerView.Adapter<KeyboardBookAdapter.MyViewHolder> {

    KeyboardBookInterface keyboardBookInterface;
    List<Chapter> arrayList;
    RecyclerView actorRecycler;
    Context context;
    int currentPosition = 0;
    Boolean isFirst = true;

    public KeyboardBookAdapter(KeyboardBookInterface keyboardBookInterface, List<Chapter> arrayList, RecyclerView actorRecycler, Context context) {
        this.keyboardBookInterface = keyboardBookInterface;
        this.arrayList = arrayList;
        this.actorRecycler = actorRecycler;
        this.context = context;
    }

    @NonNull
    @Override
    public KeyboardBookAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.actor_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final KeyboardBookAdapter.MyViewHolder holder, final int position) {
        final Chapter chapter = arrayList.get(position);

        if (chapter.getBookname().length() < 3) {
            holder.bookName.setText(chapter.getBookname());
        } else {
            holder.bookName.setText(chapter.getBookname().substring(0, 3));
        }

        if (isFirst) {
            keyboardBookInterface.getLines("" + chapter.getEntryid(), chapter.getBookname(), chapter.getBookexampletype());
            isFirst = false;
            holder.isSelectedCardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }

        try {
            Picasso.get().load(chapter.getBookimageurl()).into(holder.bookCoverIv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    CardView genreTitle = actorRecycler.getLayoutManager().findViewByPosition(currentPosition).findViewById(R.id.is_selected_layout);
                    genreTitle.setCardBackgroundColor(context.getResources().getColor(R.color.white));
                    currentPosition = holder.getAdapterPosition();
                    holder.isSelectedCardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
                } catch (Exception e) {
                    e.printStackTrace();
                    currentPosition = position;
                }
                keyboardBookInterface.getLines("" + chapter.getEntryid(), chapter.getBookname(), chapter.getBookexampletype());
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView isSelectedCardView;
        ImageView bookCoverIv;
        TextView bookName;

        public MyViewHolder(View itemView) {
            super(itemView);

            isSelectedCardView = itemView.findViewById(R.id.is_selected_layout);
            bookCoverIv = itemView.findViewById(R.id.actor_iv);
            bookName = itemView.findViewById(R.id.actor_tv);
        }
    }
}

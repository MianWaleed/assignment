package com.akkas.roleplaykeyboard.keyboard.NewKeyboard;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.IBinder;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.InputType;
import android.text.method.MetaKeyKeyListener;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.activities.HomeActivity;
import com.akkas.roleplaykeyboard.adapters.ActorsAdapter;
import com.akkas.roleplaykeyboard.adapters.ChaptersLinesAdapter;
import com.akkas.roleplaykeyboard.adapters.DialoguesAdapter;
import com.akkas.roleplaykeyboard.adapters.KeyboardBookAdapter;
import com.akkas.roleplaykeyboard.db.Chapter;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.utils.ActorLinesInterface;
import com.akkas.roleplaykeyboard.utils.ChaptersLinesInterface;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.akkas.roleplaykeyboard.utils.KeyboardBookInterface;
import com.akkas.roleplaykeyboard.utils.LinesInterface;
import com.akkas.roleplaykeyboard.utils.MyPref;

import java.util.ArrayList;
import java.util.List;

public class SoftLLKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener, KeyboardBookInterface,
        View.OnClickListener, ActorLinesInterface, LinesInterface, ChaptersLinesInterface {

    static final boolean PROCESS_HARD_KEYS = true;
    private InputMethodManager mInputMethodManager;
    private LatinKeyboardView mInputView;
    private CandidateView mCandidateView;
    private CompletionInfo[] mCompletions;

    private StringBuilder mComposing = new StringBuilder();
    private boolean mPredictionOn;
    private boolean mCompletionOn;
    private int mLastDisplayWidth;
    private boolean mCapsLock;
    private long mLastShiftTime;
    private long mMetaState;

    private LatinKeyboard mSymbolsKeyboard;
    private LatinKeyboard mSymbolsShiftedKeyboard;
    LinearLayout kv;
    private LatinKeyboard mQwertyKeyboard;
    private LatinKeyboard mCurKeyboard;
    View linearLayout;
    private String mWordSeparators;

    LinearLayout noDialoguesLayout;
    List<Roleplay> notes = new ArrayList<>();
    private Keyboard keyboard;
    Button button;
    ImageView keyboardChangerIv;
    private boolean caps = false;
    RecyclerView actorRecycler, linesRecycler;
    Context context;
    ImageView sadIv, happyIv, angryIv, coolIv;
    View sadUnderline, happyUnderline, angryUnderline, coolUnderline;
    ImageView addMoreQuoteIv;
    InputConnection ic;
    TextView noQuotesAddButton;
    ImageView changeKeyboardIv;

    List<Chapter> linesOfBooks;
    List<Chapter> list;

    CoordinatorLayout popUpLayout;
    TextView dialogueTv;
    ImageView sendDialogueBt;

    RelativeLayout moodHappyLayout;
    RelativeLayout moodSadLayout;
    RelativeLayout moodAngryLayout;
    RelativeLayout moodCoolLayout;

    ImageView deductionIv, definationIv, exampleIv;
    View deductionUnderline, definationUnderline, exampleUnderline;

    RelativeLayout deductionLayout;
    RelativeLayout definationLayout;
    RelativeLayout exampleLayout;

    ActorsAdapter actorsAdapter;
    List<Roleplay> lines;
    ImageView localDbIv;
    DialoguesAdapter dialoguesAdapter;
    ImageButton icDelete;

    @Override
    public void onCreate() {
        super.onCreate();
        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        mWordSeparators = getResources().getString(R.string.word_separators);
    }

    @Override
    public void onInitializeInterface() {
        if (mQwertyKeyboard != null) {
            int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }
        mQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
        mSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
        mSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
    }

    @Override
    public View onCreateInputView() {
        linearLayout = getLayoutInflater().inflate(R.layout.input, null);
        kv = linearLayout.findViewById(R.id.test_layout);
        mInputView = linearLayout.findViewById(R.id.keyboard);
        mInputView.setOnKeyboardActionListener(this);
        setLatinKeyboard(mQwertyKeyboard);

        context = this;

        init();

        return linearLayout;
    }

    private void init() {
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(kv);

        dialogueTv = kv.findViewById(R.id.dialogue_tv);
        sendDialogueBt = kv.findViewById(R.id.send_dialogue);

        icDelete = kv.findViewById(R.id.ic_delete);
        icDelete.setOnClickListener(this);

        keyboardChangerIv = kv.findViewById(R.id.keyboard_changer_iv);
        keyboardChangerIv.setOnClickListener(this);

        popUpLayout = kv.findViewById(R.id.pop_up_layout);

        noDialoguesLayout = kv.findViewById(R.id.no_dialogues_layout);

        noQuotesAddButton = kv.findViewById(R.id.no_quotes_add_button);
        noQuotesAddButton.setOnClickListener(this);

        deductionIv = kv.findViewById(R.id.deduction_iv);
        deductionIv.setOnClickListener(this);

        definationIv = kv.findViewById(R.id.defination_iv);
        definationIv.setOnClickListener(this);

        exampleIv = kv.findViewById(R.id.example_iv);
        exampleIv.setOnClickListener(this);

        localDbIv = kv.findViewById(R.id.local_db_iv);
        localDbIv.setOnClickListener(this);

        deductionUnderline = kv.findViewById(R.id.deduction_underline);
        definationUnderline = kv.findViewById(R.id.defination_underline);
        exampleUnderline = kv.findViewById(R.id.example_underline);

        actorRecycler = kv.findViewById(R.id.actor_recycler);
        linesRecycler = kv.findViewById(R.id.lines_recycler);

        changeKeyboardIv = kv.findViewById(R.id.ic_change_keyboard_iv);
        changeKeyboardIv.setOnClickListener(this);

        moodHappyLayout = kv.findViewById(R.id.mood_happy_layout);
        moodSadLayout = kv.findViewById(R.id.mood_sad_layout);
        moodAngryLayout = kv.findViewById(R.id.mood_angry_layout);
        moodCoolLayout = kv.findViewById(R.id.mood_cool_layout);

        deductionLayout = kv.findViewById(R.id.deduction_layout);
        definationLayout = kv.findViewById(R.id.defination_layout);
        exampleLayout = kv.findViewById(R.id.example_layout);

        ViewGroup.LayoutParams params = actorRecycler.getLayoutParams();
        params.height = (MyPref.getId(this) / 100) * 90;
        actorRecycler.setLayoutParams(params);

        ViewGroup.LayoutParams params1 = linesRecycler.getLayoutParams();
        params1.height = (MyPref.getId(this) / 100) * 90;
        linesRecycler.setLayoutParams(params1);

        loadActors("serious");


        addMoreQuoteIv = kv.findViewById(R.id.add_more_quote_iv);
        addMoreQuoteIv.setOnClickListener(this);

        happyIv = kv.findViewById(R.id.mood_happy_iv);
        sadIv = kv.findViewById(R.id.mood_sad_iv);
        angryIv = kv.findViewById(R.id.mood_angry_iv);
        coolIv = kv.findViewById(R.id.mood_cool_iv);

        happyUnderline = kv.findViewById(R.id.mood_happy_underline);
        sadUnderline = kv.findViewById(R.id.mood_sad_underline);
        angryUnderline = kv.findViewById(R.id.mood_angry_underline);
        coolUnderline = kv.findViewById(R.id.mood_cool_underline);

        happyIv.setOnClickListener(this);
        sadIv.setOnClickListener(this);
        angryIv.setOnClickListener(this);
        coolIv.setOnClickListener(this);
    }

    private void setLatinKeyboard(LatinKeyboard nextKeyboard) {
        final boolean shouldSupportLanguageSwitchKey = mInputMethodManager.shouldOfferSwitchingToNextInputMethod(getToken());
//        nextKeyboard.setLanguageSwitchKeyVisibility(shouldSupportLanguageSwitchKey);
        mInputView.setKeyboard(nextKeyboard);
    }

    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        mComposing.setLength(0);
        updateCandidates();
        if (!restarting) {
            mMetaState = 0;
        }
        mPredictionOn = false;
        mCompletionOn = false;
        mCompletions = null;

        switch (attribute.inputType & InputType.TYPE_MASK_CLASS) {
            case InputType.TYPE_CLASS_NUMBER:
            case InputType.TYPE_CLASS_DATETIME:
                mCurKeyboard = mSymbolsKeyboard;
                break;

            case InputType.TYPE_CLASS_PHONE:
                mCurKeyboard = mSymbolsKeyboard;
                break;

            case InputType.TYPE_CLASS_TEXT:
                mCurKeyboard = mQwertyKeyboard;
                mPredictionOn = true;
                int variation = attribute.inputType & InputType.TYPE_MASK_VARIATION;
                if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    mPredictionOn = false;
                }

                if (variation == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        || variation == InputType.TYPE_TEXT_VARIATION_URI
                        || variation == InputType.TYPE_TEXT_VARIATION_FILTER) {
                    mPredictionOn = false;
                }

                if ((attribute.inputType & InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE) != 0) {
                    mPredictionOn = false;
                    mCompletionOn = isFullscreenMode();
                }
                updateShiftKeyState(attribute);
                break;

            default:
                mCurKeyboard = mQwertyKeyboard;
                updateShiftKeyState(attribute);
        }

        mCurKeyboard.setImeOptions(getResources(), attribute.imeOptions);
    }

    @Override
    public void onFinishInput() {
        super.onFinishInput();
        mComposing.setLength(0);
        updateCandidates();
        setCandidatesViewShown(false);

        mCurKeyboard = mQwertyKeyboard;
        if (mInputView != null) {
            mInputView.closing();
        }
    }

    @Override
    public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);
        setLatinKeyboard(mCurKeyboard);
        mInputView.closing();
        final InputMethodSubtype subtype = mInputMethodManager.getCurrentInputMethodSubtype();
        mInputView.setSubtypeOnSpaceKey(subtype);
    }

    @Override
    public void onCurrentInputMethodSubtypeChanged(InputMethodSubtype subtype) {
        mInputView.setSubtypeOnSpaceKey(subtype);
    }

    @Override
    public void onUpdateSelection(int oldSelStart, int oldSelEnd,
                                  int newSelStart, int newSelEnd,
                                  int candidatesStart, int candidatesEnd) {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
                candidatesStart, candidatesEnd);

        if (mComposing.length() > 0 && (newSelStart != candidatesEnd
                || newSelEnd != candidatesEnd)) {
            mComposing.setLength(0);
            updateCandidates();
            InputConnection ic = getCurrentInputConnection();
            if (ic != null) {
                ic.finishComposingText();
            }
        }
    }

    @Override
    public void onDisplayCompletions(CompletionInfo[] completions) {
        if (mCompletionOn) {
            mCompletions = completions;
            if (completions == null) {
                setSuggestions(null, false, false);
                return;
            }

            List<String> stringList = new ArrayList<String>();
            for (int i = 0; i < completions.length; i++) {
                CompletionInfo ci = completions[i];
                if (ci != null) stringList.add(ci.getText().toString());
            }
            setSuggestions(stringList, true, true);
        }
    }

    private boolean translateKeyDown(int keyCode, KeyEvent event) {
        mMetaState = MetaKeyKeyListener.handleKeyDown(mMetaState,
                keyCode, event);
        int c = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(mMetaState));
        mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(mMetaState);
        InputConnection ic = getCurrentInputConnection();
        if (c == 0 || ic == null) {
            return false;
        }

        boolean dead = false;

        if ((c & KeyCharacterMap.COMBINING_ACCENT) != 0) {
            dead = true;
            c = c & KeyCharacterMap.COMBINING_ACCENT_MASK;
        }

        if (mComposing.length() > 0) {
            char accent = mComposing.charAt(mComposing.length() - 1);
            int composed = KeyEvent.getDeadChar(accent, c);

            if (composed != 0) {
                c = composed;
                mComposing.setLength(mComposing.length() - 1);
            }
        }

        onKey(c, null);

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (event.getRepeatCount() == 0 && mInputView != null) {
                    if (mInputView.handleBack()) {
                        return true;
                    }
                }
                break;

            case KeyEvent.KEYCODE_DEL:
                if (mComposing.length() > 0) {
                    onKey(Keyboard.KEYCODE_DELETE, null);
                    return true;
                }
                break;

            case KeyEvent.KEYCODE_ENTER:
                return false;

            default:
                if (PROCESS_HARD_KEYS) {
                    if (keyCode == KeyEvent.KEYCODE_SPACE
                            && (event.getMetaState() & KeyEvent.META_ALT_ON) != 0) {
                        InputConnection ic = getCurrentInputConnection();
                        if (ic != null) {
                            ic.clearMetaKeyStates(KeyEvent.META_ALT_ON);
                            keyDownUp(KeyEvent.KEYCODE_A);
                            keyDownUp(KeyEvent.KEYCODE_N);
                            keyDownUp(KeyEvent.KEYCODE_D);
                            keyDownUp(KeyEvent.KEYCODE_R);
                            keyDownUp(KeyEvent.KEYCODE_O);
                            keyDownUp(KeyEvent.KEYCODE_I);
                            keyDownUp(KeyEvent.KEYCODE_D);
                            return true;
                        }
                    }
                    if (mPredictionOn && translateKeyDown(keyCode, event)) {
                        return true;
                    }
                }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (PROCESS_HARD_KEYS) {
            if (mPredictionOn) {
                mMetaState = MetaKeyKeyListener.handleKeyUp(mMetaState,
                        keyCode, event);
            }
        }

        return super.onKeyUp(keyCode, event);
    }

    private void commitTyped(InputConnection inputConnection) {
        if (mComposing.length() > 0) {
            inputConnection.commitText(mComposing, mComposing.length());
            mComposing.setLength(0);
            updateCandidates();
        }
    }

    private void updateShiftKeyState(EditorInfo attr) {
        if (attr != null
                && mInputView != null && mQwertyKeyboard == mInputView.getKeyboard()) {
            int caps = 0;
            EditorInfo ei = getCurrentInputEditorInfo();
            if (ei != null && ei.inputType != InputType.TYPE_NULL) {
                caps = getCurrentInputConnection().getCursorCapsMode(attr.inputType);
            }
            mInputView.setShifted(mCapsLock || caps != 0);
        }
    }

    private boolean isAlphabet(int code) {
        if (Character.isLetter(code)) {
            return true;
        } else {
            return false;
        }
    }

    private void keyDownUp(int keyEventCode) {
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    }

    private void sendKey(int keyCode) {
        switch (keyCode) {
            case '\n':
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            default:
                if (keyCode >= '0' && keyCode <= '9') {
                    keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
                } else {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                }
                break;
        }
    }

    public void onKey(int primaryCode, int[] keyCodes) {
        if (isWordSeparator(primaryCode)) {
            // Handle separator
            if (mComposing.length() > 0) {
                commitTyped(getCurrentInputConnection());
            }
            sendKey(primaryCode);
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (primaryCode == Keyboard.KEYCODE_DELETE) {
            handleBackspace();
        } else if (primaryCode == Keyboard.KEYCODE_SHIFT) {
            handleShift();
        } else if (primaryCode == Keyboard.KEYCODE_CANCEL) {
            handleClose();
            return;
        } else if (primaryCode == LatinKeyboardView.KEYCODE_LANGUAGE_SWITCH) {
            handleLanguageSwitch();
            return;
        } else if (primaryCode == LatinKeyboardView.KEYCODE_OPTIONS) {
            // Show a menu or somethin'
        } else if (primaryCode == Keyboard.KEYCODE_MODE_CHANGE && mInputView != null) {
            Keyboard current = mInputView.getKeyboard();
            if (current == mSymbolsKeyboard || current == mSymbolsShiftedKeyboard) {
                setLatinKeyboard(mQwertyKeyboard);
            } else {
                setLatinKeyboard(mSymbolsKeyboard);
                mSymbolsKeyboard.setShifted(false);
            }
        } else {
            handleCharacter(primaryCode, keyCodes);
        }
    }

    public void onText(CharSequence text) {
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;
        ic.beginBatchEdit();
        if (mComposing.length() > 0) {
            commitTyped(ic);
        }
        ic.commitText(text, 0);
        ic.endBatchEdit();
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    private void updateCandidates() {
        if (!mCompletionOn) {
            if (mComposing.length() > 0) {
                ArrayList<String> list = new ArrayList<String>();
                list.add(mComposing.toString());
                setSuggestions(list, true, true);
            } else {
                setSuggestions(null, false, false);
            }
        }
    }

    public void setSuggestions(List<String> suggestions, boolean completions,
                               boolean typedWordValid) {
        if (suggestions != null && suggestions.size() > 0) {
            setCandidatesViewShown(true);
        } else if (isExtractViewShown()) {
            setCandidatesViewShown(true);
        }
        if (mCandidateView != null) {
            mCandidateView.setSuggestions(suggestions, completions, typedWordValid);
        }
    }

    private void handleBackspace() {
        final int length = mComposing.length();
        if (length > 1) {
            mComposing.delete(length - 1, length);
            getCurrentInputConnection().setComposingText(mComposing, 1);
            updateCandidates();
        } else if (length > 0) {
            mComposing.setLength(0);
            getCurrentInputConnection().commitText("", 0);
            updateCandidates();
        } else {
            keyDownUp(KeyEvent.KEYCODE_DEL);
        }
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    private void handleShift() {
        if (mInputView == null) {
            return;
        }

        Keyboard currentKeyboard = mInputView.getKeyboard();
        if (mQwertyKeyboard == currentKeyboard) {
            // Alphabet keyboard
            checkToggleCapsLock();
            mInputView.setShifted(mCapsLock || !mInputView.isShifted());
        } else if (currentKeyboard == mSymbolsKeyboard) {
            mSymbolsKeyboard.setShifted(true);
            setLatinKeyboard(mSymbolsShiftedKeyboard);
            mSymbolsShiftedKeyboard.setShifted(true);
        } else if (currentKeyboard == mSymbolsShiftedKeyboard) {
            mSymbolsShiftedKeyboard.setShifted(false);
            setLatinKeyboard(mSymbolsKeyboard);
            mSymbolsKeyboard.setShifted(false);
        }
    }

    private void handleCharacter(int primaryCode, int[] keyCodes) {
        if (isInputViewShown()) {
            if (mInputView.isShifted()) {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        if (isAlphabet(primaryCode) && mPredictionOn) {
            mComposing.append((char) primaryCode);
            getCurrentInputConnection().setComposingText(mComposing, 1);
            updateShiftKeyState(getCurrentInputEditorInfo());
            updateCandidates();
        } else {
            getCurrentInputConnection().commitText(
                    String.valueOf((char) primaryCode), 1);
        }
    }

    private void handleClose() {
        commitTyped(getCurrentInputConnection());
        requestHideSelf(0);
        mInputView.closing();
    }

    private IBinder getToken() {
        final Dialog dialog = getWindow();
        if (dialog == null) {
            return null;
        }
        final Window window = dialog.getWindow();
        if (window == null) {
            return null;
        }
        return window.getAttributes().token;
    }

    private void handleLanguageSwitch() {
//        linearLayout.findViewById(R.id.test_this_ll).setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);
    }

    private void checkToggleCapsLock() {
        long now = System.currentTimeMillis();
        if (mLastShiftTime + 800 > now) {
            mCapsLock = !mCapsLock;
            mLastShiftTime = 0;
        } else {
            mLastShiftTime = now;
        }
    }

    private String getWordSeparators() {
        return mWordSeparators;
    }

    public boolean isWordSeparator(int code) {
        String separators = getWordSeparators();
        return separators.contains(String.valueOf((char) code));
    }

    public void pickDefaultCandidate() {
        pickSuggestionManually(0);
    }

    public void pickSuggestionManually(int index) {
        if (mCompletionOn && mCompletions != null && index >= 0
                && index < mCompletions.length) {
            CompletionInfo ci = mCompletions[index];
            getCurrentInputConnection().commitCompletion(ci);
            if (mCandidateView != null) {
                mCandidateView.clear();
            }
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (mComposing.length() > 0) {
            commitTyped(getCurrentInputConnection());
        }
    }

    public void swipeRight() {
        if (mCompletionOn) {
            pickDefaultCandidate();
        }
    }

    public void swipeLeft() {
        handleBackspace();
    }

    public void swipeDown() {
        handleClose();
    }

    public void swipeUp() {
    }

    public void onPress(int primaryCode) {
    }

    public void onRelease(int primaryCode) {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.local_db_iv: {
                popUpLayout.setVisibility(View.GONE);
                Toast.makeText(context, "Under construction", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.keyboard_changer_iv: {
                popUpLayout.setVisibility(View.GONE);
                handleLanguageSwitch();
                break;
            }
            case R.id.add_more_quote_iv: {
                popUpLayout.setVisibility(View.GONE);
                startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            }
            case R.id.no_quotes_add_button: {
                popUpLayout.setVisibility(View.GONE);
                startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            }
            case R.id.mood_happy_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.VISIBLE);
                sadUnderline.setVisibility(View.INVISIBLE);
                angryUnderline.setVisibility(View.INVISIBLE);
                coolUnderline.setVisibility(View.INVISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy);
                sadIv.setImageResource(R.drawable.ic_sad_unselected);
                angryIv.setImageResource(R.drawable.ic_angry_unselected);
                coolIv.setImageResource(R.drawable.ic_cool_unselected);

                loadActors("happy");

                break;
            }
            case R.id.mood_sad_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.INVISIBLE);
                sadUnderline.setVisibility(View.VISIBLE);
                angryUnderline.setVisibility(View.INVISIBLE);
                coolUnderline.setVisibility(View.INVISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy_unselected);
                sadIv.setImageResource(R.drawable.ic_sad);
                angryIv.setImageResource(R.drawable.ic_angry_unselected);
                coolIv.setImageResource(R.drawable.ic_cool_unselected);

                loadActors("sad");

                break;
            }
            case R.id.mood_angry_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.INVISIBLE);
                sadUnderline.setVisibility(View.INVISIBLE);
                angryUnderline.setVisibility(View.VISIBLE);
                coolUnderline.setVisibility(View.INVISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy_unselected);
                sadIv.setImageResource(R.drawable.ic_sad_unselected);
                angryIv.setImageResource(R.drawable.ic_angry);
                coolIv.setImageResource(R.drawable.ic_cool_unselected);

                loadActors("serious");

                break;
            }
            case R.id.mood_cool_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.INVISIBLE);
                sadUnderline.setVisibility(View.INVISIBLE);
                angryUnderline.setVisibility(View.INVISIBLE);
                coolUnderline.setVisibility(View.VISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy_unselected);
                sadIv.setImageResource(R.drawable.ic_sad_unselected);
                angryIv.setImageResource(R.drawable.ic_angry_unselected);
                coolIv.setImageResource(R.drawable.ic_cool);

                loadActors("nervous");

                break;
            }
            case R.id.deduction_iv: {
                popUpLayout.setVisibility(View.GONE);
                deductionUnderline.setVisibility(View.VISIBLE);
                definationUnderline.setVisibility(View.INVISIBLE);
                exampleUnderline.setVisibility(View.INVISIBLE);

                deductionIv.setImageResource(R.drawable.ic_deduction);
                definationIv.setImageResource(R.drawable.ic_defination_unselected);
                exampleIv.setImageResource(R.drawable.ic_example_unselected);

                loadChapters("deduction");

                break;
            }
            case R.id.defination_iv: {
                popUpLayout.setVisibility(View.GONE);
                deductionUnderline.setVisibility(View.INVISIBLE);
                definationUnderline.setVisibility(View.VISIBLE);
                exampleUnderline.setVisibility(View.INVISIBLE);

                deductionIv.setImageResource(R.drawable.ic_deduction_unselected);
                definationIv.setImageResource(R.drawable.ic_defination);
                exampleIv.setImageResource(R.drawable.ic_example_unselected);

                loadChapters("definition");

                break;
            }
            case R.id.example_iv: {
                popUpLayout.setVisibility(View.GONE);
                deductionUnderline.setVisibility(View.INVISIBLE);
                definationUnderline.setVisibility(View.INVISIBLE);
                exampleUnderline.setVisibility(View.VISIBLE);

                deductionIv.setImageResource(R.drawable.ic_deduction_unselected);
                definationIv.setImageResource(R.drawable.ic_defination_unselected);
                exampleIv.setImageResource(R.drawable.ic_example);

                loadChapters("example");

                break;
            }
            case R.id.ic_change_keyboard_iv: {

                if (moodHappyLayout.isShown()) {
                    popUpLayout.setVisibility(View.GONE);

                    deductionLayout.setVisibility(View.VISIBLE);
                    deductionUnderline.setVisibility(View.VISIBLE);
                    definationLayout.setVisibility(View.VISIBLE);
                    exampleLayout.setVisibility(View.VISIBLE);

                    moodHappyLayout.setVisibility(View.GONE);
                    moodAngryLayout.setVisibility(View.GONE);
                    moodCoolLayout.setVisibility(View.GONE);
                    moodSadLayout.setVisibility(View.GONE);

                    deductionUnderline.setVisibility(View.VISIBLE);
                    definationUnderline.setVisibility(View.INVISIBLE);
                    exampleUnderline.setVisibility(View.INVISIBLE);

                    deductionIv.setImageResource(R.drawable.ic_deduction);
                    definationIv.setImageResource(R.drawable.ic_defination_unselected);
                    exampleIv.setImageResource(R.drawable.ic_example_unselected);

                    actorRecycler.setAdapter(null);
                    linesRecycler.setAdapter(null);

                    loadChapters("deduction");

                } else {
                    popUpLayout.setVisibility(View.GONE);

                    deductionLayout.setVisibility(View.GONE);
                    definationLayout.setVisibility(View.GONE);
                    exampleLayout.setVisibility(View.GONE);

                    moodHappyLayout.setVisibility(View.VISIBLE);
                    moodAngryLayout.setVisibility(View.VISIBLE);
                    moodCoolLayout.setVisibility(View.VISIBLE);
                    moodSadLayout.setVisibility(View.VISIBLE);

                    happyUnderline.setVisibility(View.INVISIBLE);
                    sadUnderline.setVisibility(View.INVISIBLE);
                    angryUnderline.setVisibility(View.VISIBLE);
                    coolUnderline.setVisibility(View.INVISIBLE);
                    happyIv.setImageResource(R.drawable.ic_happy_unselected);
                    sadIv.setImageResource(R.drawable.ic_sad_unselected);
                    angryIv.setImageResource(R.drawable.ic_angry);
                    coolIv.setImageResource(R.drawable.ic_cool_unselected);

                    loadActors("serious");
                }

                break;
            }
            case R.id.ic_delete: {
                try {
                    ic = getCurrentInputConnection();
                    ic.deleteSurroundingText(1, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void loadChapters(String chapter) {
        list = Chapter.findWithQuery(Chapter.class, "Select DISTINCT bookname, bookimageurl, bookexampletype from Chapter where bookexampletype = ?", chapter);

        if (list.size() > 0) {
            KeyboardBookAdapter keyboardBookAdapter = new KeyboardBookAdapter(this, list, actorRecycler, context);
            actorRecycler.setLayoutManager(new LinearLayoutManager(context));
            actorRecycler.setAdapter(keyboardBookAdapter);
            noDialoguesLayout.setVisibility(View.GONE);
        } else {
            noDialoguesLayout.setVisibility(View.VISIBLE);
        }
    }

    private void loadActors(String mood) {
        notes = Roleplay.findWithQuery(Roleplay.class, "Select DISTINCT actorname, actorimgurl, emotion from Roleplay where emotion = ?", mood);

        if (notes.size() > 0) {
            actorsAdapter = new ActorsAdapter(notes, context, this, actorRecycler);
            actorRecycler.setLayoutManager(new LinearLayoutManager(context));
            actorRecycler.setAdapter(actorsAdapter);
            noDialoguesLayout.setVisibility(View.GONE);
        } else {
            noDialoguesLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getLines(String actor, String emotion) {
        lines = Roleplay.find(Roleplay.class, "actorname = ? and emotion = ?", actor, emotion);
        dialoguesAdapter = new DialoguesAdapter(lines, this, this, linesRecycler);
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        linesRecycler.setLayoutManager(gridLayoutManager);
        linesRecycler.setAdapter(dialoguesAdapter);
    }

    @Override
    public void getLines(final String lines, int i) {
        switch (i) {
            case 0: {
                ic = getCurrentInputConnection();
                ic.commitText(lines, 1);
                break;
            }
            case 1: {
                ViewGroup.LayoutParams params = popUpLayout.getLayoutParams();
                params.height = (MyPref.getId(this) / 100) * 90;
                popUpLayout.setLayoutParams(params);
                popUpLayout.setVisibility(View.VISIBLE);


                dialogueTv.setText(lines);

                popUpLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popUpLayout.setVisibility(View.GONE);
                    }
                });

                sendDialogueBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ic = getCurrentInputConnection();
                        ic.commitText(lines, 1);
                        popUpLayout.setVisibility(View.GONE);
                    }
                });

                break;
            }
        }
    }

    @Override
    public void getLines(String id, String bookType, String bookName) {
        loadLinesOfBooks(bookName, bookType);
    }

    private void loadLinesOfBooks(String bookName, String bookType) {
        linesOfBooks = Chapter.findWithQuery(Chapter.class, "Select DISTINCT * from Chapter where bookexampletype = ? and bookname = ?", bookName, bookType);
        ChaptersLinesAdapter chaptersLinesAdapter = new ChaptersLinesAdapter(linesOfBooks, this, this, linesRecycler);
        linesRecycler.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        linesRecycler.setAdapter(chaptersLinesAdapter);
    }

    @Override
    public void getChapterLines(String chapterLines) {
        ic = getCurrentInputConnection();
        ic.commitText(chapterLines, 1);
    }

    @Override
    public void onWindowHidden() {
        popUpLayout.setVisibility(View.GONE);

        deductionLayout.setVisibility(View.GONE);
        definationLayout.setVisibility(View.GONE);
        exampleLayout.setVisibility(View.GONE);

        moodHappyLayout.setVisibility(View.VISIBLE);
        moodAngryLayout.setVisibility(View.VISIBLE);
        moodCoolLayout.setVisibility(View.VISIBLE);
        moodSadLayout.setVisibility(View.VISIBLE);

        happyUnderline.setVisibility(View.INVISIBLE);
        sadUnderline.setVisibility(View.INVISIBLE);
        angryUnderline.setVisibility(View.VISIBLE);
        coolUnderline.setVisibility(View.INVISIBLE);
        happyIv.setImageResource(R.drawable.ic_happy_unselected);
        sadIv.setImageResource(R.drawable.ic_sad_unselected);
        angryIv.setImageResource(R.drawable.ic_angry);
        coolIv.setImageResource(R.drawable.ic_cool_unselected);

        loadActors("serious");

        super.onWindowHidden();
    }

    @Override
    public void onWindowShown() {
        kv.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
        mInputView.setVisibility(View.VISIBLE);

        deductionLayout.setVisibility(View.GONE);
        definationLayout.setVisibility(View.GONE);
        exampleLayout.setVisibility(View.GONE);

        moodHappyLayout.setVisibility(View.VISIBLE);
        moodAngryLayout.setVisibility(View.VISIBLE);
        moodCoolLayout.setVisibility(View.VISIBLE);
        moodSadLayout.setVisibility(View.VISIBLE);

        happyUnderline.setVisibility(View.INVISIBLE);
        sadUnderline.setVisibility(View.INVISIBLE);
        angryUnderline.setVisibility(View.VISIBLE);
        coolUnderline.setVisibility(View.INVISIBLE);
        happyIv.setImageResource(R.drawable.ic_happy_unselected);
        sadIv.setImageResource(R.drawable.ic_sad_unselected);
        angryIv.setImageResource(R.drawable.ic_angry);
        coolIv.setImageResource(R.drawable.ic_cool_unselected);

        loadActors("serious");
        super.onWindowShown();
    }
}

package com.akkas.roleplaykeyboard.db;

import android.content.Context;

import com.orm.SugarRecord;

/**
 * Created by usman on 7/9/2018.
 */

public class Roleplay extends SugarRecord<Roleplay> {
    String emotion, dialogue, actorname, actorimgurl;
    String characterid, entryid, dialogueid;
    Context context;

    public Roleplay() {
    }

    public Roleplay(String emotion, String dialogue, String actorname, String actorimgurl, String characterId, String entryId, String dialogueID) {
        this.emotion = emotion;
        this.dialogue = dialogue;
        this.actorname = actorname;
        this.actorimgurl = actorimgurl;
        this.characterid = characterId;
        this.entryid = entryId;
        this.dialogueid = dialogueID;
    }

    public String getActorimgurl() {
        return actorimgurl;
    }

    public void setActorimgurl(String actorimgurl) {
        this.actorimgurl = actorimgurl;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public String getDialogue() {
        return dialogue;
    }

    public void setDialogue(String dialogue) {
        this.dialogue = dialogue;
    }

    public String getActorname() {
        return actorname;
    }

    public void setActorname(String actorname) {
        this.actorname = actorname;
    }

    public String getCharacterid() {
        return characterid;
    }

    public void setCharacterid(String characterid) {
        this.characterid = characterid;
    }

    public String getEntryid() {
        return entryid;
    }

    public void setEntryid(String entryid) {
        this.entryid = entryid;
    }

    public String getDialogueid() {
        return dialogueid;
    }

    public void setDialogueid(String dialogueid) {
        this.dialogueid = dialogueid;
    }
}

package com.akkas.roleplaykeyboard.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {

    public static final String ACTION_CUSTOM = "com.akkas.roleplaykeyboard.receiver.AlarmReceiver.ACTION_CUSTOM";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_CUSTOM.equals(intent.getAction())) {
            Toast.makeText(context, "" + intent.getStringExtra("enter"), Toast.LENGTH_SHORT).show();
        }
    }
}
package com.akkas.roleplaykeyboard.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.fragments.DatabaseFragment;
import com.akkas.roleplaykeyboard.fragments.RoleLibraryFragment;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.akkas.roleplaykeyboard.utils.MyPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import info.hoang8f.android.segmented.SegmentedGroup;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    SegmentedGroup segmentedGroup;
    Timer timer = new Timer();
    RadioButton roleLibraryButton, databaseButton;
    LinearLayout setupKeyboardLayout;
    ImageView goToSettingsIv, searchIv;
    TextView bannerLayoutTv;

    boolean isKeyboardEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();
        setKeyboardHeight();
        showKeyboardLayout();
        loadEnabledKeyboard();
        makeJson();
    }

    private void makeJson() {
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("data", " ");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadEnabledKeyboard() {
        InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        List<InputMethodInfo> list = im.getEnabledInputMethodList();

        for (int i = 0; i < list.size(); i++) {
            Log.d("KEY", list.get(i).getPackageName() + "\n" + list.get(i).getServiceName());
            if (list.get(i).getServiceName().equals("com.akkas.roleplaykeyboard.keyboard.NewKeyboard.SoftLLKeyboard")) {
                bannerLayoutTv.setText(R.string.set_as_current_keyboard);
                isKeyboardEnabled = true;
            }
        }
    }

    private void runnableBackground() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showKeyboardLayout();
                    }
                });
            }
        }, 0, 1000);
    }

    private void setKeyboardHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        MyPref.setId(this, height / 2);
    }

    private void init() {
        bannerLayoutTv = findViewById(R.id.banner_layout_tv);

        searchIv = findViewById(R.id.search_icon_iv);
        searchIv.setOnClickListener(this);

        goToSettingsIv = findViewById(R.id.go_to_settings_iv);
        goToSettingsIv.setOnClickListener(this);

        setupKeyboardLayout = findViewById(R.id.setup_keyboard_layout);

        segmentedGroup = findViewById(R.id.segmented2);
        roleLibraryButton = findViewById(R.id.role_library_button);
        databaseButton = findViewById(R.id.database_button);

        getSupportFragmentManager().beginTransaction().replace(R.id.videoView, new RoleLibraryFragment()).commit();

        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (roleLibraryButton.isChecked()) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.videoView, new RoleLibraryFragment()).commit();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.videoView, new DatabaseFragment()).commit();
                }
            }
        });

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }

    @Override
    protected void onResume() {
        showKeyboardLayout();
        runnableBackground();
        super.onResume();
    }


    private void showKeyboardLayout() {
        String currentKeyboard = Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
        Log.wtf("CU", currentKeyboard);
        if (currentKeyboard.equals("com.akkas.roleplaykeyboard/.keyboard.NewKeyboard.SoftLLKeyboard")) {
            setupKeyboardLayout.setVisibility(View.GONE);
        } else {
            setupKeyboardLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.go_to_settings_iv: {
                if (isKeyboardEnabled) {
                    InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                    imeManager.showInputMethodPicker();
                } else {
                    startActivityForResult(new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS), 0);
                }
                break;
            }
            case R.id.search_icon_iv: {
                startActivity(new Intent(this, SearchActivity.class));
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            bannerLayoutTv.setText(R.string.set_as_current_keyboard);
            isKeyboardEnabled = true;
        } else {
            bannerLayoutTv.setText(R.string.enable_keyboard_from_settings);
            isKeyboardEnabled = false;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.videoView, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}

package com.akkas.roleplaykeyboard.keyboard;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.activities.HomeActivity;
import com.akkas.roleplaykeyboard.adapters.ActorsAdapter;
import com.akkas.roleplaykeyboard.adapters.ChaptersLinesAdapter;
import com.akkas.roleplaykeyboard.adapters.DialoguesAdapter;
import com.akkas.roleplaykeyboard.adapters.KeyboardBookAdapter;
import com.akkas.roleplaykeyboard.db.Chapter;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.utils.ActorLinesInterface;
import com.akkas.roleplaykeyboard.utils.ChaptersLinesInterface;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.akkas.roleplaykeyboard.utils.KeyboardBookInterface;
import com.akkas.roleplaykeyboard.utils.LinesInterface;
import com.akkas.roleplaykeyboard.utils.MyPref;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usman on 7/10/2018.
 */

public class RolePlayKeyboard extends InputMethodService implements KeyboardBookInterface, KeyboardView.OnKeyboardActionListener,
        View.OnClickListener, ActorLinesInterface, LinesInterface, ChaptersLinesInterface {

    LinearLayout noDialoguesLayout;
    List<Roleplay> notes = new ArrayList<>();
    private LinearLayout kv;
    private Keyboard keyboard;
    Button button;
    ImageView keyboardChangerIv;
    private boolean caps = false;
    RecyclerView actorRecycler, linesRecycler;
    Context context;
    ImageView sadIv, happyIv, angryIv, coolIv;
    View sadUnderline, happyUnderline, angryUnderline, coolUnderline;
    ImageView addMoreQuoteIv;
    InputConnection ic;
    TextView noQuotesAddButton;
    ImageView changeKeyboardIv;

    List<Chapter> linesOfBooks;
    List<Chapter> list;

    CoordinatorLayout popUpLayout;
    TextView dialogueTv;
    ImageView sendDialogueBt;

    RelativeLayout moodHappyLayout;
    RelativeLayout moodSadLayout;
    RelativeLayout moodAngryLayout;
    RelativeLayout moodCoolLayout;

    ImageView deductionIv, definationIv, exampleIv;
    View deductionUnderline, definationUnderline, exampleUnderline;

    RelativeLayout deductionLayout;
    RelativeLayout definationLayout;
    RelativeLayout exampleLayout;

    ActorsAdapter actorsAdapter;
    List<Roleplay> lines;
    ImageView localDbIv;
    DialoguesAdapter dialoguesAdapter;
    ImageButton icDelete;

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        ic = getCurrentInputConnection();

        playClick(primaryCode);

        switch (primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                ic.deleteSurroundingText(1, 0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                keyboard.setShifted(caps);
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            default:
                char code = (char) primaryCode;
                if (Character.isLetter(code) && caps) {
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code), 1);
        }
    }


    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }

    @Override
    public View onCreateInputView() {
        context = this;
        init();
        return kv;
    }

    private void init() {
        kv = (LinearLayout) getLayoutInflater().inflate(R.layout.roleplay_keyboard, null);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(kv);

        dialogueTv = kv.findViewById(R.id.dialogue_tv);
        sendDialogueBt = kv.findViewById(R.id.send_dialogue);

        icDelete = kv.findViewById(R.id.ic_delete);
        icDelete.setOnClickListener(this);

        keyboardChangerIv = kv.findViewById(R.id.keyboard_changer_iv);
        keyboardChangerIv.setOnClickListener(this);

        popUpLayout = kv.findViewById(R.id.pop_up_layout);

        noDialoguesLayout = kv.findViewById(R.id.no_dialogues_layout);

        noQuotesAddButton = kv.findViewById(R.id.no_quotes_add_button);
        noQuotesAddButton.setOnClickListener(this);

        deductionIv = kv.findViewById(R.id.deduction_iv);
        deductionIv.setOnClickListener(this);

        definationIv = kv.findViewById(R.id.defination_iv);
        definationIv.setOnClickListener(this);

        exampleIv = kv.findViewById(R.id.example_iv);
        exampleIv.setOnClickListener(this);

        localDbIv = kv.findViewById(R.id.local_db_iv);
        localDbIv.setOnClickListener(this);

        deductionUnderline = kv.findViewById(R.id.deduction_underline);
        definationUnderline = kv.findViewById(R.id.defination_underline);
        exampleUnderline = kv.findViewById(R.id.example_underline);

        actorRecycler = kv.findViewById(R.id.actor_recycler);
        linesRecycler = kv.findViewById(R.id.lines_recycler);

        changeKeyboardIv = kv.findViewById(R.id.ic_change_keyboard_iv);
        changeKeyboardIv.setOnClickListener(this);

        moodHappyLayout = kv.findViewById(R.id.mood_happy_layout);
        moodSadLayout = kv.findViewById(R.id.mood_sad_layout);
        moodAngryLayout = kv.findViewById(R.id.mood_angry_layout);
        moodCoolLayout = kv.findViewById(R.id.mood_cool_layout);

        deductionLayout = kv.findViewById(R.id.deduction_layout);
        definationLayout = kv.findViewById(R.id.defination_layout);
        exampleLayout = kv.findViewById(R.id.example_layout);

        ViewGroup.LayoutParams params = actorRecycler.getLayoutParams();
        params.height = (MyPref.getId(this) / 100) * 90;
        actorRecycler.setLayoutParams(params);

        ViewGroup.LayoutParams params1 = linesRecycler.getLayoutParams();
        params1.height = (MyPref.getId(this) / 100) * 90;
        linesRecycler.setLayoutParams(params1);

        loadActors("serious");


        addMoreQuoteIv = kv.findViewById(R.id.add_more_quote_iv);
        addMoreQuoteIv.setOnClickListener(this);

        happyIv = kv.findViewById(R.id.mood_happy_iv);
        sadIv = kv.findViewById(R.id.mood_sad_iv);
        angryIv = kv.findViewById(R.id.mood_angry_iv);
        coolIv = kv.findViewById(R.id.mood_cool_iv);

        happyUnderline = kv.findViewById(R.id.mood_happy_underline);
        sadUnderline = kv.findViewById(R.id.mood_sad_underline);
        angryUnderline = kv.findViewById(R.id.mood_angry_underline);
        coolUnderline = kv.findViewById(R.id.mood_cool_underline);

        happyIv.setOnClickListener(this);
        sadIv.setOnClickListener(this);
        angryIv.setOnClickListener(this);
        coolIv.setOnClickListener(this);
    }

    private void playClick(int keyCode) {
        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
        switch (keyCode) {
            case 32:
                if (am != null) {
                    am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                }
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                assert am != null;
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                assert am != null;
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.local_db_iv: {
                popUpLayout.setVisibility(View.GONE);
                Toast.makeText(context, "Under construction", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.keyboard_changer_iv: {
                popUpLayout.setVisibility(View.GONE);
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();
                break;
            }
            case R.id.add_more_quote_iv: {
                popUpLayout.setVisibility(View.GONE);
                startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            }
            case R.id.no_quotes_add_button: {
                popUpLayout.setVisibility(View.GONE);
                startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            }
            case R.id.mood_happy_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.VISIBLE);
                sadUnderline.setVisibility(View.INVISIBLE);
                angryUnderline.setVisibility(View.INVISIBLE);
                coolUnderline.setVisibility(View.INVISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy);
                sadIv.setImageResource(R.drawable.ic_sad_unselected);
                angryIv.setImageResource(R.drawable.ic_angry_unselected);
                coolIv.setImageResource(R.drawable.ic_cool_unselected);

                loadActors("happy");

                break;
            }
            case R.id.mood_sad_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.INVISIBLE);
                sadUnderline.setVisibility(View.VISIBLE);
                angryUnderline.setVisibility(View.INVISIBLE);
                coolUnderline.setVisibility(View.INVISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy_unselected);
                sadIv.setImageResource(R.drawable.ic_sad);
                angryIv.setImageResource(R.drawable.ic_angry_unselected);
                coolIv.setImageResource(R.drawable.ic_cool_unselected);

                loadActors("sad");

                break;
            }
            case R.id.mood_angry_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.INVISIBLE);
                sadUnderline.setVisibility(View.INVISIBLE);
                angryUnderline.setVisibility(View.VISIBLE);
                coolUnderline.setVisibility(View.INVISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy_unselected);
                sadIv.setImageResource(R.drawable.ic_sad_unselected);
                angryIv.setImageResource(R.drawable.ic_angry);
                coolIv.setImageResource(R.drawable.ic_cool_unselected);

                loadActors("serious");

                break;
            }
            case R.id.mood_cool_iv: {
                popUpLayout.setVisibility(View.GONE);
                happyUnderline.setVisibility(View.INVISIBLE);
                sadUnderline.setVisibility(View.INVISIBLE);
                angryUnderline.setVisibility(View.INVISIBLE);
                coolUnderline.setVisibility(View.VISIBLE);
                happyIv.setImageResource(R.drawable.ic_happy_unselected);
                sadIv.setImageResource(R.drawable.ic_sad_unselected);
                angryIv.setImageResource(R.drawable.ic_angry_unselected);
                coolIv.setImageResource(R.drawable.ic_cool);

                loadActors("nervous");

                break;
            }
            case R.id.deduction_iv: {
                popUpLayout.setVisibility(View.GONE);
                deductionUnderline.setVisibility(View.VISIBLE);
                definationUnderline.setVisibility(View.INVISIBLE);
                exampleUnderline.setVisibility(View.INVISIBLE);

                deductionIv.setImageResource(R.drawable.ic_deduction);
                definationIv.setImageResource(R.drawable.ic_defination_unselected);
                exampleIv.setImageResource(R.drawable.ic_example_unselected);

                loadChapters("deduction");

                break;
            }
            case R.id.defination_iv: {
                popUpLayout.setVisibility(View.GONE);
                deductionUnderline.setVisibility(View.INVISIBLE);
                definationUnderline.setVisibility(View.VISIBLE);
                exampleUnderline.setVisibility(View.INVISIBLE);

                deductionIv.setImageResource(R.drawable.ic_deduction_unselected);
                definationIv.setImageResource(R.drawable.ic_defination);
                exampleIv.setImageResource(R.drawable.ic_example_unselected);

                loadChapters("definition");

                break;
            }
            case R.id.example_iv: {
                popUpLayout.setVisibility(View.GONE);
                deductionUnderline.setVisibility(View.INVISIBLE);
                definationUnderline.setVisibility(View.INVISIBLE);
                exampleUnderline.setVisibility(View.VISIBLE);

                deductionIv.setImageResource(R.drawable.ic_deduction_unselected);
                definationIv.setImageResource(R.drawable.ic_defination_unselected);
                exampleIv.setImageResource(R.drawable.ic_example);

                loadChapters("example");

                break;
            }
            case R.id.ic_change_keyboard_iv: {

                if (moodHappyLayout.isShown()) {
                    popUpLayout.setVisibility(View.GONE);

                    deductionLayout.setVisibility(View.VISIBLE);
                    deductionUnderline.setVisibility(View.VISIBLE);
                    definationLayout.setVisibility(View.VISIBLE);
                    exampleLayout.setVisibility(View.VISIBLE);

                    moodHappyLayout.setVisibility(View.GONE);
                    moodAngryLayout.setVisibility(View.GONE);
                    moodCoolLayout.setVisibility(View.GONE);
                    moodSadLayout.setVisibility(View.GONE);

                    deductionUnderline.setVisibility(View.VISIBLE);
                    definationUnderline.setVisibility(View.INVISIBLE);
                    exampleUnderline.setVisibility(View.INVISIBLE);

                    deductionIv.setImageResource(R.drawable.ic_deduction);
                    definationIv.setImageResource(R.drawable.ic_defination_unselected);
                    exampleIv.setImageResource(R.drawable.ic_example_unselected);

                    actorRecycler.setAdapter(null);
                    linesRecycler.setAdapter(null);

                    loadChapters("deduction");

                } else {
                    popUpLayout.setVisibility(View.GONE);

                    deductionLayout.setVisibility(View.GONE);
                    definationLayout.setVisibility(View.GONE);
                    exampleLayout.setVisibility(View.GONE);

                    moodHappyLayout.setVisibility(View.VISIBLE);
                    moodAngryLayout.setVisibility(View.VISIBLE);
                    moodCoolLayout.setVisibility(View.VISIBLE);
                    moodSadLayout.setVisibility(View.VISIBLE);

                    happyUnderline.setVisibility(View.INVISIBLE);
                    sadUnderline.setVisibility(View.INVISIBLE);
                    angryUnderline.setVisibility(View.VISIBLE);
                    coolUnderline.setVisibility(View.INVISIBLE);
                    happyIv.setImageResource(R.drawable.ic_happy_unselected);
                    sadIv.setImageResource(R.drawable.ic_sad_unselected);
                    angryIv.setImageResource(R.drawable.ic_angry);
                    coolIv.setImageResource(R.drawable.ic_cool_unselected);

                    loadActors("serious");
                }

                break;
            }
            case R.id.ic_delete: {
                try {
//                    InputConnection ic = editText.onCreateInputConnection(new EditorInfo());
//                    keyboard.setInputConnection(ic); // custom keyboard metho
                    ic = getCurrentInputConnection();
                    ic.deleteSurroundingText(1, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void loadChapters(String chapter) {
        list = Chapter.findWithQuery(Chapter.class, "Select DISTINCT bookname, bookimageurl, bookexampletype from Chapter where bookexampletype = ?", chapter);

        if (list.size() > 0) {
            KeyboardBookAdapter keyboardBookAdapter = new KeyboardBookAdapter(this, list, actorRecycler, context);
            actorRecycler.setLayoutManager(new LinearLayoutManager(context));
            actorRecycler.setAdapter(keyboardBookAdapter);
            noDialoguesLayout.setVisibility(View.GONE);
        } else {
            noDialoguesLayout.setVisibility(View.VISIBLE);
        }
    }

    private void loadActors(String mood) {
        notes = Roleplay.findWithQuery(Roleplay.class, "Select DISTINCT actorname, actorimgurl, emotion from Roleplay where emotion = ?", mood);

        if (notes.size() > 0) {
            actorsAdapter = new ActorsAdapter(notes, context, this, actorRecycler);
            actorRecycler.setLayoutManager(new LinearLayoutManager(context));
            actorRecycler.setAdapter(actorsAdapter);
            noDialoguesLayout.setVisibility(View.GONE);
        } else {
            noDialoguesLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getLines(String actor, String emotion) {
        lines = Roleplay.find(Roleplay.class, "actorname = ? and emotion = ?", actor, emotion);

//        GridLayoutManager layoutManager = new GridLayoutManager(context, 4);
        dialoguesAdapter = new DialoguesAdapter(lines, this, this, linesRecycler);
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        linesRecycler.setLayoutManager(gridLayoutManager);
        linesRecycler.setAdapter(dialoguesAdapter);
    }

    @Override
    public void getLines(final String lines, int i) {
        switch (i) {
            case 0: {
                ic = getCurrentInputConnection();
                ic.commitText(lines, 1);
                break;
            }
            case 1: {
                ViewGroup.LayoutParams params = popUpLayout.getLayoutParams();
                params.height = (MyPref.getId(this) / 100) * 90;
                popUpLayout.setLayoutParams(params);
                popUpLayout.setVisibility(View.VISIBLE);


                dialogueTv.setText(lines);

                popUpLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popUpLayout.setVisibility(View.GONE);
                    }
                });

                sendDialogueBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ic = getCurrentInputConnection();
                        ic.commitText(lines, 1);
                        popUpLayout.setVisibility(View.GONE);
                    }
                });

                break;
            }
        }
    }

    private void showInputDialog(final String lines) {
        final Dialog dialog = new Dialog(context.getApplicationContext());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.input_layout_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        TextView dialogTv = dialog.findViewById(R.id.dialogue_tv);
        dialogTv.setText(lines);

        ImageView sendDialogue = dialog.findViewById(R.id.send_dialogue);
        sendDialogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ic = getCurrentInputConnection();

                ic.commitText(lines, 1);
                dialog.cancel();
            }
        });

        dialog.show();
    }

    @Override
    public void getLines(String id, String bookType, String bookName) {
        loadLinesOfBooks(bookName, bookType);
    }

    private void loadLinesOfBooks(String bookName, String bookType) {
        linesOfBooks = Chapter.findWithQuery(Chapter.class, "Select DISTINCT * from Chapter where bookexampletype = ? and bookname = ?", bookName, bookType);

        ChaptersLinesAdapter chaptersLinesAdapter = new ChaptersLinesAdapter(linesOfBooks, this, this, linesRecycler);
        linesRecycler.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        linesRecycler.setAdapter(chaptersLinesAdapter);
    }

    @Override
    public void getChapterLines(String chapterLines) {
        ic = getCurrentInputConnection();
        ic.commitText(chapterLines, 1);
    }

    @Override
    public void onWindowHidden() {
        popUpLayout.setVisibility(View.GONE);

        deductionLayout.setVisibility(View.GONE);
        definationLayout.setVisibility(View.GONE);
        exampleLayout.setVisibility(View.GONE);

        moodHappyLayout.setVisibility(View.VISIBLE);
        moodAngryLayout.setVisibility(View.VISIBLE);
        moodCoolLayout.setVisibility(View.VISIBLE);
        moodSadLayout.setVisibility(View.VISIBLE);

        happyUnderline.setVisibility(View.INVISIBLE);
        sadUnderline.setVisibility(View.INVISIBLE);
        angryUnderline.setVisibility(View.VISIBLE);
        coolUnderline.setVisibility(View.INVISIBLE);
        happyIv.setImageResource(R.drawable.ic_happy_unselected);
        sadIv.setImageResource(R.drawable.ic_sad_unselected);
        angryIv.setImageResource(R.drawable.ic_angry);
        coolIv.setImageResource(R.drawable.ic_cool_unselected);

        loadActors("serious");

        super.onWindowHidden();
    }

    @Override
    public void onWindowShown() {
        deductionLayout.setVisibility(View.GONE);
        definationLayout.setVisibility(View.GONE);
        exampleLayout.setVisibility(View.GONE);

        moodHappyLayout.setVisibility(View.VISIBLE);
        moodAngryLayout.setVisibility(View.VISIBLE);
        moodCoolLayout.setVisibility(View.VISIBLE);
        moodSadLayout.setVisibility(View.VISIBLE);

        happyUnderline.setVisibility(View.INVISIBLE);
        sadUnderline.setVisibility(View.INVISIBLE);
        angryUnderline.setVisibility(View.VISIBLE);
        coolUnderline.setVisibility(View.INVISIBLE);
        happyIv.setImageResource(R.drawable.ic_happy_unselected);
        sadIv.setImageResource(R.drawable.ic_sad_unselected);
        angryIv.setImageResource(R.drawable.ic_angry);
        coolIv.setImageResource(R.drawable.ic_cool_unselected);

        loadActors("serious");
        super.onWindowShown();
    }
}
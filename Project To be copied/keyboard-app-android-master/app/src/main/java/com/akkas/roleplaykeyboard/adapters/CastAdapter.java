package com.akkas.roleplaykeyboard.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akkas.roleplaykeyboard.R;
import com.akkas.roleplaykeyboard.db.Roleplay;
import com.akkas.roleplaykeyboard.model.CastModel;
import com.akkas.roleplaykeyboard.network.VolleyLibrary;
import com.akkas.roleplaykeyboard.utils.AppConstants;
import com.akkas.roleplaykeyboard.utils.FontChangeCrawler;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by usman on 7/7/2018.
 */

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.MyViewHolder> {

    List<CastModel> castList;
    Context context;

    public CastAdapter(List<CastModel> castList, Context context) {
        this.castList = castList;
        this.context = context;
    }

    @Override
    public CastAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.roleplay_cast_item, parent, false);
        return new CastAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CastAdapter.MyViewHolder holder, int position) {

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "montserrat.ttf");
        fontChanger.replaceFonts(holder.viewGroup);

        final CastModel castModel = castList.get(position);

        try {
            Glide.with(context).load(castModel.getCastImageUrl()).apply(new RequestOptions().fitCenter().centerCrop().placeholder(R.drawable.actor_placeholder)).into(holder.castIv);
//            Picasso.get().load(castModel.getCastImageUrl()).fit().centerCrop().into(holder.castIv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (castModel.isSaved()) {
            holder.downloadCheckedIv.setVisibility(View.VISIBLE);
        }

        holder.castNameTv.setText(castModel.getCastName());
        List<Roleplay> notes = Roleplay.findWithQuery(Roleplay.class, "Select * from Roleplay where actorname = ?", castModel.getCastName());
        if (notes.size() > 0) {
            holder.downloadCheckedIv.setVisibility(View.VISIBLE);
            castModel.setSaved(true);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.downloadCheckedIv.isShown()) {
                    holder.downloadCheckedIv.setVisibility(View.GONE);
                    castModel.setSaved(false);
                    Roleplay.deleteAll(Roleplay.class, "characterid = ? and entryid = ?", castModel.getId() + "", castModel.getEntryId() + "");
                } else {
                    holder.downloadCheckedIv.setVisibility(View.VISIBLE);
                    castModel.setSaved(true);
                    getDialogues(castModel.getEntryId(), castModel.getId(), holder, castModel.getCastImageUrl(), castModel.getCastName());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        try {
            return castList.size();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView castIv, downloadCheckedIv;
        LinearLayout viewGroup;
        TextView castNameTv;

        public MyViewHolder(View itemView) {
            super(itemView);

            downloadCheckedIv = itemView.findViewById(R.id.download_checked_iv);
            viewGroup = itemView.findViewById(R.id.view_group);
            castIv = itemView.findViewById(R.id.cast_row_iv);
            castNameTv = itemView.findViewById(R.id.cast_row_tv);
        }
    }

    private void getDialogues(final int entryId, final int id, final MyViewHolder holder, final String actorImage, final String actorName) {
        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("start", 0);
            jsonObject.put("limit", 50);
            jsonObject.put("order", "asc");
            jsonObject.put("entry_id", entryId);
            jsonObject.put("character_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstants.GET_ACTOR_DIALOGUE, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Res", response.toString());
                try {

                    JSONArray bodyArray = response.getJSONArray("body");

                    if (bodyArray.length() == 0) {
                        Toast.makeText(context, "No Dialogues :(", Toast.LENGTH_SHORT).show();
                    }

                    for (int i = 0; i < bodyArray.length(); i++) {
                        JSONObject jsonObject1 = bodyArray.getJSONObject(i);
                        JSONObject emotionObject = jsonObject1.getJSONObject("emotion");
                        Roleplay roleplay = new Roleplay();
                        roleplay.setEmotion(emotionObject.getString("name"));
                        roleplay.setDialogue(jsonObject1.getString("description"));
                        roleplay.setActorname(actorName);
                        roleplay.setActorimgurl(actorImage);
                        roleplay.setCharacterid(id + "");
                        roleplay.setEntryid(entryId + "");
                        roleplay.setDialogueid(jsonObject1.getString("id"));
                        roleplay.save();

                        if (holder.downloadCheckedIv.getVisibility() == View.INVISIBLE) {
                            holder.downloadCheckedIv.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleyLibrary.getInstance(context).addToRequestQueue(jsonObjectRequest, "movies", false);
    }
}

package com.predrinks.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kumulos.android.Kumulos;
import com.predrinks.app.Confidentials;
import com.predrinks.app.Controller;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.utils.Utils;
import com.predrinks.app.widget.WheelView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SignUpActivity extends BaseFragmentActivity {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "SignUpActivity";
    @Bind(R.id.activity_SignUp)
    RelativeLayout activitySignUp;
    @Bind(R.id.imgBack)
    ImageView imgBack;
    @Bind(R.id.etFName)
    EditText etFName;
    @Bind(R.id.etLName)
    EditText etLName;
    @Bind(R.id.etPostcode)
    EditText etPostcode;
    @Bind(R.id.etMobile)
    EditText etMobile;
    @Bind(R.id.etBDy)
    TextView etBDy;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.etGender)
    TextView tvGender;

    @Bind(R.id.imgTick)
    ImageView imgTick;
    @Bind(R.id.imgTick1)
    ImageView imgTick1;
    @Bind(R.id.imgTick2)
    ImageView imgTick2;
    @Bind(R.id.imgTick3)
    ImageView imgTick3;
    @Bind(R.id.imgTick4)
    ImageView imgTick4;
    @Bind(R.id.imgTick5)
    ImageView imgTick5;
    @Bind(R.id.imgTick6)
    ImageView imgTick6;
    @Bind(R.id.imgTick7)
    ImageView imgTick7;

    @Bind(R.id.llSignUp)
    LinearLayout llSignUp;
    @Bind(R.id.llFaceBookSignUp)
    LinearLayout llFaceBookSignUp;
    //    @Bind(R.id.tvPrivacyPolicy)
//    TextView tvPrivacyPolicy;
    @Bind(R.id.checkboxTC)
    CheckBox checkboxTC;
    @Bind(R.id.checkboxPP)
    CheckBox checkboxPP;
    @Bind(R.id.login_button)
    LoginButton mButtonLoginFaceBook;

    @Bind(R.id.tvTC)
    TextView tvTC;
    @Bind(R.id.tvPP)
    TextView tvPP;

    private static final String[] MONTH = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final List<String> DATES = new ArrayList<>();
    private static final List<String> YEAR = new ArrayList<>();


    Context mContext;
    Activity mActivity;

    CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    boolean isTC = false, isPrivacy = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Kumulos.pushRegister(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        mContext = this;
        mActivity = this;
        isTC = false;
        isPrivacy = false;
        for (int i = 0; i <= 30; i++) {
            DATES.add(String.valueOf(i + 1));
        }
        for (int i = 1900; i < 2050; i++) {
            YEAR.add(String.valueOf(i + 1));
        }

        callbackManager = CallbackManager.Factory.create();

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        etBDy.setInputType(InputType.TYPE_NULL);

        tvTC.setPaintFlags(tvTC.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvPP.setPaintFlags(tvPP.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        checkboxTC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!isTC) {
                        String titleT = "TERMS & CONDITIONS";
                        isTC = true;
                        startPrivacyPolicy(titleT, "1");
                        checkboxTC.setChecked(true);
                    }
                }

            }
        });
        checkboxPP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!isPrivacy) {
                        String titlePP = "PRIVACY POLICY";
                        isPrivacy = true;
                        startPrivacyPolicy(titlePP, "0");
                        checkboxPP.setChecked(true);
                    }
                }

            }
        });

        etPostcode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LandingActivity.class);
        startActivity(intent);
        finish();
    }

    @OnTextChanged(R.id.etFName)
    public void onTextChangedet1(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick.setImageResource(R.mipmap.tick_icon_grey);
        }
    }

    @OnTextChanged(R.id.etLName)
    public void onTextChangedet2(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick1.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick1.setImageResource(R.mipmap.tick_icon_grey);
        }
    }


    @OnTextChanged(R.id.etPostcode)
    public void onTextChangedet8(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick6.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick6.setImageResource(R.mipmap.tick_icon_grey);
        }
    }

    @OnTextChanged(R.id.etMobile)
    public void onTextChangedet7(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString()) && Utils.isValidMobileNumber(ch.toString())) {
            imgTick7.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick7.setImageResource(R.mipmap.tick_icon_grey);
        }
    }

    @OnTextChanged(R.id.etGender)
    public void onTextChangedet3(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick2.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick2.setImageResource(R.mipmap.tick_icon_grey);
        }
    }

    @OnTextChanged(R.id.etBDy)
    public void onTextChangedet4(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick3.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick3.setImageResource(R.mipmap.tick_icon_grey);
        }
    }

    @OnTextChanged(R.id.etEmail)
    public void onTextChangedet5(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick4.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick4.setImageResource(R.mipmap.tick_icon_grey);
        }
    }

    @OnTextChanged(R.id.etPassword)
    public void onTextChangedet6(CharSequence ch, int start, int count, int after) {
        if (!TextUtils.isEmpty(ch.toString())) {
            imgTick5.setImageResource(R.mipmap.tick_icon_green);
        } else {
            imgTick5.setImageResource(R.mipmap.tick_icon_grey);
        }
    }


    @OnClick({R.id.imgBack, R.id.llSignUp, R.id.llFaceBookSignUp, R.id.tvTC, R.id.tvPP, R.id.etBDy, R.id.etGender})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                Intent intent = new Intent(this, LandingActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.llSignUp:
                Register();
                break;
            case R.id.llFaceBookSignUp:
                if (checkboxTC.isChecked())
                    onFaceBookClick();
                else {
                    Toast.makeText(mContext, "You must read and accept terms and conditions before proceeding", Toast.LENGTH_SHORT).show();
                }
                break;
//            case R.id.tvPrivacyPolicy:
//                String titleP = "PRIVACY POLICY";
//                startPrivacyPolicy(titleP, "0");
//                break;
            case R.id.tvTC:
                String titleT = "TERMS & CONDITIONS";
                isTC = true;
                startPrivacyPolicy(titleT, "1");
                checkboxTC.setChecked(true);
                break;
            case R.id.tvPP:
                String titlePP = "PRIVACY POLICY";
                isPrivacy = true;
                startPrivacyPolicy(titlePP, "0");
                checkboxPP.setChecked(true);
                break;
            case R.id.etBDy:
                hideKeyBoardByView(etLName);
                setDateTimeField();
                break;
            case R.id.etGender:
                hideKeyBoardByView(etLName);
                selectGender();
                break;
        }
    }

    private SimpleDateFormat dateFormatter;
    int monthOfYear;
    int year;
    int dayOfMonth;

    private void setDateTimeField() {
        monthOfYear = -1;
        year = -1;
        dayOfMonth = -1;

        final int yearThis, month, date, posYear, posDate;
        if (TextUtils.isEmpty(birthDay)) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            yearThis = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH) + 1;
            date = calendar.get(Calendar.DAY_OF_MONTH);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date));
        } else {
            String[] fulldate = birthDay.split("\\.");
            yearThis = Integer.parseInt(fulldate[2]);
            month = Integer.parseInt(fulldate[1]);
            date = Integer.parseInt(fulldate[0]);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date)) + 1;
        }

        View outerView = LayoutInflater.from(this).inflate(R.layout.wheel_view, null);
        WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);


        wv.setItems(Arrays.asList(MONTH));
        wv.setSeletion(month - 1);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                monthOfYear = selectedIndex;
            }
        });
        WheelView wv1 = (WheelView) outerView.findViewById(R.id.wheel_view_wv1);
        wv1.setItems(DATES);

        wv1.setSeletion(posDate - 1);
        wv1.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                dayOfMonth = Integer.parseInt(item);
            }
        });
        WheelView wv2 = (WheelView) outerView.findViewById(R.id.wheel_view_wv2);
        wv2.setItems(YEAR);
        wv2.setSeletion(posYear);
        wv2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                year = Integer.parseInt(item);
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });

        new AlertDialog.Builder(this)
                .setTitle("Date of Birth")
                .setView(outerView)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (monthOfYear == -1) {
                            monthOfYear = month - 1;
                        } else {
                            monthOfYear -= 1;
                        }
                        if (dayOfMonth == -1) {
                            dayOfMonth = posDate;
                        }
                        if (year == -1) {
                            year = Integer.parseInt(YEAR.get(posYear));
                        }
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        Date birthDate = new Date(newDate.getTimeInMillis());

                        onDateSet(year, monthOfYear, dayOfMonth, birthDate);
                    }
                })
                .show();
    }

    public void onDateSet(int year, int month, int day, Date newDate) {
        Calendar userAge = new GregorianCalendar(year, month, day);
        Calendar minAdultAge = new GregorianCalendar();
        Calendar minAdultAge1 = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        minAdultAge1.set(1901, 1, 1);
        if (minAdultAge.before(userAge) || minAdultAge1.after(userAge)) {
            Toast.makeText(mContext, "Sorry. Date of birth is invalid. Must be over 18 years old", Toast.LENGTH_SHORT).show();
        } else {
            String date = dateFormatter.format(newDate.getTime());
            date = date.replace("/", ".");
            birthDay = date;
            etBDy.setText(date);
        }
    }

    private boolean firstExecution = true;
    private AlertDialog dialog;
    String genderArray[] = {"Select", "Male", "Female", "Other"};
    Spinner spinner;
    private ContextWrapper context;

    private void selectGender() {
        context = mActivity;
        if (Utils.isBrokenSamsungDevice()) {
            context = new ContextThemeWrapper(mActivity, android.R.style.Theme_Holo_Light_Dialog);
        }
        if (context == null)
            return;

        spinner = new Spinner(context);
        firstExecution = false;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, genderArray);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                if (firstExecution) {
                    firstExecution = false;
                    return;
                }
                if (tvGender != null && arg2 != 0) {
                    tvGender.setText(genderArray[arg2]);
                    tvGender.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (dialog != null && dialog.isShowing() && arg2 != 0) {
                                dialog.dismiss();
                                tvGender.setError(null);
                            }
                        }
                    }, 1000);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spinner.setAdapter(adapter);
        spinner.setPadding(40, 10, 10, 10);
        if (dialog == null) {
            dialog = new AlertDialog.Builder(mActivity)
                    .setView(spinner)
                    .setTitle("Gender")
                    .create();
        }
        dialog.show();
    }

    String personFirstName, personLastName, personMobile, personPostCode, birthDay, personEmail, password;
    String deviceID = "", deviceUDID;
    String longitude = "0.0";
    String latitude = "0.0";
    ProgressDialog pDialog;

    private void Register() {
        if (!haveNetworkConnection()) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        deviceUDID = getLoadedData("DEVICE_UDID");
        deviceID = getLoadedData(Confidentials.REG_ID);
        GetLocation location = new GetLocation(this);
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());

        personFirstName = etFName.getText().toString();
        personLastName = etLName.getText().toString();
        personPostCode = etPostcode.getText().toString();
        personMobile = etMobile.getText().toString();
        gender = tvGender.getText().toString();
        birthDay = etBDy.getText().toString();
        personEmail = etEmail.getText().toString();
        password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(personFirstName)) {
            Snackbar.make(activitySignUp, "Please enter first name", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(personLastName)) {
            Snackbar.make(activitySignUp, "Please enter last name", Snackbar.LENGTH_LONG).show();
            return;
        }

//        if (TextUtils.isEmpty(personMobile)) {
//            Snackbar.make(activitySignUp, "Please enter mobile number", Snackbar.LENGTH_LONG).show();
//            return;
//        }
//        if (!Utils.isValidMobileNumber(personMobile)) {
//            Snackbar.make(activitySignUp, "Mobile number should be at least 10 digits", Snackbar.LENGTH_LONG).show();
//            return;
//        }
        if (TextUtils.isEmpty(personPostCode)) {
            Snackbar.make(activitySignUp, "Please enter postcode", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Utils.isValidPostcodeForUK(personPostCode)) {
            Snackbar.make(activitySignUp, "Please enter a valid postcode", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(gender)) {
            Snackbar.make(activitySignUp, "Please select gender", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(birthDay)) {
            Snackbar.make(activitySignUp, "Please select date of birth", Snackbar.LENGTH_LONG).show();
            return;
        }


        if (TextUtils.isEmpty(personEmail)) {
            Snackbar.make(activitySignUp, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(personEmail).matches()) {
            Snackbar.make(activitySignUp, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (password == null || password.isEmpty()) {
            Snackbar.make(activitySignUp, "Please enter a password", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (password == null || !Utils.isValidPassword(password)) {
            dialog_info("Confidentials", "Password must be eight characters including one uppercase letter, one lowercase letter and alphanumeric characters");
            return;
        }

        if (!checkboxTC.isChecked()) {
            Snackbar.make(activitySignUp, "You must read and accept terms and conditions before proceeding", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!checkboxPP.isChecked()) {
            Snackbar.make(activitySignUp, "You must read and accept privacy policy before proceeding", Snackbar.LENGTH_LONG).show();
            return;
        }

        birthDay = birthDay.replace("Birthday: ", "");
        birthDay = birthDay.replace(".", "/");

        Map<String, String> params = new HashMap<String, String>();

        params.put(URL_Utils.PARAM_FIRST_NAME, personFirstName);
        params.put(URL_Utils.PARAM_LAST_NAME, personLastName);
        params.put(URL_Utils.PARAM_MOBILE_NO, personMobile);
        params.put(URL_Utils.PARAM_POSTCODE, personPostCode);
        params.put(URL_Utils.PARAM_DOB, birthDay);
        params.put(URL_Utils.PARAM_EMAIL_ID, personEmail);
        params.put(URL_Utils.PARAM_GENDER, gender);
        params.put(URL_Utils.PARAM_PROFILE_PIC, "");
        params.put(URL_Utils.PARAM_REGISTRATION_MODE, "AppRegistration");
        params.put(URL_Utils.PARAM_PASSWORD, password);
        params.put(URL_Utils.PARAM_IOS_APP_VERSION, "");
        params.put(URL_Utils.PARAM_ANDROID_APP_VERSION, Controller.APP_VERSION_NAME);
        params.put(URL_Utils.PARAM_DEVICE_TYPE, getString(R.string.deviceType));
        params.put(URL_Utils.PARAM_DEVICE_ID, deviceID);
        params.put(URL_Utils.PARAM_UDID, deviceUDID);
        params.put(URL_Utils.PARAM_LATITUDE, latitude);
        params.put(URL_Utils.PARAM_LONGITUDE, longitude);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL_Utils.INSERT_USER, params, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void onFaceBookClick() {
        mButtonLoginFaceBook.performClick();
        mButtonLoginFaceBook.setReadPermissions("public_profile", "email");
        mButtonLoginFaceBook.registerCallback(callbackManager, callback);
    }


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            getProfileFaceBook(accessToken);
        }

        @Override
        public void onCancel() {
            Log.v("LoginActivity", "cancel");
        }

        @Override
        public void onError(FacebookException e) {
            e.printStackTrace();
        }
    };

    String firstName, lastName, faceBookEmail, gender = "", birthday = "";

    private void getProfileFaceBook(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                (object, response) -> {
                    Log.v("LoginActivity", response.toString());
                    try {
                        firstName = response.getJSONObject().getString("first_name");
                        lastName = response.getJSONObject().getString("last_name");
                        faceBookEmail = response.getJSONObject().getString("email");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LoginManager.getInstance().logOut();
                    onLoginService();
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, name, email,gender, birthday, age_range"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void onLoginService() {
        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(faceBookEmail)) {
            doFacebookRegister();
        } else {
            showErrorDialogAll(mActivity, "Uh oh!", "An error occurred, we were unable to create your account. Please try again");
        }
    }


    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (pDialog != null)
                pDialog.dismiss();
            try {
                Log.d(TAG, error.toString());
            } catch (RuntimeException e) {
            }

            showErrorDialogAll(mActivity, getString(R.string.app_name), getString(R.string.error_backend));
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            pDialog.dismiss();
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    saveLoadedData(Constant.USERID, response.getString("userId"));
                    setLoggedIn(true);
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    String message = response.getString("message");
                    dialog_info("Confidentials", message);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    private void doFacebookRegister() {
        if (!haveNetworkConnection()) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        deviceUDID = getLoadedData("DEVICE_UDID");
        deviceID = getLoadedData(Confidentials.REG_ID);
        GetLocation location = new GetLocation(this);
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());

        Map<String, String> params = new HashMap<String, String>();
        params.put(URL_Utils.PARAM_FIRST_NAME, firstName);
        params.put(URL_Utils.PARAM_LAST_NAME, lastName);
        params.put(URL_Utils.PARAM_DOB, checkString(birthday));
        params.put(URL_Utils.PARAM_EMAIL_ID, faceBookEmail);
        params.put(URL_Utils.PARAM_GENDER, gender);
        params.put(URL_Utils.PARAM_PROFILE_PIC, "");
        params.put(URL_Utils.PARAM_MOBILE_NO, "");
        params.put(URL_Utils.PARAM_POSTCODE, "M32 5TY");
        params.put(URL_Utils.PARAM_REGISTRATION_MODE, "Facebook");
        params.put(URL_Utils.PARAM_PASSWORD, checkString(password));
        params.put(URL_Utils.PARAM_IOS_APP_VERSION, "");
        params.put(URL_Utils.PARAM_ANDROID_APP_VERSION, Controller.APP_VERSION_NAME);
        params.put(URL_Utils.PARAM_DEVICE_TYPE, getString(R.string.deviceType));
        params.put(URL_Utils.PARAM_DEVICE_ID, deviceID);
        params.put(URL_Utils.PARAM_UDID, deviceUDID);
        params.put(URL_Utils.PARAM_LATITUDE, latitude);
        params.put(URL_Utils.PARAM_LONGITUDE, longitude);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL_Utils.INSERT_USER, params, createRequestSuccessListener, createRequestErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(5000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void startPrivacyPolicy(String title, String isPolocy) {
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        intent.putExtra("param1", isPolocy);
        intent.putExtra("title", title);
        startActivity(intent);
    }


}

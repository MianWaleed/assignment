package com.predrinks.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.predrinks.app.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends AppCompatActivity {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Bind(R.id.imgBack)
    ImageView imgBack;
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.containWebView)
    WebView urlWebView;

    private String mParam1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mParam1 = intent.getStringExtra(ARG_PARAM1);
        String title = intent.getStringExtra("title");

        tvTitleHeader.setText(title);

        urlWebView.setWebViewClient(new AppWebViewClients());
        urlWebView.getSettings().setJavaScriptEnabled(true);
        urlWebView.getSettings().setUseWideViewPort(true);
        urlWebView.getSettings().setLoadWithOverviewMode(true);
        urlWebView.loadUrl(mParam1);

    }

    @OnClick(R.id.imgBack)
    public void onClick() {
        onBackPressed();
    }

    public class AppWebViewClients extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

        }
    }

    @Override
    public void onBackPressed() {
        //finish();
        super.onBackPressed();
    }
}

package com.predrinks.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.OnLoadMoreListener;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.fragment.OfferDetailFragment;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.android.volley.VolleyLog.TAG;

public class SearchOfferAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private JSONArray OffersLists;
    Context mContext;
    public static Set<Integer> selection;

    public void clearData() {
        if(OffersLists.length()>0){
            for (int i = 0; i < OffersLists.length(); i++) {
                OffersLists.remove(i);
            }
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.imgMain)
        ImageView imgMain;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvVenueName)
        TextView tvVenueName;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.lnrMain)
        LinearLayout lnrMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            lnrMain.setOnClickListener(this);
            tvName.setSelected(true);
            tvName.setFocusable(true);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == lnrMain.getId()) {
                int pos = (int) lnrMain.getTag();

                JSONObject object;
                String eventId = "";
                try {
                    object = OffersLists.getJSONObject(pos);
                    eventId = object.getString("eventId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                OfferDetailFragment fragment = OfferDetailFragment.newInstance(eventId, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        try {
            return OffersLists.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        } catch (JSONException e) {
            e.printStackTrace();
            return VIEW_PROG;
        }

    }

    public SearchOfferAdapter(Context context, JSONArray clubLists, RecyclerView recyclerView) {
        this.mContext = context;
        this.OffersLists = clubLists;
        if (selection == null)
            selection = new HashSet<>();

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_fav_offers, parent, false);
        return new MyViewHolder(itemView);*/
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_fav_offers, parent, false);
            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyViewHolder) {
            JSONObject jsonObject = null;
            try {
                jsonObject = OffersLists.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject == null)
                return;


            ((MyViewHolder) holder).lnrMain.setBackgroundColor(Color.WHITE);
            ((MyViewHolder) holder).lnrMain.setTag(position);

            ((MyViewHolder) holder).tvName.setText(getJsonString(jsonObject, "title"));
            ((MyViewHolder) holder).tvVenueName.setText(getJsonString(jsonObject, "venueName"));
            ((MyViewHolder) holder).tvDateTime.setText(getJsonString(jsonObject, "eventEndDateTime"));

            String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");
            if (isEventDateDisabled.equals("true")) {
                ((MyViewHolder) holder).tvDateTime.setVisibility(View.INVISIBLE);
            } else {
                ((MyViewHolder) holder).tvDateTime.setVisibility(View.VISIBLE);
            }

            String imgUrl = "";
            if (TextUtils.isEmpty(getJsonString(jsonObject, "eventImage"))) {
                imgUrl = URL_Utils.URL_IMAGE_DNLOAD_DEFAULT + "images/default_con.png";
            } else {
                imgUrl = URL_Utils.URL_IMAGE_DNLOAD + getJsonString(jsonObject, "eventImage");
            }

            Log.d(TAG, "onBindViewHolder: "+imgUrl);

            Glide.with(mContext)
                    .load(imgUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.mipmap.default_club_logo)
                    //.override(150, 170)
                    .into(((MyViewHolder) holder).imgMain);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return OffersLists.length();
    }

    public void setSelection(int pos) {
        selection.add(pos);
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}

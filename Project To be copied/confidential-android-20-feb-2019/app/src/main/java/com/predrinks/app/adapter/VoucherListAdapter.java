package com.predrinks.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.activity.VoucherDetailActivity;
import com.predrinks.app.fragment.MyVouchersFragment;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.URL_Utils;
import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.android.rides.RideRequestButtonCallback;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.uber.sdk.rides.client.error.ApiError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static io.fabric.sdk.android.Fabric.TAG;

public class VoucherListAdapter extends RecyclerView.Adapter<VoucherListAdapter.MyViewHolder> {

    private JSONArray myEventList;
    Context mContext;
    Activity mActivity;
    MyVouchersFragment mMyVouchersFragment;
    int mCorrentPos = -1;
    GetLocation getLocation;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.imgMainBg)
        ImageView imgMainBg;
        @Bind(R.id.tvRemaining)
        TextView tvRemaining;
        @Bind(R.id.tvOfferTitle)
        TextView tvOfferTitle;
        @Bind(R.id.tvOfferTitle1)
        TextView tvOfferTitle1;
        @Bind(R.id.tvOfferDetail)
        TextView tvOfferDetail;
        @Bind(R.id.tvEventStatus)
        TextView tvEventStatus;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.tvNumberX)
        TextView tvNumberX;
        @Bind(R.id.tvOfferDetail1)
        TextView tvOfferDetail1;
        @Bind(R.id.rlDefault)
        RelativeLayout rlDefault;
        @Bind(R.id.tvRedeemed)
        TextView tvRedeemed;
        @Bind(R.id.tvBuyAgain)
        TextView tvBuyAgain;
        @Bind(R.id.lnrBuyAgain)
        LinearLayout lnrBuyAgain;
        @Bind(R.id.lnrContact)
        LinearLayout lnrContact;
        @Bind(R.id.lnrEventCancelled)
        LinearLayout lnrEventCancelled;
        @Bind(R.id.lnr_Main)
        LinearLayout lnr_Main;
        @Bind(R.id.rlRedeemMain)
        RelativeLayout rlRedeemMain;
        @Bind(R.id.lnrUber)
        LinearLayout lnrUber;
        @Bind(R.id.btn_RideRequestButton)
        RideRequestButton btn_RideRequestButton;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvOfferTitle.setSelected(true);
            tvOfferTitle.setFocusable(true);
            tvOfferTitle1.setSelected(true);
            tvOfferTitle1.setFocusable(true);


            tvOfferTitle.setTypeface(MainActivity.getTypeFaceLight());
            tvOfferTitle1.setTypeface(MainActivity.getTypeFaceLight());
            tvOfferDetail.setTypeface(MainActivity.getTypeFaceRegular());
            tvOfferDetail1.setTypeface(MainActivity.getTypeFaceRegular());
            tvDateTime.setTypeface(MainActivity.getTypeFaceBold());
            tvNumberX.setTypeface(MainActivity.getTypeFaceLight());

            lnrUber.setOnClickListener(this);
            lnr_Main.setOnClickListener(this);
            lnrContact.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == lnrUber.getId()) {
                int pos = (int) lnrUber.getTag();
                mCorrentPos = pos;
                JSONObject jsonObject = null;
                try {
                    jsonObject = myEventList.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                rideWithUber(jsonObject, btn_RideRequestButton);

            } else if (v.getId() == lnr_Main.getId()) {
                int pos = (int) lnr_Main.getTag();
                JSONObject jsonObject = null;
                try {
                    jsonObject = myEventList.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String eventID = getJsonString(jsonObject, "eventID");
                String eventTitle = getJsonString(jsonObject, "eventTitle");
                boolean isExpired = Boolean.parseBoolean(getJsonString(jsonObject, "isExpired"));
                boolean isActive = Boolean.parseBoolean(getJsonString(jsonObject, "isActive"));
                if (!isActive && !isExpired) {
                    return;
                }
                Intent intent = new Intent(mContext, VoucherDetailActivity.class);
                intent.putExtra("eventTitle", eventTitle);
                intent.putExtra("eventID", eventID);
                Log.d(TAG, "onClick: " + jsonObject.toString());
                intent.putExtra("eventObject", jsonObject.toString());
                mMyVouchersFragment.startActivityForResult(intent, 100);

            } else if (v.getId() == lnrContact.getId()) {
                int pos = (int) lnrContact.getTag();
                JSONObject jsonObject = null;
                try {
                    jsonObject = myEventList.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                boolean isActive = Boolean.parseBoolean(getJsonString(jsonObject, "isActive"));
                if (!isActive) {
                    String venueContactNo = getJsonString(jsonObject, "venueContactNo");
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + venueContactNo));
                    mMyVouchersFragment.startActivity(intent);
                }
            }
        }
    }

    private void rideWithUber(JSONObject jsonObject, RideRequestButton btn_RideRequestButton) {
        double venueLatitude = 0, venueLongitude = 0;
        String venueTitle = "";
        try {
            venueLatitude = jsonObject.getDouble("venueLatitude");
            venueLongitude = jsonObject.getDouble("venueLongitude");
            venueTitle = getJsonString(jsonObject, "venueTitle");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RideParameters rideParams = new RideParameters.Builder()
                .setPickupLocation(getLocation.getLatitude(), getLocation.getLongitude(), "", "")
                .setDropoffLocation(venueLatitude, venueLongitude, venueTitle, "")
                .build();

        SessionConfiguration config = new SessionConfiguration.Builder()
                .setClientId("rxyBBENyaBtmWkMqJVXozGt9AsWw1ufn ")
                .setServerToken("Eerti8j_RYlFLM4JYC38G7QdOHSEFxt93L9VqMre")
                .build();

        RideRequestButtonCallback callback = new RideRequestButtonCallback() {
            @Override
            public void onRideInformationLoaded() {

            }

            @Override
            public void onError(ApiError apiError) {

            }

            @Override
            public void onError(Throwable throwable) {

            }
        };
        ServerTokenSession session = new ServerTokenSession(config);
        btn_RideRequestButton.setSession(session);
        btn_RideRequestButton.setRideParameters(rideParams);
        btn_RideRequestButton.performClick();

        btn_RideRequestButton.setCallback(callback);
        btn_RideRequestButton.loadRideInformation();

    }

    public VoucherListAdapter(MyVouchersFragment myVouchersFragment, Activity activity, Context context, JSONArray myEventList) {
        this.mActivity = activity;
        this.mMyVouchersFragment = myVouchersFragment;
        this.mContext = context;
        this.myEventList = myEventList;
        getLocation = new GetLocation(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_voucher, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        JSONObject jsonObject = null;
        try {
            jsonObject = myEventList.getJSONObject(position);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "onBindViewHolder: " + jsonObject.toString());

        holder.tvRedeemed.setTextColor(Color.parseColor("#FFFFFF"));
        holder.tvOfferTitle.setText(getJsonString(jsonObject, "eventTitle"));
        holder.tvOfferTitle1.setText(getJsonString(jsonObject, "eventTitle"));
        holder.tvOfferDetail.setText(getJsonString(jsonObject, "venueTitle"));
        holder.tvOfferDetail1.setText(getJsonString(jsonObject, "venueTitle"));
        holder.tvDateTime.setText(getJsonString(jsonObject, "eventDate") + " | " + getJsonString(jsonObject, "eventTime"));
        holder.tvDateTime.setTextColor(Color.parseColor("#C20F0F"));
        holder.tvNumberX.setText("X" + getJsonString(jsonObject, "purchasedOffersCnt"));

        holder.lnrContact.setVisibility(View.GONE);
        holder.lnrEventCancelled.setVisibility(View.GONE);
        holder.rlRedeemMain.setVisibility(View.GONE);
        holder.lnrBuyAgain.setVisibility(View.GONE);
        holder.tvEventStatus.setVisibility(View.GONE);
        holder.lnrUber.setVisibility(View.VISIBLE);
        holder.rlDefault.setVisibility(View.VISIBLE);

        holder.lnrUber.setTag(position);
        holder.lnr_Main.setTag(position);
        holder.tvRedeemed.setTag(position);
        holder.lnrContact.setTag(position);

        String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");
        if (isEventDateDisabled.equals("true")) {
            holder.tvDateTime.setVisibility(View.INVISIBLE);
        } else {
            holder.tvDateTime.setVisibility(View.VISIBLE);
        }

        boolean isRedeemed = Boolean.parseBoolean(getJsonString(jsonObject, "isRedeemed"));
        boolean isExpired = Boolean.parseBoolean(getJsonString(jsonObject, "isExpired"));
        boolean isActive = Boolean.parseBoolean(getJsonString(jsonObject, "isActive"));


        if (isRedeemed) {
            holder.rlRedeemMain.setVisibility(View.VISIBLE);
            holder.lnrBuyAgain.setVisibility(View.VISIBLE);
            holder.tvRedeemed.setText("REDEEMED");

            holder.tvEventStatus.setVisibility(View.VISIBLE);
            holder.tvEventStatus.setText("THESE VOUCHER(s) HAVE BEEN REDEEMED");
            holder.tvEventStatus.setTextColor(Color.parseColor("#929292"));
            holder.tvDateTime.setTextColor(Color.parseColor("#C20F0F"));
            holder.lnrUber.setVisibility(View.GONE);
            holder.rlDefault.setVisibility(View.GONE);
        } else if (isExpired) {
            holder.rlRedeemMain.setVisibility(View.VISIBLE);
            holder.lnrBuyAgain.setVisibility(View.VISIBLE);
            holder.lnrContact.setVisibility(View.GONE);
            holder.lnrEventCancelled.setVisibility(View.GONE);

            holder.tvRedeemed.setText("TAP HERE TO VIEW VALID VOUCHER(s)");

            holder.tvEventStatus.setVisibility(View.VISIBLE);
            holder.tvEventStatus.setText("THIS EVENT HAS EXPIRED");
            holder.tvDateTime.setTextColor(Color.parseColor("#C20F0F"));
            holder.lnrUber.setVisibility(View.GONE);
            holder.rlDefault.setVisibility(View.GONE);
        } else {
            holder.rlDefault.setVisibility(View.VISIBLE);
            holder.lnrUber.setVisibility(View.VISIBLE);
            holder.rlRedeemMain.setVisibility(View.GONE);
            holder.lnrBuyAgain.setVisibility(View.GONE);
            holder.tvDateTime.setText(getJsonString(jsonObject, "eventDate") + " | " + getJsonString(jsonObject, "eventTime"));
            holder.tvDateTime.setTextColor(Color.parseColor("#C20F0F"));
        }

        if (!isActive && !isExpired) {
            holder.rlRedeemMain.setVisibility(View.VISIBLE);
            holder.lnrContact.setVisibility(View.VISIBLE);
            holder.lnrEventCancelled.setVisibility(View.VISIBLE);
            holder.lnrUber.setVisibility(View.GONE);
            holder.lnrBuyAgain.setVisibility(View.GONE);

            holder.tvDateTime.setText(getJsonString(jsonObject, "eventDate") + " | " + getJsonString(jsonObject, "eventTime"));
            holder.tvDateTime.setTextColor(Color.parseColor("#C20F0F"));
            holder.lnrUber.setVisibility(View.GONE);
            holder.rlDefault.setVisibility(View.GONE);
        }

        String imgUrl = "";

        if (TextUtils.isEmpty(getJsonString(jsonObject, "eventImage"))) {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD_DEFAULT + "images/default_con.png";
        } else {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + getJsonString(jsonObject, "eventImage");
        }

        Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_event_venue_image)
                .into(holder.imgMainBg);


    }

    @Override
    public int getItemCount() {
        return myEventList.length();
    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (jsonObject == null || !jsonObject.has(strKey))
                return str;

            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }


}

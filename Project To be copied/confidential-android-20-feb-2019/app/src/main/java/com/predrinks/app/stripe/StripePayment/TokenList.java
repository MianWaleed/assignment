package com.predrinks.app.stripe.StripePayment;

import com.stripe.android.model.Token;

public interface TokenList {
    public void addToList(Token token);
}

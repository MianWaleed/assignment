package com.predrinks.app.stripe.Stripe.CustomForm.Fields;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;

import com.predrinks.app.R;
import com.predrinks.app.stripe.Stripe.CustomForm.Internal.CreditCardUtil;
import com.predrinks.app.stripe.Stripe.CustomForm.Layout.CardType;
import com.predrinks.app.stripe.StripePayment.Main.StripePaymentActivity;


public class CreditCardText extends CreditEntryFieldBase {
	private CardType type;
	public static int cardTextLength;
	SharedPreferences SharedPrefExpDate;
	boolean isExpdate = false;
	public static boolean cardNoValid = false;
	
	public CreditCardText(Context context) {
		super(context);
		init();
	}

	public CreditCardText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CreditCardText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	@SuppressLint("RtlHardcoded")
	@Override
	void init() {
		super.init();
		
		
		setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//		setFocusable(true);
//		setFocusableInTouchMode(true);
	}

	/* TextWatcher Implementation Methods */
	@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	@Override
	public void afterTextChanged(Editable s) {
		String number = s.toString();
		
		cardTextLength = number.length();
		if (cardTextLength >= CreditCardUtil.CC_LEN_FOR_TYPE) {
			formatAndSetText(number);
		} else {
			if (this.type != null) {
				this.type = null;
				delegate.onCardTypeChange(CardType.INVALID);
			}
		}
		
//		if(cardTextLength == 19 && ExpDateText.isExpDateValid && SecurityCodeText.isCvcValid)
//		{
//			StripePaymentActivity.btnPayNow.setEnabled(true);
//		}
//		else
//		{
//			StripePaymentActivity.btnPayNow.setEnabled(false);
//		}
	}

	@Override
	public void formatAndSetText(String number) {
		SharedPrefExpDate = context.getSharedPreferences("SharedPrefExpDate", Context.MODE_PRIVATE);
		isExpdate = SharedPrefExpDate.getBoolean("PrefExpDate", false);
		CardType type = CreditCardUtil.findCardType(number);

		if (type.equals(CardType.INVALID)) {
            setValid(false);
            delegate.onBadInput(this);
            return;
        }

		if (this.type != type) {
            delegate.onCardTypeChange(type);
        }
		this.type = type;

		String formatted = CreditCardUtil.formatForViewing(number, type);
		if (!number.equalsIgnoreCase(formatted)) {
            this.removeTextChangedListener(this);
            this.setText(formatted);
            this.setSelection(formatted.length());
            this.addTextChangedListener(this);
        }

		if (formatted.length() >= CreditCardUtil.lengthOfFormattedStringForType(type)) {

            String remainder = null;
            if (number.startsWith(formatted)) {
                remainder = number.replace(formatted, "");
            }
            if (CreditCardUtil.isValidNumber(formatted)) {
                setValid(true);
                delegate.onCreditCardNumberValid(remainder);
                
                cardNoValid = true;
                if(cardTextLength == 19 && ExpDateText.expdate == 5 && SecurityCodeText.isCvcValid && StripePaymentActivity.postCodeEditText.getText().length() > 0)
        		{
        			StripePaymentActivity.btnPayNow.setEnabled(true);
        		}
        		else
        		{
        			StripePaymentActivity.btnPayNow.setEnabled(true);
        		}
            } else {
                setValid(false);
                delegate.onBadInput(this);
                cardNoValid = false;
                StripePaymentActivity.btnPayNow.setEnabled(true);
            }
        } else {
			setValid(false);
			StripePaymentActivity.btnPayNow.setEnabled(true);
			cardNoValid = false;
		}
	}

	public CardType getType() {
		return type;
	}

	@Override
	public String helperText() {
		return context.getString(R.string.CreditCardNumberHelp);
	}
}

package com.predrinks.app.stripe.Stripe.CustomForm.Fields;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;

import com.predrinks.app.R;
import com.predrinks.app.stripe.Stripe.CustomForm.Internal.CreditCardUtil;
import com.predrinks.app.stripe.Stripe.CustomForm.Layout.CardType;
import com.predrinks.app.stripe.StripePayment.Main.StripePaymentActivity;


public class SecurityCodeText extends CreditEntryFieldBase {

    private CardType type;

    private int length;
    public static boolean isCvcValid = false;

    public SecurityCodeText(Context context) {
        super(context);
        isCvcValid = false;
        init();
    }

    public SecurityCodeText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SecurityCodeText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void init() {
        super.init();
        setHint("CVC");
    }

    /* TextWatcher Implementation Methods */
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void afterTextChanged(Editable s) {
        if (type == null) {
            this.removeTextChangedListener(this);
            this.setText("");
            this.addTextChangedListener(this);
        }

        if (s.toString().length() == 3 || s.toString().length() == 4) {
            if (StripePaymentActivity.postCodeEditText.getText().length() > 0) {
                //StripePaymentActivity.btnPayNow.setEnabled(true);
            } else {
                //Toast.makeTxt(context, "Enter post Code", Toast.LENGTH_SHORT).show();
                //StripePaymentActivity.btnPayNow.setEnabled(false);
            }
            StripePaymentActivity.btnPayNow.setEnabled(true);
        } else {
            StripePaymentActivity.btnPayNow.setEnabled(true);
        }
    }

    public void formatAndSetText(String s) {
        setText(s);
    }

    public void textChanged(CharSequence s, int start, int before, int count) {
        if (type != null) {
            if (s.length() >= length) {
                setValid(true);
                String remainder = null;
                if (s.length() > length()) remainder = String.valueOf(s).substring(length);
                this.removeTextChangedListener(this);
                setText(String.valueOf(s).substring(0, length));
                this.addTextChangedListener(this);
                delegate.onSecurityCodeValid(remainder);
                isCvcValid = true;
            } else {
                setValid(false);
                isCvcValid = false;
            }
        }
    }

    @SuppressWarnings("unused")
    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
        this.length = CreditCardUtil.securityCodeValid(type);
    }

    @Override
    public String helperText() {
        return context.getString(R.string.SecurityCodeHelp);
    }
}

package com.predrinks.app;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.kumulos.android.Kumulos;
import com.kumulos.android.KumulosConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.customerly.Customerly;

public class Controller extends Application {

    private Context mContext;
    public Context context;
    public static String APP_VERSION_NAME = "";
    private String DEVICE_UDID = "";

    private static Controller sInstance;

    @SuppressLint("HardwareIds")
    @Override
    public void onCreate() {
        super.onCreate();
        /*Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(false)
                .build();
        Fabric.with(fabric);*/

//        Fabric.with(this, new Crashlytics());
        KumulosConfig config = new KumulosConfig
                .Builder("d56fa7b3-7217-47f5-b2ae-e89cc3b16ef1", "4NttPmsbESZQgN5MPmkGN14XkoedY14My4Hs")
                .enableCrashReporting().build();
        Kumulos.initialize(this, config);

//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
//                .build();
//        Fabric.with(this, crashlyticsKit);

        sInstance = this;
        mContext = getApplicationContext();
        Customerly.configure(this, "3a07cfcd");
        Customerly.setSurveyEnabled(true);

        DEVICE_UDID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            APP_VERSION_NAME = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(DEVICE_UDID)) {
            SharedPreferences mPrefs = mContext.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
            mPrefs.edit().putString("DEVICE_UDID", DEVICE_UDID).apply();
        }
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.predrinks.app", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    public static Controller getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }


}

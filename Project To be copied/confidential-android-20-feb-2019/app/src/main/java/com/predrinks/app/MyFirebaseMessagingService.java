package com.predrinks.app;

//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;


public class MyFirebaseMessagingService {
    public static final String CHANNEL_ID = "com.predrinks.app.fcm.ANDROID";

}/*extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    private SharedPreferences mPrefs;
    private static final String POUND_UNI_CODE = "\\u00A3";
    private static final String POUND_SIGN = "£";

    *//**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     *//*
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        String notifiBody = "";
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            notifiBody = remoteMessage.getNotification().getBody();
        }

        // Check if message contains a data payload.
        String paylod = "";
        JSONObject json = null;
        JSONArray jsonArray = null;
        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "Data Payload: " + remoteMessage.getData());

            try {
                json = new JSONObject(remoteMessage.getData());
                paylod = json.toString();
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
            if (json == null) {
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        jsonArray = new JSONArray(remoteMessage.getData());
                    }
                    paylod = jsonArray.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        // TODO(developer): Handle FCM messages here.
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + paylod);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        mPrefs = this.getApplicationContext().getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isnotification = mPrefs.getBoolean(Constant.NOTIFICATIONS, true);

        if (!Utils.isAppIsInBackground(this)){
            Intent intnt = new Intent("some_custom_id");
            intnt.putExtra("messageBody", notifiBody);
            intnt.putExtra("paylod", paylod);
            sendBroadcast(intnt);
        }else {
            if (isnotification)
                sendNotification(notifiBody, paylod);
        }


    }

    // [END receive_message]

    *//**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     *//*
    private void sendNotification(String messageBody, String data) {
        Log.e("messageBody :: ", messageBody + "data :: "+ data);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("messageBody", messageBody);
        intent.putExtra("data", data);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent, PendingIntent.FLAG_ONE_SHOT);

        String title = "";
        String message = "";
        try {
            JSONObject object = new JSONObject(data);
            title = object.getString("title");
            message = object.getString("content");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        title = title.replace("\\u00A3","£");
        message = message.replace("\\u00A3","£");

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.icon);
            notificationBuilder.setColor(getResources().getColor(R.color.icon_bg));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }

        message = message.replace(POUND_UNI_CODE, POUND_SIGN);

        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(message);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSound(defaultSoundUri);
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            //NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }
}
*/
package com.predrinks.app.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.predrinks.app.R;

import static android.support.constraint.Constraints.TAG;

public class WriteReviewsDialogFragment extends DialogFragment implements View.OnClickListener {

    private CardView cardView2;
    private TextView textView11;
    private RatingBar ratingBar;
    private EditText reviewEt;
    private TextView cancelTv;
    private TextView submitTv;
    View view;

    public WriteReviewsDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.write_review_dialog, container);
        initView(view);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), R.style.Confidentials_Pass_WideDialog);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initView(View view) {
        ratingBar = view.findViewById(R.id.ratingBar);
        reviewEt = view.findViewById(R.id.review_et);
        cancelTv = view.findViewById(R.id.cancel_tv);
        submitTv = view.findViewById(R.id.submit_tv);

        cancelTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_tv: {
                getDialog().cancel();
                break;
            }
        }
    }
}

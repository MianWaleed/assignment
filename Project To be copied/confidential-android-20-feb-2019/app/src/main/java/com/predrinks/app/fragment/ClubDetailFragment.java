package com.predrinks.app.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.adapter.ClubDetailOfferAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.ExpandableTextView;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.SimpleDividerItemDecoration;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.viewpagerindicator.CirclePageIndicator;
import com.predrinks.app.widget.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.predrinks.app.activity.MainActivity.setVisibleHeaderRightMap;


public class ClubDetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ClubDetailFragment";

    @Bind(R.id.pager)
    ViewPager viewPager;
    @Bind(R.id.titles)
    CirclePageIndicator indicator;
    @Bind(R.id.imgMain)
    ImageView imgMain;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.imgFav)
    ImageView imgFav;
    @Bind(R.id.tvAdd)
    TextView tvAdd;
    @Bind(R.id.tvAdd1)
    TextView tvAdd1;
    @Bind(R.id.tvAdd2)
    TextView tvAdd2;
    @Bind(R.id.tvOffers)
    TextView tvOffers;
    @Bind(R.id.rltSeeAll)
    LinearLayout rltSeeAll;
    @Bind(R.id.lnr_row_club)
    LinearLayout lnrRowClub;
    @Bind(R.id.lnrGetDirection)
    RelativeLayout lnrGetDirection;
    @Bind(R.id.tvClubDesc)
    TextView tvClubDesc;
    @Bind(R.id.expandable_text)
    TextView expandableText;
    @Bind(R.id.expand_collapse)
    TextView expandCollapse;
    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.tvVenueTerms)
    TextView tvVenueTerms;

    @Bind(R.id.expand_text_view)
    ExpandableTextView expandTextView;
    @Bind(R.id.expand_text_view1)
    ExpandableTextView expandTextView1;
    @Bind(R.id.expand_text_view2)
    ExpandableTextView expandTextView2;
    @Bind(R.id.recycler_view_offer)
    RecyclerView recyclerViewOffer;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.imgDefaultImage)
    ImageView imgDefaultImage;
    @Bind(R.id.scrollView)
    ScrollView scrollView;

    private String mVenueId;
    private String mTitle;

    Activity mActivity;
    Context mContext;
    SharedPreferences mPrefs;
    MyPagerAdapter myPagerAdapter;
    String mResponse = "";
    String mUserId = "";
    private GetLocation getLocation;

    public ClubDetailFragment() {
    }

    public static ClubDetailFragment newInstance(String param1, String param2) {
        ClubDetailFragment fragment = new ClubDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = getActivity();
        mResponse = "";
        getLocation = new GetLocation(mContext);
        mPrefs = mContext.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        mUserId = getLoadedData(mContext, Constant.USERID);
        if (getArguments() != null) {
            mVenueId = getArguments().getString(ARG_PARAM1);
            mTitle = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_club_detail, container, false);
        ButterKnife.bind(this, view);


        tvName.setTypeface(MainActivity.getTypeFaceRegular());
        tvAdd.setTypeface(MainActivity.getTypeFaceRegular());
        tvAdd1.setTypeface(MainActivity.getTypeFaceMedium());
        tvAdd2.setTypeface(MainActivity.getTypeFaceMedium());
        tvOffers.setTypeface(MainActivity.getTypeFaceMedium());
        tvOffers.setVisibility(View.GONE);

        tvTime.setTypeface(MainActivity.getTypeFaceMedium());
        tvVenueTerms.setTypeface(MainActivity.getTypeFaceRegular());
        tvClubDesc.setTypeface(MainActivity.getTypeFaceRegular());

        setVisibleHeaderRightMap(false);

        if (!TextUtils.isEmpty(mResponse))
            setOfferList(mResponse);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setOnChildScrollUpCallback(onChildScrollUpCallback);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float v, int i1) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                enableDisableSwipeRefresh(state == ViewPager.SCROLL_STATE_IDLE);
            }
        });


        getDataFromWeb();
        return view;
    }

    private void enableDisableSwipeRefresh(boolean enable) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(enable);
        }
    }

    SwipeRefreshLayout.OnChildScrollUpCallback onChildScrollUpCallback = new SwipeRefreshLayout.OnChildScrollUpCallback() {
        @Override
        public boolean canChildScrollUp(SwipeRefreshLayout parent, @Nullable View child) {
            if (scrollView != null)
                return scrollView.canScrollVertically(-1);
            return false;
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.header_layout_inner.setVisibility(View.GONE);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        MainActivity.setBackButton(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        VolleySingleton.getInstance().cancelPendingRequests(TAG);
    }

    @Override
    public void onRefresh() {
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getDataFromWeb();
    }

    private void getDataFromWeb() {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String mUserId = mPrefs.getString(Constant.USERID, "");
                String URL = URL_Utils.VENUE_DETAIL + "/" + mVenueId + "/user/" + mUserId + "/detail_New";
                Log.d(TAG, "onCreate: " + URL);
                CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
            }
        }, 1000);
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            Log.d(TAG, error.toString());
            if (haveNetworkConnection(mContext)) {
                getDataFromWeb();
            }
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    mResponse = response.toString();
                    setOfferList(response.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    boolean isnotification = mPrefs.getBoolean(Constant.NOTIFICATIONS, true);
                    String userId = getLoadedData(mContext, Constant.USERID);
                    //updateUserDetail(userId, isnotification, getLocation.getLatitude(), getLocation.getLongitude());
                }
            }, 1000);
        }
    };
    JSONObject jsonObject = null;
    JSONArray eventDetailUnderVenueList = null;
    boolean isFavourite = false;
    JSONArray venueImagesList = null;

    private void setOfferList(String response) {
        try {
            JSONObject responseObject = new JSONObject(response);
            jsonObject = responseObject.getJSONObject(Constant.RESULT);
            JSONArray jArray = jsonObject.getJSONArray(Constant.EVENTDETAILUNDERVENUE);
            if (jArray != null) {
                eventDetailUnderVenueList = new JSONArray();
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jArray.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
                    if (totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
                    } else {
                        eventDetailUnderVenueList.put(jsonObject);
                    }
                }
            }
            venueImagesList = jsonObject.getJSONArray(Constant.VENUEIMAGES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String imageName = getJsonString(jsonObject, "logo");
        String isFavoriteVenue = getJsonString(jsonObject, "isFavoriteVenue");


        String mainContactEmailId = getJsonString(jsonObject, "emailId");
        String mainContatcPhone = getJsonString(jsonObject, "phone");
        String address;
        address = getJsonString(jsonObject, "address1");
        MainActivity.setHeaderTitle(getJsonString(jsonObject, "name"));

        if (tvName == null)
            return;

        if (!isFavoriteVenue.equals("false")) {
            imgFav.setImageResource(R.mipmap.favourite_fill);
            isFavourite = true;
        } else {
            isFavourite = false;
            imgFav.setImageResource(R.mipmap.favourite_default);
        }

        tvName.setText(getJsonString(jsonObject, "name"));
        tvAdd.setText(address);
        tvAdd1.setText(mainContatcPhone);
        tvAdd2.setText(mainContactEmailId);
        Glide.with(mContext)
                .load( imageName)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_club_logo)
                .into(imgMain);

        String description = getJsonString(jsonObject, "description");
        String termsNConditions = getJsonString(jsonObject, "termsNConditions");

        expandTextView1.setText(termsNConditions);
        tvClubDesc.setVisibility(View.GONE);
        expandTextView2.setText(description);
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (jsonObject != null) {
                    String image = "";
                    try {
                        try {
                            image = venueImagesList.getJSONObject(0).getString("image");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            image = "";
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        image = "";
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString("jsonObject", jsonObject.toString());
                    bundle.putString("image", image);

                    Opening_Times_Fragment fragment = new Opening_Times_Fragment();
                    MainActivity.loadContentFragmentWithBack(fragment, false, bundle);
                }
            }
        });

        if (venueImagesList != null) {
            myPagerAdapter = new MyPagerAdapter(mActivity, venueImagesList);
            viewPager.setAdapter(myPagerAdapter);
            if (venueImagesList.length() > 0) {
                imgDefaultImage.setVisibility(View.GONE);
                if (venueImagesList.length() > 1) {
                    indicator.setViewPager(viewPager);
                    ((CirclePageIndicator) indicator).setSnap(true);
                    indicator.setOnPageChangeListener(mPageChangeListener);
                    indicator.setCurrentItem(0);
                    indicator.setVisibility(View.VISIBLE);

                } else {
                    indicator.setVisibility(View.GONE);
                }
            } else {
                imgDefaultImage.setVisibility(View.VISIBLE);
                indicator.setVisibility(View.GONE);
            }
        } else {
            imgDefaultImage.setVisibility(View.VISIBLE);
            indicator.setVisibility(View.GONE);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        if (eventDetailUnderVenueList != null) {
            int size = eventDetailUnderVenueList.length();
            if (size > 3) {
                size = 3;
                rltSeeAll.setVisibility(View.VISIBLE);
            } else {
                rltSeeAll.setVisibility(View.INVISIBLE);
            }
            ClubDetailOfferAdapter clubListAdapter = new ClubDetailOfferAdapter(mContext, eventDetailUnderVenueList, size);
            recyclerViewOffer.setAdapter(clubListAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerViewOffer.setLayoutManager(linearLayoutManager);
            recyclerViewOffer.addItemDecoration(new SimpleDividerItemDecoration(mActivity, 0));
            float density = getResources().getDisplayMetrics().density;
            int viewHeight = 0;
            if (density > 3.0) {
                viewHeight = 460 * size;
            } else if (density == 3.0) {
                viewHeight = 345 * size;
            } else {
                viewHeight = 230 * size;
            }
            recyclerViewOffer.getLayoutParams().height = viewHeight;

            recyclerViewOffer.addOnItemTouchListener(new RecyclerItemClickListener(mActivity, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    MainActivity.hideKeyboard(recyclerViewOffer);
                    JSONObject object;
                    String eventId = "";
                    try {
                        object = eventDetailUnderVenueList.getJSONObject(position);
                        eventId = object.getString("eventId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    OfferDetailFragment fragment = OfferDetailFragment.newInstance(eventId, "");
                    MainActivity.loadContentFragmentWithBack(fragment, false);

                }
            }));
        } else {
            rltSeeAll.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.lnr_row_club, R.id.lnrGetDirection, R.id.rltSeeAll, R.id.tvOffers, R.id.imgFav})
    public void onClick(View view) {
        Fragment fragment;
        String VenueId;
        switch (view.getId()) {
            case R.id.lnr_row_club:
                break;
            case R.id.lnrGetDirection:
                if (jsonObject != null) {
                    String lat = "0", lon = "0", add1 = "";
                    try {
                        lat = jsonObject.getString(Constant.LATITUDE);
                        lon = jsonObject.getString(Constant.LONGITUDE);
                        add1 = jsonObject.getString("address1");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!TextUtils.isEmpty(add1)) {
                        String myAdd = GetAddress(mContext, getLocation.getLatitude(), getLocation.getLongitude());
                        showDirections(mContext, myAdd, add1);
                    }
                }
                break;
            case R.id.rltSeeAll:
                VenueId = getJsonString(jsonObject, "venueId");
                fragment = OurOffersFragment.newInstance(VenueId, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);
                break;
            case R.id.tvOffers:
                VenueId = getJsonString(jsonObject, "venueId");
                fragment = OurOffersFragment.newInstance(VenueId, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);
                break;
            case R.id.imgFav:
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    break;
                }
                String VenueId1 = getJsonString(jsonObject, "venueId");
                if (!isFavourite) {
                    isFavourite = true;
                    String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + VenueId1 + "/" + "user" + "/" + mUserId + "/" + "markasfavouritevenue";
                    CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, favSuccessListener, favErrorListener);
                    VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                } else {
                    dialog_info();
                }
                break;
        }
    }


    private class MyPagerAdapter extends PagerAdapter {

        ImageView imageView;
        Context mContext;
        LayoutInflater inflater;
        JSONArray imageArray;
        List<ImageView> mList;

        public MyPagerAdapter(Context context, JSONArray array) {
            this.mContext = context;
            this.imageArray = array;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mList = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return imageArray.length();
        }

        public ImageView getImageViews(int pos) {
            return mList.get(pos);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = inflater.inflate(R.layout.images_listview, container, false);
            imageView = (ImageView) itemView.findViewById(R.id.imageView1);
            String imagename = "";
            try {
                imagename = imageArray.getJSONObject(position).getString("image");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Glide.with(mContext)
                    .load(imagename)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.mipmap.default_club_logo)
                    .centerCrop()
                    .into(imageView);

            ((ViewPager) container).addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    Response.ErrorListener favErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> favSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());

            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    if (isFavourite) {
                        imgFav.setImageResource(R.mipmap.favourite_fill);
                        Toast.makeText(mContext, "Venue favourited", Toast.LENGTH_SHORT).show();
                    } else {
                        imgFav.setImageResource(R.mipmap.favourite_default);
                        Toast.makeText(mContext, "Venue Unfavourited", Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    protected void dialog_info() {
        final Dialog dialog = new Dialog(mActivity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String VenueId1 = getJsonString(jsonObject, "venueId");
                isFavourite = false;
                String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + VenueId1 + "/" + "user" + "/" + mUserId + "/" + "removeasfavouritevenue";
                CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, favSuccessListener, favErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                dialog.dismiss();
            }
        };
        View.OnClickListener dialogCancel = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(Constant.UNLIKE);
        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
        ((View) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(dialogCancel);
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Yes");
        dialog.show();
    }

}

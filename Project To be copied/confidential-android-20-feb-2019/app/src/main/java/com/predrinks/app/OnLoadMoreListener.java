package com.predrinks.app;

public interface OnLoadMoreListener {
    void onLoadMore();
}

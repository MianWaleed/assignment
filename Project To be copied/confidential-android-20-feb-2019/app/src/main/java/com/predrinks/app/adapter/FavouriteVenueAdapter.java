package com.predrinks.app.adapter;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.fragment.ClubDetailFragment;
import com.predrinks.app.fragment.FavouritesFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FavouriteVenueAdapter extends RecyclerView.Adapter<FavouriteVenueAdapter.MyViewHolder> {


    private static final String TAG = "FavouriteVenueAdapter";
    private final SharedPreferences mPrefs;
    private JSONArray jsonArray;
    Context mContext;
    int mCorrentPos = -1;
    String mUserId = "";
    FavouritesFragment mFavouritesFragment;

    public FavouriteVenueAdapter(Context context, JSONArray moviesList, FavouritesFragment favouritesFragment) {
        this.mContext = context;
        this.jsonArray = moviesList;
        this.mFavouritesFragment = favouritesFragment;
        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        mUserId = mPrefs.getString(Constant.USERID, "");
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.imgMainBg)
        ImageView imgMainBg;
        @Bind(R.id.tvClubTitle)
        TextView tvClubTitle;
        @Bind(R.id.imgFavourite)
        ImageView imgFavourite;
        @Bind(R.id.tvAddress)
        TextView tvAddress;
        @Bind(R.id.lnrBottom)
        LinearLayout lnrBottom;
        @Bind(R.id.lnrMain)
        LinearLayout lnrMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            imgFavourite.setOnClickListener(this);
            lnrMain.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == imgFavourite.getId()) {
                int pos = (int) imgFavourite.getTag();
                mCorrentPos = pos;
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArray.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String venueId = getJsonString(jsonObject, "venueId");
                dialog_info(venueId);
            } else if (view.getId() == lnrMain.getId()) {
                int pos = (int) lnrMain.getTag();
                mCorrentPos = pos;
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArray.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                FavouritesFragment.isVenueVisible = true;
                String venueId = getJsonString(jsonObject, "venueId");
                String title = getJsonString(jsonObject, "mName");
                ClubDetailFragment fragment = ClubDetailFragment.newInstance(venueId, title);
                MainActivity.loadContentFragmentWithBack(fragment, false);
            }
        }
    }

    protected void dialog_info(final String venueId) {
        final Dialog dialog = new Dialog(mContext, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + venueId + "/user/" + mUserId + "/removeasfavouritevenue";
                CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                dialog.dismiss();
            }
        };
        View.OnClickListener dialogCancel = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };
        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(Constant.UNLIKE);
        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
        ((View) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(dialogCancel);
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Yes");
        dialog.show();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_fav_venue, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        JSONObject jsonObject = null;
        try {
            jsonObject = jsonArray.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.lnrMain.setTag(position);
        holder.imgFavourite.setTag(position);
        boolean isFav = true;
        if (isFav) {
            holder.imgFavourite.setImageResource(R.mipmap.favourite_fill);
        } else {
            holder.imgFavourite.setImageResource(R.mipmap.favourites_nav_icon);
        }

        holder.tvClubTitle.setTypeface(MainActivity.getTypeFaceMedium());
        holder.tvAddress.setTypeface(MainActivity.getTypeFaceMedium());

        holder.tvClubTitle.setText(getJsonString(jsonObject, "name"));
        String address = getJsonString(jsonObject, "address1");
        holder.tvAddress.setText(address);

        String imgUrl = "";

        if (TextUtils.isEmpty(getJsonString(jsonObject, "logo"))) {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD_DEFAULT + "images/default_con.png";
        } else {
            imgUrl =  getJsonString(jsonObject, "logo");
        }

        Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_event_venue_image)
                .into(holder.imgMainBg);
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    jsonArray.remove(mCorrentPos);
                    notifyDataSetChanged();
                    mFavouritesFragment.setResponse();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

}

package com.predrinks.app.fragment;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

import butterknife.Bind;
import butterknife.ButterKnife;


public class HomeMapFragment extends BaseFragment implements OnMapReadyCallback {

    private static final String TAG = "HomeMapFragment";


    @Bind(R.id.mapView)
    MapView mMapView;

    private GoogleMap googleMap;

    GetLocation mLocation;
    Activity mActivity;
    Context mContext;
    ProgressDialog pDialog;

    private Marker marker;
    private Hashtable<String, JSONObject> markers;
    String mResponse = "";
    private int ACCESS_FINE_LOCATION = 123;
    int mCurrentPage = 0;
    int mPageSize = 10;

    public HomeMapFragment() {
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = getActivity();
        mLocation = new GetLocation(mActivity);
        markers = new Hashtable<>();
        mResponse = "";

        if (!haveNetworkConnection(mContext)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_map, container, false);
        ButterKnife.bind(this, view);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.setTouchscreenBlocksFocus(false);
        mMapView.setClickable(false);
        mMapView.getMapAsync(this);

        String query = "";
        searchItem(query);

        if (!isLocationEnabled(getActivity())) {
            dialog_info(mActivity, getString(R.string.msg_location_title), getString(R.string.msg_location_message));
        }

        return view;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isReadLocationAllowed()) {
                centerMapOnMyLocation();
                return;
            }
            requestLocationPermission();
        } else {
            centerMapOnMyLocation();
        }
    }

    private boolean isReadLocationAllowed() {
        int result = ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);

        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        return false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestLocationPermission() {

        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == ACCESS_FINE_LOCATION) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLocation.getLocation();
                centerMapOnMyLocation();
            }
        }
    }


    private void searchItem(String query) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(mActivity);
        }
        pDialog.setMessage("Loading...");
        pDialog.show();
      //  String URL = URL_Utils.SEARCH_CRITERIA + "/" + getLoadedData(mContext, Constant.USERID) + "/venue/search/list?SearchCriteria=" + query + "&" + Constant.CURRENTPAGE + "=" + mCurrentPage + "&" + Constant.PAGESIZE + "=" + mPageSize;
        String URL = URL_Utils.SEARCH_CRITERIA + "/" + getLoadedData(mContext, Constant.USERID) + "/venue/search/list";

        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            pDialog.dismiss();
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            pDialog.dismiss();
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    mResponse = response.toString();
                    updateData(mResponse);
                } else {
                    String message = response.getString("message");
                    dialog_info(mActivity, "", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    private void updateData(String response) {
        drawMarkerArea(response);
    }

    JSONArray jsonArray = null;

    private void drawMarkerArea(String response) {
        if (googleMap == null)
            return;

        try {
            JSONObject object = new JSONObject(response);
            jsonArray = object.getJSONArray("venues");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        for (int i = 0; i < jsonArray.length(); i++) {
            String lat = "0", lon = "0", title = "", add1 = "", imagename = "";
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArray.getJSONObject(i);
                lat = jsonObject.getString(Constant.LATITUDE);
                lon = jsonObject.getString(Constant.LONGITUDE);
                title = jsonObject.getString(Constant.NAME);
                add1 = jsonObject.getString("address1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LatLng latLng = null;
            if (TextUtils.isEmpty(lat) || lat.toLowerCase().contains("n") || lat.toLowerCase().contains("s") || lat.toLowerCase().contains("e") || lat.toLowerCase().contains("w")) {
                //latLng = Utils.getLatLngFromName(mContext, add1);
            } else {
                double latitude = Double.parseDouble(lat);
                double longitude = Double.parseDouble(lon);
                latLng = new LatLng(latitude, longitude);
            }

            if (latLng == null)
                continue;

            marker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(title)
                    .snippet(add1)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker1)));
            markers.put(marker.getId(), jsonObject);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(11).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    marker.showInfoWindow();
                }
            });
        }
        googleMap.setOnInfoWindowClickListener(onInfoWindowClickListener);
        centerMapOnMyLocation();
    }

    Marker markerWindow;
    GoogleMap.OnInfoWindowClickListener onInfoWindowClickListener = new GoogleMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker1) {
            markerWindow = marker1;
            JSONObject jsonObject = null;
            if (marker1.getId() != null && markers != null && markers.size() > 0) {
                if (markers.get(marker1.getId()) != null &&
                        markers.get(marker1.getId()) != null) {
                    jsonObject = markers.get(marker1.getId());
                }
            }
            String venueId = getJsonString(jsonObject, "venueId");
            String title = getJsonString(jsonObject, "mName");
            ClubDetailFragment fragment = ClubDetailFragment.newInstance(venueId, title);
            MainActivity.loadContentFragmentWithBack(fragment, false);
        }
    };

    private Location location;
    private LatLng myLocation;

    private void centerMapOnMyLocation() {
        if (googleMap == null)
            return;

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        location = googleMap.getMyLocation();
        if (location != null) {
            myLocation = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 12));
        } else {
            if (mLocation != null) {
                myLocation = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 12));
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        MainActivity.setBackButton(false);
        MainActivity.setVisibleHeaderRightMap(false);
        MainActivity.setHeaderTitle("MAP");
        MainActivity.setVisibleHeaderMapHome(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        VolleySingleton.getInstance().cancelPendingRequests(TAG);
//        MainActivity.setVisibleHeaderMapHome(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        private View view;
        ImageLoader mImageLoader;

        public CustomInfoWindowAdapter() {
            view = mActivity.getLayoutInflater().inflate(R.layout.custom_info_window, null);
            mImageLoader = VolleySingleton.getInstance().getImageLoader();
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (HomeMapFragment.this.marker != null && HomeMapFragment.this.marker.isInfoWindowShown()) {
                HomeMapFragment.this.marker.hideInfoWindow();
                HomeMapFragment.this.marker.showInfoWindow();
            }
            return null;
        }

        String url;

        @Override
        public View getInfoWindow(final Marker marker) {

            HomeMapFragment.this.marker = marker;
            JSONObject jsonObject = null;

            if (marker.getId() != null && markers != null && markers.size() > 0) {
                if (markers.get(marker.getId()) != null &&
                        markers.get(marker.getId()) != null) {
                    jsonObject = markers.get(marker.getId());
                }
            }
            try {
                url = jsonObject.getString("logo");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            final ImageView image = ((ImageView) view.findViewById(R.id.badge));
            if (url != null && !url.equalsIgnoreCase("null") && !url.equalsIgnoreCase("")) {
                image.setVisibility(View.VISIBLE);
                mImageLoader.get( url, ImageLoader.getImageListener(image, 0, 0));
            } else {
                image.setVisibility(View.GONE);
            }

            final String title = marker.getTitle();
            final TextView titleUi = ((TextView) view.findViewById(R.id.title));
            if (title != null) {
                titleUi.setText(title);
            } else {
                titleUi.setText("");
            }

            final String snippet = marker.getSnippet();
            final TextView snippetUi = ((TextView) view
                    .findViewById(R.id.snippet));
            if (snippet != null) {
                snippetUi.setText(snippet);
                snippetUi.setVisibility(View.VISIBLE);

            } else {
                snippetUi.setText("");
                snippetUi.setVisibility(View.GONE);
            }
            return view;
        }
    }

}


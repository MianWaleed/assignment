package com.predrinks.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.predrinks.app.R;
import com.predrinks.app.model.ReviewsModel;
import com.predrinks.app.utils.ExpandableTextView;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    private List<ReviewsModel> pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView customerNameTv;
        private RatingBar ratingBar2;
        private TextView dateTv;
        private LinearLayout linearLayout;
        private ExpandableTextView descTv;
        private TextView expandableText;
        private TextView expandCollapse;

        public MyViewHolder(View view) {
            super(view);

            customerNameTv = (TextView) view.findViewById(R.id.customer_name_tv);
            ratingBar2 = (RatingBar) view.findViewById(R.id.ratingBar2);
            dateTv = (TextView) view.findViewById(R.id.date_tv);
            descTv = (ExpandableTextView) view.findViewById(R.id.desc_tv);
            expandableText = (TextView) view.findViewById(R.id.expandable_text);
            expandCollapse = (TextView) view.findViewById(R.id.expand_collapse);
        }
    }

    public ReviewsAdapter(Context context, List<ReviewsModel> heroModel) {
        this.mContext = context;
        this.pogoList = heroModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_reviews_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ReviewsModel reviewsModel = pogoList.get(position);

        holder.customerNameTv.setText(reviewsModel.getCustomerName());
        holder.descTv.setText(reviewsModel.getDesc());
        holder.dateTv.setText(reviewsModel.getDate());
        holder.ratingBar2.setMax(5);
        holder.ratingBar2.setNumStars(5);
        holder.ratingBar2.setRating(reviewsModel.getRating());
    }

    @Override
    public int getItemCount() {
        return pogoList.size();
    }
}

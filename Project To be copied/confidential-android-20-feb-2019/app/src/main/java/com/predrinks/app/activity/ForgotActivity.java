package com.predrinks.app.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotActivity extends BaseFragmentActivity {

    private static final String TAG = "Forgot Password";
    @Bind(R.id.activity_forgot)
    RelativeLayout activityForgot;
    @Bind(R.id.etEmail)
    EditText etEmail;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        ButterKnife.bind(this);
        mContext = this;
    }

    @OnClick({R.id.imgBack, R.id.llRecoverPassword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.llRecoverPassword:
                String email = etEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Snackbar.make(activityForgot, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
                    break;
                } else {
                    resetPassword();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    ProgressDialog pDialog;
    String personEmail;

    private void resetPassword() {
        if (!haveNetworkConnection(this)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        personEmail = etEmail.getText().toString();

        if (TextUtils.isEmpty(personEmail)) {
            Snackbar.make(activityForgot, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(personEmail).matches()) {
            Snackbar.make(activityForgot, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put(URL_Utils.PARAM_EMAIL_ID, personEmail);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        String URL = URL_Utils.RESETPASSWORD + "?" + URL_Utils.PARAM_EMAIL_ID + "=" + personEmail;

        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }


    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            pDialog.dismiss();
            try {
                Log.d(TAG, error.toString());
            } catch (RuntimeException e) {
            }
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            pDialog.dismiss();
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    String title = getResources().getString(R.string.app_name);
                    String message = response.getString("message");
                    dialog_info(title, message, true);
                } else {
                    String title = getResources().getString(R.string.app_name);
                    String message = "Error sending email. Please check your configuration and try again.";
                    dialog_info(title, message, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    protected void dialog_info(String title, String message, final boolean isSignUp) {
        final Dialog dialog = new Dialog(ForgotActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isSignUp) {
                    startActivity(new Intent(ForgotActivity.this, LoginActivity.class));
                    finish();
                }

            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}

package com.predrinks.app.widget;

public interface GiftOnClickListener {
    void onGiftClick(String offerTitle, String venueTitle, String myOfferId);
}

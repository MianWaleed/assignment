package com.predrinks.app.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.Confidentials;
import com.predrinks.app.OnLoadMoreListener;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.adapter.OffersAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.widget.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "HomeFragment";

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;
    @Bind(R.id.lnrTop)
    LinearLayout lnrTop;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private String mParam1;
    private String mParam2;

    Activity mActivity;
    Context mContext;
    int mCurrentPage = 1;
    int mPageSize = 10;
    private Parcelable layoutManagerSavedState;
    private SharedPreferences mPrefs;
    private String successResponse = "";
    private String mUserId = "0";

    public HomeFragment() {

    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mActivity = getActivity();
        mContext = getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        successResponse = "";
        mUserId = "0";
        if (isLoggedIn(mActivity)) {
            mUserId = getLoadedData(mContext, Constant.USERID);
        }
        mPrefs = mActivity.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        saveLoadedData(mContext, Constant.EVENT_LIST, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        hideKeyBoardByView(mContext, view);

        MainActivity.setBackButton(false);
        recyclerView.setVerticalScrollBarEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));

        if (!haveNetworkConnection(mContext)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.header_layout_inner.setVisibility(View.VISIBLE);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        MainActivity.setBackButton(false);
        MainActivity.setVisibleHeaderRightMap(false);
        MainActivity.setHeaderTitle("HOME");
        MainActivity.setVisibleHeaderMapHome(true);

        successResponse = getLoadedData(mContext, Constant.EVENT_LIST);
        if (!TextUtils.isEmpty(successResponse)) {
            setAdapterEvents(successResponse);
            //getEventList(mUserId);
        } else {
            getDataFromWeb();
        }
        try {
            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPosition((int) currentVisiblePosition);
            currentVisiblePosition = 0;
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        VolleySingleton.getInstance().cancelPendingRequests(TAG);
        //       MainActivity.setVisibleHeaderMapHome(false);
        try {
            currentVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(true);
        }

        getUserDetail(mContext);
//        Toast.makeText(mActivity, "Refresh", Toast.LENGTH_SHORT).show();

//        getDataFromWeb();
//        getEventList(mUserId);
    }

    private void getDataFromWeb() {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);

        getUserDetail(mContext);
    }

    protected void getUserDetail(final Context context) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());
                getEventList(mUserId);
            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                JSONObject result = null;
                try {
                    result = response.getJSONObject("result");
                    BaseFragment.setQrCode(getActivity(), result.getString("qrcode"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                saveLoadedData(context, Constant.USER_DETAIL, result.toString());

                getEventList(mUserId);
            }
        };
        String URL = URL_Utils.USER + "/" + mUserId + "/detail";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, "MyCustomTag");
    }

    private void getEventList(String finalParamUserId) {
        mCurrentPage = 1;
        if (TextUtils.isEmpty(finalParamUserId) || !MainActivity.getLocation.canGetLocation()) {
            finalParamUserId = "0";
        }
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);

                setAdapterEvents("");
                updateUserDetail();
            }
        };

        Response.Listener<JSONObject> successListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                updateUserDetail();
                saveLoadedData(mContext, Constant.EVENT_LIST, response.toString());
                successResponse = response.toString();
                setAdapterEvents(successResponse);
            }
        };
        String URL = URL_Utils.EVENT_LIST + "?" + Constant.CURRENTPAGE + "=" + mCurrentPage + "&" + Constant.PAGESIZE + "=" + mPageSize + "&" + Constant.SORT_KEY + "=" + finalParamUserId;
        Log.d(TAG, "getEventList: url " + URL);
        CustomRequest jsObjRequest = new CustomRequest(URL, null, successListener, errorListener);

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    private void updateUserDetail() {
        MainActivity.getLocation.getLocation();
        boolean isnotification = mPrefs.getBoolean(Constant.NOTIFICATIONS, true);
        updateUserDetail(mContext, mUserId, isnotification, MainActivity.getLocation.getLatitude(), MainActivity.getLocation.getLongitude());
    }

    protected Handler handler;
    JSONArray mJsonArray = null;
    OffersAdapter clubListAdapter;
    long currentVisiblePosition = 0;

    private void setAdapterEvents(String response) {
        Log.d(TAG, "setAdapterEvents: " + response);
        JSONArray jsonArray = null;
        mJsonArray = new JSONArray();
        try {
            JSONObject object = new JSONObject(response);
            mCurrentPage = object.getInt("currentPage");
            JSONArray jArray = object.getJSONArray("result");

            JSONArray pinnedArray = new JSONArray();
            JSONArray otherOffers = new JSONArray();

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jsonObject = jArray.getJSONObject(i);
                if (jsonObject.getBoolean("pinned")) {
                    pinnedArray.put(jsonObject);
                } else {
                    otherOffers.put(jsonObject);
                }
            }

            pinnedArray = getSortedList(pinnedArray);

            OffersAdapter.shuffleJsonArray(otherOffers);

            for (int i = 0; i < otherOffers.length(); i++) {
                JSONObject jsonObject = otherOffers.getJSONObject(i);
                pinnedArray.put(jsonObject);
            }

            jArray = new JSONArray();
            jArray = pinnedArray;

            if (jArray != null) {
                jsonArray = new JSONArray();
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jArray.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
                    Log.d(TAG, "setAdapterEvents: " + totalRemainingOffers);
                    if (totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
                    } else {
                        if (!totalRemainingOffers.equals("0 Remaining")) {
//                            ((OffersAdapter.MyViewHolder) holder).itemView.setVisibility(View.GONE);
                            jsonArray.put(jsonObject);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (lnrEmpty != null) {
            if (jsonArray == null || jsonArray.length() == 0) {
                lnrEmpty.setVisibility(View.VISIBLE);
                lnrTop.setVisibility(View.GONE);
                return;
            } else {
                lnrEmpty.setVisibility(View.GONE);
                lnrTop.setVisibility(View.VISIBLE);
            }
        }

        if (jsonArray == null) {
            return;
        }
        if (recyclerView == null) {
            return;
        }


        boolean isSubscribed = false;
        String userdetail = getLoadedData(mActivity, Constant.USER_DETAIL);
        if (!TextUtils.isEmpty(userdetail)) {
            JSONObject object = null;
            try {
                object = new JSONObject(userdetail);
                isSubscribed = object.getBoolean("isSubscribed");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (isSubscribed) {
                mJsonArray = jsonArray;
            } else {
                int pos = 0;
                mJsonArray.put(new JSONObject());
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (pos == 5) {
                        mJsonArray.put(new JSONObject());
                        pos = 0;
                    }
                    pos += 1;
                    try {
                        mJsonArray.put(jsonArray.get(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (pos == 5) {
                    mJsonArray.put(new JSONObject());
                }
            }
        } else {
            int pos = 0;
            mJsonArray.put(new JSONObject());
            for (int i = 0; i < jsonArray.length(); i++) {
                if (pos == 5) {
                    mJsonArray.put(new JSONObject());
                    pos = 0;
                }
                pos += 1;
                try {
                    mJsonArray.put(jsonArray.get(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (pos == 5) {
                mJsonArray.put(new JSONObject());
            }
        }

        clubListAdapter = new OffersAdapter(mContext, mJsonArray, recyclerView);
        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        //recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(clubListAdapter);
        if (layoutManagerSavedState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(layoutManagerSavedState);
        }
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mActivity, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                JSONObject object = null;
                String eventId = "";
                try {
                    object = mJsonArray.getJSONObject(position);
                    eventId = object.getString("eventId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (object == null || object.length() == 0) {
                    if (!isLoggedIn(mActivity)) {
                        dialog_Login(mActivity);
                        return;
                    }
                    SubscriptionsFragment fragment = SubscriptionsFragment.newInstance("", "");
                    MainActivity.loadContentFragmentWithBack(fragment, false);
                } else {
                    OfferDetailFragment fragment = OfferDetailFragment.newInstance(eventId, "");
                    MainActivity.loadContentFragmentWithBack(fragment, false);
                }
            }
        }));
        clubListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //add null , so the adapter will check view_type and show progress bar at bottom
                if (mJsonArray.length() > 2) {
                    handler.postDelayed(new Runnable() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void run() {
                            //   remove progress item
                            Log.d(TAG, "LoadMore: ");
                            if (!isLoading) {
                                isLoading = true;
                                mJsonArray.put(null);
                                clubListAdapter.notifyItemInserted(mJsonArray.length() - 1);
                            }

                            loadMoreData(mUserId);
                            //clubListAdapter.setLoaded();
                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                    }, 1000);

                }
            }
        });
        handler = new Handler();
    }

    public JSONArray getSortedList(JSONArray array) throws JSONException {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            list.add(array.getJSONObject(i));
        }
        Collections.sort(list, new SortBasedOnMessageId());

        JSONArray resultArray = new JSONArray(list);

        return resultArray;
    }

    public class SortBasedOnMessageId implements Comparator<JSONObject> {
        /*
         * (non-Javadoc)
         *
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         * lhs- 1st message in the form of json object. rhs- 2nd message in the form
         * of json object.
         */
        @Override
        public int compare(JSONObject lhs, JSONObject rhs) {
            try {
                return lhs.getInt("pinned_order") > rhs.getInt("pinned_order") ? 1 : (lhs
                        .getInt("pinned_order") < rhs.getInt("pinned_order") ? -1 : 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return 0;

        }
    }

    boolean isLoading = false;

    private void loadMoreData(String finalParamUserId) {
        if (TextUtils.isEmpty(finalParamUserId) || !MainActivity.getLocation.canGetLocation()) {
            finalParamUserId = "0";
        }
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                clubListAdapter.setLoaded();
            }
        };

        Response.Listener<JSONObject> successListener = new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                String saveResponse = getLoadedData(mContext, Constant.EVENT_LIST);
                JSONArray savejArray = null;
                JSONObject saveObject = null;
                try {
                    saveObject = new JSONObject(saveResponse);
                    savejArray = saveObject.getJSONArray("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray jsonArray = null;
                mJsonArray.remove(mJsonArray.length() - 1);
                clubListAdapter.notifyItemRemoved(mJsonArray.length());
                isLoading = false;
                try {
                    JSONArray jArray = response.getJSONArray("result");
                    if (jArray != null) {
                        jsonArray = new JSONArray();
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = jArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            savejArray.put(jsonObject);
                            String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
                            if (totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
                            } else {
                                jsonArray.put(jsonObject);
                            }
                        }
                        if (response.getInt("currentPage") > mCurrentPage) {
                            saveObject.put("result", savejArray);
                            saveObject.put("currentPage", mCurrentPage);
                            saveLoadedData(mContext, Constant.EVENT_LIST, saveObject.toString());

                            mCurrentPage = response.getInt("currentPage");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                boolean isSubscribed = false;
                String userdetail = getLoadedData(mActivity, Constant.USER_DETAIL);
                if (!TextUtils.isEmpty(userdetail)) {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(userdetail);
                        isSubscribed = object.getBoolean("isSubscribed");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (isSubscribed) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            // mJsonArray = jsonArray;
                            try {
                                mJsonArray.put(jsonArray.get(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        int pos = jsonArray.length() - 1;
                        // mJsonArray.put(new JSONObject());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (pos == 5) {
                                mJsonArray.put(new JSONObject());
                                pos = 0;
                            }
                            pos += 1;
                            try {
                                mJsonArray.put(jsonArray.get(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (pos == 5) {
                            mJsonArray.put(new JSONObject());
                        }
                    }
                } else {
                    int pos = jsonArray.length() - 1;
                    // mJsonArray.put(new JSONObject());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (pos == 5) {
                            mJsonArray.put(new JSONObject());
                            pos = 0;
                        }
                        pos += 1;
                        try {
                            mJsonArray.put(jsonArray.get(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (pos == 5) {
                        mJsonArray.put(new JSONObject());
                    }
                }
                //clubListAdapter.notifyItemInserted(mJsonArray.length()-1);
                clubListAdapter.notifyDataSetChanged();
                clubListAdapter.setLoaded();
            }
        };
        String URL = URL_Utils.EVENT_LIST + "?" + Constant.CURRENTPAGE + "=" + (mCurrentPage + 1) + "&" + Constant.PAGESIZE + "=" + mPageSize + "&" + Constant.SORT_KEY + "=" + finalParamUserId;

        CustomRequest jsObjRequest = new CustomRequest(URL, null, successListener, errorListener);

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }
}

package com.predrinks.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GiftSendModel implements Serializable {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("user")
    @Expose
    private User user;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public class User implements Serializable{
        @SerializedName("UserID")
        @Expose
        private Integer userID;
        @SerializedName("Firstname")
        @Expose
        private String firstname;
        @SerializedName("Lastname")
        @Expose
        private String lastname;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("DOB")
        @Expose
        private String dOB;
        @SerializedName("Gender")
        @Expose
        private String gender;
        @SerializedName("MobileNo")
        @Expose
        private String mobileNo;
        @SerializedName("PostCode")
        @Expose
        private String postCode;
        @SerializedName("ProfilePic")
        @Expose
        private String profilePic;
        @SerializedName("RegistrationMode")
        @Expose
        private String registrationMode;
        @SerializedName("iOSAppVersion")
        @Expose
        private Object iOSAppVersion;
        @SerializedName("AndroidAppVersion")
        @Expose
        private String androidAppVersion;
        @SerializedName("DeviceType")
        @Expose
        private String deviceType;
        @SerializedName("DeviceID")
        @Expose
        private Object deviceID;
        @SerializedName("UDID")
        @Expose
        private String uDID;
        @SerializedName("Latitude")
        @Expose
        private String latitude;
        @SerializedName("Longitude")
        @Expose
        private String longitude;
        @SerializedName("NotificationsRequired")
        @Expose
        private Integer notificationsRequired;
        @SerializedName("AcceptTermsAndConditions")
        @Expose
        private Integer acceptTermsAndConditions;
        @SerializedName("Subscribed")
        @Expose
        private Integer subscribed;
        @SerializedName("SubscriptionAmount")
        @Expose
        private String subscriptionAmount;
        @SerializedName("SubscriptionMode")
        @Expose
        private Object subscriptionMode;
        @SerializedName("SubscriptionPeriod")
        @Expose
        private Integer subscriptionPeriod;
        @SerializedName("SubscriptionStartsOn")
        @Expose
        private Object subscriptionStartsOn;
        @SerializedName("SubscriptionEndsOn")
        @Expose
        private Object subscriptionEndsOn;
        @SerializedName("SubscriptionReason")
        @Expose
        private Object subscriptionReason;
        @SerializedName("Active")
        @Expose
        private Integer active;
        @SerializedName("Deleted")
        @Expose
        private Integer deleted;
        @SerializedName("LastLogin")
        @Expose
        private String lastLogin;
        @SerializedName("DateCreated")
        @Expose
        private String dateCreated;
        @SerializedName("DateUpdated")
        @Expose
        private String dateUpdated;
        @SerializedName("reset_token")
        @Expose
        private Object resetToken;
        @SerializedName("install_id")
        @Expose
        private String installId;
        @SerializedName("info_requested")
        @Expose
        private Integer infoRequested;
        @SerializedName("qrcode")
        @Expose
        private Object qrcode;

        public Integer getUserID() {
            return userID;
        }

        public void setUserID(Integer userID) {
            this.userID = userID;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public String getDOB() {
            return dOB;
        }

        public void setDOB(String dOB) {
            this.dOB = dOB;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getRegistrationMode() {
            return registrationMode;
        }

        public void setRegistrationMode(String registrationMode) {
            this.registrationMode = registrationMode;
        }

        public Object getIOSAppVersion() {
            return iOSAppVersion;
        }

        public void setIOSAppVersion(Object iOSAppVersion) {
            this.iOSAppVersion = iOSAppVersion;
        }

        public String getAndroidAppVersion() {
            return androidAppVersion;
        }

        public void setAndroidAppVersion(String androidAppVersion) {
            this.androidAppVersion = androidAppVersion;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public Object getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(Object deviceID) {
            this.deviceID = deviceID;
        }

        public String getUDID() {
            return uDID;
        }

        public void setUDID(String uDID) {
            this.uDID = uDID;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public Integer getNotificationsRequired() {
            return notificationsRequired;
        }

        public void setNotificationsRequired(Integer notificationsRequired) {
            this.notificationsRequired = notificationsRequired;
        }

        public Integer getAcceptTermsAndConditions() {
            return acceptTermsAndConditions;
        }

        public void setAcceptTermsAndConditions(Integer acceptTermsAndConditions) {
            this.acceptTermsAndConditions = acceptTermsAndConditions;
        }

        public Integer getSubscribed() {
            return subscribed;
        }

        public void setSubscribed(Integer subscribed) {
            this.subscribed = subscribed;
        }

        public String getSubscriptionAmount() {
            return subscriptionAmount;
        }

        public void setSubscriptionAmount(String subscriptionAmount) {
            this.subscriptionAmount = subscriptionAmount;
        }

        public Object getSubscriptionMode() {
            return subscriptionMode;
        }

        public void setSubscriptionMode(Object subscriptionMode) {
            this.subscriptionMode = subscriptionMode;
        }

        public Integer getSubscriptionPeriod() {
            return subscriptionPeriod;
        }

        public void setSubscriptionPeriod(Integer subscriptionPeriod) {
            this.subscriptionPeriod = subscriptionPeriod;
        }

        public Object getSubscriptionStartsOn() {
            return subscriptionStartsOn;
        }

        public void setSubscriptionStartsOn(Object subscriptionStartsOn) {
            this.subscriptionStartsOn = subscriptionStartsOn;
        }

        public Object getSubscriptionEndsOn() {
            return subscriptionEndsOn;
        }

        public void setSubscriptionEndsOn(Object subscriptionEndsOn) {
            this.subscriptionEndsOn = subscriptionEndsOn;
        }

        public Object getSubscriptionReason() {
            return subscriptionReason;
        }

        public void setSubscriptionReason(Object subscriptionReason) {
            this.subscriptionReason = subscriptionReason;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

        public Integer getDeleted() {
            return deleted;
        }

        public void setDeleted(Integer deleted) {
            this.deleted = deleted;
        }

        public String getLastLogin() {
            return lastLogin;
        }

        public void setLastLogin(String lastLogin) {
            this.lastLogin = lastLogin;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getDateUpdated() {
            return dateUpdated;
        }

        public void setDateUpdated(String dateUpdated) {
            this.dateUpdated = dateUpdated;
        }

        public Object getResetToken() {
            return resetToken;
        }

        public void setResetToken(Object resetToken) {
            this.resetToken = resetToken;
        }

        public String getInstallId() {
            return installId;
        }

        public void setInstallId(String installId) {
            this.installId = installId;
        }

        public Integer getInfoRequested() {
            return infoRequested;
        }

        public void setInfoRequested(Integer infoRequested) {
            this.infoRequested = infoRequested;
        }

        public Object getQrcode() {
            return qrcode;
        }

        public void setQrcode(Object qrcode) {
            this.qrcode = qrcode;
        }
    }
}
package com.predrinks.app.fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.adapter.FavouriteOfferAdapter;
import com.predrinks.app.adapter.FavouriteVenueAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.SimpleDividerItemDecoration;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.widget.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.predrinks.app.activity.MainActivity.isTrue;


public class FavouritesFragment extends BaseFragment {
    public static int S_NotifyNumber = 0;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "FavouritesFragment";
    @Bind(R.id.tvVenues)
    TextView tvVenues;
    @Bind(R.id.tabVenues)
    LinearLayout tabVenues;
    @Bind(R.id.tvOffers)
    TextView tvOffers;
    @Bind(R.id.txtOffNum)
    TextView txtOffNum;
    @Bind(R.id.tabOffers)
    LinearLayout tabOffers;
    @Bind(R.id.recycler_view_venue)
    RecyclerView recyclerViewVenue;
    @Bind(R.id.recycler_view_offer)
    RecyclerView recyclerViewOffer;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;
    @Bind(R.id.lnrTab)
    LinearLayout lnrTab;
    @Bind(R.id.tvEmptyText)
    TextView tvEmptyText;
    @Bind(R.id.tvEmptySearch)
    TextView tvEmptySearch;


    private String mParam1;
    private String mParam2;
    Activity mActivity;
    Context mContext;
    Paint mPaint;
    int mCurrentPage = 0;
    int mPageSize = 10;

    private static String EMPTY_MSG_VENUE = "YOU HAVE NOT ADDED YOUR FAVOURITE VENUES YET.\n\n WHY NOT SHOW SOME LOVE?";
    private static String EMPTY_SEARCH_VENUE = "SEARCH VENUES";
    private static String EMPTY_MSG_OFFER = "YOU HAVE NOT ADDED YOUR FAVOURITE VENUES YET.\n\n WHY NOT SHOW SOME LOVE?";
    private static String EMPTY_SEARCH_OFFER = "SEARCH VENUES";

    private String mEventFavResponse;
    private String mVenueFavResponse;
    public static boolean isVenueVisible;
    FavouritesFragment favouritesFragment;

    public FavouritesFragment() {
        // Required empty public constructor
    }

    public static FavouritesFragment newInstance(String param1, String param2) {
        FavouritesFragment fragment = new FavouritesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = this.getActivity();
        favouritesFragment = this;
        isVenueVisible = false;
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);
        ButterKnife.bind(this, view);
        setOffNum(S_NotifyNumber);
        setTabUI(isVenueVisible);

        if(isTrue){
            isVenueVisible = true;
            setTabUI(isVenueVisible);
            if (TextUtils.isEmpty(mVenueFavResponse)) {
                setService(isVenueVisible);
            } else {
                setVenueList(mVenueFavResponse);
            }
        }

        if (TextUtils.isEmpty(mParam1)) {
            setService(isVenueVisible);
        } else {
            if (mParam1.equals("0")) {
                isVenueVisible = true;
            } else {
                isVenueVisible = false;
            }
            setService(isVenueVisible);
        }

        tvEmptySearch.setPaintFlags(mPaint.getFlags());
        if (!TextUtils.isEmpty(mEventFavResponse)) {
            if (isVenueVisible) {
                if (!TextUtils.isEmpty(getLoadedData(mActivity, "mVenueFavResponse"))) {
                    setVenueList(mVenueFavResponse);
                }
            } else {
                if (!TextUtils.isEmpty(getLoadedData(mActivity, "mEventFavResponse"))) {
                    setEventList(mEventFavResponse);
                }
            }
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.header_layout_inner.setVisibility(View.VISIBLE);
        MainActivity.setBackButton(false);
        MainActivity.setVisibleHeaderRightMap(false);
        MainActivity.setHeaderTitle("MY FAVOURITES");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.tabVenues, R.id.tabOffers, R.id.tvEmptySearch})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tabVenues:
                isVenueVisible = true;
                setTabUI(isVenueVisible);
                if (TextUtils.isEmpty(mVenueFavResponse)) {
                    setService(isVenueVisible);
                } else {
                    setVenueList(mVenueFavResponse);
                }
                break;
            case R.id.tabOffers:
                isVenueVisible = false;
                setTabUI(isVenueVisible);
                if (TextUtils.isEmpty(mEventFavResponse)) {
                    setService(isVenueVisible);
                } else {
                    setEventList(mEventFavResponse);
                }
                break;
            case R.id.tvEmptySearch:
                MainActivity.setSearchTabsClick();
                break;
        }
    }

    private void setTabUI(boolean isVenue) {
        if (isVenue) {
            recyclerViewVenue.setVisibility(View.VISIBLE);
            recyclerViewOffer.setVisibility(View.GONE);
            tabVenues.setBackgroundColor(getResources().getColor(R.color.tab_bg));
            tvVenues.setTextColor(getResources().getColor(R.color.white));
            tabOffers.setBackgroundColor(getResources().getColor(R.color.white));
            tvOffers.setTextColor(getResources().getColor(R.color.txt_color_dark_gray));
        } else {
            recyclerViewVenue.setVisibility(View.GONE);
            recyclerViewOffer.setVisibility(View.VISIBLE);
            tabVenues.setBackgroundColor(getResources().getColor(R.color.white));
            tvVenues.setTextColor(getResources().getColor(R.color.txt_color_dark_gray));
            tabOffers.setBackgroundColor(getResources().getColor(R.color.tab_bg));
            tvOffers.setTextColor(getResources().getColor(R.color.white));
        }
    }

    public void setResponse() {
        mEventFavResponse = "";
        mVenueFavResponse = "";
        setService(isVenueVisible);
    }

    public void setOffNum(int notiNumber) {
        if (notiNumber < 1) {
            txtOffNum.setText("" + notiNumber);
            txtOffNum.setVisibility(View.GONE);
        } else {
            txtOffNum.setText("" + notiNumber);
            txtOffNum.setVisibility(View.VISIBLE);
        }
    }

    private void setService(boolean isVenue) {
        if (!isVenue) {
            if (tvEmptyText != null) {
                tvEmptyText.setText(EMPTY_MSG_OFFER);
                tvEmptySearch.setText(EMPTY_SEARCH_OFFER);
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerViewVenue.setVisibility(View.GONE);
                recyclerViewOffer.setVisibility(View.GONE);
            }
            String URL = URL_Utils.USER + "/" + getLoadedData(mContext, Constant.USERID) + "/event/favourite/list"+ "?" + Constant.CURRENTPAGE + "=" + mCurrentPage + "&" + Constant.PAGESIZE + "=" + mPageSize;
            CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListenerEvent, ErrorListenerEvent);
            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
        } else {
            if (tvEmptyText != null) {
                tvEmptyText.setText(EMPTY_MSG_VENUE);
                tvEmptySearch.setText(EMPTY_SEARCH_VENUE);
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerViewVenue.setVisibility(View.GONE);
                recyclerViewOffer.setVisibility(View.GONE);
            }

            String URL = URL_Utils.USER + "/" + getLoadedData(mContext, Constant.USERID) + "/venue/favourite/list"+ "?" + Constant.CURRENTPAGE + "=" + mCurrentPage + "&" + Constant.PAGESIZE + "=" + mPageSize;
            CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListenerVenue, ErrorListenerVenue);
            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
        }
    }

    JSONArray jsonArrayVenue;
    FavouriteVenueAdapter favouriteVenueAdapter;

    private void setVenueList(String response) {
        if (recyclerViewVenue == null)
            return;
        recyclerViewVenue.setVerticalScrollBarEnabled(false);
        try {
            JSONObject object = new JSONObject(response);
            jsonArrayVenue = object.getJSONArray("result");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonArrayVenue != null && jsonArrayVenue.length() > 0) {
            favouriteVenueAdapter = new FavouriteVenueAdapter(mContext, jsonArrayVenue, favouritesFragment);
            lnrEmpty.setVisibility(View.GONE);
            recyclerViewVenue.setVisibility(View.VISIBLE);
            recyclerViewOffer.setVisibility(View.GONE);
            recyclerViewVenue.setAdapter(favouriteVenueAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerViewVenue.setLayoutManager(linearLayoutManager);
        }
        tvEmptyText.setText(EMPTY_MSG_VENUE);
        tvEmptySearch.setText(EMPTY_SEARCH_VENUE);
        if (jsonArrayVenue.length() == 0) {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerViewVenue.setVisibility(View.GONE);
            recyclerViewOffer.setVisibility(View.GONE);

        } else {
            lnrEmpty.setVisibility(View.GONE);
            recyclerViewVenue.setVisibility(View.VISIBLE);
            recyclerViewOffer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        VolleySingleton.getInstance().cancelPendingRequests(TAG);
    }

    FavouriteOfferAdapter clubListAdapter;
    JSONArray jsonArray = null;

    private void setEventList(String response) {
        if (recyclerViewOffer == null)
            return;

        recyclerViewOffer.setVerticalScrollBarEnabled(false);
        try {
            JSONObject object = new JSONObject(response);
            JSONArray jArray = object.getJSONArray("result");
            if (jArray != null) {
                jsonArray = new JSONArray();
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jArray.getJSONObject(i);
                        String totalRemainingOffers = jsonObject.getString("totalRemainingOffers");

                        if(!totalRemainingOffers.equals("0 Remaining")){
                            jsonArray.put(jsonObject);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonArray != null && jsonArray.length() > 0) {
            clubListAdapter = new FavouriteOfferAdapter(mContext, jsonArray, true);
            lnrEmpty.setVisibility(View.GONE);
            recyclerViewVenue.setVisibility(View.GONE);
            recyclerViewOffer.setVisibility(View.VISIBLE);
            recyclerViewOffer.setAdapter(clubListAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
                @Override
                public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
                    super.onLayoutChildren(recycler, state);
                    //TODO if the items are filtered, considered hiding the fast scroller here
                    final int firstVisibleItemPosition = findFirstVisibleItemPosition();
                    if (firstVisibleItemPosition != 0) {
                        if (firstVisibleItemPosition == -1)
                            return;
                    }
                }
            };
            recyclerViewOffer.setLayoutManager(linearLayoutManager);
            recyclerViewOffer.addItemDecoration(new SimpleDividerItemDecoration(mActivity, 0));

            recyclerViewOffer.addOnItemTouchListener(new RecyclerItemClickListener(mActivity, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    MainActivity.hideKeyboard(recyclerViewOffer);

                    clubListAdapter.setSelection(position);
                    isVenueVisible = false;
                    JSONObject object;
                    String eventId = "";
                    try {
                        object = jsonArray.getJSONObject(position);
                        eventId = object.getString("eventId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    OfferDetailFragment fragment = OfferDetailFragment.newInstance(eventId, "");
                    MainActivity.loadContentFragmentWithBack(fragment, false);

                }
            }));
        }
        tvEmptyText.setText(EMPTY_MSG_OFFER);
        tvEmptySearch.setText(EMPTY_SEARCH_OFFER);
        S_NotifyNumber = jsonArray.length();
        setOffNum(S_NotifyNumber);
        if (jsonArray.length() == 0) {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerViewVenue.setVisibility(View.GONE);
            recyclerViewOffer.setVisibility(View.GONE);
        } else {
            lnrEmpty.setVisibility(View.GONE);
            recyclerViewVenue.setVisibility(View.GONE);
            recyclerViewOffer.setVisibility(View.VISIBLE);
        }
    }

    Response.ErrorListener ErrorListenerEvent = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());

            if (lnrEmpty != null) {
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerViewVenue.setVisibility(View.GONE);
                recyclerViewOffer.setVisibility(View.GONE);
                tvEmptyText.setText(EMPTY_MSG_OFFER);
                tvEmptySearch.setText(EMPTY_SEARCH_OFFER);
            }
        }
    };

    Response.Listener<JSONObject> SuccessListenerEvent = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    setEventList(response.toString());
                    mEventFavResponse = response.toString();
                    saveLoadedData(mContext, "mEventFavResponse", mEventFavResponse);

                } else if (success.equals("2")) {
                    setEventList(response.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Response.ErrorListener ErrorListenerVenue = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
            if (lnrEmpty != null) {
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerViewVenue.setVisibility(View.GONE);
                recyclerViewOffer.setVisibility(View.GONE);
                tvEmptyText.setText(EMPTY_MSG_VENUE);
                tvEmptySearch.setText(EMPTY_SEARCH_VENUE);
            }
        }
    };

    Response.Listener<JSONObject> SuccessListenerVenue = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    setVenueList(response.toString());
                    mVenueFavResponse = response.toString();
                    saveLoadedData(mContext, "mVenueFavResponse", mVenueFavResponse);
                } else if (success.equals("2")) {
                    setVenueList(response.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}

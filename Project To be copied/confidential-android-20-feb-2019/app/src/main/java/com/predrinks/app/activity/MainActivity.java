package com.predrinks.app.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kumulos.android.Kumulos;
import com.kumulos.android.PushMessage;
import com.predrinks.app.Confidentials;
import com.predrinks.app.Controller;
import com.predrinks.app.R;
import com.predrinks.app.activity.gift.ShareInAppActivity;
import com.predrinks.app.bus.MessageEvent;
import com.predrinks.app.fragment.ClubDetailFragment;
import com.predrinks.app.fragment.ConfidentialsPassFragment;
import com.predrinks.app.fragment.FavouritesFragment;
import com.predrinks.app.fragment.HomeFragment;
import com.predrinks.app.fragment.HomeMapFragment;
import com.predrinks.app.fragment.MoreFragment;
import com.predrinks.app.fragment.MyVouchersFragment;
import com.predrinks.app.fragment.OfferDetailFragment;
import com.predrinks.app.fragment.SearchFragment;
import com.predrinks.app.fragment.SearchMapFragment;
import com.predrinks.app.fragment.SubscriptionsFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.URL_Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.predrinks.app.Confidentials.NAME_PREFERENCE;
import static com.predrinks.app.adapter.FavouriteOfferAdapter.selection;

public class MainActivity extends BaseFragmentActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private static Typeface fromAsset;
    public static int ITEM_ADD_TO_CART = 0;

    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.new_notifications)
    TextView newNotifications;
    @Bind(R.id.llHeaderRight)
    RelativeLayout llHeaderRight;
    @Bind(R.id.imgHome)
    ImageView imgHome;
    @Bind(R.id.tabHome)
    View tabHome;
    @Bind(R.id.llMainHeader)
    LinearLayout llMainHeader;

    @Bind(R.id.llHome)
    LinearLayout llHome;
    @Bind(R.id.imgSearch)
    ImageView imgSearch;
    @Bind(R.id.tabSearch)
    View tabSearch;
    LinearLayout llSearch;
    @Bind(R.id.tabVoucher)
    View tabVoucher;
    @Bind(R.id.llVoucher)
    LinearLayout llVoucher;
    @Bind(R.id.txt_Fav)
    TextView txtFav;
    @Bind(R.id.tabFavourites)
    View tabFavourites;
    @Bind(R.id.llFavourites)
    LinearLayout llFavourites;
    @Bind(R.id.tabMore)
    View tabMore;
    @Bind(R.id.llMore)
    LinearLayout llMore;
    @Bind(R.id.content_frame)
    FrameLayout contentFrame;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.imgBack)
    ImageView imgBack;
    public static LinearLayout header_layout_inner;

    public static FragmentManager manager;
    SharedPreferences mPrefs;
    static Context mContext;
    FragmentActivity mActivity;
    @Bind(R.id.tvTitleMap)
    TextView tvTitleMap;
    @Bind(R.id.tvTitleMapHome)
    TextView tvTitleMapHome;
    @Bind(R.id.rltHeaderRightMap)
    LinearLayout rltHeaderRightMap;
    @Bind(R.id.lnrHeaderMapHome)
    LinearLayout lnrHeaderMapHome;

    public static boolean isMapClick = false;
    public static boolean isMapHomeClick = false;
    public static boolean isPaymentSuccess = false;

    public static Typeface REGULAR, BOLD, THIN, MEDIUM, LIGHT;
    boolean isOncreateCalled = false;
    public static GetLocation getLocation;
    String longitude = "0.0";
    String latitude = "0.0";
    private FirebaseAnalytics mFirebaseAnalytics;

    public static boolean isTrue = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        startActivity(new Intent(this, ShareInAppActivity.class));

//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mActivity = this;
        mContext = this;
        isOncreateCalled = true;
        getLocation = new GetLocation(mActivity);
        View headerView = LayoutInflater.from(this).inflate(R.layout.header, null);
        ButterKnife.bind(llMainHeader, headerView);
        ITEM_ADD_TO_CART = 0;
        saveLoadedData(Constant.EVENT_LIST, "");

        header_layout_inner = findViewById(R.id.mainmenu);

        mPrefs = getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isnotification = mPrefs.getBoolean(Constant.NOTIFICATIONS, true);
        setBoolean(isnotification, Constant.NOTIFICATIONS);
        isMapClick = false;
        isMapHomeClick = false;
        isPaymentSuccess = false;
        llSearch = findViewById(R.id.llSearch);
        llSearch.setOnClickListener(this);

        REGULAR = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        MEDIUM = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        BOLD = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        THIN = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
        LIGHT = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        manager = getFragmentManager();

        initTabs();

        if (savedInstanceState == null) {
            lnrHeaderMapHome.setVisibility(View.VISIBLE);
            HomeFragment fragment = new HomeFragment();
            loadContentFragment(fragment, (Bundle) null);
        }

        setBackWeak(imgBack);
        tvTitleMap.setText("Map");
        tvTitleMapHome.setText("Map");

        tvTitleHeader.setFocusable(true);
        tvTitleHeader.setSelected(true);
        setHeaderTitle(tvTitleHeader);
        setHeaderNotification(newNotifications);
        setHeaderRightMap(rltHeaderRightMap);
        setHeaderMapHome(lnrHeaderMapHome);
        setSearchTabs(llSearch);

        setBackButton(false);
        setVisibleHeaderRightMap(false);
        selection = new HashSet<>();
        rltHeaderRightMap.setVisibility(View.GONE);
        lnrHeaderMapHome.setVisibility(View.VISIBLE);

        initFont();
        String token = mPrefs.getString(Confidentials.REG_ID, "");
        Log.e(TAG + " Token :: ", token);

        if (!haveNetworkConnection()) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }

        handleIntent(getIntent());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isReadLocationAllowed()) {
                requestLocationPermission();
            }
        }


        if (getIntent().getExtras() != null) {
            if (getIntent().hasExtra("whereTo")) {
                Log.d(TAG, "onCreate: " + getIntent().getStringExtra("whereTo"));
                if (getIntent().getStringExtra("whereTo").equals("mypurchases")) {
                    isMapClick = false;
                    isMapHomeClick = false;
                    setBackButton(false);
                    setHeaderTitle("MY PURCHASES");
                    tabHome.setBackgroundColor(Color.TRANSPARENT);
                    tabSearch.setBackgroundColor(Color.TRANSPARENT);
                    tabVoucher.setBackgroundColor(Color.WHITE);
                    tabFavourites.setBackgroundColor(Color.TRANSPARENT);
                    tabMore.setBackgroundColor(Color.TRANSPARENT);
                    Fragment fragment = new MyVouchersFragment();
                    loadContentFragment(fragment, (Bundle) null);
                } else if (getIntent().getStringExtra("whereTo").equals("favouritevenues")) {
                    setBackButton(false);
                    isMapClick = false;
                    isTrue = true;
                    isMapHomeClick = false;
                    setHeaderTitle("MY FAVOURITES");
                    tabHome.setBackgroundColor(Color.TRANSPARENT);
                    tabSearch.setBackgroundColor(Color.TRANSPARENT);
                    tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                    tabFavourites.setBackgroundColor(Color.WHITE);
                    tabMore.setBackgroundColor(Color.TRANSPARENT);
                    Fragment fragment = new FavouritesFragment();
                    loadContentFragment(fragment, (Bundle) null);
                } else if (getIntent().getStringExtra("whereTo").equals("favouriteevents")) {
                    setBackButton(false);
                    isMapClick = false;
                    isMapHomeClick = false;
                    setHeaderTitle("MY FAVOURITES");
                    tabHome.setBackgroundColor(Color.TRANSPARENT);
                    tabSearch.setBackgroundColor(Color.TRANSPARENT);
                    tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                    tabFavourites.setBackgroundColor(Color.WHITE);
                    tabMore.setBackgroundColor(Color.TRANSPARENT);
                    Fragment fragment = new FavouritesFragment();
                    loadContentFragment(fragment, (Bundle) null);
                } else if (getIntent().getStringExtra("whereTo").equals("emailaddress")) {
                    final Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    if (getIntent().hasExtra("email"))
                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getIntent().getStringExtra("email")});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "From Notification");
                    intent.putExtra(Intent.EXTRA_TEXT, "Details weren't given");

                    final PackageManager pm = getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
                    ResolveInfo best = null;
                    for (final ResolveInfo info : matches)
                        if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                            best = info;
                    if (best != null)
                        intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                    startActivityForResult(intent, 1);
                } else if (getIntent().getStringExtra("whereTo").equals("details")) {
                    Intent intent = new Intent(this, OfferDetailActivity.class);
                    intent.putExtra("eventId", getIntent().getStringExtra("eventId"));
                    startActivity(intent);
                }
            }
        }
        loadFromNotification();

        new Handler().postDelayed(this::sendInstallId, 5000);
    }

    private void sendInstallId() {

        String URL = URL_Utils.USER + "/" + getLoadedData(Constant.USERID) + "/update/install-id";
        Map<String, String> params = new HashMap<String, String>();
        params.put("install_id", Kumulos.getCurrentUserIdentifier(this));

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, params,
                response -> Log.d(TAG, "sendInstallId onResponse: " + response),
                error -> Log.d(TAG, "sendInstallId onErrorResponse: " + error));

        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void showChangePasswordDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.change_password_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setCancelable(false);

        dialog.show();
    }

    private void loadFromNotification() {
        if (getIntent().hasExtra("noti")) {
            PushMessage pushMessage = getIntent().getParcelableExtra("noti");
            JSONObject jsonObject = pushMessage.getData();

            try {
                if (jsonObject.getString("type").equals("appPage")) {
                    if (jsonObject.getString("appPage").equals("Venue")) {
                        Intent intent = new Intent(this, OfferDetailActivity.class);
                        intent.putExtra("eventId", jsonObject.getString("eventId"));
                        startActivity(intent);
                    } else if (jsonObject.getString("appPage").equals("MyPurchases")) {
                        isMapClick = false;
                        isMapHomeClick = false;
                        setBackButton(false);
                        setHeaderTitle("MY PURCHASES");
                        tabHome.setBackgroundColor(Color.TRANSPARENT);
                        tabSearch.setBackgroundColor(Color.TRANSPARENT);
                        tabVoucher.setBackgroundColor(Color.WHITE);
                        tabFavourites.setBackgroundColor(Color.TRANSPARENT);
                        tabMore.setBackgroundColor(Color.TRANSPARENT);
                        Fragment fragment = new MyVouchersFragment();
                        loadContentFragment(fragment, (Bundle) null);
                    } else if (jsonObject.getString("appPage").equals("FavouriteEvents")) {
                        setBackButton(false);
                        isMapClick = false;
                        isMapHomeClick = false;
                        setHeaderTitle("MY FAVOURITES");
                        tabHome.setBackgroundColor(Color.TRANSPARENT);
                        tabSearch.setBackgroundColor(Color.TRANSPARENT);
                        tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                        tabFavourites.setBackgroundColor(Color.WHITE);
                        tabMore.setBackgroundColor(Color.TRANSPARENT);
                        Fragment fragment = new FavouritesFragment();
                        loadContentFragment(fragment, (Bundle) null);
                    }
                } else if (jsonObject.getString("type").equals("Email")) {
                    sendEmail(jsonObject.getString("email"));
                    Log.d(TAG, "loadFromNotification: " + jsonObject.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendEmail(String email) {
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, "From Notification");
        intent.putExtra(Intent.EXTRA_TEXT, "Details weren't given");

        final PackageManager pm = getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
            llVoucher.performClick();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocation.getLocation();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setBackButton(false);
        if (isMapClick) {
            tvTitleMap.setText("List");
        } else {
            tvTitleMap.setText("Map");
        }
        if (isMapHomeClick) {
            tvTitleMapHome.setText("List");
        } else {
            tvTitleMapHome.setText("Map");
        }

        if (!TextUtils.isEmpty(getLoadedData("numberOfAddToCart"))) {
            ITEM_ADD_TO_CART = Integer.parseInt(getLoadedData("numberOfAddToCart"));
        } else {
            saveLoadedData("numberOfAddToCart", "" + ITEM_ADD_TO_CART);
        }

        setHeaderNotificationNumber(ITEM_ADD_TO_CART);

        if (isPaymentSuccess)
            llVoucher.performClick();

        if (isOncreateCalled) {
            if (getIntent().getExtras() != null) {
                for (String key : getIntent().getExtras().keySet()) {
                    Object value = getIntent().getExtras().get(key);
                    Log.d(TAG, "Key: " + key + " Value: " + value);
                }
                if (getIntent().getExtras().containsKey("data")) {
                    final String data = getIntent().getExtras().getString("data");
                    if (!TextUtils.isEmpty(data)) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getNotificationData(data, false);
                            }
                        }, 1000);

                    }
                }
            }
        }
        registerReceiver(mNotificationReceiver, new IntentFilter("some_custom_id"));
        checkAppVersion(mActivity);

        String mUserId = "0";
        if (isLoggedIn()) {
            mUserId = getLoadedData(Constant.USERID);
        }
        try {
            long expTime = mPrefs.getLong("alarmExpireTime", 0);
            if (expTime > 0) {
                if (System.currentTimeMillis() >= expTime)
                    removeCartItems(mUserId);
            }

        } catch (NumberFormatException e) {

        }


    }

    @Override
    protected void onPause() {

        super.onPause();
        isOncreateCalled = false;
    }

    Handler handler = new Handler();

    boolean mIsnotiReceive = false;

    private void getNotificationData(String data, boolean isnotiReceive) {
        final Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                isOncreateCalled = false;
            }
        };

        final Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                isOncreateCalled = false;
                try {
                    String success = response.getString("status");
                    if (success.equals("1")) {
                        JSONObject notification = response.getJSONObject("notification");
                        if (notification != null) {
                            if (mIsnotiReceive) {
                                dialog_yes_no(notification);
                            } else {
                                try {
                                    title = notification.getString("title");
                                    content = notification.getString("content");
                                    actionName = notification.getString("actionName");
                                    Log.e("notification :: ", notification.toString());
                                } catch (JSONException w) {
                                    w.printStackTrace();
                                }
                                String actionLink = "", moduleID = "", action = "", actionName = "";
                                try {
                                    action = notification.getString("action");
                                    if (action.equals("Email")) {
                                        actionLink = notification.getString("actionLink");
                                        actionName = notification.getString("actionName");
                                        onSendMail2(actionLink, actionName);

                                    } else if (action.equals("WebsiteAddress")) {
                                        actionLink = notification.getString("actionLink");
                                        String link = "";
                                        if (!actionLink.contains("http")) {
                                            link = "http://";
                                        }
                                        link = link + actionLink;
                                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

                                    } else if (action.equals("AppPage")) {
                                        moduleID = notification.getString("moduleID");
                                        if (moduleID.equals("-1") || moduleID.equals("1")) {
                                            llHome.performClick();
                                        } else if (moduleID.equals("-2") || moduleID.equals("2")) {
                                            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            setBackButton(false);
                                            setHeaderTitle("MY FAVOURITES");
                                            tabHome.setBackgroundColor(Color.TRANSPARENT);
                                            tabSearch.setBackgroundColor(Color.TRANSPARENT);
                                            tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                                            tabFavourites.setBackgroundColor(Color.WHITE);
                                            tabMore.setBackgroundColor(Color.TRANSPARENT);
                                            Fragment fragment = FavouritesFragment.newInstance("0", "");
                                            loadContentFragment(fragment);
                                        } else if (moduleID.equals("-3") || moduleID.equals("3")) {
                                            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            setBackButton(false);
                                            setHeaderTitle("MY FAVOURITES");
                                            tabHome.setBackgroundColor(Color.TRANSPARENT);
                                            tabSearch.setBackgroundColor(Color.TRANSPARENT);
                                            tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                                            tabFavourites.setBackgroundColor(Color.WHITE);
                                            tabMore.setBackgroundColor(Color.TRANSPARENT);
                                            Fragment fragment = FavouritesFragment.newInstance("1", "");
                                            loadContentFragment(fragment);
                                        } else if (moduleID.equals("-4") || moduleID.equals("4")) {
                                            llVoucher.performClick();
                                        }

                                    } else if (action.equals("Venue")) {
                                        moduleID = notification.getString("moduleID");
                                        ClubDetailFragment fragment = ClubDetailFragment.newInstance(moduleID, "");
                                        MainActivity.loadContentFragmentWithBack(fragment, false);
                                    } else if (action.equals("Event")) {
                                        moduleID = notification.getString("moduleID");
                                        OfferDetailFragment fragment = OfferDetailFragment.newInstance(moduleID, "");
                                        MainActivity.loadContentFragmentWithBack(fragment, false);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        mIsnotiReceive = isnotiReceive;
        JSONObject mainObject = null;
        String type = "", id = "", title = "";
        try {
            mainObject = new JSONObject(data);
            JSONObject body = new JSONObject(mainObject.getString("body"));
            type = mainObject.getString("notiType");
            title = mainObject.getString("title");
            id = body.getString("ID");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (type.equals("GeoMessage")) {
            final String finalId = id;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String URL = URL_Utils.NOTIFICATION + "/" + finalId;
                    CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);

                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                }
            }, 500);


        } else if (type.equals("EventNoti")) {
            if (!mIsnotiReceive) {
                OfferDetailFragment fragment = OfferDetailFragment.newInstance(id, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);
            } else {
                dialogEventDetail(id, title);
            }

        }
    }

    private void initTabs() {
        tabHome.setBackgroundColor(Color.WHITE);
        tabSearch.setBackgroundColor(Color.TRANSPARENT);
        tabVoucher.setBackgroundColor(Color.TRANSPARENT);
        tabFavourites.setBackgroundColor(Color.TRANSPARENT);
        tabMore.setBackgroundColor(Color.TRANSPARENT);

    }


    public void setTvTitleHeader(String title) {
        tvTitleHeader.setText(title);
    }

    @OnClick({R.id.llHeaderRight, R.id.confidentials_pass_iv, R.id.llHome, R.id.llSearch, R.id.llVoucher, R.id.llFavourites, R.id.llMore, R.id.imgBack, R.id.rltHeaderRightMap, R.id.lnrHeaderMapHome})
    public void onClick(View view) {
        Fragment fragment;
        isPaymentSuccess = false;
        rltHeaderRightMap.setVisibility(View.GONE);
        lnrHeaderMapHome.setVisibility(View.GONE);
        onHideKeyBoard(this);

        switch (view.getId()) {
            case R.id.confidentials_pass_iv: {
//                showConfidentialsPass();
                startActivity(new Intent(this, HeroOffersActivity.class));
                break;
            }
            case R.id.llHeaderRight:
                if (!isLoggedIn()) {
                    dialog_Login(mActivity);
                } else {
                    Intent intent = new Intent(this, CartMainActivity.class);
                    startActivityForResult(intent, 101);
                }
                break;
            case R.id.llHome:
                isMapClick = false;
                isMapHomeClick = false;
                lnrHeaderMapHome.setVisibility(View.VISIBLE);
                //  manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                tvTitleMapHome.setText("Map");
                setBackButton(false);
                setHeaderTitle("HOME");

                tabHome.setBackgroundColor(Color.WHITE);
                tabSearch.setBackgroundColor(Color.TRANSPARENT);
                tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                tabFavourites.setBackgroundColor(Color.TRANSPARENT);
                tabMore.setBackgroundColor(Color.TRANSPARENT);
                fragment = new HomeFragment();
                loadContentFragment(fragment, (Bundle) null);

                break;
            case R.id.llSearch:
                //txtFav.setText("6");
                rltHeaderRightMap.setVisibility(View.VISIBLE);
                //   manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                setBackButton(false);
                tvTitleMap.setText("Map");
                isMapClick = false;
                isMapHomeClick = false;
                setHeaderTitle("SEARCH");
                tabHome.setBackgroundColor(Color.TRANSPARENT);
                tabSearch.setBackgroundColor(Color.WHITE);
                tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                tabFavourites.setBackgroundColor(Color.TRANSPARENT);
                tabMore.setBackgroundColor(Color.TRANSPARENT);
                fragment = new SearchFragment();
                loadContentFragment(fragment, (Bundle) null);
                break;
            case R.id.llVoucher:
                isMapClick = false;
                isMapHomeClick = false;
                //  manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                setBackButton(false);
                setHeaderTitle("MY PURCHASES");
                tabHome.setBackgroundColor(Color.TRANSPARENT);
                tabSearch.setBackgroundColor(Color.TRANSPARENT);
                tabVoucher.setBackgroundColor(Color.WHITE);
                tabFavourites.setBackgroundColor(Color.TRANSPARENT);
                tabMore.setBackgroundColor(Color.TRANSPARENT);
                fragment = new MyVouchersFragment();
                loadContentFragment(fragment, (Bundle) null);
                break;
            case R.id.llFavourites:
                //  manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                setBackButton(false);
                isMapClick = false;
                isMapHomeClick = false;
                setHeaderTitle("MY FAVOURITES");
                tabHome.setBackgroundColor(Color.TRANSPARENT);
                tabSearch.setBackgroundColor(Color.TRANSPARENT);
                tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                tabFavourites.setBackgroundColor(Color.WHITE);
                tabMore.setBackgroundColor(Color.TRANSPARENT);
                fragment = new FavouritesFragment();
                loadContentFragment(fragment, (Bundle) null);
                break;
            case R.id.llMore:
                isMapClick = false;
                isMapHomeClick = false;
                // manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                setBackButton(false);
                setHeaderTitle("MORE");
                tabHome.setBackgroundColor(Color.TRANSPARENT);
                tabSearch.setBackgroundColor(Color.TRANSPARENT);
                tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                tabFavourites.setBackgroundColor(Color.TRANSPARENT);
                tabMore.setBackgroundColor(Color.WHITE);
                fragment = new MoreFragment();
                loadContentFragment(fragment, (Bundle) null);
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.rltHeaderRightMap:
                rltHeaderRightMap.setVisibility(View.VISIBLE);
                if (!isMapClick) {
                    tvTitleMap.setText("List");
                    isMapClick = true;
                    fragment = new SearchMapFragment();
                    loadContentFragmentWithTransition(fragment, false, (Bundle) null);
                } else {
                    isMapClick = false;
                    tvTitleMap.setText("Map");
                    onBackPressed();
                }
                break;
            case R.id.lnrHeaderMapHome:
                lnrHeaderMapHome.setVisibility(View.VISIBLE);
                if (!isMapHomeClick) {
                    tvTitleMapHome.setText("List");
                    isMapHomeClick = true;
                    fragment = new HomeMapFragment();
                    loadContentFragmentWithTransition(fragment, false, (Bundle) null);
                } else {
                    isMapHomeClick = false;
                    tvTitleMapHome.setText("Map");
                    onBackPressed();
                }
                break;
        }
    }

    private void showConfidentialsPass() {
        FragmentManager fm = getFragmentManager();
        ConfidentialsPassFragment editNameDialogFragment = new ConfidentialsPassFragment();
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    static WeakReference<ImageView> imageViewReference;
    static WeakReference<TextView> headerTitleReference;
    static WeakReference<TextView> headerNotificationsReference;
    static WeakReference<LinearLayout> rltHeaderRightMapReference;
    static WeakReference<LinearLayout> rltHeaderMapHomeReference;
    public static WeakReference<LinearLayout> llSearchReference;

    public static void setSearchTabs(LinearLayout llSearch) {
        llSearchReference = new WeakReference<>(llSearch);
    }

    public static void setSearchTabsClick() {
        llSearchReference.get().performClick();
    }

    public static void setBackWeak(ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
    }

    public static void setBackButton(boolean isVisible) {
        if (isVisible) {
            imageViewReference.get().setVisibility(View.VISIBLE);
        } else {
            imageViewReference.get().setVisibility(View.GONE);
        }
    }


    public static void setHeaderTitle(TextView textView) {
        headerTitleReference = new WeakReference<TextView>(textView);
        headerTitleReference.get().setFocusable(true);
        headerTitleReference.get().setSelected(true);
    }

    public static void setHeaderTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            headerTitleReference.get().setVisibility(View.VISIBLE);
            headerTitleReference.get().setText(title);

            headerTitleReference.get().setFocusable(true);
            headerTitleReference.get().setSelected(true);
        } else {
            headerTitleReference.get().setVisibility(View.GONE);
        }
    }

    public static void setHeaderNotification(TextView textView) {
        headerNotificationsReference = new WeakReference<TextView>(textView);
    }

    public static void setHeaderNotificationNumber(int notificationNumber) {
        try {
            if (notificationNumber == 0) {
                headerNotificationsReference.get().setVisibility(View.GONE);
            } else {
                headerNotificationsReference.get().setText("" + notificationNumber);
                headerNotificationsReference.get().setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static void setHeaderRightMap(LinearLayout rltHeaderRightMap) {
        rltHeaderRightMapReference = new WeakReference<LinearLayout>(rltHeaderRightMap);
    }

    public static void setHeaderMapHome(LinearLayout rltHeaderMapHome) {
        rltHeaderMapHomeReference = new WeakReference<LinearLayout>(rltHeaderMapHome);
    }

    public static void setVisibleHeaderRightMap(boolean isVisible) {
        if (isVisible) {
            rltHeaderRightMapReference.get().setVisibility(View.VISIBLE);
        } else {
            rltHeaderRightMapReference.get().setVisibility(View.GONE);
        }
    }

    public static void setVisibleHeaderMapHome(boolean isVisible) {
        if (isVisible) {
            rltHeaderMapHomeReference.get().setVisibility(View.VISIBLE);
        } else {
            rltHeaderMapHomeReference.get().setVisibility(View.GONE);
        }
    }

    public static void loadContentFragment(Fragment fragment, Bundle... bundles) {
        try {
            String backStateName = fragment.getClass().getName();
            if (bundles != null && bundles.length > 0) {
                // If any data is present
                fragment.setArguments(bundles[0]);
            }
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment, backStateName);
            //ft.addToBackStack(backStateName);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
    }

    public static void loadContentFragmentWithBack(Fragment fragment, boolean flag, Bundle... bundles) {
        try {
            String backStateName = fragment.getClass().getName();
            if (bundles != null && bundles.length > 0) {
                // If any data is present
                fragment.setArguments(bundles[0]);
            }
            FragmentTransaction ft = manager.beginTransaction();
           /* if (flag) {
                ft.add(R.id.content_frame, fragment);
            } else {*/
            ft.replace(R.id.content_frame, fragment, backStateName);
            ft.addToBackStack(backStateName);
            //  }
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

    }


    public static void loadContentFragmentWithTransition(Fragment fragment, boolean flag, Bundle... bundles) {
        try {
            String backStateName = fragment.getClass().getName();
            if (bundles != null && bundles.length > 0) {
                fragment.setArguments(bundles[0]);
            }
            FragmentTransaction ft = manager.beginTransaction();
            if (flag) {
                ft.add(R.id.content_frame, fragment);
            } else {
                //ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
                ft.setCustomAnimations(
                        R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out);
                ft.replace(R.id.content_frame, fragment, backStateName);
                ft.addToBackStack(backStateName);
            }
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
    }


    public static Fragment getVisibleFragment() {
        Fragment f = manager.findFragmentById(R.id.content_frame);
        return f;
    }


    @Override
    public void onBackPressed() {
        manageBack();
    }

    boolean doubleBackToExitPressedOnce = false;

    public void manageBack() {
        onHideKeyBoard(this);
        if (manager.getBackStackEntryCount() > 0) {
            String fragmentClassName = getVisibleFragment().getClass().getName();
            fragmentClassName = fragmentClassName.substring(fragmentClassName.lastIndexOf(".") + 1);
            switch (fragmentClassName) {
                default:
                    // setBackButton(false);
                    manager.popBackStackImmediate();
            }
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
            //finish();
        }

    }

    private void initFont() {
        getTypeFaceMedium();
        getTypeFaceRegular();
        getTypeFaceBold();
        getTypeFaceLight();
    }

    public static Typeface getTypeFaceMedium() {
        if (mContext == null) {
            return fromAsset;
        }
        fromAsset = Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Medium.otf");
        return fromAsset;
    }

    public static Typeface getTypeFaceBold() {
        if (mContext == null) {
            return fromAsset;
        }
        fromAsset = Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Bold.otf");
        return fromAsset;
    }

    public static Typeface getTypeFaceRegular() {
        if (mContext == null) {
            return fromAsset;
        }
        fromAsset = Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Regular.otf");
        return fromAsset;
    }

    public static Typeface getTypeFaceLight() {
        if (mContext == null) {
            return fromAsset;
        }
        fromAsset = Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Light.otf");
        return fromAsset;
    }

    public static Typeface getTypeFaceSemiBold() {
        if (mContext == null) {
            return fromAsset;
        }
        fromAsset = Typeface.createFromAsset(mContext.getAssets(), "fonts/SF-UI-Text-Semibold.otf");
        return fromAsset;
    }

    public static void hideKeyboard(View view) {
        if (view == null)
            return;

        if (mContext == null)
            return;

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }

    public static void onHideKeyBoard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void onSendMail2(String email, String subject) {

        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, "");

        final PackageManager pm = getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        // startActivity(intent);
        mActivity.startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (data != null && data.hasExtra("SearchOffers"))
                MainActivity.setSearchTabsClick();
        }

        if (requestCode == 10001) {
            SubscriptionsFragment fragment = (SubscriptionsFragment) getFragmentManager().findFragmentById(R.id.content_frame);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mNotificationReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    protected BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            if (intent.getExtras() != null) {
                if (intent.getExtras().containsKey("paylod")) {
                    String data = intent.getExtras().getString("paylod");
                    if (!TextUtils.isEmpty(data)) {
                        mIsnotiReceive = false;
                        getNotificationData(data, true);
                    }
                } else if (intent.getExtras().containsKey("cartItemRemove")) {
                    String mUserId = "0";
                    if (isLoggedIn()) {
                        mUserId = getLoadedData(Constant.USERID);
                    }
                    removeCartItems(mUserId);
                }
            }

        }
    };
    String title = "", content = "", actionName = "";

    public void dialog_yes_no(final JSONObject notification) {

        try {
            title = notification.getString("title");
            content = notification.getString("content");
            actionName = notification.getString("actionName");
            Log.e("notification :: ", notification.toString());
        } catch (JSONException w) {
            w.printStackTrace();
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity, R.style.DialogSlideAnim);
                // dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_info_yesno);
                dialog.setCancelable(false);

                View.OnClickListener dialogClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null)
                            dialog.dismiss();

                        String actionLink = "", moduleID = "", action = "", actionName = "";
                        try {
                            action = notification.getString("action");
                            if (action.equals("Email")) {
                                actionLink = notification.getString("actionLink");
                                actionName = notification.getString("actionName");
                                onSendMail2(actionLink, actionName);

                            } else if (action.equals("WebsiteAddress")) {
                                actionLink = notification.getString("actionLink");
                                String link = "";
                                if (!actionLink.contains("http")) {
                                    link = "http://";
                                }
                                link = link + actionLink;
                                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

                            } else if (action.equals("AppPage")) {
                                moduleID = notification.getString("moduleID");
                                if (moduleID.equals("-1") || moduleID.equals("1")) {
                                    llHome.performClick();
                                } else if (moduleID.equals("-2") || moduleID.equals("2")) {
                                    manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    setBackButton(false);
                                    setHeaderTitle("MY FAVOURITES");
                                    tabHome.setBackgroundColor(Color.TRANSPARENT);
                                    tabSearch.setBackgroundColor(Color.TRANSPARENT);
                                    tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                                    tabFavourites.setBackgroundColor(Color.WHITE);
                                    tabMore.setBackgroundColor(Color.TRANSPARENT);
                                    Fragment fragment = FavouritesFragment.newInstance("0", "");
                                    loadContentFragment(fragment);
                                } else if (moduleID.equals("-3") || moduleID.equals("3")) {
                                    manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    setBackButton(false);
                                    setHeaderTitle("MY FAVOURITES");
                                    tabHome.setBackgroundColor(Color.TRANSPARENT);
                                    tabSearch.setBackgroundColor(Color.TRANSPARENT);
                                    tabVoucher.setBackgroundColor(Color.TRANSPARENT);
                                    tabFavourites.setBackgroundColor(Color.WHITE);
                                    tabMore.setBackgroundColor(Color.TRANSPARENT);
                                    Fragment fragment = FavouritesFragment.newInstance("1", "");
                                    loadContentFragment(fragment);
                                } else if (moduleID.equals("-4") || moduleID.equals("4")) {
                                    llVoucher.performClick();
                                }

                            } else if (action.equals("Venue")) {
                                moduleID = notification.getString("moduleID");
                                ClubDetailFragment fragment = ClubDetailFragment.newInstance(moduleID, "");
                                MainActivity.loadContentFragmentWithBack(fragment, false);
                            } else if (action.equals("Event")) {
                                moduleID = notification.getString("moduleID");
                                OfferDetailFragment fragment = OfferDetailFragment.newInstance(moduleID, "");
                                MainActivity.loadContentFragmentWithBack(fragment, false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                View.OnClickListener dialogCancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                };
                ((TextView) dialog.findViewById(R.id.title)).setText(title);
                ((TextView) dialog.findViewById(R.id.message)).setText(content);

                ((Button) dialog.findViewById(R.id.okBtn)).setText(actionName);
                ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
                ((Button) dialog.findViewById(R.id.cancelBtn)).setText("Cancel");
                ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(dialogCancelClick);

                dialog.show();
            }
        });

    }

    public void dialogEventDetail(final String id, final String title) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity, R.style.DialogSlideAnim);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_info_yesno);
                dialog.setCancelable(false);

                View.OnClickListener dialogClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null)
                            dialog.dismiss();

                        OfferDetailFragment fragment = OfferDetailFragment.newInstance(id, "");
                        MainActivity.loadContentFragmentWithBack(fragment, false);
                    }
                };

                View.OnClickListener dialogCancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                };
                ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
                ((TextView) dialog.findViewById(R.id.message)).setText(title);

                ((Button) dialog.findViewById(R.id.okBtn)).setText("Ok");
                ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
                ((Button) dialog.findViewById(R.id.cancelBtn)).setText("Cancel");
                ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(dialogCancelClick);

                dialog.show();
            }
        });


    }


    public static void checkAppVersion(final Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences(NAME_PREFERENCE, MODE_PRIVATE);

        String webVersion = "0.0";
        webVersion = preferences.getString("WebAppVersion", "");
        if (TextUtils.isEmpty(webVersion)) {
            webVersion = "0.0";
        }
        double webAppVersion = Double.parseDouble(webVersion);
        double appVersion = Double.parseDouble(Controller.APP_VERSION_NAME);

        if (webAppVersion > appVersion) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.version_title);
            builder.setMessage(R.string.version_msg);
            builder.setCancelable(false);

            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getApplicationContext().getPackageName())));
                }
            });

            builder.show();
            return;
        }
    }


    //We are calling this method to check the permission status
    private boolean isReadLocationAllowed() {
        //Getting the permission status
        int result = ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    @TargetApi(Build.VERSION_CODES.M)
    private void requestLocationPermission() {

        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == ACCESS_FINE_LOCATION) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    private int ACCESS_FINE_LOCATION = 123;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.isFromTheSupport()) {
            isPaymentSuccess = false;
            rltHeaderRightMap.setVisibility(View.GONE);
            lnrHeaderMapHome.setVisibility(View.GONE);
            onHideKeyBoard(this);
            isMapClick = false;
            isMapHomeClick = false;
            // manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            setBackButton(false);
            setHeaderTitle("MORE");
            tabHome.setBackgroundColor(Color.TRANSPARENT);
            tabSearch.setBackgroundColor(Color.TRANSPARENT);
            tabVoucher.setBackgroundColor(Color.TRANSPARENT);
            tabFavourites.setBackgroundColor(Color.TRANSPARENT);
            tabMore.setBackgroundColor(Color.WHITE);
            MoreFragment fragment = new MoreFragment();
            loadContentFragment(fragment, (Bundle) null);
        }
    }
}

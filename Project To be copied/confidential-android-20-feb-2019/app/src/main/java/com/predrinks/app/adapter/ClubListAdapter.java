package com.predrinks.app.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.Confidentials;
import com.predrinks.app.OnLoadMoreListener;
import com.predrinks.app.R;
import com.predrinks.app.activity.LandingActivity;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.fragment.ClubDetailFragment;
import com.predrinks.app.fragment.OurOffersFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClubListAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;


    private static final String TAG = "ClubListAdapter";
    private final SharedPreferences mPrefs;
    Paint paint;
    private JSONArray clubLists;
    Context mContext;
    String mUserId = "";
    private boolean isFavourite = false;
    ArrayList<Boolean> favList;
    int mCorrentPos = -1;

    public void clearData() {
        if (clubLists.length() > 0) {
            for (int i = 0; i < clubLists.length(); i++) {
                clubLists.remove(i);
            }
        }
    }

    public ClubListAdapter(Context context, JSONArray clubLists, RecyclerView recyclerView) {
        this.mContext = context;
        this.clubLists = clubLists;
        isFavourite = false;
        favList = new ArrayList<>(clubLists.length());
        mCorrentPos = -1;

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

        for (int i = 0; i < clubLists.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = clubLists.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            String isFav = getJsonString(jsonObject, "isFavoriteVenue");
            if (!isFav.equals("false")) {
                favList.add(i, true);
            } else {
                favList.add(i, false);
            }
        }

        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        mUserId = mPrefs.getString(Constant.USERID, "");

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.imgMain)
        ImageView imgMain;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.imgFav)
        ImageView imgFav;
        @Bind(R.id.tvAdd)
        TextView tvAdd;
        @Bind(R.id.tvAdd1)
        TextView tvAdd1;
        @Bind(R.id.tvAdd2)
        TextView tvAdd2;
        @Bind(R.id.tvOffers)
        TextView tvOffers;
        @Bind(R.id.lnr_row_club)
        LinearLayout lnr_row_club;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvName.setTypeface(MainActivity.getTypeFaceRegular());
            tvAdd.setTypeface(MainActivity.getTypeFaceRegular());
            tvAdd1.setTypeface(MainActivity.getTypeFaceRegular());
            tvAdd2.setTypeface(MainActivity.getTypeFaceRegular());

            tvOffers.setOnClickListener(this);
            imgFav.setOnClickListener(this);
            lnr_row_club.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == tvOffers.getId()) {
                int pos = (int) tvOffers.getTag();
                mCorrentPos = pos;
                JSONObject jsonObject = null;
                try {
                    jsonObject = clubLists.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                String VenueId = getJsonString(jsonObject, "venueId");
                OurOffersFragment fragment = OurOffersFragment.newInstance(VenueId, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);

            } else if (v.getId() == imgFav.getId()) {
                boolean isLoggedIn = mPrefs.getBoolean("isLoggedIn", false);
                if (!isLoggedIn) {
                    dialog_Login(mContext);
                }
                int pos = (int) imgFav.getTag();
                mCorrentPos = pos;
                JSONObject jsonObject = null;
                try {
                    jsonObject = clubLists.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                String VenueId = getJsonString(jsonObject, "venueId");
                if (!favList.get(pos)) {
                    isFavourite = true;
                    String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + VenueId + "/" + "user" + "/" + mUserId + "/" + "markasfavouritevenue";
                    CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
                    VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                } else {
                    dialog_info(VenueId);
                }


            } else if (v.getId() == lnr_row_club.getId()) {
                int pos = (int) lnr_row_club.getTag();
                mCorrentPos = pos;
                JSONObject jsonObject = null;
                try {
                    jsonObject = clubLists.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                String venueId = getJsonString(jsonObject, "venueId");
                String title = getJsonString(jsonObject, "mName");
                ClubDetailFragment fragment = ClubDetailFragment.newInstance(venueId, title);
                MainActivity.loadContentFragmentWithBack(fragment, false);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        try {
            return clubLists.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        } catch (JSONException e) {
            e.printStackTrace();
            return VIEW_PROG;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       /* View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_club, parent, false);
        return new MyViewHolder(itemView);*/

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_club, parent, false);
            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyViewHolder) {
            JSONObject jsonObject = null;
            try {
                jsonObject = clubLists.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            String imagename = getJsonString(jsonObject, "logo");
            ((MyViewHolder) holder).tvName.setText(getJsonString(jsonObject, "name"));
            String add2 = getJsonString(jsonObject, "address2");
            String address;
            if (TextUtils.isEmpty(add2)) {
                address = getJsonString(jsonObject, "address1");
            } else {
                address = getJsonString(jsonObject, "address1");
            }

            ((MyViewHolder) holder).tvAdd.setText(" " + address.replace(",", "\n"));
            ((MyViewHolder) holder).tvAdd.setLineSpacing((float) 1.5, (float) 1.0);
            ((MyViewHolder) holder).tvAdd1.setText(getJsonString(jsonObject, "postCode"));
            ((MyViewHolder) holder).tvAdd1.setVisibility(View.GONE);
            ((MyViewHolder) holder).tvAdd2.setVisibility(View.INVISIBLE);
            ((MyViewHolder) holder).tvAdd2.setText(getJsonString(jsonObject, "city"));
            ((MyViewHolder) holder).lnr_row_club.setFocusable(true);

            if (favList.get(position)) {
                ((MyViewHolder) holder).imgFav.setImageResource(R.mipmap.favourite_fill);
            } else {
                ((MyViewHolder) holder).imgFav.setImageResource(R.mipmap.favourite_default);
            }


            ((MyViewHolder) holder).lnr_row_club.setTag(position);
            ((MyViewHolder) holder).imgFav.setTag(position);
            ((MyViewHolder) holder).tvOffers.setTag(position);
            ((MyViewHolder) holder).tvOffers.setPaintFlags(paint.getFlags());
            int activeEventCnt = 0;
            String cnt = getJsonString(jsonObject, "activeEventCnt");
            if (!TextUtils.isEmpty(cnt))
                activeEventCnt = Integer.parseInt(cnt);

            if (activeEventCnt > 0) {
                ((MyViewHolder) holder).tvOffers.setVisibility(View.VISIBLE);
            } else {
                ((MyViewHolder) holder).tvOffers.setVisibility(View.GONE);
            }

            if (TextUtils.isEmpty(imagename)) {
                ((MyViewHolder) holder).imgMain.setImageResource(R.mipmap.default_club_logo);
            } else {
                Glide.with(mContext)
                        .load( imagename)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .error(R.mipmap.default_club_logo)
                        .into(((MyViewHolder) holder).imgMain);
            }

            Log.d(TAG, "onBindViewHolder: " + URL_Utils.URL_IMAGE_DNLOAD + "      " + imagename);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return clubLists.length();
    }


    public ArrayList<Boolean> getFavList() {
        return favList;
    }

    public ArrayList<Boolean> setFavList() {
        return favList;
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        if (jsonObject == null)
            return "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    if (isFavourite) {
                        favList.remove(mCorrentPos);
                        favList.add(mCorrentPos, true);
                        Toast.makeText(mContext, "Venue favourited", Toast.LENGTH_SHORT).show();
                    } else {
                        favList.remove(mCorrentPos);
                        favList.add(mCorrentPos, false);
                        Toast.makeText(mContext, "Venue Unfavourited", Toast.LENGTH_SHORT).show();
                    }
                    notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    protected void dialog_info(final String VenueId) {
        final Dialog dialog = new Dialog(mContext, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFavourite = false;
                String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + VenueId + "/" + "user" + "/" + mUserId + "/" + "removeasfavouritevenue";
                CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                dialog.dismiss();
            }
        };
        View.OnClickListener dialogCancel = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };


        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(Constant.UNLIKE);
        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
        ((View) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(dialogCancel);
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Yes");

        dialog.show();
    }

    protected void dialog_Login(final Context activity) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, LandingActivity.class);
                activity.startActivity(intent);
                dialog.dismiss();
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText("Login to continue");
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Login");

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);
        dialog.show();
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}

package com.predrinks.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.predrinks.app.R;
import com.predrinks.app.profile.UpdateProfileDialogFragment;
import com.predrinks.app.utils.Constant;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LandingActivity extends BaseFragmentActivity {


    @Bind(R.id.view_pager)
    ViewPager viewPager;
    @Bind(R.id.llLogin)
    LinearLayout llLogin;
    @Bind(R.id.llSignUp)
    LinearLayout llSignUp;
    @Bind(R.id.tvSkip)
    TextView tvSkip;
    private MyPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ButterKnife.bind(this);

        adapter = new MyPagerAdapter(this);
        viewPager.setAdapter(adapter);

//        updateProfile();
//        showChangePasswordDialog();
    }

//    private void updateProfile() {
//        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
//        UpdateProfileDialogFragment editNameDialogFragment = new UpdateProfileDialogFragment();
//        editNameDialogFragment.show(fm, "fragment_edit_name");
//    }

    @OnClick({R.id.llLogin, R.id.llSignUp, R.id.tvSkip})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.llLogin:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.llSignUp:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tvSkip:
                saveLoadedData(Constant.USERID, "0");
                intent = new Intent(LandingActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private class MyPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        String[] DescList = new String[]{"THE BEST DEALS\nFOOD, BOOZE, NEWS\nOFFERS & DISCOUNTS"};


        public MyPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return DescList.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.list_item_landing, container, false);

            TextView tvDesc = (TextView) itemView.findViewById(R.id.tvDesc);
            tvDesc.setText(DescList[position]);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

}

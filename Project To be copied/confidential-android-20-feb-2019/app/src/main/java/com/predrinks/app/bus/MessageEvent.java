package com.predrinks.app.bus;

public class MessageEvent {
    boolean isFromTheSupport;

    public MessageEvent() {
    }

    public MessageEvent(boolean isFromTheSupport) {
        this.isFromTheSupport = isFromTheSupport;
    }

    public boolean isFromTheSupport() {
        return isFromTheSupport;
    }

    public void setFromTheSupport(boolean fromTheSupport) {
        isFromTheSupport = fromTheSupport;
    }
}

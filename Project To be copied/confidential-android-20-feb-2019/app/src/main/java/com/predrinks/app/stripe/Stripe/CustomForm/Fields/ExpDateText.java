package com.predrinks.app.stripe.Stripe.CustomForm.Fields;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.Editable;
import android.util.AttributeSet;

import com.predrinks.app.R;
import com.predrinks.app.stripe.Stripe.CustomForm.Internal.CreditCardUtil;
import com.predrinks.app.stripe.StripePayment.Main.StripePaymentActivity;


public class ExpDateText extends CreditEntryFieldBase {

	private String previousString;
	public static boolean isExpDateValid=false;
	
	SharedPreferences SharedPrefExpDate;
	Editor editorExpDate;
	Context context;
	
	public static int expdate;

	public ExpDateText(Context context) {
		super(context);
		this.context = context;
		init();
	}

	public ExpDateText(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}

	public ExpDateText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}

	void init() {
		super.init();
		setHint("MM/YY");
		
		
	}

	/* TextWatcher Implementation Methods */
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		previousString = s.toString();
	}

	public void afterTextChanged(Editable s) {
		SharedPrefExpDate = context.getSharedPreferences("SharedPrefExpDate", Context.MODE_PRIVATE);
		editorExpDate = SharedPrefExpDate.edit();
		String updatedString = s.toString();

		// if delete occurred do not format
		if (updatedString.length() > previousString.length()) {
			formatAndSetText(updatedString);
		}
		
		if(updatedString.length() == 5 && SecurityCodeText.isCvcValid && StripePaymentActivity.postCodeEditText.getText().length() > 0)
		{
			expdate = updatedString.length();
			StripePaymentActivity.btnPayNow.setEnabled(true);
			isExpDateValid = true;
			editorExpDate.putBoolean("PrefExpDate", true);
			editorExpDate.commit();
		}
		else
		{
			expdate = updatedString.length();
			StripePaymentActivity.btnPayNow.setEnabled(true);
			isExpDateValid = false;
			editorExpDate.putBoolean("PrefExpDate", false);
			editorExpDate.commit();
		}
	}

	public void formatAndSetText(String updatedString) {
		this.removeTextChangedListener(this);
		String formatted = CreditCardUtil.formatExpirationDate(updatedString);
		this.setText(formatted);
		this.setSelection(formatted.length());
		this.addTextChangedListener(this);

		if(formatted.length() == 5) {
            setValid(true);
            String remainder = null;
            if(updatedString.startsWith(formatted)) {
                remainder = updatedString.replace(formatted, "");
            }
            delegate.onExpirationDateValid(remainder);
        } else if(formatted.length() < updatedString.length()) {
            setValid(false);
            delegate.onBadInput(this);
        }
	}

	@Override
	public String helperText() {
		return context.getString(R.string.ExpirationDateHelp);
	}
}

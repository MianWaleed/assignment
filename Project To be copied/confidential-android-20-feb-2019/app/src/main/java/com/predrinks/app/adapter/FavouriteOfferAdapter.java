package com.predrinks.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.fragment.OfferDetailFragment;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FavouriteOfferAdapter extends RecyclerView.Adapter<FavouriteOfferAdapter.MyViewHolder> {


    private JSONArray OffersLists;
    Context mContext;
    public static Set<Integer> selection;
    private boolean isFavouriteFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.imgMain)
        ImageView imgMain;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvVenueName)
        TextView tvVenueName;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.lnrMain)
        LinearLayout lnrMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            lnrMain.setOnClickListener(this);
            tvName.setSelected(true);
            tvName.setFocusable(true);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == lnrMain.getId()) {
                int pos = (int) lnrMain.getTag();

                JSONObject object;
                String eventId = "";
                try {
                    object = OffersLists.getJSONObject(pos);
                    eventId = object.getString("eventId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                OfferDetailFragment fragment = OfferDetailFragment.newInstance(eventId, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);
            }
        }
    }


    public FavouriteOfferAdapter(Context context, JSONArray clubLists, boolean isFavourite) {
        this.mContext = context;
        this.OffersLists = clubLists;
        if (selection == null)
            selection = new HashSet<>();

        isFavouriteFragment = isFavourite;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_fav_offers, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        JSONObject jsonObject = null;
        try {
            jsonObject = OffersLists.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonObject == null)
            return;

        if (isFavouriteFragment) {
            if (selection.contains(position)) {
                holder.lnrMain.setBackgroundColor(Color.WHITE);
            } else {
                holder.lnrMain.setBackgroundColor(Color.parseColor("#F0F0F0"));
            }
        } else {
            holder.lnrMain.setBackgroundColor(Color.WHITE);
        }
        holder.lnrMain.setTag(position);

        holder.tvName.setText(getJsonString(jsonObject, "title"));
        holder.tvVenueName.setText(getJsonString(jsonObject, "venueName"));
        holder.tvDateTime.setText(getJsonString(jsonObject, "eventEndDateTime"));

        String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");
        if (isEventDateDisabled.equals("true")){
            holder.tvDateTime.setVisibility(View.INVISIBLE);
        }else {
            holder.tvDateTime.setVisibility(View.VISIBLE);
        }

        String imgUrl = "";
        if (TextUtils.isEmpty(getJsonString(jsonObject, "eventImage"))) {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD_DEFAULT + "images/default_con.png";
        } else {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + getJsonString(jsonObject, "eventImage");
        }

        Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_club_logo)
                //.override(150, 170)
                .into(holder.imgMain);

      /*  Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .dontAnimate()
                .error(R.mipmap.default_club_logo)
                .into(holder.imgMain);*/
    }

    @Override
    public int getItemCount() {
        return OffersLists.length();
    }

    public void setSelection(int pos) {
        selection.add(pos);
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

}

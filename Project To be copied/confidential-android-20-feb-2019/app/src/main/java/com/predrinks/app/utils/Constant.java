package com.predrinks.app.utils;



public class Constant {
    public static final String CONFIDENTIALS                    = "Confidentials";
    public static final String USERID                           = "userID";
    public static final String NOTIFICATIONS                    = "notificationScreen";
    public static final String CURRENTPAGE                      = "CurrentPage";
    public static final String PAGESIZE                         = "PageSize";
    public static final String RESULT                           = "result";
    public static final String OFFERLIST                        = "offerList";
    public static final String NAME                             = "name";
    public static final String  LATITUDE				        = "latitude";
    public static final String  LONGITUDE				        = "longitude";
    public static final String EVENTDETAILUNDERVENUE            = "eventDetailUnderVenue";
    public static final String VENUEDETAIL                      = "venueDetail";
    public static final String VENUEIMAGES                      = "venueImages";
    public static final String CARTITEMS                        = "CartItems";
    public static final String ISADDEDTOCART                    = "IsAddedToCart";
    public static final String USER_DETAIL                      = "userDetail";
    public static final String EVENT_LIST                       = "eventList";

    public static final String SORT_KEY                         = "SortKey";
    public static final String CARDREGION                       = "CardRegion";
    public static final String STRIPETOKENTOBEPROCESSED         = "StripeTokenToBeProcessed";
    public static final String NETAMOUNT                        = "NetAmount";
    public static final String TRANSACTION_FEE                  = "TransactionFee";
    public static final String ORDERID                          = "OrderId";
    public static final String FAV_VENUE_NOTI                   = "favVenueNoti";
    public static final String UNLIKE                           = "Are you sure you want to un-favourite this venue?";



}

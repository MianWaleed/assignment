package com.predrinks.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClubDetailOfferAdapter extends RecyclerView.Adapter<ClubDetailOfferAdapter.MyViewHolder> {


    private JSONArray OffersLists;
    Context mContext;
    public static Set<Integer> selection;
    int mSize;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgMain)
        ImageView imgMain;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvVenueName)
        TextView tvVenueName;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.lnrMain)
        LinearLayout lnrMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public ClubDetailOfferAdapter(Context context, JSONArray clubLists, int size) {
        this.mContext = context;
        this.OffersLists = clubLists;
        mSize = size;
        if (selection == null)
            selection = new HashSet<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_club_detail_offers, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        JSONObject jsonObject = null;
        try {
            jsonObject = OffersLists.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String imagename = getJsonString(jsonObject, "eventImage");
        String title = getJsonString(jsonObject, "title");
        String venueName = getJsonString(jsonObject, "subTitle");
        String eventEndDateTime = getJsonString(jsonObject, "eventEndDateTime");
        String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");

        holder.tvName.setText(title);
        holder.tvVenueName.setText(venueName);
        holder.tvDateTime.setText(eventEndDateTime);

        if (isEventDateDisabled.equals("true")){
            holder.tvDateTime.setVisibility(View.INVISIBLE);
        }else {
            holder.tvDateTime.setVisibility(View.VISIBLE);
        }

        holder.lnrMain.setBackgroundColor(Color.WHITE);

        Glide.with(mContext)
                .load(URL_Utils.URL_IMAGE_DNLOAD + imagename)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.mipmap.default_event_venue_image)
                .into(holder.imgMain);

    }

    @Override
    public int getItemCount() {
        return mSize;
    }

    public void setSelection(int pos) {
        selection.add(pos);
    }


    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

}

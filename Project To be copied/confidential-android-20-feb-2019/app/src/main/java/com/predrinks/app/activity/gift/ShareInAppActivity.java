package com.predrinks.app.activity.gift;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.predrinks.app.R;
import com.predrinks.app.model.GiftSendModel;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ShareInAppActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backIv;
    private EditText emailEt;
    private Button findBt;
    Gson gson;
    String myOfferID, offerTitle, venueTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_in_app);
        initView();
    }

    private void initView() {
        if (getIntent().hasExtra("myOfferID")) {
            myOfferID = getIntent().getStringExtra("myOfferID");
            offerTitle = getIntent().getStringExtra("offerTitle");
            venueTitle = getIntent().getStringExtra("venueTitle");
        }

        backIv = (ImageView) findViewById(R.id.back_iv);
        emailEt = (EditText) findViewById(R.id.message_et);
        findBt = (Button) findViewById(R.id.send_bt);

        backIv.setOnClickListener(this);
        findBt.setOnClickListener(this);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_iv: {
                break;
            }
            case R.id.send_bt: {
                sendData();
//                startActivity(new Intent(this, ShareGiftActivity.class));
                break;
            }
        }
    }

    private void sendData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEt.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_Utils.USER + "/" + "498" + "/search/for/email", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GiftSendModel giftSendModel = gson.fromJson(response.toString(), GiftSendModel.class);
                if (giftSendModel.getSuccess().equals("1")) {
                    Bundle b
                             =new Bundle();
                    b.putSerializable("model" ,giftSendModel.getUser());

                    startActivity(new Intent(ShareInAppActivity.this, ShareGiftActivity.class)
                            .putExtra("model", b)
                            .putExtra("myOfferID", myOfferID)
                            .putExtra("offerTitle", offerTitle)
                            .putExtra("venueTitle", venueTitle));
                } else {
                    Toast.makeText(ShareInAppActivity.this, "User not found", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}

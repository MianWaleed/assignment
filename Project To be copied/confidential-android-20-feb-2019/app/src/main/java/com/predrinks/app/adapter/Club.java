package com.predrinks.app.adapter;

public class Club {

   private String clubImage;
   private String name;
   private String address;
   private String address1;
   private String address2;
   private boolean isFavourite;

   public Club() {
   }

   public Club(String clubImage, String name, String address, String address1, String address2, boolean isFavourite) {
      this.clubImage = clubImage;
      this.name = name;
      this.address = address;
      this.address1 = address1;
      this.address2 = address2;
      this.isFavourite = isFavourite;
   }

   public String getClubImage() {
      return clubImage;
   }

   public String getName() {
      return name;
   }

   public String getAddress() {
      return address;
   }

   public String getAddress1() {
      return address1;
   }

   public String getAddress2() {
      return address2;
   }

   public boolean isFavourite() {
      return isFavourite;
   }
}

package com.predrinks.app.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;

public class MoreAdapter extends BaseAdapter {

    Context mContext;
    String[] mNames;
    LayoutInflater inflater;

    public MoreAdapter(Context mContext, String[] names) {
        this.mContext = mContext;
        mNames = names;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mNames.length;
    }

    @Override
    public Object getItem(int i) {
        return mNames[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_more, parent, false);
        TextView txtOtherMatchesTitle = (TextView) rowView.findViewById(R.id.tvTitle);
        txtOtherMatchesTitle.setText(mNames[i]);

        ImageView imgNext = (ImageView) rowView.findViewById(R.id.imgNext);

        if (isLoggedIn(mContext)) {
            if (i == 8) {
                imgNext.setVisibility(View.INVISIBLE);
            }
        } else if (i == 7) {
            imgNext.setVisibility(View.INVISIBLE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

        txtOtherMatchesTitle.setTypeface(MainActivity.getTypeFaceRegular());

        return rowView;
    }

    public boolean isLoggedIn(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isLoggedIn = mPrefs.getBoolean("isLoggedIn", false);
        return isLoggedIn;
    }
}

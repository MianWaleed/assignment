package com.predrinks.app.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.OnLoadMoreListener;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.adapter.ClubListAdapter;
import com.predrinks.app.adapter.SearchOfferAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.SimpleDividerItemDecoration;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.predrinks.app.activity.MainActivity.onHideKeyBoard;
import static com.predrinks.app.activity.MainActivity.setVisibleHeaderRightMap;

public class SearchFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "SearchFragment";
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;
    @Bind(R.id.rlTab1)
    RelativeLayout rlTab1;
    @Bind(R.id.rlTab2)
    RelativeLayout rlTab2;
    @Bind(R.id.tvQueryText)
    TextView tvQueryText;
    @Bind(R.id.rlTabText1)
    TextView rlTabText1;
    @Bind(R.id.rlTabText2)
    TextView rlTabText2;
    @Bind(R.id.imgClear)
    ImageView imgClear;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private String mParam1;
    private String mParam2;
    Activity mActivity;
    Context mContext;

    String mResponseSearch = "";
    String mQuery = "";
    int mCurrentPage = 1;
    int mPageSize = 5;
    public static boolean isVenueSelected;

    public SearchFragment() {
    }

    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mResponseSearch = "";
        isVenueSelected = true;
        mQuery = "";
        mCurrentPage = 1;
        mPageSize = 10;
    }

    @Override
    public void onRefresh() {
        etSearch.setText(mQuery);
        searchItem(mQuery);
        if (!haveNetworkConnection(mContext)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);

        MainActivity.setBackButton(false);
        recyclerView.setVerticalScrollBarEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        swipeRefreshLayout.setOnRefreshListener(this);

        setVisibleHeaderRightMap(true);

        mResponseSearch = getLoadedData(mContext, "searchResponse");

        settabUI(isVenueSelected);

        etSearch.addTextChangedListener(new TextWatcher() {
            boolean isOnTextChanged = false;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isOnTextChanged = true;
            }

            @Override
            public void afterTextChanged(Editable charSequence) {
                if (isOnTextChanged) {
                    isOnTextChanged = false;

                    searchItem(charSequence.toString());
                    mQuery = charSequence.toString();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.header_layout_inner.setVisibility(View.VISIBLE);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        etSearch.setText(mQuery);
        MainActivity.setBackButton(false);
        MainActivity.setVisibleHeaderRightMap(true);
        MainActivity.setHeaderTitle("SEARCH");
    }

    @OnClick(R.id.imgClear)
    public void onClick() {
        etSearch.setText("");
        MainActivity.hideKeyboard(etSearch);
        mQuery = "";
        searchItem(mQuery);
    }

    private void settabUI(boolean isvenue) {
        if (isvenue) {
//            clubListAdapter.clearData();

            recyclerView.setAdapter(null);
            etSearch.setHint("Search Partners");
            rlTab1.setBackgroundColor(getResources().getColor(R.color.tab_bg));
            rlTabText1.setTextColor(getResources().getColor(R.color.white));
            rlTab2.setBackgroundColor(getResources().getColor(R.color.white));
            rlTabText2.setTextColor(getResources().getColor(R.color.txt_color_dark_gray));
        } else {

            recyclerView.setAdapter(null);
            etSearch.setHint("Search Offers");
            rlTab1.setBackgroundColor(getResources().getColor(R.color.white));
            rlTabText1.setTextColor(getResources().getColor(R.color.txt_color_dark_gray));
            rlTab2.setBackgroundColor(getResources().getColor(R.color.tab_bg));
            rlTabText2.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void searchItem(String query) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);

        mCurrentPage = 1;
        mPageSize = 10;
        query = query.replaceAll(" ", "%20");
        String URL;
        /*if (TextUtils.isEmpty(query)) {
            query = "~~";
            URL = URL_Utils.SEARCH_CRITERIA + "/" + getLoadedData(mContext, Constant.USERID) + "/venue/search/list?SearchCriteria=" + query;
        } else {*/
        URL = URL_Utils.SEARCH_CRITERIA + "/" + getLoadedData(mContext, Constant.USERID) + "/venue/search/list?SearchCriteria=" + query + "&" + Constant.CURRENTPAGE + "=" + mCurrentPage + "&" + Constant.PAGESIZE + "=" + mPageSize;
        // }

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, URL, null, createRequestSuccessListener, createRequestErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    ClubListAdapter clubListAdapter;
    SearchOfferAdapter searchOfferAdapter;
    JSONArray venuesJsonArray = null, eventsJsonArray = null;
    LinearLayoutManager linearLayoutManager;

    private void setAdapter(String response) {
        venuesJsonArray = null;
        eventsJsonArray = null;
        recyclerView.setAdapter(null);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        try {
            JSONObject object = new JSONObject(response);
            venuesJsonArray = object.getJSONArray("venues");
            JSONArray jArray = object.getJSONArray("events");
            if (jArray != null) {
                eventsJsonArray = new JSONArray();
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jArray.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
                    if (totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
                    } else {
                        eventsJsonArray.put(jsonObject);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        bindData();
    }

    Handler handler;

    private void bindData() {
        if (recyclerView == null)
            return;

        if (isVenueSelected) {
            if (venuesJsonArray != null && venuesJsonArray.length() > 0) {
                clubListAdapter = new ClubListAdapter(mContext, venuesJsonArray, recyclerView);
                if (recyclerView != null) {
                    recyclerView.setAdapter(null);
                    recyclerView.setVisibility(View.VISIBLE);
                    lnrEmpty.setVisibility(View.GONE);
                    //  recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(clubListAdapter);

                    recyclerView.getRecycledViewPool().clear();
                    clubListAdapter.notifyDataSetChanged();
                    //  recyclerView.addOnItemTouchListener(null);
                    recyclerView.addItemDecoration(new SimpleDividerItemDecoration(mActivity, R.drawable.search_divider));
                    if (lnrEmpty != null && venuesJsonArray.length() == 0) {
                        lnrEmpty.setVisibility(View.VISIBLE);
                        setSearchText(mQuery);
                    }

                    clubListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            //add null , so the adapter will check view_type and show progress bar at bottom
                            handler.postDelayed(new Runnable() {
                                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                @Override
                                public void run() {
                                    //   remove progress item
                                    if (!isLoading) {
                                        isLoading = true;
                                        venuesJsonArray.put(null);
                                        clubListAdapter.notifyItemInserted(venuesJsonArray.length() - 1);
                                    }

                                    loadMoreData(mQuery);
                                    //clubListAdapter.setLoaded();
                                    //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                                }
                            }, 1000);

                        }
                    });
                    handler = new Handler();
                }
            } else {
                if (lnrEmpty != null) {
                    recyclerView.setVisibility(View.GONE);
                    lnrEmpty.setVisibility(View.VISIBLE);
                    setSearchText(mQuery);
                }
            }

        } else {
            if (eventsJsonArray != null && eventsJsonArray.length() > 0) {
                recyclerView.setAdapter(null);
                searchOfferAdapter = new SearchOfferAdapter(mContext, eventsJsonArray, recyclerView);
                if (recyclerView != null) {
                    recyclerView.setAdapter(null);
                    recyclerView.setVisibility(View.VISIBLE);
                    lnrEmpty.setVisibility(View.GONE);

                    //recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(searchOfferAdapter);
                    recyclerView.getRecycledViewPool().clear();
                    searchOfferAdapter.notifyDataSetChanged();
                    //  recyclerView.addOnItemTouchListener(null);
                    recyclerView.addItemDecoration(new SimpleDividerItemDecoration(mActivity, R.drawable.search_divider));
                    if (lnrEmpty != null && eventsJsonArray.length() == 0) {
                        lnrEmpty.setVisibility(View.VISIBLE);
                        setSearchText(mQuery);
                    }

                    searchOfferAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            //add null , so the adapter will check view_type and show progress bar at bottom
                            handler.postDelayed(new Runnable() {
                                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                @Override
                                public void run() {
                                    //   remove progress item
                                    if (!isLoading) {
                                        isLoading = true;
                                        eventsJsonArray.put(null);
                                        searchOfferAdapter.notifyItemInserted(eventsJsonArray.length() - 1);
                                    }

                                    loadMoreData(mQuery);
                                    //clubListAdapter.setLoaded();
                                    //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                                }
                            }, 1000);

                        }
                    });
                    handler = new Handler();
                }

            } else {
                if (lnrEmpty != null) {
                    recyclerView.setVisibility(View.GONE);
                    lnrEmpty.setVisibility(View.VISIBLE);
                    setSearchText(mQuery);
                }
            }

        }
    }


    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            if (haveNetworkConnection(mContext)) {
                searchItem(mQuery);
            }
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    setAdapter(response.toString());
                } else if (success.equals("2")) {
                    setAdapter(response.toString());
                } else {
                    String message = response.getString("message");
                    dialog_info(mActivity, "", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    public void onPause() {
        VolleySingleton.getInstance().cancelPendingRequests(TAG);
        super.onPause();
        onHideKeyBoard(mActivity);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }


    @OnClick({R.id.rlTab1, R.id.rlTab2})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlTab1:
                isVenueSelected = true;
                settabUI(isVenueSelected);
                //searchItem(etSearch.getText().toString());
                bindData();
                break;
            case R.id.rlTab2:
                isVenueSelected = false;
                settabUI(isVenueSelected);
                // searchItem(etSearch.getText().toString());
                bindData();
                break;
        }
    }

    boolean isLoading = false;

    private void loadMoreData(String query) {
        Response.ErrorListener ErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        };

        Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        //  setAdapter(response.toString());
                        try {
                            //JSONObject object = new JSONObject(response);
                            JSONArray venuesArray = response.getJSONArray("venues");
                            JSONArray jArray = response.getJSONArray("events");
                            if (isVenueSelected) {
                                venuesJsonArray.remove(venuesJsonArray.length() - 1);
                                clubListAdapter.notifyItemRemoved(venuesJsonArray.length());
                            } else {
                                eventsJsonArray.remove(eventsJsonArray.length() - 1);
                                if (eventsJsonArray.length() != 0 && searchOfferAdapter != null) {
                                    searchOfferAdapter.notifyItemRemoved(eventsJsonArray.length());
                                }
                            }

                            if (jArray != null) {
                                //  eventsJsonArray = new JSONArray();
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = jArray.getJSONObject(i);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
                                    if (totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
                                    } else {
                                        eventsJsonArray.put(jsonObject);
                                    }
                                }
                            }

                            if (venuesArray != null) {
                                for (int i = 0; i < venuesArray.length(); i++) {
                                    venuesJsonArray.put(venuesArray.get(i));
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = venuesArray.getJSONObject(i);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    String isFav = getJsonString(jsonObject, "isFavoriteVenue");
                                    if (!isFav.equals("false")) {
                                        if (clubListAdapter.getFavList() != null)
                                            clubListAdapter.getFavList().add(i, true);
                                    } else {
                                        if (clubListAdapter.getFavList() != null)
                                            clubListAdapter.getFavList().add(i, false);
                                    }
                                }
                            }
                            if (response.getInt("currentPage") > mCurrentPage) {
                                mCurrentPage = response.getInt("currentPage");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (isVenueSelected) {
                            if (clubListAdapter != null) {
                                clubListAdapter.notifyDataSetChanged();
                                clubListAdapter.setLoaded();
                            }
                        } else {
                            if (searchOfferAdapter != null) {
                                searchOfferAdapter.notifyDataSetChanged();
                                searchOfferAdapter.setLoaded();
                            }
                        }
                        //bindData();
                    } else if (success.equals("2")) {
                        if (isVenueSelected) {
                            if (clubListAdapter != null) {
                                clubListAdapter.notifyDataSetChanged();
                                clubListAdapter.setLoaded();
                            }
                        } else {
                            if (searchOfferAdapter != null) {
                                searchOfferAdapter.notifyDataSetChanged();
                                searchOfferAdapter.setLoaded();
                            }
                        }
                    } else {
                        String message = response.getString("message");
                        dialog_info(mActivity, "", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        query = query.replaceAll(" ", "%20");

        String URL = URL_Utils.SEARCH_CRITERIA + "/" + getLoadedData(mContext, Constant.USERID) + "/venue/search/list?SearchCriteria=" + query + "&" + Constant.CURRENTPAGE + "=" + (mCurrentPage + 1) + "&" + Constant.PAGESIZE + "=" + mPageSize;

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, URL, null, SuccessListener, ErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }


    private void setSearchText(String query) {
        if (!TextUtils.isEmpty(query)) {
            tvQueryText.setText("\"" + mQuery + "\"");
        } else {
            tvQueryText.setText(mQuery);
        }

    }
}

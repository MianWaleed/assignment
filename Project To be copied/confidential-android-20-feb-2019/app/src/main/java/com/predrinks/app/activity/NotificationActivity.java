package com.predrinks.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.adapter.NotificationListAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationActivity extends BaseFragmentActivity {

    private static final String TAG = "NotificationActivity";
    @Bind(R.id.lnrBack)
    LinearLayout lnrBack;
    @Bind(R.id.llMainHeader)
    LinearLayout llMainHeader;

    @Bind(R.id.switchNotification)
    Switch switchNotification;


    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.tvNotiVenues)
    TextView tvNotiVenues;

    @Bind(R.id.tvNotification)
    TextView tvNotification;
    @Bind(R.id.listView)
    ListView listView;

    String mUserId = "";
    String mResponse;

    Activity mActivity;
    Context mContext;
    JSONObject myNotification;
    JSONArray favVenueNotiList;

    boolean isGeneralNotify = false;
    SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notification);

        mActivity = this;
        mContext = this.getBaseContext();
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        ButterKnife.bind(this);
        mPrefs = getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isnotification = mPrefs.getBoolean(Constant.NOTIFICATIONS, true);

        isGeneralNotify = false;

        isGeneralNotify = isnotification;

        switchNotification.setChecked(isGeneralNotify);

        mUserId = getLoadedData(Constant.USERID);

        tvNotification.setTypeface(MainActivity.getTypeFaceRegular());
        textView.setTypeface(MainActivity.getTypeFaceRegular());
        tvNotiVenues.setTypeface(MainActivity.getTypeFaceRegular());

        String response = getLoadedData("notification");
        if (!TextUtils.isEmpty(response)) {
            updateUI(response);
        }

        getDataFromWeb();
    }

    private void getDataFromWeb() {
        String URL = URL_Utils.USER + "/" + mUserId + "/notification";


        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());

        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());

            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    mResponse = response.toString();
                    updateUI(response.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void updateUI(String response) {
        boolean receiveGeneralNoti = false;
        try {
            JSONObject responseObject = new JSONObject(response);
            myNotification = responseObject.getJSONObject(Constant.RESULT);
            receiveGeneralNoti = myNotification.getBoolean("receiveGeneralNoti");
            favVenueNotiList = myNotification.getJSONArray(Constant.FAV_VENUE_NOTI);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setBoolean(receiveGeneralNoti, Constant.NOTIFICATIONS);
        switchNotification.setChecked(receiveGeneralNoti);

        saveLoadedData("notification", response);

        if (receiveGeneralNoti) {
            if (favVenueNotiList != null && favVenueNotiList.length() > 0) {
                NotificationListAdapter adapter = new NotificationListAdapter(mContext, favVenueNotiList, mUserId);
                listView.setAdapter(adapter);
            }
            listView.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.GONE);
        }

    }

    @OnClick({R.id.lnrBack, R.id.switchNotification})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnrBack:
                finish();
                break;
            case R.id.switchNotification:
                isGeneralNotify = switchNotification.isChecked();
                String isNotify = String.valueOf(isGeneralNotify);
                postNotifi();
                break;
        }
    }

    private void postNotifi() {
        isGeneralNotify = switchNotification.isChecked();
        String isNotify = String.valueOf(isGeneralNotify);

        String URL = URL_Utils.USER + "/" + mUserId + "/receiveGeneralNotification/" + isNotify;
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, SuccessListener, ErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    Response.ErrorListener ErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    getDataFromWeb();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


}

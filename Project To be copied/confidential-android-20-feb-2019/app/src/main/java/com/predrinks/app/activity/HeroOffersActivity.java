package com.predrinks.app.activity;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.predrinks.app.R;
import com.predrinks.app.adapter.HeroOffersAdaper;
import com.predrinks.app.fragment.ConfidentialsPassFragment;
import com.predrinks.app.model.HeroVoucherModel;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.URL_Utils;

import static com.android.volley.Request.Method.GET;

public class HeroOffersActivity extends AppCompatActivity implements View.OnClickListener {

    private ConstraintLayout containerRl;
    private TextView textView;
    private ImageView confidentialsPassIv;
    private ImageView backIv;
    private RecyclerView heroOfferRv;
    ProgressBar progressBar;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_offers);

        initView();
        getDataFromApi();
    }

    private void getDataFromApi() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, URL_Utils.USER + "/" + "498" + "/confidentials/pass", null, response -> {
            progressBar.setVisibility(View.GONE);
            HeroVoucherModel heroVoucherModel = gson.fromJson(response.toString(), HeroVoucherModel.class);
            HeroOffersAdaper heroOffersAdaper = new HeroOffersAdaper(getApplicationContext(), heroVoucherModel.getData());
            heroOfferRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            heroOfferRv.setAdapter(heroOffersAdaper);
        }, error -> {

        });

        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void initView() {
        progressBar = findViewById(R.id.progressBar);

        containerRl = findViewById(R.id.container_rl);
        textView = findViewById(R.id.textView);
        confidentialsPassIv = findViewById(R.id.confidentials_pass_iv);
        backIv = findViewById(R.id.back_iv);
        heroOfferRv = findViewById(R.id.hero_offer_rv);

        confidentialsPassIv.setOnClickListener(this);
        backIv.setOnClickListener(this);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_iv: {
                finish();
                break;
            }
            case R.id.confidentials_pass_iv: {
                FragmentManager fm = getFragmentManager();
                ConfidentialsPassFragment editNameDialogFragment = new ConfidentialsPassFragment();
                editNameDialogFragment.show(fm, "fragment_edit_name");
            }
        }
    }
}

package com.predrinks.app.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ListView;

public class GetLocation {

	public GetLocation(Context context) {
		this.context = context;
		getLocation();
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	Context context;

	Location location;                        // location
	double latitude;                        // latitude
	double longitude;                        // longitude
	boolean isGPSEnable = false;            // flag for GPS status
	boolean isNetworkEnable = false;        // flag for network status
	boolean canGetLocation = false;            // flag for GPS status

	// The Minimum distance to change updates in meter 
	static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	// The minimum Time between updates in miliseconds
	static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

	// Declaring the location manager
	LocationManager locationManager;

	ListView l;

	LocationListener ll = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onLocationChanged(Location loc) {
			// TODO Auto-generated method stub
			if (loc != null) {
				location = loc;
				latitude = location.getLatitude();
				longitude = location.getLongitude();
			}
		}

	};


	public boolean canGetLocation() {
		return this.canGetLocation;
	}
	public void getLocation() {
		try {
			locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);            //getting GPS status
			isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);    //getting network status
			if (!isGPSEnable && !isNetworkEnable) {
				//No Network Provider Is Enable...
				//Toast.makeText(context, "Error with GPS OR NETWORK", Toast.LENGTH_SHORT).show();
			} else {
				canGetLocation = true;
				// First get the location from network provider
				if (isNetworkEnable) {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, ll);
					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}
					}
				}
				// If GPS is Enable get Lat/Long using GPS Services
				if (isGPSEnable) {
					if (location == null) {
						if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
							return;
						}
						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, ll);
						if(locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if(location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();
							}
						}
					}
				}
			}
		}
		catch(Exception e){
			Log.e("getLocation", "Error", e);
		}
    }
	
	
}

package com.predrinks.app.adapter;

public class CardDetail {
	
	public String cardLast4="", cardNumber="",expire_month="",expire_year="",postalCode="";
	
	public CardDetail(String cardLast4, String cardNumber, String expire_month, String expire_year, String postalCode)
	{
		this.cardLast4 = cardLast4;
		this.cardNumber = cardNumber;
		this.expire_month = expire_month;
		this.expire_year = expire_year;
		this.postalCode = postalCode;
	}

}

package com.predrinks.app.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.activity.gift.ShareInAppActivity;
import com.predrinks.app.adapter.RedeemedListAdapter;
import com.predrinks.app.fragment.MyVouchersFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.ExpandableTextView;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.widget.GiftOnClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VoucherDetailActivity extends BaseFragmentActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_EVENT_TITLE = "eventTitle";
    private static final String ARG_EVENT_ID = "eventID";
    private static final String ARG_EVENT_OBJECT = "eventObject";
    private static final String TAG = "VoucherDetailActivity";


    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.activity_voucher_detail)
    LinearLayout activityVoucherDetail;
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.imgBack)
    ImageView imgBack;
    @Bind(R.id.imgAdd)
    ImageView imgAdd;
    @Bind(R.id.tvADD)
    TextView tvADD;
    @Bind(R.id.lnrQrAdd)
    LinearLayout lnrQrAdd;
    TextView tvInst;
    TextView tvInstDet;
    TextView tvOverView;
    TextView tvOffersTC;

    ExpandableTextView expandTextView;
    ExpandableTextView expandTextViewTC;


    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    private Context mContext;
    private FragmentActivity mActivity;

    private String eventTitle, eventID, eventObject;
    String mUserId = "";
    private ProgressDialog pDialog;
    ViewGroup header;
    private LayoutInflater layoutinflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_detail);
        ButterKnife.bind(this);
        mContext = this;
        mActivity = this;

        mUserId = getLoadedData(Constant.USERID);

        layoutinflater = getLayoutInflater();
        header = (ViewGroup) layoutinflater.inflate(R.layout.list_header, listView, false);

        tvInst = (TextView) header.findViewById(R.id.tvInst);
        tvInstDet = (TextView) header.findViewById(R.id.tvInstDet);
        tvOverView = (TextView) header.findViewById(R.id.tvOverView);
        tvOffersTC = (TextView) header.findViewById(R.id.tvOffersTC);
        expandTextView = (ExpandableTextView) header.findViewById(R.id.expand_text_view);
        expandTextViewTC = (ExpandableTextView) header.findViewById(R.id.expand_text_viewTC);

        tvInst.setTypeface(MainActivity.getTypeFaceRegular());
        tvInstDet.setTypeface(MainActivity.getTypeFaceRegular());
        tvOverView.setTypeface(MainActivity.getTypeFaceRegular());
        tvOffersTC.setTypeface(MainActivity.getTypeFaceRegular());

        Intent intent = getIntent();
        eventTitle = intent.getStringExtra(ARG_EVENT_TITLE);
        eventID = intent.getStringExtra(ARG_EVENT_ID);
        eventObject = intent.getStringExtra(ARG_EVENT_OBJECT);

        swipeRefreshLayout.setOnRefreshListener(this);
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(eventObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        boolean isExpired = Boolean.parseBoolean(getJsonString(jsonObject, "isExpired"));
        if (isExpired) {
            lnrQrAdd.setVisibility(View.INVISIBLE);
        } else {
            lnrQrAdd.setVisibility(View.VISIBLE);
        }

        tvTitleHeader.setText(eventTitle);
        pDialog.show();


    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getDataFromWeb();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getDataFromWeb();
    }

    private void getDataFromWeb() {
        String URL = URL_Utils.USER + "/" + mUserId + "/purchasedevents/" + eventID + "/offers";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }


    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);

            if (pDialog != null)
                pDialog.dismiss();

            Log.d(TAG, error.toString());
            mResponse = getLoadedData("voucherDetail" + eventID);
            if (!TextUtils.isEmpty(mResponse)) {
                updateUI(mResponse);
            }
        }
    };


    private String mResponse;
    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            if (pDialog != null)
                pDialog.dismiss();
            try {
                String status = response.getString("status");
                if (status.equals("1")) {
                    mResponse = response.toString();
                    updateUI(mResponse);
                    saveLoadedData("voucherDetail" + eventID, mResponse);
                } else {
                    updateUI(response.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    JSONArray myOfferList = null;
    RedeemedListAdapter redeemedListAdapter;

    private void updateUI(String response) {

        String eventOverview = "", eventTNC = "";
        try {
            JSONObject responseObject = new JSONObject(response);
            eventOverview = getJsonString(responseObject, "eventOverview");
            eventTNC = getJsonString(responseObject, "eventTNC");
            myOfferList = responseObject.getJSONArray(Constant.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!eventOverview.isEmpty()) {
            expandTextView.setVisibility(View.VISIBLE);
        } else {
            expandTextView.setVisibility(View.GONE);
        }

        if (!eventTNC.isEmpty()) {
            expandTextViewTC.setVisibility(View.VISIBLE);
        } else {
            expandTextViewTC.setVisibility(View.GONE);
        }

        expandTextView.setText(eventOverview);
        expandTextViewTC.setText1(eventTNC, "");

        tvTitleHeader.setText(eventTitle);

        if (myOfferList == null) {
            return;
        }

        JSONArray jsonArray = new JSONArray();

        Log.d(TAG, "updateUI: " + myOfferList.toString());

        for (int i = 0; i < myOfferList.length(); i++) {
            try {
                JSONObject obj = myOfferList.getJSONObject(i);
                if (MyVouchersFragment.WHAT_IT_IS == MyVouchersFragment.PURCHASED) {
                    boolean isExpired = obj.getBoolean("isExpired");
                    boolean isRedeemed = obj.getBoolean("isRedeemed");
                    boolean isGifted = obj.getBoolean("isGifted");

                    if (!isRedeemed && !isExpired && !isGifted) {
                        jsonArray.put(obj);
                    }
                } else if (MyVouchersFragment.WHAT_IT_IS == MyVouchersFragment.REDEEMED) {
                    boolean isRedeemed = obj.getBoolean("isRedeemed");

                    if (isRedeemed)
                        jsonArray.put(obj);

                } else if (MyVouchersFragment.WHAT_IT_IS == MyVouchersFragment.EXPIRED) {
                    boolean isExpired = obj.getBoolean("isExpired");
                    boolean isRedeemed = obj.getBoolean("isRedeemed");

                    if (isExpired && !isRedeemed) {
                        jsonArray.put(obj);
                    }
                } else if (MyVouchersFragment.WHAT_IT_IS == MyVouchersFragment.GIFTED) {
                    boolean isGifted = obj.getBoolean("isGifted");

                    if (isGifted)
                        jsonArray.put(obj);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        redeemedListAdapter = new RedeemedListAdapter(mContext, giftListener, jsonArray);

        listView.removeHeaderView(header);
        listView.addHeaderView(header, null, false);
        listView.setAdapter(redeemedListAdapter);
    }

    @OnClick({R.id.imgBack, R.id.lnrQrAdd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.lnrQrAdd:
                Intent intent = new Intent(mContext, OfferDetailActivity.class);
                intent.putExtra("eventId", eventID);
                startActivityForResult(intent, 100);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != 112) {
            setResult(resultCode, data);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (jsonObject == null || !jsonObject.has(strKey))
                return str;

            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    GiftOnClickListener giftListener = new GiftOnClickListener() {
        @Override
        public void onGiftClick(String offerTitle, String venueTitle, String myOfferID) {
            dialog_gift(offerTitle, venueTitle, myOfferID);
        }
    };


    protected void dialog_gift(final String offerTitle, final String venueTitle,
                               final String myOfferID) {
        final Dialog dialog = new Dialog(VoucherDetailActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_gift);
        dialog.setCancelable(false);

        View.OnClickListener cancelBtn = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };
        View.OnClickListener lnrShareGift = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getShareUrl(myOfferID);

            }
        };
        View.OnClickListener lnrEmailGift = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(mActivity, SendGiftActivity.class);
                intent.putExtra("offerTitle", offerTitle);
                intent.putExtra("venueTitle", venueTitle);
                intent.putExtra("myOfferID", myOfferID);
                startActivity(intent);
            }
        };

         dialog.findViewById(R.id.lnrShareGift).setOnClickListener(lnrShareGift);
         dialog.findViewById(R.id.lnrEmailGift).setOnClickListener(lnrEmailGift);

        ( dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelBtn);

        dialog.findViewById(R.id.share_in_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ShareInAppActivity.class);
                intent.putExtra("offerTitle", offerTitle);
                intent.putExtra("venueTitle", venueTitle);
                intent.putExtra("myOfferID", myOfferID);
                startActivity(intent);
            }
        });

        dialog.show();
    }

    private void getShareUrl(String myOfferID) {
        Response.ErrorListener createErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pDialog != null)
                    pDialog.dismiss();
                try {
                    Log.d(TAG, error.toString());
                } catch (RuntimeException e) {
                }
            }
        };

        Response.Listener<JSONObject> createSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                if (pDialog != null)
                    pDialog.dismiss();
                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        String giftURL = response.getString("giftURL");
                        //  String shareMessage = "Hi there!\nI have sent you a gift from Confidentials, Please see the link below.\n" + giftURL;
                        String shareMessage = "I have just sent you a gift voucher from Confidentials. Click the link below to access your QR code. \n" + giftURL;
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, ""));
                    } else {
                        String message = response.getString("message");
                        // dialog_info("Confidentials", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        if (pDialog != null) {
            pDialog.show();
        }

        Map<String, String> params = new HashMap<String, String>();
        // params.put(URL_Utils.PARAM_USERID, "");
        params.put(URL_Utils.PARAM_MYOFFER_ID, myOfferID);
        // params.put(URL_Utils.PARAM_TO_EMAIL_ID, "");
        // params.put(URL_Utils.PARAM_MESSAGE, "");

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL_Utils.SHARE_MYOFFER, params, createSuccessListener, createErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }


}

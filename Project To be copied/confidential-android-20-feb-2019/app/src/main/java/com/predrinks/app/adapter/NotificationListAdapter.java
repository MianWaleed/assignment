package com.predrinks.app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NotificationListAdapter extends BaseAdapter {

    private static final String TAG = "NotificationListAdapter";
    Context mContext;
    JSONArray favVenueNotiList;
    LayoutInflater inflater;
    String mUserId = "";
    public NotificationListAdapter(Context mContext, JSONArray favVenueNotiList, String mUserId) {
        this.mContext = mContext;
        this.favVenueNotiList = favVenueNotiList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mUserId = mUserId;
    }

    @Override
    public int getCount() {
        return favVenueNotiList.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return favVenueNotiList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null){
            view = inflater.inflate(R.layout.row_notification_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }
        holder.switchClub.setTag(i);
        JSONObject jsonObject = null;
        try {
            jsonObject = favVenueNotiList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String clubName = getJsonString(jsonObject, "venueName");
        boolean receiveNoti = false;
        try {
             receiveNoti = jsonObject != null && jsonObject.getBoolean("receiveNoti");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.tvNotification.setText(clubName);
        holder.switchClub.setChecked(receiveNoti);
        holder.switchClub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                boolean ischacked = holder.switchClub.isChecked();
                JSONObject jsonObject = null;
                try {
                    jsonObject = favVenueNotiList.getJSONObject(pos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String venueId = getJsonString(jsonObject, "venueID");
                postNotifi(venueId, String.valueOf(ischacked));
            }
        });

        return view;
    }

    class ViewHolder{
        @Bind(R.id.tvNotification)
        TextView tvNotification;
        @Bind(R.id.switchClub)
        Switch switchClub;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void postNotifi(String VenueId, String siNotifiy){
        String URL = URL_Utils.VENUE_DETAIL+"/"+ VenueId + "/user/" + mUserId + "/receiveVenueNotification/"+siNotifiy;
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST,URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (jsonObject == null || !jsonObject.has(strKey))
                return str;

            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }
}

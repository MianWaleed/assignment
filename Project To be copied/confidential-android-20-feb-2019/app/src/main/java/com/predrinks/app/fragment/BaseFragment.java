package com.predrinks.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.activity.LandingActivity;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.activity.ProfileActivity;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.profile.UpdateProfileDialogFragment;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static android.support.constraint.Constraints.TAG;

public class BaseFragment extends Fragment {
    private SharedPreferences mPrefs;

    public static String GetAddress(Context mActivity, double lat, double lon) {
        Geocoder geocoder = new Geocoder(mActivity, Locale.ENGLISH);
        String ret = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if (addresses != null && addresses.size() != 0) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                ret = strReturnedAddress.toString();
            } else {
                ret = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
            ret = "";
        }
        return ret;
    }

    protected void getUserDetail(final Context context, String userId) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());

            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("getUserDetail", response.toString());
                JSONObject result = null;
                try {
                    result = response.getJSONObject("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                saveLoadedData(context, Constant.USER_DETAIL, result.toString());
            }
        };
        String URL = URL_Utils.USER + "/" + userId + "/detail";
        Log.d(TAG, "getUserDetail: "+URL);
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void setQrCode(Context context, String qrCode) {
        SharedPreferences prefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, MODE_PRIVATE);
        prefs.edit().putString("qr_code", qrCode).apply();
    }

    public static String getQrCode(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, MODE_PRIVATE);
        return prefs.getString("qr_code", "");
    }

    protected void updateUserDetail(final Context context, String userId, boolean isnotify, double lat, double lang) {
        final String finalUserId = userId;
        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());
                getWebAppVersion(context);

                try {
                    long expTime = mPrefs.getLong("alarmExpireTime", 0);
                    if (expTime > 0) {
                        if (System.currentTimeMillis() >= expTime)
                            removeCartItems(context, finalUserId);
                    }

                } catch (NumberFormatException e) {

                }
            }
        };
        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                getWebAppVersion(context);
                try {
                    long expTime = mPrefs.getLong("alarmExpireTime", 0);
                    if (expTime > 0) {
                        if (System.currentTimeMillis() >= expTime)
                            removeCartItems(context, finalUserId);
                    }
                } catch (NumberFormatException e) {

                }

            }
        };
        if (com.stripe.android.util.TextUtils.isBlank(userId))
            userId = "0";

        Map<String, String> params = new HashMap<String, String>();
        String URL = URL_Utils.USER + "/" + userId + "/updateUserDetails/" + isnotify + "/" + lat + "/" + lang + "/0";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, params, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    protected void removeCartItems(final Context context, String userId) {
        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());
                mPrefs.edit().putLong("alarmExpireTime", 0).apply();
            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                mPrefs.edit().putLong("alarmExpireTime", 0).apply();
                MainActivity.ITEM_ADD_TO_CART = 0;
                MainActivity.setHeaderNotificationNumber(MainActivity.ITEM_ADD_TO_CART);
                saveLoadedData(context, "numberOfAddToCart", "" + MainActivity.ITEM_ADD_TO_CART);
            }
        };
        String URL = URL_Utils.USER + "/" + userId + "/DeleteCart";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }

    protected void getWebAppVersion(final Context context) {
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());
            }
        };

        Response.Listener<JSONObject> successListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                try {
                    String status = response.getString("success");
                    if (status.equals("1")) {
                        String storeAppVersion = response.getString("storeAppVersion");
                        saveLoadedData(context, "WebAppVersion", storeAppVersion);

                        int requestedInfo = response.getInt("info_requested");

                        if (requestedInfo == 1) {

                            FragmentManager fm = getFragmentManager();
                            UpdateProfileDialogFragment editNameDialogFragment = new UpdateProfileDialogFragment();
                            editNameDialogFragment.show(fm, "fragment_edit_name");
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String URL = URL_Utils.USER + "/android/forceUpdate?userId="+getLoadedData(context, Constant.USERID);
        Log.d(TAG, "getWebAppVersion: " + URL);
        Log.d(TAG, "getWebAppVersion: " + getLoadedData(context, Constant.USERID));
        CustomRequest jsObjRequest = new CustomRequest(URL, null, successListener, errorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    protected void showDirections(Context context, String your, String dest) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" +
                "saddr=" + your + "&daddr=" + dest));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        context.startActivity(intent);
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    protected boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    protected void clearAppData(Context context) {
        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.clear();
        editor.apply();
    }

    protected void saveLoadedData(Context context, String TAG, String data) {
        if (context == null)
            return;

        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(TAG, data);
        editor.apply();
    }

    protected String getLoadedData(Context context, String TAG) {
        if (context == null)
            return "";

        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        String data = mPrefs.getString(TAG, "");
        return data;
    }

    protected boolean isLoggedIn(Context context) {
        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isLoggedIn = mPrefs.getBoolean("isLoggedIn", false);
        return isLoggedIn;
    }

    protected boolean getBooleanValue(Context context, String TAG) {
        mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isLoggedIn = mPrefs.getBoolean(TAG, false);
        return isLoggedIn;
    }

    protected void hideKeyBoard(Activity context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(context.getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    protected void hideKeyBoardByView(Context context, View view) {

        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
        }

    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (jsonObject == null || !jsonObject.has(strKey))
                return str;

            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }


    protected void dialog_info(Activity activity, String title, String message) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    protected void dialog_Login(final Activity activity) {
        String mes;
        String textbtn;
        mes = "Login to continue";
        textbtn = "Login";

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, LandingActivity.class);
                startActivity(intent);
                activity.finish();
                dialog.dismiss();
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(mes);
        ((Button) dialog.findViewById(R.id.okBtn)).setText(textbtn);

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);
        dialog.show();
    }

    protected void dialog_Profile(final Activity activity) {
        String mes;
        String textbtn;
        mes = "To continue please update your date of birth in MY ACCOUNT";
        textbtn = "Update";

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, ProfileActivity.class);
                startActivity(intent);
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(mes);
        ((Button) dialog.findViewById(R.id.okBtn)).setText(textbtn);

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        dialog.show();
    }

}

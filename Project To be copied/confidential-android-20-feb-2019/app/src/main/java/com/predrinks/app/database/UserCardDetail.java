package com.predrinks.app.database;

public class UserCardDetail {

    String userId;
    String cardLastfour;
    String cardToken;
    int cardType;
    String cardNumber;
    int cardExpMonth;
    int cardExpYear;
    String postcode;

    public UserCardDetail(String userId, String cardLastfour, String cardToken, int cardType, String cardNumber, int cardExpMonth, int cardExpYear, String postcode) {
        this.userId = userId;
        this.cardLastfour = cardLastfour;
        this.cardToken = cardToken;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.cardExpMonth = cardExpMonth;
        this.cardExpYear = cardExpYear;
        this.postcode = postcode;
    }

    public String getUserId() {
        return userId;
    }

    public String getCardLastfour() {
        return cardLastfour;
    }

    public String getCardToken() {
        return cardToken;
    }

    public int getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public int getCardExpMonth() {
        return cardExpMonth;
    }

    public int getCardExpYear() {
        return cardExpYear;
    }

    public String getPostcode() {
        return postcode;
    }
}

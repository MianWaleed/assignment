package com.predrinks.app.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.activity.LandingActivity;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.fragment.SubscriptionsFragment;
import com.predrinks.app.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OfferDetailAdapter extends BaseAdapter {

    Activity mActivity;
    Context mContext;
    JSONArray mJsonArray;
    LayoutInflater inflater;
    private int mPosition = -1;
    String[] qty = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
    public Set<Integer> mSelectedPos;
    public HashMap<Integer, Integer> mQtyList;

    String[] qtyList;
    private SharedPreferences mPrefs;
    boolean isSubscribed = false;
    ClickListener clickListener;


    public OfferDetailAdapter(Activity activity, Context context, JSONArray jsonArray, ClickListener clickListener) {
        this.clickListener = clickListener;
        this.mActivity = activity;
        this.mContext = context;
        mJsonArray = jsonArray;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPosition = -1;
        mSelectedPos = new HashSet<>();
        qtyList = mContext.getResources().getStringArray(R.array.qty);
        mQtyList = new HashMap<>();

        mPrefs = mContext.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        String userdetail = mPrefs.getString(Constant.USER_DETAIL, "");
        if (!TextUtils.isEmpty(userdetail)) {
            JSONObject object = null;
            try {
                object = new JSONObject(userdetail);
                isSubscribed = object.getBoolean("isSubscribed");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getCount() {
        return mJsonArray.length();
    }

    @Override
    public Object getItem(int i) {
        //return mNames[i];
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    boolean isSpinnerClick = false;

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.row_offer_choice, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);

        }

        holder.tagsContainer.removeAllViews();
        holder.horizontalScrollView.setBackgroundResource(R.drawable.tags_container_bg);

        holder.tvTitle.setTypeface(MainActivity.getTypeFaceRegular());
        holder.tvPriceCancel.setTypeface(MainActivity.getTypeFaceBold());
        holder.tvPriceNew.setTypeface(MainActivity.getTypeFaceBold());
        holder.tvRemaining.setTypeface(MainActivity.getTypeFaceMedium());

        holder.tvVoucherLocked.setTypeface(MainActivity.getTypeFaceBold());
        holder.tvSubscribeDetail.setTypeface(MainActivity.getTypeFaceRegular());
        holder.tvSubscribeHere.setTypeface(MainActivity.getTypeFaceBold());

        holder.tvTitle.setSelected(true);
        holder.tvTitle.setFocusable(true);

        holder.spinner.setTag(position);
        holder.lnr_main_row_offer.setTag(position);

        JSONObject jsonObject = null;

        try {
            jsonObject = mJsonArray.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isSubscribed) {
            holder.lnrVoucherLocked.setVisibility(View.GONE);
            holder.lnrMain.setEnabled(true);
            holder.lnr_main_row_offer.setEnabled(true);
        } else {
            String isSubscribedOffer = getJsonString(jsonObject, "isSubscribedOffer");
            if (isSubscribedOffer.equals("true")) {
                holder.lnrVoucherLocked.setVisibility(View.VISIBLE);
                holder.lnrMain.setVisibility(View.GONE);
            } else {
                holder.lnrVoucherLocked.setVisibility(View.GONE);
                holder.lnrMain.setVisibility(View.VISIBLE);
            }
        }


        String title = getJsonString(jsonObject, "title") + " - " + "£" + getJsonString(jsonObject, "discountedPrice");
        String discountType = getJsonString(jsonObject, "discountType");
        String totalSold = getJsonString(jsonObject, "totalSold");
        String originalPrice = getJsonString(jsonObject, "originalPrice");
        String discountAmount = getJsonString(jsonObject, "discountAmount");
        String offerWiseRemaining = getJsonString(jsonObject, "offerWiseRemaining");

        try {
            if (jsonObject.has("tags")) {
                holder.horizontalScrollView.setVisibility(View.VISIBLE);
                JSONArray jsonArray = jsonObject.getJSONArray("tags");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    TextView textView = new TextView(mContext);
                    textView.setText("#" + jsonObject1.getString("name"));
                    textView.setId(i);
                    textView.setBackgroundResource(R.drawable.tags_bg);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(10, 5, 10, 5);
                    textView.setLayoutParams(layoutParams);
                    textView.setTextAppearance(R.style.TextAppearance_AppCompat_Medium);
                    textView.setPadding(20, 20, 20, 20);
                    holder.tagsContainer.addView(textView);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.tvTitle.setText(title);
        holder.tvPriceCancel.setText("£" + originalPrice);
        holder.tvRemaining.setText(offerWiseRemaining.toUpperCase());

        if (discountType.equals("Percentage")) {
            holder.tvPriceNew.setText("SAVE " + discountAmount + "%");
        } else {
            holder.tvPriceNew.setText("SAVE " + "£" + discountAmount);
        }

        holder.tvPriceCancel.setPaintFlags(holder.tvPriceCancel.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.writeReviewTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.showWriteReview();
            }
        });

        holder.showReviewTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.showReviews();
            }
        });

        holder.rlSubscribeHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isLoggedIn = mPrefs.getBoolean("isLoggedIn", false);
                if (!isLoggedIn) {
                    dialog_Login(mActivity);
                    return;
                }
                SubscriptionsFragment fragment = SubscriptionsFragment.newInstance("", "");
                MainActivity.loadContentFragmentWithBack(fragment, false);

            }
        });
        holder.lnr_main_row_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                if (mSelectedPos.contains(position)) {
                    removeCurrentPosition(position);
                    holder.spinner.setEnabled(false);
                    isSpinnerClick = false;
                    mQtyList.remove(position);
                } else {
                    setCurrentPosition(position);
                    mQtyList.put(position, 0);
                    holder.spinner.setEnabled(true);
                    isSpinnerClick = true;
                }
                notifyDataSetChanged();
            }
        });

        String totalRemainingOffers = getJsonString(jsonObject, "offerForSale");
        int remaining = Integer.parseInt(totalRemainingOffers);
        ArrayList<String> arrayList = new ArrayList<String>();
        if (remaining < 20) {
            for (int i = 0; i < remaining; i++) {
                arrayList.add(qty[i]);
            }
        } else {
            for (int i = 0; i < 20; i++) {
                arrayList.add(qty[i]);
            }
        }

        ArrayAdapter<String> adp = new ArrayAdapter<String>(mContext, R.layout.simple_spinner_item, arrayList);
        holder.spinner.setAdapter(adp);
        if (mSelectedPos.contains(position)) {
            holder.checkBox.setChecked(true);
            holder.spinner.setEnabled(true);
            holder.spinner.setVisibility(View.VISIBLE);
            holder.spinner.setSelection(mQtyList.get(position), false);

            holder.lnr_main_row_offer.setBackgroundColor(Color.parseColor("#F0FAEF"));
        } else {
            holder.checkBox.setChecked(false);
            holder.spinner.setEnabled(false);
            holder.spinner.setClickable(false);
            holder.spinner.setSelection(0, false);
            ;
            holder.lnr_main_row_offer.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                int posAdapter = (int) parent.getTag();
                if (mSelectedPos.contains(posAdapter)) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = mJsonArray.getJSONObject(posAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    int offer = Integer.parseInt(getJsonString(jsonObject, "offerForSale"));
                    if (pos < offer) {
                        mQtyList.put(posAdapter, pos);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        return view;
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    public void setCurrentPosition(int position) {
        mPosition = position;
        mSelectedPos.add(position);
    }

    public void removeCurrentPosition(int position) {
        mPosition = position;
        mSelectedPos.remove(position);
    }

    public Set<Integer> getSelectedPosition() {
        if (mSelectedPos == null)
            mSelectedPos = new HashSet<>();

        return mSelectedPos;
    }

    public HashMap<Integer, Integer> getQtyListPosition() {
        return mQtyList;
    }

    static class ViewHolder {
        @Bind(R.id.checkBox)
        CheckBox checkBox;
        @Bind(R.id.tvTitle)
        TextView tvTitle;
        @Bind(R.id.tvPriceCancel)
        TextView tvPriceCancel;
        @Bind(R.id.tvPriceNew)
        TextView tvPriceNew;
        @Bind(R.id.tvRemaining)
        TextView tvRemaining;
        @Bind(R.id.tvNumberPicker)
        TextView tvNumberPicker;
        @Bind(R.id.spinner)
        Spinner spinner;
        @Bind(R.id.lnr_main_row_offer1)
        LinearLayout lnr_main_row_offer;
        @Bind(R.id.lnrSpinner)
        RelativeLayout lnrSpinner;
        @Bind(R.id.lnrNumberPicker)
        LinearLayout lnrNumberPicker;
        @Bind(R.id.lnrMain)
        LinearLayout lnrMain;
        @Bind(R.id.lnrVoucherLocked)
        LinearLayout lnrVoucherLocked;
        @Bind(R.id.rlSubscribeHere)
        RelativeLayout rlSubscribeHere;

        @Bind(R.id.tvVoucherLocked)
        TextView tvVoucherLocked;
        @Bind(R.id.tvSubscribeDetail)
        TextView tvSubscribeDetail;
        @Bind(R.id.tvSubscribeHere)
        TextView tvSubscribeHere;

        @Bind(R.id.tags_container)
        LinearLayout tagsContainer;

        @Bind(R.id.horizontal_container)
        ConstraintLayout horizontalScrollView;


        @Bind(R.id.write_review_tv)
        TextView writeReviewTv;

        @Bind(R.id.show_review_tv)
        TextView showReviewTv;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    protected void dialog_Login(final Activity activity) {
        String mes;
        String textbtn;
        mes = "Login to continue";
        textbtn = "Login";

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, LandingActivity.class);
                activity.startActivity(intent);
                activity.finish();

            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(mes);
        ((Button) dialog.findViewById(R.id.okBtn)).setText(textbtn);

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);
        dialog.show();
    }

    public interface ClickListener {
        public void showWriteReview();

        public void showReviews();
    }
}

package com.predrinks.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.LinearLayout;

import com.predrinks.app.R;

public class PaymentSuccessActivity extends AppCompatActivity implements View.OnClickListener{

    LinearLayout lnrShare, lnrMyOffers;

    String shareMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);

        lnrShare = (LinearLayout) findViewById(R.id.lnrShare);
        lnrMyOffers = (LinearLayout) findViewById(R.id.lnrMyOffers);
        lnrShare.setOnClickListener(this);
        lnrMyOffers.setOnClickListener(this);

        String myUri = "http://onelink.to/rg8phe";
        Spanned link = Html.fromHtml("<a href=\""+myUri+"\">Link</a>");
        shareMessage = "I've just saved money with Confidentials App and you can too! Download Confidentials App today on your iPhone: https://goo.gl/78ymV1 , Android: https://goo.gl/YMXCVA";

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lnrShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, ""));
                break;
            case R.id.lnrMyOffers:
                MainActivity.isPaymentSuccess = true;
                setResult(RESULT_OK);
                finish();
                break;
        }
    }
}

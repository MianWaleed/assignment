package com.predrinks.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.predrinks.app.Confidentials;
import com.predrinks.app.Controller;
import com.predrinks.app.R;
import com.predrinks.app.fragment.BaseFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseFragmentActivity {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "LoginActivity";
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.activity_login)
    RelativeLayout activityLogin;

    @Bind(R.id.login_button)
    LoginButton mButtonLoginFaceBook;

    Context mContext;

    CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private FragmentActivity mActivity;

    private boolean isLoginInProcess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        etEmail.setText("lucyh@confidentials.com");
//        etPassword.setText("123456");

        mContext = this;
        mActivity = this;
        isLoginInProcess = false;
        callbackManager = CallbackManager.Factory.create();
        printKeyHash();
    }

    @Override
    public void onBackPressed() {
        if (!isLoginInProcess) {
            Intent intent = new Intent(this, LandingActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick({R.id.imgBack, R.id.llLogin, R.id.llFaceBookLogin, R.id.tvForgotPassword})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.imgBack:
                intent = new Intent(this, LandingActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.llLogin:
                BaseFragment.setQrCode(this, "");
                login();
                break;
            case R.id.llFaceBookLogin:
                onFaceBookClick();
                break;
            case R.id.tvForgotPassword:
                intent = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(intent);
                break;
        }
    }


    private void loadNewApiImplementation(final String personEmail, String password) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("email", personEmail);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "loadNewApiImplementation: " + jsonObject);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_Utils.LOGIN_USER, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "loadNewApiImplementation: onREs " + response);

//            Log.d(TAG, response.toString());
                if (pDialog != null)
                    pDialog.dismiss();
                isLoginInProcess = false;
                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        setEmail(LoginActivity.this, personEmail);
                        saveLoadedData(Constant.USERID, response.getString("userId"));
                        setLoggedIn(true);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        String message = response.getString("message");
                        dialog_info("Confidentials", "Bad username/password, please try again");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "loadNewApiImplementation: error" + error);

            }
        });
        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    String personEmail, password;
    String deviceID = "0", deviceUDID;
    String longitude = "0.0";
    String latitude = "0.0";
    ProgressDialog pDialog;

    private void login() {
        if (!haveNetworkConnection(this)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        deviceUDID = getLoadedData("DEVICE_UDID");
        deviceID = getLoadedData(Confidentials.REG_ID);
        GetLocation location = new GetLocation(this);
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());

        personEmail = etEmail.getText().toString();
        password = etPassword.getText().toString().trim();


        if (TextUtils.isEmpty(personEmail)) {
            Snackbar.make(activityLogin, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(personEmail).matches()) {
            Snackbar.make(activityLogin, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (password == null || password.isEmpty()) {
            Snackbar.make(activityLogin, "Please enter a password", Snackbar.LENGTH_SHORT).show();
            return;
        }
        isLoginInProcess = true;

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

//        Old implementation for the Azure
//        String URL = URL_Utils.LOGIN_USER + "?" + URL_Utils.PARAM_EMAIL_ID + "=" + personEmail + "&" + URL_Utils.PARAM_PASSWORD + "=" + password + "&" + URL_Utils.PARAM_IOS_APP_VERSION + "=" + "" + "&" + URL_Utils.PARAM_ANDROID_APP_VERSION + "=" + Controller.APP_VERSION_NAME + "&" + URL_Utils.PARAM_DEVICE_TYPE + "=" + getString(R.string.deviceType) + "&" + URL_Utils.PARAM_DEVICE_ID + "=" + deviceID + "&" + URL_Utils.PARAM_UDID + "=" + deviceUDID;
//        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
//        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

//        New Implementation for Akkas Demo
        loadNewApiImplementation(personEmail, password);
    }


    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (pDialog != null)
                pDialog.dismiss();

            isLoginInProcess = false;
            try {
                Log.d(TAG, error.toString());
            } catch (RuntimeException e) {
            }
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, "loadNewApiImplementation: onREs " + response);

            if (pDialog != null)
                pDialog.dismiss();
            isLoginInProcess = false;
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    saveLoadedData(Constant.USERID, response.getString("userId"));
                    setLoggedIn(true);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    String message = response.getString("message");
                    dialog_info("Confidentials", "Bad username/password, please try again");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void onFaceBookClick() {
        isLoginInProcess = true;
        mButtonLoginFaceBook.performClick();
        mButtonLoginFaceBook.setReadPermissions("public_profile", "email");
        mButtonLoginFaceBook.registerCallback(callbackManager, callback);
    }


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            AccessToken accessToken = loginResult.getAccessToken();
            getProfileFaceBook(accessToken);
        }

        @Override
        public void onCancel() {
            isLoginInProcess = false;
            Log.v("LoginActivity", "cancel");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d(TAG, "onError: " + e.toString());
            e.printStackTrace();
            isLoginInProcess = false;
        }
    };

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("YOUR PACKAGE NAME", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }

    String firstName, lastName, email, gender = "", birthday = "";

    private void getProfileFaceBook(AccessToken accessToken) {
        Log.d(TAG, "getProfileFaceBook: " + accessToken.toString());
        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString());
                        try {
                            firstName = response.getJSONObject().getString("first_name");
                            lastName = response.getJSONObject().getString("last_name");
                            email = response.getJSONObject().getString("email");
                            gender = response.getJSONObject().getString("gender");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        LoginManager.getInstance().logOut();
                        onLoginService();
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, name, email, gender, birthday"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void onLoginService() {
        Log.d(TAG, "onLoginService: " + firstName + "   " + email);
        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(email)) {
            doFacebookRegister();
        } else {
            isLoginInProcess = false;
            showErrorDialogAll(mActivity, "Uh oh!", "An error occurred, we were unable to create your account. Please try again");
        }
    }

    private void doFacebookRegister() {
        if (!haveNetworkConnection(this)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            isLoginInProcess = false;
            return;
        }

        try {
            pDialog = new ProgressDialog(mActivity);
            pDialog.setMessage("Loading...");
            pDialog.show();
        } catch (WindowManager.BadTokenException e) {
        }


        deviceUDID = getLoadedData("DEVICE_UDID");
        deviceID = getLoadedData(Confidentials.REG_ID);
        GetLocation location = new GetLocation(this);
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());

        Map<String, String> params = new HashMap<String, String>();
        params.put(URL_Utils.PARAM_FIRST_NAME, firstName);
        params.put(URL_Utils.PARAM_LAST_NAME, lastName);
        params.put(URL_Utils.PARAM_DOB, birthday);
        params.put(URL_Utils.PARAM_EMAIL_ID, email);

        params.put(URL_Utils.PARAM_GENDER, gender);
        params.put(URL_Utils.PARAM_PROFILE_PIC, "");
        params.put(URL_Utils.PARAM_REGISTRATION_MODE, "Facebook");
        params.put(URL_Utils.PARAM_PASSWORD, "");
        params.put(URL_Utils.PARAM_IOS_APP_VERSION, "");
        params.put(URL_Utils.PARAM_ANDROID_APP_VERSION, Controller.APP_VERSION_NAME);
        params.put(URL_Utils.PARAM_DEVICE_TYPE, getString(R.string.deviceType));
        params.put(URL_Utils.PARAM_DEVICE_ID, deviceID);
        params.put(URL_Utils.PARAM_UDID, deviceUDID);
        params.put(URL_Utils.PARAM_LATITUDE, latitude);
        params.put(URL_Utils.PARAM_LONGITUDE, longitude);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL_Utils.INSERT_USER, params, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

}

package com.predrinks.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Opening_Times_Fragment extends BaseFragment  {
	
	private final String TAG = "Opening Times";
	private boolean isDataLoaded = false;
	FragmentActivity mActivity;
	ImageView imgMain;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		isDataLoaded = false;
	}
	JSONObject jsonObject;
	String image;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (FragmentActivity) getActivity();

		Bundle bundle = getArguments();
		String json =  bundle.getString("jsonObject");
		try {
			jsonObject = new JSONObject(json);
			image = bundle.getString("image");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	View view;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = (LinearLayout)inflater.inflate(R.layout.opening_times, container, false);

		MainActivity.setHeaderTitle("OPENING TIMES");
		bindLinearLayout();
		bindTextView();


		if (jsonObject != null){

			if (TextUtils.isEmpty(image)){

			}
			String monStartTime = getJsonString(jsonObject, "monStartTime");
			String monEndTime = getJsonString(jsonObject, "monEndTime");

			String tueStartTime = getJsonString(jsonObject, "tueStartTime");
			String tueEndTime = getJsonString(jsonObject, "tueEndTime");

			String wedStartTime = getJsonString(jsonObject, "wedStartTime");
			String wedEndTime = getJsonString(jsonObject, "wedEndTime");

			String thrStartTime = getJsonString(jsonObject, "thrStartTime");
			String thrEndTime = getJsonString(jsonObject, "thrEndTime");

			String friStartTime = getJsonString(jsonObject, "friStartTime");
			String friEndTime = getJsonString(jsonObject, "friEndTime");

			String satStartTime = getJsonString(jsonObject, "satStartTime");
			String satEndTime = getJsonString(jsonObject, "satEndTime");

			String sunStartTime = getJsonString(jsonObject, "sunStartTime");
			String sunEndTime = getJsonString(jsonObject, "sunEndTime");

			setTime("Monday", 	 monStartTime, monEndTime);
			setTime("Tuesday", 	 tueStartTime, tueEndTime);
			setTime("Wednesday", wedStartTime, wedEndTime);
			setTime("Thursday",  thrStartTime, thrEndTime);
			setTime("Friday", 	 friStartTime, friEndTime);
			setTime("Saturday",  satStartTime, satEndTime);
			setTime("Sunday", 	 sunStartTime, sunEndTime);

			Glide.with(mActivity)
					.load(URL_Utils.URL_IMAGE_DNLOAD + image)
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.error(R.mipmap.default_event_venue_image)
					.into(imgMain);
		}
		
		return view;
	}
	
	private LinearLayout openingTimesLayout;
	private void bindLinearLayout() {
		openingTimesLayout = (LinearLayout) view.findViewById(R.id.openingTimesLayout);
		imgMain = (ImageView) view.findViewById(R.id.imgMain);
	}
	
	private TextView mondayOpeningTime,
			 tuesdayOpeningTime,
			 wednesdayOpeningTime,
			 thursdayOpeningTime,
			 fridayOpeningTime,
			 saturdayOpeningTime,
			 sundayOpeningTime;
	private Map<String, TextView> dayTextMap;
	private void bindTextView() {
		dayTextMap = new HashMap<String, TextView>();
		
		mondayOpeningTime		= (TextView) view.findViewById(R.id.mondayOpeningTime	);
		tuesdayOpeningTime      = (TextView) view.findViewById(R.id.tuesdayOpeningTime   );
		wednesdayOpeningTime    = (TextView) view.findViewById(R.id.wednesdayOpeningTime );
		thursdayOpeningTime     = (TextView) view.findViewById(R.id.thursdayOpeningTime  );
		fridayOpeningTime       = (TextView) view.findViewById(R.id.fridayOpeningTime    );
		saturdayOpeningTime     = (TextView) view.findViewById(R.id.saturdayOpeningTime  );
		sundayOpeningTime       = (TextView) view.findViewById(R.id.sundayOpeningTime    );
		
		dayTextMap.put("Monday", 	mondayOpeningTime);
		dayTextMap.put("Tuesday", 	tuesdayOpeningTime);
		dayTextMap.put("Wednesday", wednesdayOpeningTime);
		dayTextMap.put("Thursday", 	thursdayOpeningTime);
		dayTextMap.put("Friday", 	fridayOpeningTime);
		dayTextMap.put("Saturday", 	saturdayOpeningTime);
		dayTextMap.put("Sunday", 	sundayOpeningTime);
	}
	
	private void setTime(String day, String from, String to) {
		String timings = from + " - " + to;
		TextView dayView  = dayTextMap.get(day); 


		dayView.setText(timings);
		dayView.setTextColor(getResources().getColor(R.color.color_opening_time_time_label));
	}
	

}

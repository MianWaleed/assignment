package com.predrinks.app.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;

import static android.support.constraint.Constraints.TAG;

public class ConfidentialsPassFragment extends DialogFragment {

    public ConfidentialsPassFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_confidentials_pass_dialog, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), R.style.Confidentials_Pass_WideDialog);
    }

    ImageView qrCodeIv;
    private ConstraintLayout isNotAHero;
    private ConstraintLayout isAHeroLayout;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: " + BaseFragment.getQrCode(getActivity()));

        isNotAHero = view.findViewById(R.id.is_not_a_hero);
        isAHeroLayout = view.findViewById(R.id.is_a_hero_layout);

        qrCodeIv = view.findViewById(R.id.qr_code_iv);
        if (BaseFragment.getQrCode(getActivity()).trim().length() != 0) {
            isAHeroLayout.setVisibility(View.VISIBLE);
            isNotAHero.setVisibility(View.GONE);
            Glide.with(getActivity())
                    .load(BaseFragment.getQrCode(getActivity()))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_qr_code)
                    .into(qrCodeIv);
        } else {
            isNotAHero.setVisibility(View.VISIBLE);
            isAHeroLayout.setVisibility(View.GONE);
        }

        super.onViewCreated(view, savedInstanceState);
    }
}

package com.predrinks.app.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendGiftActivity extends BaseFragmentActivity {

    private static final String TAG = "SendGiftActivity";
    @Bind(R.id.imgBack)
    ImageView imgBack;
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etMessage)
    TextInputEditText etMessage;
    @Bind(R.id.tvItem)
    TextView tvItem;
    @Bind(R.id.tvVenue)
    TextView tvVenue;
    @Bind(R.id.activity_send_gift)
    RelativeLayout activitySendGift;
    private ProgressDialog pDialog;
    private Activity mActivity;

    String myOfferID, offerTitle, venueTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_gift);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("myOfferID")){
            myOfferID = getIntent().getStringExtra("myOfferID");
            offerTitle = getIntent().getStringExtra("offerTitle");
            venueTitle = getIntent().getStringExtra("venueTitle");
        }

        tvItem.setText(offerTitle);
        tvVenue.setText(venueTitle);

        mActivity = this;
    }

    @OnClick({R.id.imgBack, R.id.btnSend})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.btnSend:
                sendGifttoMail();
                break;
        }
    }

    String email, message, mUserId;

    private void sendGifttoMail() {
        email = etEmail.getText().toString();
        message = etMessage.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Snackbar.make(activitySendGift, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Snackbar.make(activitySendGift, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(message)) {
            Snackbar.make(activitySendGift, "Please enter a message", Snackbar.LENGTH_LONG).show();
            return;
        }

        if (pDialog == null) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Loading...");
            pDialog.show();
        }

        mUserId = "0";
        if (isLoggedIn()) {
            mUserId = getLoadedData(Constant.USERID);
        }


        Map<String, String> params = new HashMap<String, String>();
        params.put(URL_Utils.PARAM_USERID, mUserId);
        params.put(URL_Utils.PARAM_MYOFFER_ID, myOfferID);
        params.put(URL_Utils.PARAM_TO_EMAIL_ID, email);
        params.put(URL_Utils.PARAM_MESSAGE, message);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL_Utils.EMAIL_MYOFFER, params, createSuccessListener, createErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }


    Response.ErrorListener createErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (pDialog != null)
                pDialog.dismiss();
            try {
                Log.d(TAG, error.toString());
            } catch (RuntimeException e) {
            }
        }
    };

    Response.Listener<JSONObject> createSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            if (pDialog != null)
                pDialog.dismiss();
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    Intent intent = new Intent(mActivity, GiftSendSuccessActivity.class);
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();
                } else {
                    String message = response.getString("message");
                    dialog_info("Confidentials", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}

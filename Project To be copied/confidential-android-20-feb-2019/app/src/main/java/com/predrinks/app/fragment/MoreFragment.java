package com.predrinks.app.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.Confidentials;
import com.predrinks.app.Controller;
import com.predrinks.app.R;
import com.predrinks.app.activity.LandingActivity;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.activity.NotificationActivity;
import com.predrinks.app.activity.PrivacyPolicyActivity;
import com.predrinks.app.activity.ProfileActivity;
import com.predrinks.app.adapter.MoreAdapter;
import com.predrinks.app.bus.MessageEvent;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.predrinks.app.activity.MainActivity.setBackButton;
import static com.predrinks.app.activity.MainActivity.setHeaderTitle;
import static com.predrinks.app.database.PreDrinkOrderDAO.DATABASE_NAME;
import static com.predrinks.app.widget.WheelView.TAG;


public class MoreFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.imgDesapLogo)
    ImageView imgDesapLogo;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvApp)
    TextView tvApp;
    @Bind(R.id.tvAppVersion)
    TextView tvAppVersion;
    String[] mNames = new String[]{"Offer Notifications settings", "My Account", "Support", /*"Live Customer Support",*/
            "Your Feedback", "Privacy Policy", "Terms & Conditions", "Fees", "Logout"};
    String[] mNamesNew = new String[]{"Offer Notifications settings", "My Account", "Support",
            "Your Feedback", "Privacy Policy", "Terms & Conditions", "Fees", "Login"};

    private String mParam1;
    private String mParam2;

    Context mContext;
    Activity mActivity;
    private SharedPreferences mPrefs;


    public MoreFragment() {
    }

    public static MoreFragment newInstance(String param1, String param2) {
        MoreFragment fragment = new MoreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mActivity = getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mPrefs = mActivity.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);

        Date currentTime = Calendar.getInstance().getTime();

        Log.d(TAG, "onCreate: " + currentTime.toString());
        Log.d(TAG, "onCreate: " + currentTime.getHours());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, view);
        MoreAdapter adapter;
        if (isLoggedIn(mContext)) {
            adapter = new MoreAdapter(mContext, mNames);
        } else {
            adapter = new MoreAdapter(mContext, mNamesNew);
        }
        listView.setAdapter(adapter);
        MainActivity.setListViewHeightBasedOnChildren(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                setFragment(i);
            }
        });

        tvTitle.setTypeface(MainActivity.getTypeFaceRegular());
        tvApp.setTypeface(MainActivity.getTypeFaceRegular());

        tvApp.setText("Confidentials v" + Controller.APP_VERSION_NAME);
        //tvApp.setText("Confidentials v"+ "2.2");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.header_layout_inner.setVisibility(View.VISIBLE);
        setBackButton(false);
        MainActivity.setVisibleHeaderRightMap(false);
        setHeaderTitle("MORE");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean isnotification = mPrefs.getBoolean(Constant.NOTIFICATIONS, true);
                String userId = getLoadedData(mContext, Constant.USERID);
                // updateUserDetail(userId, isnotification, MainActivity.getLocation.getLatitude(), MainActivity.getLocation.getLongitude());
            }
        }, 1000);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    String deviceID = "0", deviceUDID;

    private void setFragment(int pos) {
        Log.d(TAG, "setFragment: pos => " + pos);
        if (isLoggedIn(mContext)) {
            if (pos == 0) {
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                intent.putExtra("title", "NOTIFICATIONS");
                startActivity(intent);
            } else if (pos == 1) {
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("title", "MY ACCOUNT");
                startActivity(intent);
            } else if (pos == 2) {
                String extraDetail = "Device Info \n";
                String userdetail = getLoadedData(mActivity, Constant.USER_DETAIL);
                if (!TextUtils.isEmpty(userdetail)) {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(userdetail);
                        String personFirstName = object.getString("firstName");
                        String personLastName = object.getString("lastName");
                        String fName = "", lName = "";
                        String personEmail = object.getString("emailId");
                        try {
                            fName = personFirstName.substring(0, 1).toUpperCase() + personFirstName.substring(1);
                            lName = personLastName.substring(0, 1).toUpperCase() + personLastName.substring(1);
                        } catch (IndexOutOfBoundsException e) {
                        }
                        try {
                            extraDetail += "Name: " + fName + " " + lName + "\n";
                            extraDetail += "E-Mail: " + personEmail + "\n";
                            extraDetail += "Device: " + Build.MODEL + "\n";
                            extraDetail += "OS Version: Android " + android.os.Build.VERSION.RELEASE + "\n";
                            PackageInfo pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                            String versionName = Controller.APP_VERSION_NAME;
                            extraDetail += "App Version: " + versionName + "\n\n\n";
                            onSendMail2("vouchers@confidentials.com", "Confidentials - Support Query", extraDetail);

                        } catch (Exception e) {
                            e.getStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

//            } else if (pos == 3) {
//                Customerly.registerUser(BaseFragmentActivity.getEmail(mContext));
//                Log.d(TAG, "setFragment: " + BaseFragmentActivity.getEmail(mContext));
//                Customerly.openSupport(getActivity());
            } else if (pos == 3) {
                onSendMail2("vouchers@confidentials.com", "Confidentials - Feedback", "");
            } else if (pos == 4) {
                String title = "PRIVACY POLICY";
                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                intent.putExtra("param1", "0");
                intent.putExtra("title", title);
                startActivity(intent);
            } else if (pos == 5) {
                String title = "TERMS & CONDITIONS";
                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                intent.putExtra("param1", "1");
                intent.putExtra("title", title);
                startActivity(intent);
            } else if (pos == 6) {
                String title = "Transaction Fees";
                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                intent.putExtra("param1", "2");
                intent.putExtra("title", title);
                startActivity(intent);
            } else if (pos == 7) {
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                String userId = getLoadedData(mContext, Constant.USERID);
                dialog_info(userId);
            }
        } else {
            if (pos == 0) {
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                intent.putExtra("title", "NOTIFICATIONS");
                startActivity(intent);
            } else if (pos == 1) {
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("title", "MY ACCOUNT");
                startActivity(intent);
            } else if (pos == 2) {
                String extraDetail = "Device Info \n";
                String userdetail = getLoadedData(mActivity, Constant.USER_DETAIL);
                if (!TextUtils.isEmpty(userdetail)) {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(userdetail);
                        String personFirstName = object.getString("firstName");
                        String personLastName = object.getString("lastName");
                        String fName = "", lName = "";
                        String personEmail = object.getString("emailId");
                        try {
                            fName = personFirstName.substring(0, 1).toUpperCase() + personFirstName.substring(1);
                            lName = personLastName.substring(0, 1).toUpperCase() + personLastName.substring(1);
                        } catch (IndexOutOfBoundsException e) {
                        }
                        try {
                            extraDetail += "Name: " + fName + " " + lName + "\n";
                            extraDetail += "E-Mail: " + personEmail + "\n";
                            extraDetail += "Device: " + Build.MODEL + "\n";
                            extraDetail += "OS Version: Android " + android.os.Build.VERSION.RELEASE + "\n";
                            PackageInfo pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                            String versionName = Controller.APP_VERSION_NAME;
                            extraDetail += "App Version: " + versionName + "\n\n\n";
                            onSendMail2("vouchers@confidentials.com", "Confidentials - Support Query", extraDetail);

                        } catch (Exception e) {
                            e.getStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } else if (pos == 3) {
                onSendMail2("vouchers@confidentials.com", "Confidentials - Feedback", "");
            } else if (pos == 4) {
                String title = "PRIVACY POLICY";
                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                intent.putExtra("param1", "0");
                intent.putExtra("title", title);
                startActivity(intent);
            } else if (pos == 5) {
                String title = "TERMS & CONDITIONS";
                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                intent.putExtra("param1", "1");
                intent.putExtra("title", title);
                startActivity(intent);
            } else if (pos == 6) {
                String title = "Transaction Fees";
                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                intent.putExtra("param1", "2");
                intent.putExtra("title", title);
                startActivity(intent);
            } else if (pos == 7) {
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                String userId = getLoadedData(mContext, Constant.USERID);
                dialog_info(userId);
            }
        }
    }

    protected void dialog_info(final String userId) {
        final Dialog dialog = new Dialog(mActivity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLogout(userId);
                dialog.dismiss();
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText("Are you sure you want to logout?");
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Logout");

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);
        dialog.show();
    }

    protected void getLogout(String userId) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("More", error.toString());
                try {
                    deviceID = getLoadedData(mContext, Confidentials.REG_ID);
                    deviceUDID = getLoadedData(mContext, "DEVICE_UDID");
                    clearAppData(mContext);
                    mContext.deleteDatabase(DATABASE_NAME);

                    saveLoadedData(mContext, "DEVICE_UDID", deviceUDID);
                    saveLoadedData(mContext, Confidentials.REG_ID, deviceID);
                    Intent intent = new Intent(mActivity, LandingActivity.class);
                    startActivity(intent);
                    if (mActivity != null)
                        mActivity.finish();
                } catch (NullPointerException e) {
                }
            }
        };
        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("More", response.toString());
                try {
                    deviceID = getLoadedData(mContext, Confidentials.REG_ID);
                    deviceUDID = getLoadedData(mContext, "DEVICE_UDID");
                    clearAppData(mContext);
                    mContext.deleteDatabase(DATABASE_NAME);

                    saveLoadedData(mContext, "DEVICE_UDID", deviceUDID);
                    saveLoadedData(mContext, Confidentials.REG_ID, deviceID);
                    Intent intent = new Intent(mActivity, LandingActivity.class);
                    startActivity(intent);
                    if (mActivity != null)
                        mActivity.finish();
                } catch (NullPointerException e) {
                }

            }
        };
        String URL = URL_Utils.USER + "/" + userId + "/logout";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void onSendMail2(String email, String subject, String extraDetail) {

        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, extraDetail);

        final PackageManager pm = getActivity().getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EventBus.getDefault().post(new MessageEvent(true));

//        isMapClick = false;
//        isMapHomeClick = false;
//        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        setBackButton(false);
//        setHeaderTitle("MORE");
//        MainActivity.isMapClick = false;
//        MainActivity.isMapHomeClick = false;
//        MainActivity.tabHome.setBackgroundColor(Color.TRANSPARENT);
//        MainActivity.tabSearch.setBackgroundColor(Color.TRANSPARENT);
//        MainActivity.tabVoucher.setBackgroundColor(Color.TRANSPARENT);
//        MainActivity.tabFavourites.setBackgroundColor(Color.TRANSPARENT);
//        MainActivity.tabMore.setBackgroundColor(Color.WHITE);

//        try {
//            new Handler().postDelayed(() -> {
//                InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputManager.hideSoftInputFromWindow(Objects.requireNonNull(mActivity.getCurrentFocus()).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            }, 300);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}

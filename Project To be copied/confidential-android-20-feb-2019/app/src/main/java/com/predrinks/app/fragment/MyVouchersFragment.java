package com.predrinks.app.fragment;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.adapter.VoucherListAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.GetLocation;
import com.predrinks.app.utils.URL_Utils;
import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.client.SessionConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MyVouchersFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "MyVouchersFragment";
    private static final int ACCESS_FINE_LOCATION = 123;

    @Bind(R.id.tvSearch)
    TextView tvSearch;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.purchased_bt)
    Button purchasedBt;
    @Bind(R.id.redeemed_bt)
    Button redeemedBt;
    @Bind(R.id.expired_bt)
    Button expiredBt;
    @Bind(R.id.gifted_bt)
    Button giftedBt;

    @Bind(R.id.empty_tv)
    TextView emptyTv;

    private String mParam1;
    private String mParam2;
    Activity mActivity;
    Context mContext;
    Paint paint;
    String mUserId = "";
    private String mResponse;
    private GetLocation getLocation;

    private ConstraintLayout giftedFiltersContainer;
    private TextView sentGiftsTv;
    private TextView receviedGiftsTv;
    private TextView filterSelectedLayout;
    JSONArray myEventList = null;
    VoucherListAdapter clubListAdapter;

    public static int WHAT_IT_IS = 0;
    public static int PURCHASED = 0;
    public static int REDEEMED = 1;
    public static int EXPIRED = 2;
    public static int GIFTED = 3;
    public static int SENT = 4;
    public static int RECEIVED = 5;


    public static MyVouchersFragment newInstance(String param1, String param2) {
        MyVouchersFragment fragment = new MyVouchersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mUserId = getLoadedData(mContext, Constant.USERID);

        getLocation = new GetLocation(mContext);

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);
        SessionConfiguration config = new SessionConfiguration.Builder()
                .setClientId("rxyBBENyaBtmWkMqJVXozGt9AsWw1ufn")
                .setServerToken("Eerti8j_RYlFLM4JYC38G7QdOHSEFxt93L9VqMre")
                .setScopes(Arrays.asList(Scope.PROFILE))
                .setEnvironment(SessionConfiguration.Environment.SANDBOX)
                .build();
        UberSdk.initialize(config);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isReadStorageAllowed()) {
                requestLocationPermission();
            }
        }

        if (!haveNetworkConnection(mContext)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
    }

    private void getDataFromWeb() {
        String URL = URL_Utils.USER + "/" + mUserId + "/purchased/events";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            Log.d(TAG, "onResponse: " + response);
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            try {
                String success = response.getString("status");
                if (success.equals("1")) {
                    mResponse = response.toString();
                    updateUI(mResponse);
                    saveLoadedData(mActivity, "purchasedEvents", mResponse);
                } else {
                    saveLoadedData(mActivity, "purchasedEvents", response.toString());
                    updateUI(response.toString());
                }

                purchasedBt.setBackgroundColor(getResources().getColor(R.color.white));
                purchasedBt.setTextColor(getResources().getColor(R.color.black));

                giftedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
                giftedBt.setTextColor(getResources().getColor(R.color.white));

                expiredBt.setBackgroundColor(getResources().getColor(R.color.app_color));
                expiredBt.setTextColor(getResources().getColor(R.color.white));

                redeemedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
                redeemedBt.setTextColor(getResources().getColor(R.color.white));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    private void updateUI(String response) {
        try {
            JSONObject responseObject = new JSONObject(response);
            myEventList = responseObject.getJSONArray(Constant.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myEventList == null || myEventList.length() == 0) {
            if (lnrEmpty != null) {
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            return;
        } else {
            if (lnrEmpty != null) {
                lnrEmpty.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        }
        if (recyclerView == null)
            return;

        getPurchasedOnly(myEventList);

//        clubListAdapter = new VoucherListAdapter(this, mActivity, mContext, getPurchasedOnly(myEventList));
//        recyclerView.setAdapter(clubListAdapter);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void getPurchasedOnly(JSONArray myEventList) {
        WHAT_IT_IS = PURCHASED;

        JSONArray j = new JSONArray();
        try {
            for (int i = 0; i < myEventList.length(); ++i) {
                JSONObject obj = myEventList.getJSONObject(i);
                boolean isExpired = obj.getBoolean("isExpired");
                boolean isActive = obj.getBoolean("isActive");
                boolean isRedeemed = obj.getBoolean("isRedeemed");
                if (!isRedeemed && !isExpired && isActive) {
                    j.put(obj);
                }
            }
        } catch (JSONException e) {
            // handle exception
        }

        loadNewTab(j, "WHEN YOU HAVE REDEEMED OFFERS,\nYOU WILL SEE THEM HERE.\n\nLET US HELP YOU PRESTART\nYOUR NIGHT");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        String call = data.getStringExtra("call");
        Fragment fragment;
        if (call.equals("OurOffersFragment")) {
            String VenueId = data.getStringExtra("venueId");
            fragment = OurOffersFragment.newInstance(VenueId, "");
            MainActivity.loadContentFragmentWithBack(fragment, false);
        } else if (call.equals("GetDirectionMapFragment")) {
            String venueresp = data.getStringExtra("venueresp");
            String lat = "0", lon = "0", title = "", subtitle = "", add1 = "", add2 = "";
            try {
                JSONObject object = new JSONObject(venueresp);
                lat = object.getString(Constant.LATITUDE);
                lon = object.getString(Constant.LONGITUDE);
                add1 = object.getString("address1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(add1)) {
                String myAdd = GetAddress(mContext, getLocation.getLatitude(), getLocation.getLongitude());
                showDirections(mContext, myAdd, add1);
            }
        } else if (call.equals("ClubDetailFragment")) {
            String venueId = data.getStringExtra("venueId");
            String title = data.getStringExtra("name");
            fragment = ClubDetailFragment.newInstance(venueId, title);
            MainActivity.loadContentFragmentWithBack(fragment, false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    LinearLayout filterLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_vouchers, container, false);
        initView(view);
        ButterKnife.bind(this, view);
        tvSearch.setPaintFlags(paint.getFlags());
        recyclerView.setVerticalScrollBarEnabled(false);
        filterLayout = view.findViewById(R.id.filters_layout);

        if (isLoggedIn(mContext)) {
            mResponse = getLoadedData(mActivity, "purchasedEvents");
            if (!TextUtils.isEmpty(mResponse)) {
                updateUI(mResponse);
            }
        } else {
            if (lnrEmpty != null) {
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
        swipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (isLoggedIn(mContext)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(true);
                    getDataFromWeb();
                }
            }, 500);

        } else {
            if (lnrEmpty != null) {
                filterLayout.setVisibility(View.GONE);
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
        MainActivity.header_layout_inner.setVisibility(View.VISIBLE);
        MainActivity.setBackButton(false);
        MainActivity.setVisibleHeaderRightMap(false);
        MainActivity.setHeaderTitle("MY PURCHASES");
    }

    @Override
    public void onPause() {
        VolleySingleton.getInstance().cancelPendingRequests("");
        super.onPause();
    }

    @Override
    public void onRefresh() {
        giftedFiltersContainer.setVisibility(View.GONE);
        if (isLoggedIn(mContext))
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(true);
                getDataFromWeb();
            } else {
                if (lnrEmpty != null) {
                    lnrEmpty.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.tvSearch)
    public void onClick() {
        MainActivity.setSearchTabsClick();
    }


    private boolean isReadStorageAllowed() {
        int result = ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        return false;
    }

    @OnClick(R.id.purchased_bt)
    public void purchasedTab() {
        giftedFiltersContainer.setVisibility(View.GONE);

        WHAT_IT_IS = PURCHASED;

        purchasedBt.setBackgroundColor(getResources().getColor(R.color.white));
        purchasedBt.setTextColor(getResources().getColor(R.color.black));

        giftedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        giftedBt.setTextColor(getResources().getColor(R.color.white));

        expiredBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        expiredBt.setTextColor(getResources().getColor(R.color.white));

        redeemedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        redeemedBt.setTextColor(getResources().getColor(R.color.white));

        JSONArray j = new JSONArray();
        if (myEventList.length() > 0) {

            try {
                for (int i = 0; i < myEventList.length(); ++i) {
                    JSONObject obj = myEventList.getJSONObject(i);
                    boolean isExpired = obj.getBoolean("isExpired");
                    boolean isActive = obj.getBoolean("isActive");
                    boolean isRedeemed = obj.getBoolean("isRedeemed");
                    if (!isRedeemed && !isExpired && isActive) {
                        j.put(obj);
                    }
                }
            } catch (JSONException e) {
                // handle exception
            }

            loadNewTab(j, "WHEN YOU HAVE REDEEMED OFFERS,\nYOU WILL SEE THEM HERE.\n\nLET US HELP YOU PRESTART\nYOUR NIGHT");
        }
    }

    @OnClick(R.id.expired_bt)
    public void expiredTab() {
        giftedFiltersContainer.setVisibility(View.GONE);

        WHAT_IT_IS = EXPIRED;

        purchasedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        purchasedBt.setTextColor(getResources().getColor(R.color.white));

        giftedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        giftedBt.setTextColor(getResources().getColor(R.color.white));

        expiredBt.setBackgroundColor(getResources().getColor(R.color.white));
        expiredBt.setTextColor(getResources().getColor(R.color.black));

        redeemedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        redeemedBt.setTextColor(getResources().getColor(R.color.white));

        JSONArray j = new JSONArray();
        if (myEventList.length() > 0) {

            try {
                for (int i = 0; i < myEventList.length(); ++i) {
                    JSONObject obj = myEventList.getJSONObject(i);
                    boolean isExpired = obj.getBoolean("isExpired");
//                    boolean isActive = obj.getBoolean("isActive");
//                    boolean isRedeemed = obj.getBoolean("isRedeemed");

//                    !isRedeemed && !isActive &&
                    if (isExpired) {
                        j.put(obj);
                    }
                }
            } catch (JSONException e) {
                // handle exception
            }

            loadNewTab(j, "WHEN YOU HAVE EXPIRED OFFERS,\nYOU WILL SEE THEM HERE.");
        }
    }

    @OnClick(R.id.gifted_bt)
    public void giftedTab() {
        WHAT_IT_IS = GIFTED;

        sentGiftsTv.performClick();
        giftedFiltersContainer.setVisibility(View.VISIBLE);

        purchasedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        purchasedBt.setTextColor(getResources().getColor(R.color.white));

        giftedBt.setBackgroundColor(getResources().getColor(R.color.white));
        giftedBt.setTextColor(getResources().getColor(R.color.black));

        expiredBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        expiredBt.setTextColor(getResources().getColor(R.color.white));

        redeemedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        redeemedBt.setTextColor(getResources().getColor(R.color.white));

        JSONArray j = new JSONArray();
        if (myEventList.length() > 0) {

            try {
                for (int i = 0; i < myEventList.length(); ++i) {
                    JSONObject obj = myEventList.getJSONObject(i);
                    boolean isGifted = obj.getBoolean("isGifted");

                    if (isGifted) {
                        j.put(obj);
                    }
                }
            } catch (JSONException e) {
                // handle exception
            }

            loadNewTab(j, "WHEN YOU HAVE GIFTED OFFERS,\nYOU WILL SEE THEM HERE.");
        }
    }

    @OnClick(R.id.redeemed_bt)
    public void redeemedTab() {
        giftedFiltersContainer.setVisibility(View.GONE);

        WHAT_IT_IS = REDEEMED;

        purchasedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        purchasedBt.setTextColor(getResources().getColor(R.color.white));

        giftedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        giftedBt.setTextColor(getResources().getColor(R.color.white));

        expiredBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        expiredBt.setTextColor(getResources().getColor(R.color.white));

        redeemedBt.setBackgroundColor(getResources().getColor(R.color.white));
        redeemedBt.setTextColor(getResources().getColor(R.color.black));

        JSONArray j = new JSONArray();

        if (myEventList.length() > 0) {
            try {
                for (int i = 0; i < myEventList.length(); ++i) {
                    JSONObject obj = myEventList.getJSONObject(i);
                    boolean isRedeemed = obj.getBoolean("isRedeemed");
                    if (isRedeemed) {
                        j.put(obj);
                    }
                }
            } catch (JSONException e) {
                // handle exception
            }

            loadNewTab(j, "WHEN YOU HAVE REDEEMED OFFERS,\nYOU WILL SEE THEM HERE.");
        }
    }

    private void loadNewTab(JSONArray myEventList, String message) {
        if (myEventList == null || myEventList.length() == 0) {
            if (lnrEmpty != null) {
                emptyTv.setText(message);
                lnrEmpty.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            return;
        } else {
            if (lnrEmpty != null) {
                lnrEmpty.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        }
        if (recyclerView == null)
            return;

        JSONArray jsonArray = new JSONArray();
        Log.d(TAG, "loadNewTab: " + jsonArray.toString());

        for (int i = 0; i < myEventList.length(); i++) {
            try {
                JSONObject eventObj = myEventList.getJSONObject(i);
                boolean uniqueEvent = true;

                for (int j = 0; j < jsonArray.length(); j++) {
                    JSONObject addedEventObj = jsonArray.getJSONObject(j);
                    if (addedEventObj.getInt("eventID") == (eventObj.getInt("eventID"))) {
                        uniqueEvent = false;
                        break;
                    }
                }

                if (uniqueEvent) {
                    jsonArray.put(eventObj);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        clubListAdapter = new VoucherListAdapter(this, mActivity, mContext, jsonArray);
        recyclerView.setAdapter(clubListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestLocationPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
        }
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    private void initView(View view) {
        giftedFiltersContainer = view.findViewById(R.id.gifted_filters_container);
        sentGiftsTv = view.findViewById(R.id.sent_gifts_tv);
        receviedGiftsTv = view.findViewById(R.id.recevied_gifts_tv);

        sentGiftsTv.setOnClickListener(this);
        receviedGiftsTv.setOnClickListener(this);

        filterSelectedLayout = view.findViewById(R.id.filter_selected_layout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sent_gifts_tv: {
                sentGiftsTv.setTextColor(getActivity().getResources().getColor(R.color.white));
                receviedGiftsTv.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                TransitionManager.beginDelayedTransition(giftedFiltersContainer, new ChangeBounds());
                changeIndicator(sentGiftsTv);
                loadSentGiftVouchers();
                WHAT_IT_IS = GIFTED;
                break;
            }
            case R.id.recevied_gifts_tv: {
                receviedGiftsTv.setTextColor(getActivity().getResources().getColor(R.color.white));
                sentGiftsTv.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                TransitionManager.beginDelayedTransition(giftedFiltersContainer, new ChangeBounds());
                changeIndicator(receviedGiftsTv);
                loadReceivedGiftVouchers();
                WHAT_IT_IS = SENT;
                break;
            }
        }
    }

    private void loadReceivedGiftVouchers() {
        giftedFiltersContainer.setVisibility(View.VISIBLE);

        purchasedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        purchasedBt.setTextColor(getResources().getColor(R.color.white));

        giftedBt.setBackgroundColor(getResources().getColor(R.color.white));
        giftedBt.setTextColor(getResources().getColor(R.color.black));

        expiredBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        expiredBt.setTextColor(getResources().getColor(R.color.white));

        redeemedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        redeemedBt.setTextColor(getResources().getColor(R.color.white));

        JSONArray j = new JSONArray();
        if (myEventList.length() > 0) {

            try {
                for (int i = 0; i < myEventList.length(); ++i) {
                    JSONObject obj = myEventList.getJSONObject(i);
                    boolean isGifted = obj.getBoolean("isGifted");
                    boolean isGiftReceived = obj.getBoolean("isGiftReceived");

                    if (isGifted && isGiftReceived) {
                        j.put(obj);
                    }
                }
            } catch (JSONException e) {
                // handle exception
            }

            loadNewTab(j, "WHEN YOU HAVE GIFTED OFFERS,\nYOU WILL SEE THEM HERE.");
        }
    }

    private void loadSentGiftVouchers() {
        giftedFiltersContainer.setVisibility(View.VISIBLE);

        purchasedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        purchasedBt.setTextColor(getResources().getColor(R.color.white));

        giftedBt.setBackgroundColor(getResources().getColor(R.color.white));
        giftedBt.setTextColor(getResources().getColor(R.color.black));

        expiredBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        expiredBt.setTextColor(getResources().getColor(R.color.white));

        redeemedBt.setBackgroundColor(getResources().getColor(R.color.app_color));
        redeemedBt.setTextColor(getResources().getColor(R.color.white));

        JSONArray j = new JSONArray();
        if (myEventList.length() > 0) {

            try {
                for (int i = 0; i < myEventList.length(); ++i) {
                    JSONObject obj = myEventList.getJSONObject(i);
                    boolean isGifted = obj.getBoolean("isGifted");
                    boolean isGiftReceived = obj.getBoolean("isGiftReceived");
                    if (isGifted && !isGiftReceived) {
                        j.put(obj);
                    }
                }
            } catch (JSONException e) {
                // handle exception
            }

            loadNewTab(j, "WHEN YOU HAVE GIFTED OFFERS,\nYOU WILL SEE THEM HERE.");
        }
    }

    private void changeIndicator(View selectedView) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(giftedFiltersContainer);
        constraintSet.connect(filterSelectedLayout.getId(), ConstraintSet.START, selectedView.getId(), ConstraintSet.START, 0);
        constraintSet.connect(filterSelectedLayout.getId(), ConstraintSet.END, selectedView.getId(), ConstraintSet.END, 0);
        constraintSet.applyTo(giftedFiltersContainer);
    }
}
package com.predrinks.app.utils;

import java.io.Serializable;

public class URL_Utils implements Serializable {
//    public static final String URL_IMAGE_DNLOAD = "";    // live
//    public static final String BASE_URL         = "https://app.confidentials.com/api/"; // live

    public String DOMAIN = "http://178.62.60.182/";

//    public static final String URL_IMAGE_DNLOAD_DEFAULT = "http://178.62.60.182/";    // live
//    private static final String BASE_URL = "http://178.62.60.182/api/";
//    public static final String URL_IMAGE_DNLOAD = "http://178.62.60.182/"; // sandbox

    public static final String URL_IMAGE_DNLOAD_DEFAULT = "http://confidentials.akkastechdemo.com/";    // live
    private static final String BASE_URL = "http://confidentials.akkastechdemo.com/api/";
    public static final String URL_IMAGE_DNLOAD = "http://confidentials.akkastechdemo.com/"; // sandbox

//    public static final String URL_IMAGE_DNLOAD_DEFAULT = "http://predrinks.azurewebsites.net/";    // live
//    private static final String BASE_URL = "http://predrinks.azurewebsites.net/api/";
//    public static final String URL_IMAGE_DNLOAD = "http://predrinks.azurewebsites.net/"; // sandbox

//    public static final String BASE_URL = "http://192.168.0.91/confidentials/public/api/";
//    public static final String URL_IMAGE_DNLOAD = "http://192.168.0.91/confidentials/public/";    // live
//    public static final String URL_IMAGE_DNLOAD_DEFAULT = "192.168.0.91/confidentials/public/";    // live

    /**
     * URL of CONFIDENTIALS
     */

    public static final String INSERT_USER = BASE_URL + "register/user";
    public static final String LOGIN_USER = BASE_URL + "registereduser/login";
    public static final String USER = BASE_URL + "user";
    public static final String EDIT_PROFILE = BASE_URL + "registered/user/edit";
    public static final String RESETPASSWORD = BASE_URL + "resetpassword";
    public static final String EVENT_LIST = BASE_URL + "event/list";
    public static final String PRIVACY_POLICY = BASE_URL + "privacyPolicy";
    public static final String TERMS_AND_CONDITIONS = BASE_URL + "termsAndConditions";
    public static final String MARKAS_FAVOURITE_VENUE = BASE_URL + "venue";
    public static final String SEARCH_CRITERIA = BASE_URL + "user";
    public static final String VENUE_DETAIL = BASE_URL + "venue";
    public static final String OFFER = BASE_URL + "venue";
    public static final String ADD_CART_NEW = BASE_URL + "add/cart_new";
    public static final String UPDATE_OFFER_QUANTITY_INCART = BASE_URL + "UpdateOfferQuantityInCart";
    public static final String PLACE_ORDER_NEW = BASE_URL + "place/order_new";
    public static final String NOTIFICATION = BASE_URL + "notification";
    public static final String AVAILABLEOFFERQUANTITY = BASE_URL + "AvailableOfferQuantity";
    public static final String STRIPEPUBLISHABLEKEY = BASE_URL + "stripePublishableKey";
    public static final String UPDATE_USER_DETAILS = BASE_URL + "updateUserDetails";
    public static final String GET_REMAINING_OFFER_QTY = BASE_URL + "GetRemainingOfferQty";
    public static final String TRANSACTION_FEES = BASE_URL + "transactionFees";
    public static final String SHARE_MYOFFER = BASE_URL + "share/myoffer";
    public static final String EMAIL_MYOFFER = BASE_URL + "email/myoffer";
    public static final String RESET_PASSWORD = BASE_URL + "change/password";
    public static final String INSTALL_ID = BASE_URL + "";
    public static final String REMOVE_ITEMS_FROM_CART = BASE_URL + "user/";

    public static final String USER_ID = "UserID";

    // Insert User
    public static final String PARAM_REGISTRATION_MODE = "RegistrationMode";
    public static final String PARAM_FIRST_NAME = "FirstName";
    public static final String PARAM_LAST_NAME = "LastName";
    public static final String PARAM_MOBILE_NO = "MobileNo";
    public static final String PARAM_POSTCODE = "PostCode";
    public static final String PARAM_EMAIL_ID = "EmailId";
    public static final String PARAM_PASSWORD = "Password";
    public static final String PARAM_DOB = "DOB";
    public static final String PARAM_GENDER = "Gender";
    public static final String PARAM_LONGITUDE = "Longitude";
    public static final String PARAM_LATITUDE = "Latitude";
    public static final String PARAM_DEVICE_ID = "DeviceID";
    public static final String PARAM_DEVICE_TYPE = "DeviceType";

    public static final String PARAM_ANDROID_APP_VERSION = "AndroidAppVersion";
    public static final String PARAM_IOS_APP_VERSION = "IOSAppVersion";
    public static final String PARAM_PROFILE_PIC = "ProfilePic";
    public static final String PARAM_UDID = "Udid";

    public static final String PARAM_USERID = "UserID";
    public static final String PARAM_MYOFFER_ID = "MyOfferID";
    public static final String PARAM_TO_EMAIL_ID = "ToEmailID";
    public static final String PARAM_MESSAGE = "Message";
}

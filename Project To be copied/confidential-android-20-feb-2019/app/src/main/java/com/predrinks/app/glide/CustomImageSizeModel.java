package com.predrinks.app.glide;

public interface CustomImageSizeModel {
    String requestCustomSizeUrl(int width, int height);
}

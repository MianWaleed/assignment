package com.predrinks.app.stripe;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.predrinks.app.R;
import com.predrinks.app.activity.BaseFragmentActivity;
import com.predrinks.app.stripe.StripePayment.Main.StripePaymentActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentMethod extends BaseFragmentActivity implements OnClickListener {

	private LinearLayout 	lytDebitCard,lytCreditCard,lytCash;
	//private View			actionbar_back;
	private ListView		cardList;
	private JSONArray 		stripeCardDetails;
	public static boolean backTrue = false;
	
	Bundle bundle;
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
	//	setContentView(R.layout.payment_method);

		setUpActionBar();
		init();

		findAllValues();

		setListeners();

//		setValues();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setValues();
	}
	
	private final String TAG = "Payment Method";
	private void setUpActionBar() {
		//setActionBar();
		//setActionBarTitle(TAG);
	}
	
	private void init() {
		Intent intent = getIntent();
		 bundle = intent.getExtras();
		 
	}
	private void findAllValues() {
		//actionbar_back	= findViewById(R.id.actionbar_back);
		/*lytDebitCard	= (LinearLayout) findViewById(R.id.lytDebitCard);
		lytCreditCard	= (LinearLayout) findViewById(R.id.lytCreditCard);
		lytCash			= (LinearLayout) findViewById(R.id.lytCash);
		cardList		= (ListView) findViewById(R.id.cardList);*/
	}
	private void setListeners() {
		//actionbar_back 	.setOnClickListener(this);
		lytDebitCard	.setOnClickListener(this);
		lytCreditCard	.setOnClickListener(this);
		lytCash			.setOnClickListener(this);
		cardList		.setOnItemClickListener(CardClick);
	}
	
	private void setValues() {
		/*if (getLoadedData(Config.STRIPE_DETAILS).equals("") || getLoadedData(Config.STRIPE_DETAILS).equals(null)){
			((View)findViewById(R.id.txtPayUsing)).setVisibility(View.GONE);
			return;
		}*/
		
		String[] cardFourDigit = null;
		String getlo = getLoadedData(Config.STRIPE_DETAILS); 
		try {
			stripeCardDetails = new JSONArray(getlo);
			cardFourDigit = new String[stripeCardDetails.length()];
			for (int i = 0; i < stripeCardDetails.length(); i++) {
				JSONObject jsonObject = stripeCardDetails.getJSONObject(i);
//				JSONObject card = new JSONObject(jsonObject.getString(URL_Utils.CARD_DETAILS));
				
				cardFourDigit[i] = "XXXX-XXXX-XXXX-"+jsonObject.getString("cardLastfour");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.card_list_item, R.id.cardFourDigit, cardFourDigit);
		cardList.setAdapter(adapter);
		
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		/*if (id == R.id.actionbar_back){
			onBackPressed();
		} */
		/* if (id == R.id.lytCash){
			callIntent("Cash");
		}
		else if (id == R.id.lytDebitCard ){
			callIntent("Debit Card");
		}
		else if (id == R.id.lytCreditCard){
			callIntent("Credit Card");
		}*/
	}
	
	private void callIntent(String paymentType){
		backTrue = true;
		//Intent intent = new Intent(PaymentMethod.this,ConfirmOrderActivity.class);
		Intent intent = new Intent(PaymentMethod.this,StripePaymentActivity.class);
		//bundle.putString("paymentType", paymentType);
		intent.putExtra("fromExistCard", false);
		intent.putExtra("paymentType",paymentType);
		//intent.putExtras(bundle);
		startActivity(intent);
 	}
	
	private OnItemClickListener CardClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			backTrue = false;
			//Intent intent = new Intent(PaymentMethod.this,ConfirmOrderActivity.class);
			Intent intent = new Intent(PaymentMethod.this,StripePaymentActivity.class);
			bundle.putString("paymentType", "Card");
			try {
				bundle.putString("reciept_details", getLoadedData(Config.STRIPE_DETAILS));
			} catch (Exception e) {
				e.printStackTrace();
			}
			intent.putExtra("fromExistCard", true);
			intent.putExtra("ExistCardPosition", position);
			intent.putExtras(bundle);
			startActivity(intent);
		}
	};
}

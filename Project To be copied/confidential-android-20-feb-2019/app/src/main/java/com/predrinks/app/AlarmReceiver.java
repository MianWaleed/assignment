package com.predrinks.app;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.predrinks.app.activity.CartMainActivity;
import com.predrinks.app.utils.Utils;

public class AlarmReceiver extends BroadcastReceiver {

    String TAG = "AlarmReceiver";
    public static final int DAILY_REMINDER_REQUEST_CODE = 321;

    String title = "Confidentials";

    @Override

    public void onReceive(Context context, Intent intent) {
        String desc = intent.getStringExtra("msg");

        if (!TextUtils.isEmpty(desc) && desc.contains("Your voucher in the cart got expired")) {
            if (!Utils.isAppIsInBackground(context)) {
                Intent intnt = new Intent("some_custom_id");
                intnt.putExtra("cartItemRemove", desc);
                context.sendBroadcast(intnt);
            }
            return;
        }
        showNotification(context, CartMainActivity.class, title, desc);
    }


    public void showNotification(Context context, Class<?> cls, String title, String content) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent notificationIntent = new Intent(context, cls);
        if (Utils.isAppIsInBackground(context)) {
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        /*TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(cls);
        stackBuilder.addNextIntent(notificationIntent);*/

       // PendingIntent pendingIntent = stackBuilder.getPendingIntent( DAILY_REMINDER_REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, notificationIntent, PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, MyFirebaseMessagingService.CHANNEL_ID);

        Notification notification;
        if (!TextUtils.isEmpty(content) && content.contains("Your voucher in the cart got expired")) {
            notification = builder.setContentTitle(title)
                    .setContentText(content)
                    .setAutoCancel(true)
                    .setSound(alarmSound).setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            notification = builder.setContentTitle(title)
                    .setAutoCancel(true)
                    .setSound(alarmSound).setSmallIcon(R.mipmap.ic_launcher)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(content))
                    .setContentIntent(pendingIntent).build();
        }


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(MyFirebaseMessagingService.CHANNEL_ID, name, importance);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            //NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }


        notificationManager.notify(DAILY_REMINDER_REQUEST_CODE, notification);
    }
}

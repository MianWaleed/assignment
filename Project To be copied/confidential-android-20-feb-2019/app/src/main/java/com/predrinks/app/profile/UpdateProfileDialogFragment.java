package com.predrinks.app.profile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.utils.Utils;
import com.predrinks.app.widget.WheelView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.support.constraint.Constraints.TAG;

public class UpdateProfileDialogFragment extends DialogFragment implements View.OnClickListener, TextWatcher {

    Button skipBt, nextBt;
    ConstraintLayout onBroadLayout;
    LinearLayout updateInfoLayout;
    Boolean isNext = true;

    EditText mobileEt, postCodeEt;
    ImageView checkedMobileIv, checkedPostIv, checkedGenderIv, checkedBirthdayIv;
    TextView genderTv, birthdayTv;

    Context context;
    private AlertDialog dialog;
    Spinner spinner;
    String genderArray[] = {"Select", "Male", "Female", "Other"};
    private boolean firstExecution = true;
    private SimpleDateFormat dateFormatter;
    int monthOfYear;
    int year;
    int dayOfMonth;
    String birthDay;
    ImageView checkedFirstNameIv;
    ImageView checkedLastIv;
    ImageView checkedEmailIv;

    private static final String[] MONTH = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final List<String> DATES = new ArrayList<>();
    private static final List<String> YEAR = new ArrayList<>();

    public UpdateProfileDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_profile_dialog, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), R.style.WideDialog);
    }

    EditText firstNameEt, lastNameEt, emailEt;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        context = getActivity();

        checkedFirstNameIv = view.findViewById(R.id.checked_first_name_iv);
        checkedLastIv = view.findViewById(R.id.checked_last_name_iv);
        checkedEmailIv = view.findViewById(R.id.checked_email_iv);

        mobileEt = view.findViewById(R.id.mobile_et);
        mobileEt.addTextChangedListener(this);

        firstNameEt = view.findViewById(R.id.first_name_et);
        firstNameEt.addTextChangedListener(this);

        lastNameEt = view.findViewById(R.id.last_name_et);
        lastNameEt.addTextChangedListener(this);

        emailEt = view.findViewById(R.id.message_et);
        emailEt.addTextChangedListener(this);

        checkedMobileIv = view.findViewById(R.id.checked_mobile_iv);

        postCodeEt = view.findViewById(R.id.post_code_et);
        checkedPostIv = view.findViewById(R.id.checked_post_iv);

        postCodeEt.addTextChangedListener(this);

        genderTv = view.findViewById(R.id.gender_tv);
        checkedGenderIv = view.findViewById(R.id.checked_gender_iv);

        birthdayTv = view.findViewById(R.id.birthday_tv);
        checkedBirthdayIv = view.findViewById(R.id.checked_birthday_iv);

        updateInfoLayout = view.findViewById(R.id.update_info_layout);
        skipBt = view.findViewById(R.id.bt_skip);
        nextBt = view.findViewById(R.id.bt_next);
        onBroadLayout = view.findViewById(R.id.on_broad_layout);

        skipBt.setOnClickListener(this);
        nextBt.setOnClickListener(this);

        genderTv.setOnClickListener(this);
        genderTv.addTextChangedListener(this);

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        birthdayTv.setOnClickListener(this);
        birthdayTv.setInputType(InputType.TYPE_NULL);
        birthdayTv.addTextChangedListener(this);

        SharedPreferences mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        String data = mPrefs.getString(Constant.USERID, "");
        getUserDetail(data);

        super.onViewCreated(view, savedInstanceState);
    }

    private void getUserDetail(String userId) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onError: " + error);

            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: " + response);
                try {
                    String message = response.getString("success");
                    if (message.equals("1")) {
                        updateUI(response.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        };

        String url = URL_Utils.USER + "/" + userId + "/" + "detail";
        CustomRequest jsObjRequest = new CustomRequest(url, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void updateUI(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        JSONObject userDetail = jsonObject.getJSONObject("result");

        firstName = checkString(userDetail.getString("firstName"));
        lastName = checkString(userDetail.getString("lastName"));
        if (userDetail.has("postCode"))
            postcode = checkString(userDetail.getString("postCode"));
        if (userDetail.has("mobileNo"))
            mobileNo = checkString(userDetail.getString("mobileNo"));
        email = checkString(userDetail.getString("emailId"));
        birthDay = checkString(userDetail.getString("dOB"));
        gender = checkString(userDetail.getString("gender"));

        String fName = "", lName = "";
        try {
            fName = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
            lName = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
        } catch (IndexOutOfBoundsException e) {
        }

        firstNameEt.setText(fName);
        lastNameEt.setText(lName);

        postCodeEt.setText(postcode);
        mobileEt.setText(mobileNo);

        birthDay = birthDay.replace("/", ".");
        birthdayTv.setText(birthDay);
        genderTv.setText(gender);
        emailEt.setText(email);
    }

    private String checkString(String string) {
        if (string == null || string.trim().length() == 0 || string.equals("null")) {
            return "";
        }
        return string;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_next: {
                if (isNext) {
                    isNext = false;
                    onBroadLayout.setVisibility(View.GONE);
                    updateInfoLayout.setVisibility(View.VISIBLE);
                    nextBt.setText("Save");
                } else {
                    hitApi();
                }
                break;
            }
            case R.id.bt_skip: {
                hitSkipApi();
                break;
            }
            case R.id.gender_tv: {
                selectGender();
                break;
            }
            case R.id.birthday_tv: {
                setDateTimeField();
                break;
            }
        }
    }

    private void hitSkipApi() {
        SharedPreferences mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        String data = mPrefs.getString(Constant.USERID, "");
        JsonObjectRequest j = new JsonObjectRequest(Request.Method.POST, URL_Utils.USER + "/" + data + "/skip/info", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                getDialog().cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getDialog().cancel();
            }
        });

        VolleySingleton.getInstance().addToRequestQueue(j);
    }

    private void hitApi() {
        JSONObject jsonObject = new JSONObject();

        Log.d(TAG, "hitApi: DOB => " + dob + " Gender => " + gender + "    mobileNo => " + mobileNo + "    postcode => " + postcode);

        try {
            jsonObject.put("DOB", dob);
            jsonObject.put("Gender", gender);
            jsonObject.put("MobileNo", mobileNo);
            jsonObject.put("PostCode", postcode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SharedPreferences mPrefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        String data = mPrefs.getString(Constant.USERID, "");

        JsonObjectRequest j = new JsonObjectRequest(Request.Method.POST, URL_Utils.USER + "/" + data + "/update/info", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                getDialog().cancel();
                Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().addToRequestQueue(j);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    String dob, gender, mobileNo, postcode;
    String firstName, lastName, email;

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable == mobileEt.getEditableText()) {
            mobileNo = mobileEt.getText().toString();
            if (!TextUtils.isEmpty(editable.toString())) {
                checkedMobileIv.setImageResource(R.drawable.ic_new_checked);
            } else {
                checkedMobileIv.setImageResource(R.drawable.ic_new_un_checked);
            }
        } else if (editable == postCodeEt.getEditableText()) {
            postcode = postCodeEt.getText().toString();
            if (!TextUtils.isEmpty(editable.toString())) {
                checkedPostIv.setImageResource(R.drawable.ic_new_checked);
            } else {
                checkedPostIv.setImageResource(R.drawable.ic_new_un_checked);
            }
        } else if (editable == genderTv.getEditableText()) {
            if (!TextUtils.isEmpty(editable.toString())) {
                gender = genderTv.getText().toString();
                checkedGenderIv.setImageResource(R.drawable.ic_new_checked);
            } else {
                gender = "";
                checkedGenderIv.setImageResource(R.drawable.ic_new_un_checked);
            }
        } else if (editable == birthdayTv.getEditableText()) {
            if (!TextUtils.isEmpty(editable.toString())) {
                dob = birthdayTv.getText().toString();
                checkedBirthdayIv.setImageResource(R.drawable.ic_new_checked);
            } else {
                dob = "";
                checkedBirthdayIv.setImageResource(R.drawable.ic_new_un_checked);
            }
        } else if (editable == firstNameEt.getEditableText()) {
            if (!TextUtils.isEmpty(editable.toString())) {
                firstName = firstNameEt.getText().toString();
                checkedFirstNameIv.setImageResource(R.drawable.ic_new_checked);
            } else {
                firstName = "";
                checkedFirstNameIv.setImageResource(R.drawable.ic_new_un_checked);
            }
        } else if (editable == lastNameEt.getEditableText()) {
            if (!TextUtils.isEmpty(editable.toString())) {
                lastName = lastNameEt.getText().toString();
                checkedLastIv.setImageResource(R.drawable.ic_new_checked);
            } else {
                firstName = "";
                checkedLastIv.setImageResource(R.drawable.ic_new_un_checked);
            }
        }
    }

    private void setDateTimeField() {
        for (int i = 0; i <= 30; i++) {
            DATES.add(String.valueOf(i + 1));
        }
        for (int i = 1900; i < 2050; i++) {
            YEAR.add(String.valueOf(i + 1));
        }

        monthOfYear = -1;
        year = -1;
        dayOfMonth = -1;

        final int yearThis, month, date, posYear, posDate;
        if (TextUtils.isEmpty(birthDay)) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            yearThis = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH) + 1;
            date = calendar.get(Calendar.DAY_OF_MONTH);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date));
        } else {
            String[] fulldate = birthDay.split("\\.");
            yearThis = Integer.parseInt(fulldate[2]);
            month = Integer.parseInt(fulldate[1]);
            date = Integer.parseInt(fulldate[0]);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date)) + 1;
        }

        View outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view, null);
        WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);


        wv.setItems(Arrays.asList(MONTH));
        wv.setSeletion(month - 1);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                monthOfYear = selectedIndex;
            }
        });
        WheelView wv1 = (WheelView) outerView.findViewById(R.id.wheel_view_wv1);
        wv1.setItems(DATES);

        wv1.setSeletion(posDate - 1);
        wv1.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                dayOfMonth = Integer.parseInt(item);
            }
        });
        WheelView wv2 = (WheelView) outerView.findViewById(R.id.wheel_view_wv2);
        wv2.setItems(YEAR);
        wv2.setSeletion(posYear);
        wv2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                year = Integer.parseInt(item);
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });

        new AlertDialog.Builder(getActivity())
                .setTitle("Date of Birth")
                .setView(outerView)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (monthOfYear == -1) {
                            monthOfYear = month - 1;
                        } else {
                            monthOfYear -= 1;
                        }
                        if (dayOfMonth == -1) {
                            dayOfMonth = posDate;
                        }
                        if (year == -1) {
                            year = Integer.parseInt(YEAR.get(posYear));
                        }
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        Date birthDate = new Date(newDate.getTimeInMillis());

                        onDateSet(year, monthOfYear, dayOfMonth, birthDate);
                    }
                })
                .show();
    }

    public void onDateSet(int year, int month, int day, Date newDate) {
        Calendar userAge = new GregorianCalendar(year, month, day);
        Calendar minAdultAge = new GregorianCalendar();
        Calendar minAdultAge1 = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        minAdultAge1.set(1901, 1, 1);
        if (minAdultAge.before(userAge) || minAdultAge1.after(userAge)) {
            isBirthdaySelected = false;
            dob = "";
            birthdayTv.setText("DOB");
            Toast.makeText(getActivity(), "Sorry. Date of birth is invalid. Must be over 18 years old", Toast.LENGTH_SHORT).show();
            checkedBirthdayIv.setImageResource(R.drawable.ic_new_un_checked);
        } else {
            isBirthdaySelected = true;
            String date = dateFormatter.format(newDate.getTime());
            date = date.replace("/", ".");
            birthDay = date;
            birthdayTv.setText(date);
        }
    }

    boolean isGenderSelected = false, isBirthdaySelected = false;


    private void selectGender() {
        context = getActivity();
        if (Utils.isBrokenSamsungDevice()) {
            context = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        }
        if (context == null)
            return;

        spinner = new Spinner(context);
        firstExecution = false;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, genderArray);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                if (arg2 == 0) {
                    isGenderSelected = false;
                    genderTv.setText("Select gender");
                    checkedGenderIv.setImageResource(R.drawable.ic_new_un_checked);
                    gender = "";
                } else {
                    isGenderSelected = true;

                    if (firstExecution) {
                        firstExecution = false;
                        return;
                    }
                    if (genderTv != null && arg2 != 0) {
                        genderTv.setText(genderArray[arg2]);
                        genderTv.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (dialog != null && dialog.isShowing() && arg2 != 0) {
                                    dialog.dismiss();
                                    genderTv.setError(null);
                                }
                            }
                        }, 500);

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spinner.setAdapter(adapter);
        spinner.setPadding(40, 10, 10, 10);
        if (dialog == null) {
            dialog = new AlertDialog.Builder(getActivity())
                    .setView(spinner)
                    .setTitle("Gender")
                    .create();
        }
        dialog.show();
    }
}

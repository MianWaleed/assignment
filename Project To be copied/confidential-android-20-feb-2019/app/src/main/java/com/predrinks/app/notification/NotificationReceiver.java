package com.predrinks.app.notification;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kumulos.android.PushBroadcastReceiver;
import com.kumulos.android.PushMessage;
import com.predrinks.app.activity.MainActivity;

public class NotificationReceiver extends PushBroadcastReceiver {
    @Override
    protected void onPushReceived(Context context, PushMessage pushMessage) {
        Log.d(TAG, "onPushReceived: " + pushMessage.getMessage());
        super.onPushReceived(context, pushMessage);
    }

    @Override
    protected void onPushOpened(Context context, PushMessage pushMessage) {
        super.onPushOpened(context, pushMessage);
    }

    @Override
    protected Notification buildNotification(Context context, PushMessage pushMessage) {
        Log.d(TAG, "onPushReceived: " + pushMessage.getMessage());
        return super.buildNotification(context, pushMessage);
    }

    @Override
    protected Intent getPushOpenActivityIntent(Context context, PushMessage pushMessage) {
        Log.d(TAG, "onPushReceived: " + pushMessage.getMessage());

        Intent intent = new Intent(context, MainActivity.class).putExtra("noti", pushMessage);
        return intent;
    }

    @Override
    protected Intent getBackgroundPushServiceIntent(Context context, PushMessage pushMessage) {
        Log.d(TAG, "onPushReceived: " + pushMessage.getMessage());
        return super.getBackgroundPushServiceIntent(context, pushMessage);
    }
}

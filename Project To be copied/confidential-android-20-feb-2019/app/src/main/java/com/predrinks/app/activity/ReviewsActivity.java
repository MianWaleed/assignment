package com.predrinks.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.predrinks.app.R;
import com.predrinks.app.adapter.ReviewsAdapter;
import com.predrinks.app.model.ReviewsModel;

import java.util.ArrayList;
import java.util.List;

public class ReviewsActivity extends AppCompatActivity implements View.OnClickListener {

    private ConstraintLayout backLayout;
    private TextView clubToolbarNameTv;
    private ImageView cartIv;
    private ImageView confidentialPassIv;
    private TextView offerNameTv;
    private TextView clubNameBigTv;
    private RecyclerView reviewsRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        initView();
    }

    private void initView() {
        backLayout = (ConstraintLayout) findViewById(R.id.back_layout);
        clubToolbarNameTv = (TextView) findViewById(R.id.club_toolbar_name_tv);
        cartIv = (ImageView) findViewById(R.id.cart_iv);
        confidentialPassIv = (ImageView) findViewById(R.id.confidential_pass_iv);
        offerNameTv = (TextView) findViewById(R.id.offer_name_tv);
        clubNameBigTv = (TextView) findViewById(R.id.club_name_big_tv);
        reviewsRv = (RecyclerView) findViewById(R.id.reviews_rv);

        backLayout.setOnClickListener(this);
        cartIv.setOnClickListener(this);
        confidentialPassIv.setOnClickListener(this);

        List<ReviewsModel> arrayList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            ReviewsModel reviewsModel = new ReviewsModel();
            reviewsModel.setCustomerName("Lilly Aldrige");
            reviewsModel.setDate("10 July, 2019");
            reviewsModel.setDesc("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
            reviewsModel.setRating(i);

            arrayList.add(reviewsModel);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(this, arrayList);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reviewsRv.getContext(),
                linearLayoutManager.getOrientation());
        reviewsRv.addItemDecoration(dividerItemDecoration);
        reviewsRv.setLayoutManager(new LinearLayoutManager(this));
        reviewsRv.setAdapter(reviewsAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_layout: {
                finish();
                break;
            }
            case R.id.cart_iv: {
                startActivity(new Intent(this, CartMainActivity.class));
                break;
            }
            case R.id.confidential_pass_iv: {
                startActivity(new Intent(this, HeroOffersActivity.class));
                break;
            }
        }
    }
}
package com.predrinks.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;

public class PreDrinkOrderDAO {

    public static String DATABASE_NAME = "Confidentials.db";
    final int DATABASE_VERSION = 2;

    public static String KEY_PRE_DRINK_ID = "_id";


    public static String KEY_USER_ID = "userID";
    public static String KEY_CARDLAST_FOUR = "cardLastfour";
    public static String KEY_CARD_TOKEN = "cardToken";
    public static String KEY_CARD_TYPE = "cardType";
    public static String KEY_CARD_NUMBER = "cardNumber";
    public static String KEY_CARD_EXP_MONTH = "cardExpMonth";
    public static String KEY_CARD_EXP_YEAR = "cardExpYear";
    public static String KEY_CARD_POST_CODE = "postCode";


    public static String TABLE_PRE_DRINK_CARD = "PreDrinkCard";


    public static String TRUE = "true";
    public static String FALSE = "false";

    private String CREATE_TABLE_PREDRINK_CARD = "CREATE TABLE " + TABLE_PRE_DRINK_CARD
            + "("
            + KEY_PRE_DRINK_ID + " integer PRIMARY KEY autoincrement,"
            + KEY_USER_ID + " text DEFAULT '', "
            + KEY_CARDLAST_FOUR + " text DEFAULT '', "
            + KEY_CARD_TOKEN + " text DEFAULT '', "
            + KEY_CARD_TYPE + " integer DEFAULT '0', "
            + KEY_CARD_NUMBER + " text DEFAULT '', "
            + KEY_CARD_EXP_MONTH + " integer DEFAULT '0', "
            + KEY_CARD_EXP_YEAR + " integer DEFAULT '0', "
            + KEY_CARD_POST_CODE + " integer DEFAULT '0' "
            + ");";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase sqLiteDatabase;

    public PreDrinkOrderDAO(Context c) {
        this.context = c;
        DBHelper = new DatabaseHelper(context);
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_PREDRINK_CARD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRE_DRINK_CARD);
            onCreate(db);
        }
    }

    // ---opens the database---
    public PreDrinkOrderDAO open() throws SQLException {
        sqLiteDatabase = DBHelper.getWritableDatabase();
        return this;
    }

    // close the database
    public void close() throws SQLException {
        DBHelper.close();
    }

    public void insertOffers(String userID, String cardLastfour, String cardToken, int cardType, String cardNumber, int cardExpMonth, int cardExpYear, String postCode) {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userID);
        values.put(KEY_CARDLAST_FOUR, cardLastfour);
        values.put(KEY_CARD_TOKEN, cardToken);
        values.put(KEY_CARD_TYPE, cardType);
        values.put(KEY_CARD_NUMBER, cardNumber);
        values.put(KEY_CARD_EXP_MONTH, cardExpMonth);
        values.put(KEY_CARD_EXP_YEAR, cardExpYear);
        values.put(KEY_CARD_POST_CODE, postCode);
        long id = sqLiteDatabase.insert(TABLE_PRE_DRINK_CARD, null, values);
        Log.e("Insert id :", String.valueOf(id));
    }

    public boolean isUserExist(String userID) {
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_PRE_DRINK_CARD + " where " + KEY_USER_ID + " = '" + userID + "'", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (cursor == null)
            return false;

        return (cursor.getCount() > 0 ? true : false);
    }

    public boolean iscardsExist(String userID, String cardLastfour) {
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_PRE_DRINK_CARD + " where " + KEY_USER_ID + " = '" + userID + "'  and " + KEY_CARDLAST_FOUR + " = '" + cardLastfour + "'", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (cursor == null)
            return false;

        return (cursor.getCount() > 0 ? true : false);
    }

    public void removeCard(String paramUserID, String Lastfour) {
        String where = "" + KEY_USER_ID + "=? AND " + KEY_CARDLAST_FOUR + "=?";
        String[] whereArgs = {String.valueOf(paramUserID), String.valueOf(Lastfour)};
        long id =  sqLiteDatabase.delete(TABLE_PRE_DRINK_CARD, where, whereArgs);
        Log.e("removeCard id :", String.valueOf(id));
    }

    public ArrayList<UserCardDetail> getCardList(String paramUserID) {
        ArrayList<UserCardDetail> itemsArrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_PRE_DRINK_CARD + " where " + KEY_USER_ID + " = '" + paramUserID + "'", null);
            if (cursor != null && cursor.getCount() > 0) {
                String userID, cardLastfour, cardToken, cardNumber, postcode;
                int cardExpMonth, cardExpYear, cardType;

                if (cursor.moveToFirst()) {
                    do {
                        userID = cursor.getString(1);
                        cardLastfour = cursor.getString(2);
                        cardToken = cursor.getString(3);
                        cardType = cursor.getInt(4);
                        cardNumber = cursor.getString(5);
                        cardExpMonth = cursor.getInt(6);
                        cardExpYear = cursor.getInt(7);
                        postcode = cursor.getString(8);

                        UserCardDetail userCardDetail = new UserCardDetail(userID, cardLastfour, cardToken, cardType, cardNumber, cardExpMonth, cardExpYear, postcode);
                        itemsArrayList.add(userCardDetail);
                    } while (cursor.moveToNext());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (cursor != null)
                cursor.close();
        }
        Log.e("Date vise Sorting", itemsArrayList.toString());
        return itemsArrayList;
    }
}

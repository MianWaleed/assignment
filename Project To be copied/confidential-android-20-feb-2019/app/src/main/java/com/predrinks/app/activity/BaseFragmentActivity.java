package com.predrinks.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseFragmentActivity extends AppCompatActivity {

    private SharedPreferences mPrefs;
    private String TAG = "";

    protected void getUserDetail(String userId) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());

            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                JSONObject result = null;
                try {
                    result = response.getJSONObject("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                saveLoadedData(Constant.USER_DETAIL, result.toString());
            }
        };
        String URL = URL_Utils.USER + "/" + userId + "/detail";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }


    protected void removeCartItems(String userId) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());
                mPrefs.edit().putLong("alarmExpireTime", 0).apply();
            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                mPrefs.edit().putLong("alarmExpireTime", 0).apply();
                MainActivity.ITEM_ADD_TO_CART = 0;
                MainActivity.setHeaderNotificationNumber(MainActivity.ITEM_ADD_TO_CART);
                saveLoadedData( "numberOfAddToCart", "" + MainActivity.ITEM_ADD_TO_CART);
            }
        };
        String URL = URL_Utils.USER + "/" + userId + "/DeleteCart";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }

    protected void saveLoadedData(String TAG, String data) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(TAG, data);
        editor.apply();
    }

    protected String getLoadedData(String TAG) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        String data = mPrefs.getString(TAG, "");
        return data;
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences prefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, Context.MODE_PRIVATE);
        prefs.edit().putString("email", email).apply();
    }

    public static String getEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Confidentials.NAME_PREFERENCE, Context.MODE_PRIVATE);
        return prefs.getString("email", "");
    }


    protected boolean isLoggedIn() {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        boolean isLoggedIn = mPrefs.getBoolean("isLoggedIn", false);
        return isLoggedIn;
    }

    protected void setLoggedIn(boolean isLogin) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean("isLoggedIn", isLogin);
        editor.apply();
    }

    protected void setBoolean(boolean isTrue, String prefName) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(prefName, isTrue);
        editor.apply();
    }

    protected void clearAppData() {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.clear();
        editor.apply();
    }

    protected String checkString(String string) {
        if (string == null || string.trim().length() == 0 || string.equals("null")) {
            return "";
        }
        return string;
    }

    protected void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) BaseFragmentActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    protected void hideKeyBoard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    protected void hideKeyBoardByView(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) BaseFragmentActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
        }

    }

    protected void hideKeyBoard(EditText editText) {
        try {
            InputMethodManager imm = (InputMethodManager) BaseFragmentActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e) {

        }

    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !android.text.TextUtils.isEmpty(locationProviders);
        }
    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (!android.text.TextUtils.isEmpty(strKey))
                str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    protected void dialog_info(Context context, String title, String message) {
        final Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    protected void dialog_info(String title, String message) {
        final Dialog dialog = new Dialog(BaseFragmentActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    protected void dialog_info_Fees(String title, String message, String message1) {
        final Dialog dialog = new Dialog(BaseFragmentActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_fees);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);
        ((TextView) dialog.findViewById(R.id.message1)).setText(message1);
        ((TextView) dialog.findViewById(R.id.message)).setTextColor(getResources().getColor(R.color.color_dialog_title));

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    protected void dialog_Login(final Activity activity) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, LandingActivity.class);
                startActivity(intent);
                activity.finish();
                dialog.dismiss();
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText("Login to continue");
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Login");

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);

        dialog.show();
    }

    protected void dialog_Login1(final Activity activity, String mes, String textbtn) {

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceID = "0", deviceUDID = "";
                deviceID = getLoadedData(Confidentials.REG_ID);
                deviceUDID = getLoadedData("DEVICE_UDID");
                clearAppData();
                setLoggedIn(false);
                saveLoadedData("DEVICE_UDID", deviceUDID);
                saveLoadedData(Confidentials.REG_ID, deviceID);
                Intent intent = new Intent(activity, LandingActivity.class);
                startActivity(intent);
                if (activity != null)
                    activity.finish();
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(mes);
        ((Button) dialog.findViewById(R.id.okBtn)).setText(textbtn);

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);
        dialog.show();
    }

    protected void dialog_Profile(final Activity activity) {
        String mes;
        String textbtn;
        mes = "To continue please update your date of birth in MY ACCOUNT";
        textbtn = "Update";

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, ProfileActivity.class);
                startActivity(intent);
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(mes);
        ((Button) dialog.findViewById(R.id.okBtn)).setText(textbtn);

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        dialog.show();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        try {
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
        } catch (ClassCastException e) {
            e.printStackTrace();
            try {
                AbsListView.LayoutParams params = (AbsListView.LayoutParams) listView.getLayoutParams();
                params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
                listView.setLayoutParams(params);
            } catch (ClassCastException e1) {
                e1.printStackTrace();
            }
        }


    }

    protected void showErrorDialogAll(Activity activity, String title, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setCancelable(false);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("OK", null);
            builder.show();
        } catch (RuntimeException e) {

        }

    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}

package com.predrinks.app.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.OnLoadMoreListener;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.support.constraint.Constraints.TAG;

public class OffersAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private JSONArray pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imgMainBg)
        ImageView imgMainBg;
        @Bind(R.id.tvRemaining)
        TextView tvRemaining;
        @Bind(R.id.tvOfferTitle)
        TextView tvOfferTitle;
        @Bind(R.id.tvOfferDetail)
        TextView tvOfferDetail;
        @Bind(R.id.tvClubTitle)
        TextView tvClubTitle;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.lnr_Main)
        LinearLayout lnr_Main;
        @Bind(R.id.lnrSubscribe)
        LinearLayout lnrSubscribe;
        @Bind(R.id.tvSubscribeDesc)
        TextView tvSubscribeDesc;
        @Bind(R.id.tvSubscribeHere)
        TextView tvSubscribeHere;

        @Bind(R.id.featured_iv)
        ImageView featuredIv;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvOfferTitle.setSelected(true);
            tvOfferDetail.setSelected(true);
            tvOfferTitle.setFocusable(true);
            tvOfferDetail.setFocusable(true);
        }
    }


    public OffersAdapter(Context context, JSONArray pogoList, RecyclerView recyclerView) {
        this.mContext = context;
        this.pogoList = pogoList;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }

    }

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            return pogoList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        } catch (JSONException e) {
            e.printStackTrace();
            return VIEW_PROG;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offer_list, parent, false);
        return new MyViewHolder(itemView);*/

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offer_list, parent, false);
            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            ((MyViewHolder) holder).tvOfferTitle.setTypeface(MainActivity.getTypeFaceLight());
            ((MyViewHolder) holder).tvOfferDetail.setTypeface(MainActivity.getTypeFaceSemiBold());
            ((MyViewHolder) holder).tvClubTitle.setTypeface(MainActivity.getTypeFaceSemiBold());
            ((MyViewHolder) holder).tvDateTime.setTypeface(MainActivity.getTypeFaceLight());
            ((MyViewHolder) holder).tvSubscribeDesc.setTypeface(MainActivity.getTypeFaceBold());
            ((MyViewHolder) holder).tvSubscribeHere.setTypeface(MainActivity.getTypeFaceRegular());


            JSONObject jsonObject = null;
            try {
                jsonObject = pogoList.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject != null && !jsonObject.has("eventImage")) {
                ((MyViewHolder) holder).lnr_Main.setVisibility(View.GONE);
                ((MyViewHolder) holder).lnrSubscribe.setVisibility(View.VISIBLE);
                return;
            } else {
                ((MyViewHolder) holder).lnr_Main.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).lnrSubscribe.setVisibility(View.GONE);
            }


            try {
                assert jsonObject != null;
                if (jsonObject.getBoolean("pinned"))
                    ((MyViewHolder) holder).featuredIv.setVisibility(View.VISIBLE);
                else ((MyViewHolder) holder).featuredIv.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            String imagename = getJsonString(jsonObject, "eventImage");
            String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
            String title = getJsonString(jsonObject, "title");
            String subTitle = getJsonString(jsonObject, "subTitle");
            String venueName = getJsonString(jsonObject, "venueName");
            String eventEndDateTime = getJsonString(jsonObject, "eventEndDateTime");
            String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");

            ((MyViewHolder) holder).tvOfferTitle.setText(title);
            ((MyViewHolder) holder).tvOfferDetail.setText(subTitle);
            ((MyViewHolder) holder).tvClubTitle.setText(venueName);
            ((MyViewHolder) holder).tvDateTime.setText(eventEndDateTime);

            if (isEventDateDisabled.equals("true")) {
                ((MyViewHolder) holder).tvDateTime.setVisibility(View.INVISIBLE);
            } else {
                ((MyViewHolder) holder).tvDateTime.setVisibility(View.VISIBLE);
            }

            Log.d(TAG, "onBindViewHolder: " + totalRemainingOffers);


            if (totalRemainingOffers.contains("-") || totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
                ((MyViewHolder) holder).tvRemaining.setVisibility(View.GONE);
            } else {
                ((MyViewHolder) holder).tvRemaining.setText(totalRemainingOffers.toUpperCase());
                ((MyViewHolder) holder).tvRemaining.setVisibility(View.VISIBLE);
            }

            String imgUrl = "";

            if (TextUtils.isEmpty(imagename)) {
                imgUrl = URL_Utils.URL_IMAGE_DNLOAD_DEFAULT + "images/default_con.png";
            } else {
                imgUrl = URL_Utils.URL_IMAGE_DNLOAD + imagename;
            }

            Glide.with(mContext)
                    .load(imgUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .dontAnimate()
                    .error(R.mipmap.default_event_venue_image)
                    .into(((MyViewHolder) holder).imgMainBg);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return pogoList.length();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}


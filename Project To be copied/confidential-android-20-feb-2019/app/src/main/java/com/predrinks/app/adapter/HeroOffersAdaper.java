package com.predrinks.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.model.HeroVoucherModel;
import com.predrinks.app.utils.ExpandableTextView;
import com.predrinks.app.utils.URL_Utils;

import java.util.List;

public class HeroOffersAdaper extends RecyclerView.Adapter<HeroOffersAdaper.MyViewHolder> {

    private List<HeroVoucherModel.Datum> pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView offerIv;
        TextView voucherTv, discountTv;
        TextView availedTv, notAvailedTv, eventNameTv;
        ExpandableTextView descTv;

        public MyViewHolder(View view) {
            super(view);

            availedTv = view.findViewById(R.id.avail_and_not_tv);
            notAvailedTv = view.findViewById(R.id.avail_not_tv);
            eventNameTv = view.findViewById(R.id.event_name_tv);
            offerIv = view.findViewById(R.id.imgMainBg);
            voucherTv = view.findViewById(R.id.tvOfferTitle);
            descTv = view.findViewById(R.id.desc_tv);
            discountTv = view.findViewById(R.id.tvOfferDetail);
        }
    }

    public HeroOffersAdaper(Context context, List<HeroVoucherModel.Datum> heroModel) {
        this.mContext = context;
        this.pogoList = heroModel;
    }

    @Override
    public HeroOffersAdaper.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hero_offers_item, parent, false);
        return new HeroOffersAdaper.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HeroOffersAdaper.MyViewHolder holder, int position) {
        HeroVoucherModel.Datum heroModel = pogoList.get(position);

        holder.discountTv.setText(heroModel.getPrice());
        holder.voucherTv.setText(heroModel.getTitle());
        holder.descTv.setText(heroModel.getMessage());

        String imgUrl = "";

        if (TextUtils.isEmpty(heroModel.getImage())) {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + "images/default_con.png";
        } else {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + heroModel.getImage();
        }

        Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_event_venue_image)
                .into(holder.offerIv);

        holder.eventNameTv.setText(heroModel.getVenue());
        if (heroModel.getAvailed())
            holder.availedTv.setVisibility(View.VISIBLE);
        else holder.notAvailedTv.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return pogoList.size();
    }
}

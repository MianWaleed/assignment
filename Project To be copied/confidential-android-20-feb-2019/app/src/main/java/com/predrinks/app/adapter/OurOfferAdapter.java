package com.predrinks.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OurOfferAdapter extends RecyclerView.Adapter<OurOfferAdapter.MyViewHolder> {


    private JSONArray OffersLists;
    Context mContext;
    public static Set<Integer> selection;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgMain)
        ImageView imgMain;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvSubTitle)
        TextView tvSubTitle;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.lnrMain)
        LinearLayout lnrMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvName.setSelected(true);
            tvName.setFocusable(true);
        }
    }


    public OurOfferAdapter(Context context, JSONArray clubLists) {
        this.mContext = context;
        this.OffersLists = clubLists;
        if (selection == null)
            selection = new HashSet<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_our_offers, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        JSONObject jsonObject = null;
        try {
            jsonObject = OffersLists.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonObject == null)
            return;

        if (selection.contains(position)) {
            holder.lnrMain.setBackgroundColor(Color.WHITE);
        } else {
            holder.lnrMain.setBackgroundColor(Color.parseColor("#F0F0F0"));
        }

        holder.tvName.setText(getJsonString(jsonObject, "title"));
        holder.tvSubTitle.setText(getJsonString(jsonObject, "subTitle"));
        holder.tvDateTime.setText(getJsonString(jsonObject, "eventEndDateTime"));
        String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");
        if (isEventDateDisabled.equals("true")){
            holder.tvDateTime.setVisibility(View.INVISIBLE);
        }else {
            holder.tvDateTime.setVisibility(View.VISIBLE);
        }

        String imagename = getJsonString(jsonObject, "eventImage");


        if (TextUtils.isEmpty(imagename)) {
            holder.imgMain.setImageResource(R.mipmap.default_club_logo);
        } else {
            Glide.with(mContext)
                    .load(URL_Utils.URL_IMAGE_DNLOAD + imagename)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .error(R.mipmap.default_club_logo)
                    .into(holder.imgMain);
        }

    }

    @Override
    public int getItemCount() {
        return OffersLists.length();
    }

    private int getImgResource(String imageName) {
        int number = mContext.getResources().getIdentifier(imageName, "mipmap", mContext.getPackageName());
        if (number == 0)
            number = getImgResourceDrawable(imageName);
        return number;
    }

    private int getImgResourceDrawable(String imageName) {
        int nulber = mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
        return nulber;
    }


    public void setSelection(int pos) {
        selection.add(pos);
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

}

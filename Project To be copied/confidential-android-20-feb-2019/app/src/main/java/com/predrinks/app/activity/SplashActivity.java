package com.predrinks.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.kumulos.android.Kumulos;
import com.predrinks.app.R;

public class SplashActivity extends BaseFragmentActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private int progressStatus = 0;
    private static int progress;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Kumulos.pushRegister(this);


        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus = doSomeWork();
                }
                handler.post(() -> {
                    Intent intent = null;
                    if (!isLoggedIn())
                        intent = new Intent(SplashActivity.this, LandingActivity.class);
                    else {
                        if (getIntent() != null && getIntent().getData() != null) {
                            if (getIntent().getData().toString().toLowerCase().equals("confidentials://home")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "home");
                            } else if (getIntent().getData().toString().toLowerCase().equals("confidentials://mypurchases")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "mypurchases");
                            } else if (getIntent().getData().toString().toLowerCase().equals("confidentials://favouritevenues")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "favouritevenues");
                            } else if (getIntent().getData().toString().toLowerCase().equals("confidentials://favouriteevents")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "favouriteevents");
                            } else if (getIntent().getData().toString().toLowerCase().equals("confidentials://emailaddress")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "emailaddress");
                            } else if (getIntent().getData().toString().toLowerCase().contains("mypurchases")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "mypurchases");
                            } else if (getIntent().getData().toString().toLowerCase().contains("email")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class)
                                        .putExtra("email", getIntent().getData().toString().toLowerCase().split("emailaddress=")[1].split("&")[0])
                                        .putExtra("whereTo", "emailaddress");
                            } else if (getIntent().getData().toString().contains("eventId")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class)
                                        .putExtra("whereTo", "details")
                                        .putExtra("eventId", getIntent().getData().toString().split("eventId=")[1]);
                            } else if (getIntent().getData().toString().contains("FavouriteVenues")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "favouritevenues");
                            } else if (getIntent().getData().toString().contains("FavouriteEvents")) {
                                intent = new Intent(SplashActivity.this, MainActivity.class).putExtra("whereTo", "favouriteevents");
                            } else {
                                intent = new Intent(SplashActivity.this, MainActivity.class);
                            }
                        } else
                            intent = new Intent(SplashActivity.this, MainActivity.class);
                    }
                    startActivity(intent);
                    finish();
                });
            }

            private int doSomeWork() {
                try {
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return ++progress;
            }
        }).start();
    }
}

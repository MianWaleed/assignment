package com.predrinks.app.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.adapter.OfferDetailAdapter;
import com.predrinks.app.fragment.ConfidentialsPassFragment;
import com.predrinks.app.fragment.WriteReviewsDialogFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.stripe.StripePayment.Main.StripePaymentActivity;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.ExpandableTextView;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OfferDetailActivity extends BaseFragmentActivity implements SwipeRefreshLayout.OnRefreshListener, OfferDetailAdapter.ClickListener {

    private static final String TAG = "OfferDetailActivity";
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.new_notifications)
    TextView newNotifications;
    @Bind(R.id.llHeaderRight)
    RelativeLayout llHeaderRight;
    @Bind(R.id.imgMainBg)
    ImageView imgMainBg;
    @Bind(R.id.tvRemaining)
    TextView tvRemaining;
    @Bind(R.id.tvOfferTitle)
    TextView tvOfferTitle;
    @Bind(R.id.tvOfferDetail)
    TextView tvOfferDetail;
    @Bind(R.id.tvShare)
    ImageView tvShare;
    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.expand_text_view)
    ExpandableTextView expandTextView;
    @Bind(R.id.expand_text_view1)
    ExpandableTextView expandTextView1;
    @Bind(R.id.expand_text_view_overview)
    ExpandableTextView expandTextViewOverview;

    @Bind(R.id.imgMain)
    ImageView imgMain;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.imgFav)
    ImageView imgFav;
    @Bind(R.id.tvAdd)
    TextView tvAdd;
    @Bind(R.id.tvAdd1)
    TextView tvAdd1;
    @Bind(R.id.tvAdd2)
    TextView tvAdd2;
    @Bind(R.id.tvOffers)
    TextView tvOffers;
    @Bind(R.id.lnrGetDirection)
    RelativeLayout lnrGetDirection;
    @Bind(R.id.tvClubDesc)
    TextView tvClubDesc;
    @Bind(R.id.tvBuyNow)
    TextView tvBuyNow;
    @Bind(R.id.tvAddToCart)
    TextView tvAddToCart;

    @Bind(R.id.tvOverView)
    TextView tvOverView;
    @Bind(R.id.tvOverViewDetail)
    TextView tvOverViewDetail;
    @Bind(R.id.tvSmart)
    TextView tvSmart;
    @Bind(R.id.tvDateTime)
    TextView tvDateTime;
    @Bind(R.id.tvOffersTC)
    TextView tvOffersTC;
    @Bind(R.id.scrollView)
    ScrollView scrollView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.activity_offer_detail)
    RelativeLayout activityOfferDetail;

    @Bind(R.id.lnr_row_club)
    LinearLayout lnr_row_club;

    OfferDetailAdapter adapter;
    private Context mContext;
    private Activity mActivity;

    Paint paint;

    String mUserId = "", eventId = "";
    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_detail);
        ButterKnife.bind(this);
        mContext = this;
        mActivity = this;

        View rowClubView = LayoutInflater.from(mActivity).inflate(R.layout.row_club, null);
        ButterKnife.bind(lnr_row_club, rowClubView);

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

        mPrefs = mActivity.getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);

        tvOffers.setPaintFlags(paint.getFlags());
        mUserId = getLoadedData(Constant.USERID);

        Intent intent = getIntent();
        eventId = intent.getStringExtra("eventId");

        setTextSize();

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setOnChildScrollUpCallback(onChildScrollUpCallback);

        if (MainActivity.ITEM_ADD_TO_CART > 0) {
            newNotifications.setVisibility(View.VISIBLE);
            newNotifications.setText("" + MainActivity.ITEM_ADD_TO_CART);
        } else {
            newNotifications.setVisibility(View.INVISIBLE);
        }

        if (!haveNetworkConnection(mActivity)) {
            Toast.makeText(mActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDataFromWeb();
            }
        }, 1000);
    }

    SwipeRefreshLayout.OnChildScrollUpCallback onChildScrollUpCallback = new SwipeRefreshLayout.OnChildScrollUpCallback() {
        @Override
        public boolean canChildScrollUp(SwipeRefreshLayout parent, @Nullable View child) {
            if (scrollView != null)
                return scrollView.canScrollVertically(-1);

            return false;
        }
    };

    @Override
    public void onRefresh() {
        getDataFromWeb();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        MainActivity.setBackButton(true);
        MainActivity.setVisibleHeaderRightMap(false);
        getDataFromWeb();
    }

    private void getDataFromWeb() {
        String URL = URL_Utils.USER + "/" + mUserId + "/event/" + eventId + "/detail";
        Log.d(TAG, "getDataFromWeb: "+URL);
        swipeRefreshLayout.setRefreshing(true);
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void setTextSize() {
        tvOfferTitle.setTypeface(MainActivity.getTypeFaceLight());
        tvOfferDetail.setTypeface(MainActivity.getTypeFaceMedium());
        tvRemaining.setTypeface(MainActivity.getTypeFaceBold());
        tvOverView.setTypeface(MainActivity.getTypeFaceRegular());
        tvOffersTC.setTypeface(MainActivity.getTypeFaceBold());
        tvOverViewDetail.setTypeface(MainActivity.getTypeFaceRegular());
        tvSmart.setTypeface(MainActivity.getTypeFaceMedium());
        tvDateTime.setTypeface(MainActivity.getTypeFaceBold());
        tvClubDesc.setTypeface(MainActivity.getTypeFaceRegular());
        tvBuyNow.setTypeface(MainActivity.getTypeFaceRegular());
        tvAddToCart.setTypeface(MainActivity.getTypeFaceRegular());
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            Log.d(TAG, error.toString());
        }
    };


    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);

            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    updateUI(response.toString());
                } else {
                    updateUI(response.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    JSONObject jsonObject = null;
    JSONArray offerList = null;
    JSONArray venueDetailList = null;
    String shareMessage = "", venueId;
    boolean mIsActive = false;

    private void updateUI(String response) {
        try {
            JSONObject responseObject = new JSONObject(response);
            jsonObject = responseObject.getJSONObject(Constant.RESULT);
            if (jsonObject != null)
                mIsActive = jsonObject.getBoolean("isActive");

            venueDetailList = jsonObject.getJSONArray(Constant.VENUEDETAIL);
            offerList = jsonObject.getJSONArray(Constant.OFFERLIST);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        venueId = getJsonString(jsonObject, "venueId");
        String title = getJsonString(jsonObject, "title");
        String subTitle = getJsonString(jsonObject, "subTitle");
        String eventImage = getJsonString(jsonObject, "eventImage");
        String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
        String dressCode = getJsonString(jsonObject, "dressCode");
        String eventEndDateTime = getJsonString(jsonObject, "eventEndDateTime");
        String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");

        tvTitleHeader.setSelected(true);
        tvTitleHeader.setFocusable(true);
        tvOfferTitle.setSelected(true);
        tvOfferTitle.setFocusable(true);
        tvOfferDetail.setSelected(true);
        tvOfferDetail.setFocusable(true);


        MainActivity.setHeaderTitle(title);
        tvTitleHeader.setText(title);

        tvOfferTitle.setText(title);
        tvOfferDetail.setText(subTitle);
        tvDateTime.setText(eventEndDateTime);
        tvSmart.setText(dressCode);

        if (isEventDateDisabled.equals("true")){
            tvDateTime.setVisibility(View.INVISIBLE);
        }else {
            tvDateTime.setVisibility(View.VISIBLE);
        }

        if (totalRemainingOffers.contains("-") || totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out")) {
            tvRemaining.setVisibility(View.GONE);
        } else {
            tvRemaining.setText(totalRemainingOffers.toUpperCase());
            tvRemaining.setVisibility(View.VISIBLE);
        }
        expandTextViewOverview.setText(getJsonString(jsonObject, "overview"), "");

        String tc = getJsonString(jsonObject, "termsNConditions");
        expandTextView.setText(tc, "");
        Glide.with(mContext)
                .load( eventImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_event_venue_image)
                .into(imgMainBg);

        JSONObject objectvenueDetail = null;
        if (venueDetailList != null) {
            try {
                objectvenueDetail = venueDetailList.getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String imageName = getJsonString(objectvenueDetail, "logo");
        String name = getJsonString(objectvenueDetail, "name");
        String isFavoriteVenue = getJsonString(objectvenueDetail, "isFavoriteVenue");
        String add2 = getJsonString(objectvenueDetail, "address2");
        String description = getJsonString(objectvenueDetail, "description");

        String address;
        if (TextUtils.isEmpty(add2)) {
            address = getJsonString(objectvenueDetail, "address1");
        } else {
            address = getJsonString(objectvenueDetail, "address1");
        }
        if (!isFavoriteVenue.equals("false")) {
            imgFav.setImageResource(R.mipmap.favourite_fill);
            isFavourite = true;
        } else {
            isFavourite = false;
            imgFav.setImageResource(R.mipmap.favourite_default);
        }
        String[] addressNew = address.split(",");
        tvName.setText(name);

        tvAdd.setText(" "+address.replace(",", "\n"));
        tvAdd.setLineSpacing((float) 1.5, (float) 1.0);
        tvAdd1.setText(getJsonString(objectvenueDetail, "postCode"));
        tvAdd1.setVisibility(View.GONE);
        tvAdd2.setVisibility(View.INVISIBLE);
        tvAdd2.setText(getJsonString(objectvenueDetail, "city"));
        expandTextView1.setText(description, "");

        Glide.with(mContext)
                .load(URL_Utils.URL_IMAGE_DNLOAD + imageName)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_club_logo)
                .into(imgMain);

        if (offerList == null || offerList.length() == 0) {
            adapter = null;
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = 1;
            listView.setLayoutParams(params);
            listView.setAdapter(null);
            return;
        }
        adapter = new OfferDetailAdapter(mActivity, mContext, offerList,this);
        listView.setFocusable(false);
        listView.setAdapter(adapter);
        MainActivity.setListViewHeightBasedOnChildren(listView);

        String offer = "", discountedPrice = "", discountAmount = "", discountType = "", originalPrice = "";
        try {
            offer = offerList.getJSONObject(0).getString("title");
            discountedPrice = offerList.getJSONObject(0).getString("discountedPrice");
            discountAmount = offerList.getJSONObject(0).getString("discountAmount");
            discountType = getJsonString(offerList.getJSONObject(0), "discountType");
            originalPrice = getJsonString(offerList.getJSONObject(0), "originalPrice");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String offerDiscount = "";
        if (discountType.equals("Percentage")) {
            offerDiscount = "SAVE " + discountAmount + "%";
        } else {
            offerDiscount =  "SAVE " + "£" + discountAmount;
        }

        shareMessage = "£"+ originalPrice +" gift voucher @ " + name + " for £" + discountedPrice + ". Download Confidentials app to "+ offerDiscount +" on "+ offer + " £" + originalPrice +" gift voucher \n iPhone: https://goo.gl/78ymV1 , Android: https://goo.gl/YMXCVA";

    }

    boolean isFavourite = false;
    boolean isAddToCart = false;
    Set<Integer> mSelected;
    HashMap<Integer, Integer> mQtyList;

    @OnClick({R.id.llHeaderRight, R.id.tvShare, R.id.lnrGetDirection, R.id.tvAddToCart, R.id.tvBuyNow, R.id.tvOffers, R.id.imgFav, R.id.lnr_row_club, R.id.imgBack})
    public void onClick(View view) {
        Intent intent;
        mSelected = new HashSet<>();
        mQtyList = new HashMap<>();
        switch (view.getId()) {
            case R.id.llHeaderRight:
                intent = new Intent(this, CartMainActivity.class);
                startActivity(intent);
                break;
            case R.id.tvShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, ""));
                break;
            case R.id.lnrGetDirection:
                String venueresp = "";
                try {
                    venueresp = venueDetailList.getJSONObject(0).toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                intent = new Intent();
                intent.putExtra("venueresp", venueresp);
                intent.putExtra("call", "GetDirectionMapFragment");
                setResult(100, intent);
                finish();
                break;
            case R.id.tvAddToCart:

                if (!haveNetworkConnection(mActivity)) {
                    Toast.makeText(mActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
                    break;
                }
                if (adapter == null) {
                    dialog_info(Constant.CONFIDENTIALS, "No offers available");
                    break;
                }

                if (!isLoggedIn()) {
                    dialog_Login(mActivity);
                    break;
                }
                try {
                    String date = getBirthDate(getLoadedData(Constant.USER_DETAIL));
                    if (TextUtils.isEmpty(date)) {
                        dialog_Profile(mActivity);
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mTransactionFee = 0.00;
                isAddToCart = true;
                mSelected = adapter.getSelectedPosition();
                mQtyList = adapter.getQtyListPosition();
                if (mSelected.size() > 0) {
                    addtoCart();
                } else {
                    Toast.makeText(mActivity, "Please select an offer first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvBuyNow:

                if (!haveNetworkConnection(mActivity)) {
                    Toast.makeText(mActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
                    break;
                }

                if (adapter == null) {
                    dialog_info(Constant.CONFIDENTIALS, "No offers available");
                    break;
                }
                if (!isLoggedIn()) {
                    dialog_Login(mActivity);
                    break;
                }
                try {
                    String date = getBirthDate(getLoadedData(Constant.USER_DETAIL));
                    if (TextUtils.isEmpty(date)) {
                        dialog_Profile(mActivity);
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mTransactionFee = 0.00;
                double mTotal = 0;
                isAddToCart = false;
                mSelected = adapter.getSelectedPosition();
                mQtyList = adapter.getQtyListPosition();

                ArrayList<Integer> arrayListSelected = new ArrayList<Integer>();
                for (Integer str : mSelected) {
                    arrayListSelected.add(str);
                }
                final JSONArray paramOfferArray = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                for (int i = 0; i < arrayListSelected.size(); i++) {
                    int pos = arrayListSelected.get(i);
                    try {
                        JSONObject innerObject = new JSONObject();
                        double price = offerList.getJSONObject(pos).getDouble("discountedPrice");
                        String offerId = offerList.getJSONObject(pos).getString("offerId");
                        double transactionFee = offerList.getJSONObject(pos).getDouble("transactionFee");
                        int Quantity = mQtyList.get(pos);
                        Quantity += 1;

                        double total = price * Quantity;

                        String TotalPrice = String.format("%.2f", total);
                        if (TotalPrice.contains(",")) {
                            if (!TotalPrice.contains(".")) {
                                TotalPrice = TotalPrice.replace(",", ".");
                            }
                        }

                        double totalp = Double.parseDouble(TotalPrice);
                        innerObject.put("OfferID", Integer.parseInt(offerId));
                        innerObject.put("Quantity", Quantity);
                        innerObject.put("Price", price);
                        innerObject.put("TotalPrice", totalp);
                        innerObject.put("VenueID", venueId);
                        innerObject.put("EventID", eventId);

                        String userDetail = getLoadedData(Constant.USER_DETAIL);
                        JSONObject userData = new JSONObject(userDetail);
                        boolean isSubscribed = userData.getBoolean("isSubscribed");
                        if (!isSubscribed){
                            if (transactionFee > mTransactionFee) {
                                mTransactionFee = transactionFee;
                            }
                        }
                        paramOfferArray.put(innerObject);
                        mTotal += totalp;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
//                double finalAmt = mTotal - mTransactionFee;
                if (paramOfferArray.length() > 0) {
                    if (mTotal >= 3) {
                        JSONObject js = new JSONObject();
                        try {
                            js.put(Constant.ORDERID, 0);
                            js.put(URL_Utils.USER_ID, Long.parseLong(mUserId));
                            js.put(Constant.NETAMOUNT, mTotal);
                            js.put(Constant.STRIPETOKENTOBEPROCESSED, "");
                            js.put(Constant.CARDREGION, "");
                            js.put(Constant.CARTITEMS, paramOfferArray);
                            js.put(Constant.ISADDEDTOCART, false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String URL = URL_Utils.GET_REMAINING_OFFER_QTY;
                        final double finalMTotal = mTotal + mTransactionFee;
                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, js,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.d(TAG, response.toString());
                                        try {
                                            String status = response.getString("status");
                                            String message = response.getString("message");
                                            boolean isBack = false;
                                            if (status.equals("1")) {
                                                leftOutOffers = new JSONArray(response.getString("leftOutOffers"));
                                                if (leftOutOffers.length() > 0){
                                                    for (int i = 0; i < leftOutOffers.length(); i++) {
                                                        JSONObject object = (JSONObject) leftOutOffers.get(i);
                                                        if (object != null){
                                                            long remainingOfferQty = object.getLong("remainingOfferQty");
                                                            if (remainingOfferQty < 0){
                                                                isBack = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    if (!isBack){
                                                        Intent intent = new Intent(mActivity, StripePaymentActivity.class);
                                                        intent.putExtra("NetAmount", finalMTotal);
                                                        intent.putExtra("orderId", 0);
                                                        intent.putExtra("VenueID", venueId);
                                                        intent.putExtra("CartItems", paramOfferArray.toString());
                                                        intent.putExtra("IsAddedToCart", false);
                                                        intent.putExtra("transactionFee", mTransactionFee);
                                                        //
                                                        hideKeyBoard(mActivity);
                                                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                                                        startActivity(intent);
                                                    }else {
                                                        showDialog();
                                                    }
                                                }else {
                                                }
                                            }  else {
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                                headers.put("Accept", "application/json");
                                return headers;
                            }

                            @Override
                            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                                try {
                                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                                    return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                                } catch (UnsupportedEncodingException e) {
                                    return Response.error(new ParseError(e));
                                } catch (JSONException je) {
                                    return Response.error(new ParseError(je));
                                }
                            }

                            @Override
                            protected VolleyError parseNetworkError(VolleyError volleyError) {
                                if (volleyError.networkResponse != null
                                        && volleyError.networkResponse.data != null) {
                                    VolleyError error = new VolleyError(new String(
                                            volleyError.networkResponse.data));
                                    volleyError = error;
                                }

                                return volleyError;
                            }

                        };
                        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                                10000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
                    } else {
                        Toast.makeText(mActivity, "Minimum spend £3.00", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Please select an offer first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvOffers:
                String VenueId = getJsonString(jsonObject, "venueId");
                intent = new Intent();
                intent.putExtra("venueId", VenueId);
                intent.putExtra("call", "OurOffersFragment");
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.imgFav:
                if (!isLoggedIn()) {
                    dialog_Login(mActivity);
                    break;
                }
                if (venueDetailList == null){
                    break;
                }
                JSONObject jsonObject = null;
                try {
                    jsonObject = venueDetailList.getJSONObject(0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String VenueId1 = getJsonString(jsonObject, "venueId");
                if (!isFavourite) {
                    isFavourite = true;
                    String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + VenueId1 + "/" + "user" + "/" + mUserId + "/" + "markasfavouritevenue";
                    CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, favSuccessListener, favErrorListener);
                    VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                } else {
                    dialog_info(VenueId1);

                }
                break;
            case R.id.lnr_row_club:
                JSONObject object = null;
                if (venueDetailList != null) {
                    try {
                        object = venueDetailList.getJSONObject(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (object == null)
                    break;

                String venueId = getJsonString(object, "venueId");
                String title = getJsonString(object, "name");
                intent = new Intent();
                intent.putExtra("venueId", venueId);
                intent.putExtra("name", title);
                intent.putExtra("call", "ClubDetailFragment");
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }
    JSONArray leftOutOffers;
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(112, intent);
        finish();
    }

    private String getBirthDate(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        String birthDay = jsonObject.getString("dOB");
        return birthDay;
    }


    private void showDialog(){
        final Dialog dialog = new Dialog(mActivity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText("We cannot proceed further, Please review your order, one or more offers are no longer available.");

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    double mTransactionFee = 0.00;
    private void addtoCart() {
        ArrayList<Integer> arrayListSelected = new ArrayList<Integer>();
        for (Integer str : mSelected) {
            arrayListSelected.add(str);
        }
        JSONArray paramOfferArray = new JSONArray();
        Map<String, String> params = new HashMap<String, String>();
        for (int i = 0; i < arrayListSelected.size(); i++) {
            int pos = arrayListSelected.get(i);
            try {
                JSONObject innerObject = new JSONObject();
                double price = offerList.getJSONObject(pos).getDouble("discountedPrice");
                double transactionFee = offerList.getJSONObject(pos).getDouble("transactionFee");
                String offerId = offerList.getJSONObject(pos).getString("offerId");
                int Quantity = mQtyList.get(pos);
                Quantity += 1;

                double total = price * Quantity;
                String TotalPrice = String.format("%.2f", total);
                if (TotalPrice.contains(",")) {
                    if (!TotalPrice.contains(".")) {
                        TotalPrice = TotalPrice.replace(",", ".");
                    }
                }
                double totalp = Double.parseDouble(TotalPrice);
                innerObject.put("OfferID", Integer.parseInt(offerId));
                innerObject.put("Quantity", Quantity);
                innerObject.put("Price", price);
                innerObject.put("TotalPrice", totalp);
                innerObject.put("VenueID", venueId);
                innerObject.put("eventId", eventId);

                String userDetail = getLoadedData(Constant.USER_DETAIL);
                JSONObject userData = new JSONObject(userDetail);
                boolean isSubscribed = userData.getBoolean("isSubscribed");
                if (!isSubscribed){
                    if (transactionFee > mTransactionFee) {
                        mTransactionFee = transactionFee;
                    }
                }

                paramOfferArray.put(innerObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject js = new JSONObject();
        try {
            js.put("UserId", mUserId);
            js.put("IsAddedToCart", true);
            js.put("CartItems", paramOfferArray);
            js.put("TransactionFee", mTransactionFee);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String URL = URL_Utils.ADD_CART_NEW;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            String success = response.getString("success");
                            if (success.equals("1")) {
                                if (mPrefs.getLong("alarmExpireTime", 0) == 0) {
                                    Utils.startAlarmBroadcastReceiver(mActivity, 1111, (3 * AlarmManager.INTERVAL_FIFTEEN_MINUTES));
                                    Utils.startAlarmBroadcastReceiver(mActivity, 1113, AlarmManager.INTERVAL_HOUR);
                                    mPrefs.edit().putLong("alarmExpireTime", System.currentTimeMillis() + AlarmManager.INTERVAL_HOUR).apply();
                                }
                                MainActivity.ITEM_ADD_TO_CART += mSelected.size();
                                MainActivity.ITEM_ADD_TO_CART = response.getInt("cartItemCount");
                                MainActivity.setHeaderNotificationNumber(MainActivity.ITEM_ADD_TO_CART);
                                saveLoadedData("numberOfAddToCart", "" + MainActivity.ITEM_ADD_TO_CART);
                                mSelected.clear();
                                mQtyList.clear();
                                if (MainActivity.ITEM_ADD_TO_CART > 0) {
                                    newNotifications.setVisibility(View.VISIBLE);
                                    newNotifications.setText("" + MainActivity.ITEM_ADD_TO_CART);
                                } else {
                                    newNotifications.setVisibility(View.INVISIBLE);
                                }
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }

                                if (!isAddToCart) {
                                    startActivity(new Intent(mActivity, CartMainActivity.class));
                                } else {
                                    final Dialog alertDialog = new Dialog(mActivity);
                                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    alertDialog.setContentView(R.layout.add_to_cart_dialog);
                                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    alertDialog.show();
                                    alertDialog.setCancelable(true);
                                    alertDialog.setCanceledOnTouchOutside(true);
                                    new CountDownTimer(2000, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                        }

                                        @Override
                                        public void onFinish() {
                                            alertDialog.dismiss();
                                        }
                                    }.start();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);

    }

    Response.ErrorListener favErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };


    Response.Listener<JSONObject> favSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    if (isFavourite) {
                        imgFav.setImageResource(R.mipmap.favourite_fill);
                        Toast.makeText(mContext, "Venue favourited", Toast.LENGTH_SHORT).show();
                    } else {
                        imgFav.setImageResource(R.mipmap.favourite_default);
                        Toast.makeText(mContext, "Venue Unfavourited", Toast.LENGTH_SHORT).show();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    protected void dialog_info(final String VenueId) {
        final Dialog dialog = new Dialog(mContext, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFavourite = false;
                String URL = URL_Utils.MARKAS_FAVOURITE_VENUE + "/" + VenueId + "/" + "user" + "/" + mUserId + "/" + "removeasfavouritevenue";
                CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, favSuccessListener, favErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                dialog.dismiss();
            }
        };
        View.OnClickListener dialogCancel = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };


        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
        ((TextView) dialog.findViewById(R.id.message)).setText(Constant.UNLIKE);
        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
        ((View) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(dialogCancel);
        ((Button) dialog.findViewById(R.id.okBtn)).setText("Yes");
        dialog.show();
    }

    @Override
    public void showWriteReview() {
        android.app.FragmentManager fm = getFragmentManager();
        WriteReviewsDialogFragment editNameDialogFragment = new WriteReviewsDialogFragment();
        editNameDialogFragment.show(fm, "");
    }

    @Override
    public void showReviews() {
        startActivity(new Intent(this, ReviewsActivity.class));
    }
}

package com.predrinks.app.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.billing.IabHelper;
import com.predrinks.app.billing.IabResult;
import com.predrinks.app.billing.Inventory;
import com.predrinks.app.billing.Purchase;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubscriptionsFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "SubscriptionsFragment";
    @Bind(R.id.tvhero)
    TextView tvhero;
    @Bind(R.id.tvAccess)
    TextView tvAccess;
    @Bind(R.id.tvVoucher)
    TextView tvVoucher;
    @Bind(R.id.tvMonthSubscription)
    TextView tvMonthSubscription;
    @Bind(R.id.tvMonthlyPrice)
    TextView tvMonthlyPrice;
    @Bind(R.id.rlyMonthly)
    RelativeLayout rlyMonthly;
    @Bind(R.id.tvYearSubscription)
    TextView tvYearSubscription;
    @Bind(R.id.tvYearlyPrice)
    TextView tvYearlyPrice;
    @Bind(R.id.rlyYearly)
    RelativeLayout rlyYearly;
    @Bind(R.id.tvAllSubscription)
    TextView tvAllSubscription;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    Activity mActivity;
    Context mContext;

    static final String SKU_INFINITE_MONTHLY = "infinite_monthly";
    static final String SKU_INFINITE_YEARLY = "013";
    static final int RC_REQUEST = 10001;
    IabHelper iabHelper;
    private boolean mBillingServiceReady;
    private boolean subBool;
    String mUserId = "";

    public SubscriptionsFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubscriptionsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SubscriptionsFragment newInstance(String param1, String param2) {
        SubscriptionsFragment fragment = new SubscriptionsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mActivity = getActivity();
        mContext = getActivity();

        mUserId = getLoadedData(mContext, Constant.USERID);
        initialiseBilling();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscriptions, container, false);
        ButterKnife.bind(this, view);
        hideKeyBoardByView(mContext, view);

        tvhero.setTypeface(MainActivity.getTypeFaceRegular());
        tvAccess.setTypeface(MainActivity.getTypeFaceRegular());
        tvVoucher.setTypeface(MainActivity.getTypeFaceRegular());
        tvMonthSubscription.setTypeface(MainActivity.getTypeFaceBold());
        tvMonthlyPrice.setTypeface(MainActivity.getTypeFaceRegular());
        tvYearSubscription.setTypeface(MainActivity.getTypeFaceBold());
        tvYearlyPrice.setTypeface(MainActivity.getTypeFaceRegular());
        tvAllSubscription.setTypeface(MainActivity.getTypeFaceRegular());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.header_layout_inner.setVisibility(View.VISIBLE);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        MainActivity.setBackButton(true);
        MainActivity.setVisibleHeaderRightMap(false);
        MainActivity.setHeaderTitle("SUBSCRIPTION");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (iabHelper != null) {
            try {
                iabHelper.dispose();
            } catch (IabHelper.IabAsyncInProgressException e) {
                e.printStackTrace();
            }
            iabHelper = null;
        }
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.rlyMonthly, R.id.rlyYearly})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlyMonthly:
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                if (mBillingServiceReady) {
                    if (iabHelper != null)
                        iabHelper.flagEndAsync();
                    String payload = "";
                    try {
                        iabHelper.launchPurchaseFlow(mActivity, SKU_INFINITE_MONTHLY, RC_REQUEST, mPurchaseFinishedListener, payload);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }
                } else {
                    showToast();
                }
                break;
            case R.id.rlyYearly:
                if (!isLoggedIn(mActivity)) {
                    dialog_Login(mActivity);
                    return;
                }
                showDialog(mActivity);
                break;
        }
    }

    protected void showDialog(final Activity activity) {
        String title, mes;
        String textbtn;
        title = "Confirm purchase";
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String date = df.format(c);
        mes = "Pay £36.99 for 1-year subscription starting "+date;

        textbtn = "Continue";

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_yesno);
        dialog.setCancelable(false);

        View.OnClickListener loginClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (mBillingServiceReady) {
                    if (iabHelper != null)
                        iabHelper.flagEndAsync();
                    String payload = "" + 013;
                    try {
                        iabHelper.launchSubscriptionPurchaseFlow(mActivity, SKU_INFINITE_YEARLY, RC_REQUEST, mPurchaseFinishedListener, payload);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }
                } else {
                    showToast();
                }
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(mes);
        ((Button) dialog.findViewById(R.id.okBtn)).setText(textbtn);

        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(loginClick);
        ((Button) dialog.findViewById(R.id.cancelBtn)).setOnClickListener(cancelClick);
        dialog.show();
    }

    private void showToast() {
        Toast.makeText(getActivity(), "Purchase requires Google Play Store (billing) on your Android.", Toast.LENGTH_LONG).show();
    }


    private void initialiseBilling() {
        if (iabHelper != null) {
            return;
        }

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgK0gDQmS66DXqJXTQnUDPfSDDpOUOVqjrmJllkJy3MHfa306jVhxVz/QlF3XzBf+ZYp5Sm0AHSH/uHAwQnN1UTT2IwCZgzexMXcVgHjer+b3fYVZ6YpxPlyXgFVFdwge8b4NnoQTh4fXrqZHE1xYGyTOxT2Agmc4vFU4lrNTc1NJNzMooVUck471cBmeKAzGuTakq8lm7ARDUzJuxw+IZ+skPXAEW8HiYl9D05TcwfS0jcT09aM7mb/aaw70uaj7QqL6Y/koCxm+MhYM6aD8k4UsK3BfzkiloYJ4Bj+IgU/GGbMOFud8w4K4My52Xq35wIeHdPaDzJClmjqgPVZTmQIDAQAB";
        iabHelper = new IabHelper(getActivity(), base64EncodedPublicKey);
        iabHelper.enableDebugLogging(true);

        Log.d(TAG, "Starting setup.");
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                // Have we been disposed of in the meantime? If so, quit.
                if (iabHelper == null) {
                    return;
                }

                // Something went wrong
                if (!result.isSuccess()) {
                    Log.d(TAG, "Problem setting up in-app billing: " + result.getMessage());
                    return;
                }
                // IAB is fully set up. Now, let's get an inventory of stuff we
                // own.
                Log.d(TAG, "In-app Billing is set up OK");
                mBillingServiceReady = true;

                try {
                    iabHelper.queryInventoryAsync(iabInventoryListener());
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private View window;
    // Callback for when a purchase is finished
    private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "mPurchaseFinishedListener");
            // if we were disposed of in the meantime, quit.
            if (iabHelper == null) {
                return;
            }

            // Don't complain if cancelling
            if (result.getResponse() == IabHelper.IABHELPER_USER_CANCELLED) {
                Log.d(TAG, "User canceled purchase");
                return;
            }

            if (!result.isSuccess()) {
                Log.d(TAG, "Error purchasing: " + result.getMessage());
                if (result.getResponse() == 7) {
                    Toast.makeText(getActivity(), "item already purchased!", Toast.LENGTH_LONG).show();
                }
                return;
            }

            if (!verifyDeveloperPayload(purchase)) {
                Log.d(TAG, "Error purchasing. Authenticity verification failed.");
                return;
            }

            // Purchase was success! Update accordingly
            if (purchase.getSku().equals(SKU_INFINITE_YEARLY) || purchase.getSku().equals(SKU_INFINITE_MONTHLY)) {
                Toast.makeText(getActivity(), "You sucessfully subscribe", Toast.LENGTH_SHORT).show();
                subBool = true;

            }

            initialiseStuff(subBool);
        }
    };


    private IabHelper.QueryInventoryFinishedListener iabInventoryListener() {
        return new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                // Have we been disposed of in the meantime? If so, quit.
                if (iabHelper == null) {
                    return;
                }

                // Something went wrong
                if (!result.isSuccess()) {
                    Log.d(TAG, "onFailure: QueryInventoryFinishedListener," + "result: " + result);
                    return;
                }
                Log.d(TAG, "Checking for purchases");
                //  Purchase purchaseSubscription = inventory.getPurchase(ITEM_SKU_SUBSCRIPTION);

                Purchase subscriptionMonthly = inventory.getPurchase(SKU_INFINITE_MONTHLY);
                Purchase subscriptionYearly = inventory.getPurchase(SKU_INFINITE_YEARLY);
                if (subscriptionMonthly != null && subscriptionMonthly.isAutoRenewing()) {
                    //mAutoRenewEnabled = true;
                    subBool = (subscriptionMonthly != null && verifyDeveloperPayload(subscriptionMonthly));

                } else if (subscriptionYearly != null && subscriptionYearly.isAutoRenewing()) {
                    //mAutoRenewEnabled = true;
                    subBool = (subscriptionYearly != null && verifyDeveloperPayload(subscriptionYearly));
                } else {
                    // mAutoRenewEnabled = false;
                    subBool = false;
                }

                initialiseStuff(subBool);
            }
        };
    }

    private void initialiseStuff(boolean subBool) {
        if (subBool) {
            getDataFromWeb();
        } else {
        }
    }

    boolean verifyDeveloperPayload(Purchase purchase) {
        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (iabHelper == null)
            return;
        // Pass on the activity result to the helper for handling
        if (!iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    private void getDataFromWeb() {
        double SubscriptionAmount = 36.99;
        String SubscriptionMode = "Yearly"; // SubscriptionMode: Monthly, Yearly
        String URL = URL_Utils.USER + "/" + mUserId + "/updatesubscrition/" + SubscriptionMode + "/amount/" + SubscriptionAmount + "";
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
            getDataFromWeb();
        }
    };


    private String mResponse;
    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    mResponse = response.toString();
                    saveLoadedData(mActivity, "updatesubscrition", mResponse);
                }
                getUserDetail(mActivity, mUserId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    protected void getUserDetail(final Context context, String userId) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("BaseFragment", error.toString());

            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("BaseFragment", response.toString());
                JSONObject result = null;
                try {
                    result = response.getJSONObject("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                saveLoadedData(context, Constant.USER_DETAIL, result.toString());
            }
        };
        String URL = URL_Utils.USER + "/" + userId + "/detail";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }
}

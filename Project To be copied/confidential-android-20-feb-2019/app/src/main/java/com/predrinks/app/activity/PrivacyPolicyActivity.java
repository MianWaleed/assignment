package com.predrinks.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrivacyPolicyActivity extends BaseFragmentActivity {
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "PrivacyPolicyActivity";
    String mJson;
    JSONArray mJsonArray;

    @Bind(R.id.tvDesc)
    TextView tvDesc;
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;


    private String mParam1;
    ProgressDialog pDialog;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        ButterKnife.bind(this);

        mContext = this;

        Intent intent = getIntent();
        mParam1 = intent.getStringExtra(ARG_PARAM1);
        String title = intent.getStringExtra("title");
        tvTitleHeader.setText(title);

        String url = "";
        if (mParam1.equals("0")){
            url = URL_Utils.PRIVACY_POLICY;
        }else if (mParam1.equals("1")){
            url = URL_Utils.TERMS_AND_CONDITIONS;
        }else if (mParam1.equals("2")) {
            url = URL_Utils.TRANSACTION_FEES;
        }

        if (!haveNetworkConnection(this)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        CustomRequest jsObjRequest = new CustomRequest(url, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }


    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            pDialog.dismiss();
            try {
                Log.d(TAG, error.toString());
            } catch (RuntimeException e) {
            }
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            pDialog.dismiss();
            try {
                String message = response.getString("success");
                if (message.equals("1")) {
                    String Content = response.getString("content");
                    tvDesc.setText(Content);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void onClick() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

package com.predrinks.app.stripe.StripePayment.Main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.activity.BaseFragmentActivity;
import com.predrinks.app.activity.PaymentSuccessActivity;
import com.predrinks.app.adapter.CardDetail;
import com.predrinks.app.database.PreDrinkOrderDAO;
import com.predrinks.app.database.UserCardDetail;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.stripe.Stripe.CustomForm.Fields.CreditCardText;
import com.predrinks.app.stripe.Stripe.CustomForm.Fields.ExpDateText;
import com.predrinks.app.stripe.Stripe.CustomForm.Fields.SecurityCodeText;
import com.predrinks.app.stripe.Stripe.CustomForm.Internal.CreditCardEntry;
import com.predrinks.app.stripe.Stripe.CustomForm.Layout.CardValidCallback;
import com.predrinks.app.stripe.Stripe.CustomForm.Layout.CreditCard;
import com.predrinks.app.stripe.Stripe.CustomForm.Layout.CreditCardForm;
import com.predrinks.app.stripe.StripePayment.ErrorDialogFragment;
import com.predrinks.app.stripe.StripePayment.PaymentForm;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.utils.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cards.pay.paycardsrecognizer.sdk.ScanCardIntent;

public class StripePaymentActivity extends BaseFragmentActivity implements PaymentForm {

    @Bind(R.id.actionbar_title)
    TextView tvActionbarTitle;

    @Bind(R.id.relStripeMain)
    RelativeLayout relStripeMain;
    @Bind(R.id.rt_right)
    RelativeLayout rt_right;
    @Bind(R.id.tvCardDetail)
    TextView tvCardDetail;
    @Bind(R.id.txtPay)
    TextView txtPay;
    @Bind(R.id.imgBack)
    ImageView imgBack;
    @Bind(R.id.listSavedCard)
    ListView listSavedCard;
    @Bind(R.id.checkboxSAveCard)
    CheckBox checkboxSAveCard;
    @Bind(R.id.iconInfo)
    ImageView iconInfo;
    @Bind(R.id.tvTransactionFees)
    TextView tvTransactionFees;

    FragmentActivity mainActivity;
    public static Button btnPayNow = null;
    Button cancelButton;

    private static final String TAG = "Payment";
    CreditCardForm noZipForm;
    public static EditText postCodeEditText;
    LinearLayout postCodeContainer;

    public static TextView hintTextView = null;

    String cardLast4 = "", cardToken = "", cardNumber = "", card_region = "", token_id = "";
    int cardExpMonth, cardExpYear, cardType;
    SharedPreferences SharedPrefExpDate, mPrefs;
    Editor editorExpDate;
    double netAmount, mTransactionFee = 0.00;
    WebView webview1;

    SharedPreferences sharedPrefPayment, sharedPrefOfferPayment;
    Editor editorPayment, editorOfferPayment;
    int paymentCount = 0;

    CardValidCallback cardValidCallback = new CardValidCallback() {
        @Override
        public void cardValid(CreditCard card) {
            Log.d(TAG, "valid card: " + card);
        }
    };

    RequestQueue requestQueue;
    private View back;
    Window window;

    boolean isSaveCard = false;
    int orderId = 0;
    JSONArray cartItemsArray;

    Context mContext;
    private boolean IsAddedToCart;
    private String mUserId;

    PreDrinkOrderDAO preDrinkOrderDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripepayment);
        ButterKnife.bind(this);
        SecurityCodeText.isCvcValid = false;
        mContext = this;
        mainActivity = this;
        window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        progDialog = new ProgressDialog(mContext);
        progDialog.setCancelable(false);
        checkboxSAveCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isSaveCard = b;
            }
        });

        mPrefs = getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);
        mUserId = getLoadedData(Constant.USERID);
        Intent intent = getIntent();

        preDrinkOrderDAO = new PreDrinkOrderDAO(mainActivity);

        if (intent.getExtras() != null) {
            orderId = intent.getIntExtra("orderId", 0);
            String item = intent.getStringExtra("CartItems");
            try {
                cartItemsArray = new JSONArray(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            netAmount = intent.getDoubleExtra("NetAmount", 0.00);
            mTransactionFee = intent.getDoubleExtra("transactionFee", 0.00);
            IsAddedToCart = intent.getBooleanExtra("IsAddedToCart", false);

            // netAmount = netAmount + mTransactionFee;
        }

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvActionbarTitle.setText("PAYMENT");
        tvTransactionFees.setText("Transaction fee: £" + String.format("%.2f", mTransactionFee));
        iconInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_info_Fees("Transaction Fees",
                        "When you make a purchase through our services, we may charge you a transaction fee. These fees go towards improving our products and services in the future.",
                        "Terms and Conditions apply, see the “More” section for details");
            }
        });

        hideKeyBoard();
        SharedPrefExpDate = getSharedPreferences("SharedPrefExpDate", Context.MODE_PRIVATE);
        editorExpDate = SharedPrefExpDate.edit();

        sharedPrefPayment = getSharedPreferences("SharedPrefPayment", Context.MODE_PRIVATE);
        editorPayment = sharedPrefPayment.edit();

        sharedPrefOfferPayment = getSharedPreferences("SharedPrefOfferPayment", Context.MODE_PRIVATE);
        editorOfferPayment = sharedPrefOfferPayment.edit();

        requestQueue = Volley.newRequestQueue(this);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }


        btnPayNow = (Button) findViewById(R.id.btnPayNow);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        hintTextView = (TextView) findViewById(R.id.hintTextView);
        postCodeEditText = (EditText) findViewById(R.id.postCodeEditText);
        postCodeContainer = (LinearLayout) findViewById(R.id.postCodeContainer);
        webview1 = (WebView) findViewById(R.id.webview1);

        String payAmount = getString(R.string.pound_sign) + String.valueOf(String.format("%.2f", netAmount));

        txtPay.setText(getString(R.string.payment) + " " + payAmount);
        btnPayNow.setText(getString(R.string.payment) + " " + payAmount);


        noZipForm = (CreditCardForm) findViewById(R.id.form_no_zip);
        noZipForm.setOnCardValidCallback(cardValidCallback);

        noZipForm.setFocusable(true);
        noZipForm.setFocusableInTouchMode(true);
        noZipForm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        txtPay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                editorOfferPayment.putBoolean("OfferPaymentClose", true);
                editorOfferPayment.apply();

                payClick();
            }
        });

        btnPayNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                payClick();
            }
        });
        rt_right.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                payClick();
            }
        });

        cancelButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        postCodeEditText.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start, int end, Spanned spanned, int dStart, int dEnd) {
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches("[a-zA-Z0-9 ]+")) {
                            return cs;
                        }
                        return "";
                    }
                }
        });

        postCodeEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() > 0) {
                    btnPayNow.setEnabled(true);
                } else {
                    btnPayNow.setEnabled(false);
                }
                btnPayNow.setEnabled(true);
            }
        });


        CreditCardEntry.textFourDigits.setEnabled(true);
        CreditCardEntry.textFourDigits.setFocusable(true);
        CreditCardEntry.textFourDigits.setFocusableInTouchMode(true);
        CreditCardEntry.expDateText.setEnabled(true);
        CreditCardEntry.expDateText.setFocusable(true);
        CreditCardEntry.expDateText.setFocusableInTouchMode(true);
        setSaveCard();

        if (!haveNetworkConnection()) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }
        getRemainingOfferQty();
    }


    static final int REQUEST_CODE_SCAN_CARD = 11111;

    @OnClick(R.id.img_info_icon)
    public void scanCard() {
        Intent intent = new ScanCardIntent.Builder(this).build();
        startActivityForResult(intent, REQUEST_CODE_SCAN_CARD);
    }

    JSONArray leftOutOffers;

    private void getRemainingOfferQty() {

        JSONObject js = new JSONObject();
        try {
            js.put(Constant.ORDERID, orderId);
            js.put(URL_Utils.USER_ID, Long.parseLong(mUserId));
            js.put(Constant.NETAMOUNT, netAmount);
            js.put(Constant.STRIPETOKENTOBEPROCESSED, "");
            js.put(Constant.CARDREGION, "");
            js.put(Constant.CARTITEMS, cartItemsArray);
            js.put(Constant.ISADDEDTOCART, IsAddedToCart);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String URL = URL_Utils.GET_REMAINING_OFFER_QTY;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress1();
                        Log.d(TAG, response.toString());
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            boolean isBack = false;
                            if (status.equals("1")) {
                                leftOutOffers = new JSONArray(response.getString("leftOutOffers"));
                                if (leftOutOffers.length() > 0) {
                                    for (int i = 0; i < leftOutOffers.length(); i++) {
                                        JSONObject object = (JSONObject) leftOutOffers.get(i);
                                        if (object != null) {
                                            long remainingOfferQty = object.getLong("remainingOfferQty");
                                            if (remainingOfferQty < 0) {
                                                isBack = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!isBack) {
                                        getUserDetail(mUserId);
                                    } else {
                                        final Dialog dialog = new Dialog(StripePaymentActivity.this, R.style.DialogSlideAnim);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setContentView(R.layout.dialog_info);
                                        dialog.setCancelable(false);

                                        View.OnClickListener dialogClick = new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        };

                                        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
                                        ((TextView) dialog.findViewById(R.id.message)).setText("We cannot proceed further, Please review your order, one or more offers are no longer available.");

                                        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

                                        dialog.show();
                                    }
                                } else {
                                    getUserDetail(mUserId);
                                }
                            } else {
                                getUserDetail(mUserId);
                                message = "Your card has been declined. Please check your card details and try again.";
                                //dialog_info("ERROR..!!!", message);
                                dialog_info(Constant.CONFIDENTIALS, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                getUserDetail(mUserId);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null
                        && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(
                            volleyError.networkResponse.data));
                    volleyError = error;
                }

                return volleyError;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void payClick() {
        if (CreditCardText.cardTextLength != 19) {
            Snackbar.make(relStripeMain, "Please check your card details", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (ExpDateText.expdate != 5) {
            Snackbar.make(relStripeMain, "Please check expiry date", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (!SecurityCodeText.isCvcValid) {
            Snackbar.make(relStripeMain, "Please check your CVC number", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(postCodeEditText.getText().toString())) {
            Snackbar.make(relStripeMain, "Please enter your postcode", Snackbar.LENGTH_SHORT).show();
            return;
        }
        String userdetail = getLoadedData(Constant.USER_DETAIL);
        if (!TextUtils.isEmpty(userdetail)) {
            JSONObject object = null;
            try {
                object = new JSONObject(userdetail);
                boolean isActive = object.getBoolean("isActive");
                String emailId = object.getString("emailId");
                if (!isActive) {
                    dialog_Login1(mainActivity, "Sorry, " + emailId + "does not appear to be an active user. Please contact Confidentials support from Home > More(...) > Support from within this app or email us at support@predrinks.com", "OK");
                    return;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            getUserDetail(mUserId);
        }
        double amount = netAmount;
        if (amount >= 3) {
            //saveCreditCard();
            checkQuentity();
        } else {
            Snackbar.make(relStripeMain, "Minimum spend £3.00", Snackbar.LENGTH_SHORT).show();
        }
    }

    String offerIDstring;

    private void checkQuentity() {
        if (!haveNetworkConnection(mainActivity)) {
            Toast.makeText(mainActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }
        offerIDstring = "";
        for (int i = 0; i < cartItemsArray.length(); i++) {
            JSONObject object = null;
            try {
                object = cartItemsArray.getJSONObject(i);
                if (TextUtils.isEmpty(offerIDstring))
                    offerIDstring += object.getString("OfferID");
                else {
                    offerIDstring += "," + object.getString("OfferID");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Response.ErrorListener ErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                hideProgress1();
                Toast.makeText(mainActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        };

        Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    String success = response.getString("status");
                    if (success.equals("1")) {
                        String responses = response.toString();
                        JSONArray jsonArray = new JSONObject(responses).getJSONArray("availabeOfferQty");

                        boolean isOrederValid = true;
                        if (jsonArray.length() == cartItemsArray.length()) {
                            for (int i = 0; i < cartItemsArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String qut = object.optString("availableQty");
                                boolean isExpired = object.getBoolean("isExpired");
                                boolean isActive = object.getBoolean("isActive");
                                if (!isActive || isExpired) {
                                    isOrederValid = false;
                                    break;
                                } else if (!TextUtils.isEmpty(qut)) {
                                    int qty = Integer.parseInt(qut);
                                    int userQty = cartItemsArray.getJSONObject(i).getInt("Quantity");
                                    if (qty < 1 && qty < userQty) {
                                        isOrederValid = false;
                                        break;
                                    }
                                }
                            }
                            if (isOrederValid) {
                                // placeOrder(orderId, paramCartList);
                                //saveCreditCard();
                                getStripeKey();
                            } else {
                                hideProgress1();
                                dialog_info(Constant.CONFIDENTIALS, "Please review your cart, one or more offers are no longer available");
                            }
                        }
                    } else {
                        hideProgress1();
                        Toast.makeText(mainActivity, R.string.error_backend, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress1();
                }
            }
        };

        String URL = URL_Utils.AVAILABLEOFFERQUANTITY + "/" + offerIDstring;
        CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
        showProgress1();
    }

    ArrayList<CardDetail> arrCardDetail;
    CardListAdapter cardListAdapter;

    private void setSaveCard() {
        preDrinkOrderDAO.open();
        if (preDrinkOrderDAO.isUserExist(mUserId)) {
            List<UserCardDetail> userCardDetailList = preDrinkOrderDAO.getCardList(mUserId);
            String[] cardFourDigit = null;
            arrCardDetail = new ArrayList<>();
            cardFourDigit = new String[userCardDetailList.size()];
            for (int i = 0; i < userCardDetailList.size(); i++) {
                UserCardDetail userCardDetail = userCardDetailList.get(i);

                cardFourDigit[i] = "XXXX XXXX XXXX " + userCardDetail.getCardLastfour();
                arrCardDetail.add(new CardDetail(userCardDetail.getCardLastfour(), userCardDetail.getCardNumber(), String.valueOf(userCardDetail.getCardExpMonth()), String.valueOf(userCardDetail.getCardExpYear()), userCardDetail.getPostcode()));
            }

            if (arrCardDetail.size() == 0) {
                tvCardDetail.setVisibility(View.GONE);
            } else {
                cardListAdapter = new CardListAdapter(this, arrCardDetail);
                listSavedCard.setAdapter(cardListAdapter);
                tvCardDetail.setVisibility(View.VISIBLE);
            }
        } else {
            tvCardDetail.setVisibility(View.GONE);
            listSavedCard.setAdapter(null);
        }

        listSavedCard.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                cancelDialog(i, arrCardDetail);
                return false;
            }
        });
    }

    private void cancelDialog(final int pos, final ArrayList<CardDetail> arrCardDetail) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(Constant.CONFIDENTIALS);
        alertDialogBuilder.setMessage("Are you sure you want to remove card?");
        alertDialogBuilder.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        CardDetail cardDetail = arrCardDetail.get(pos);
                        preDrinkOrderDAO.removeCard(mUserId, cardDetail.cardLast4);
                        setSaveCard();
                    }
                });

        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    class CardListAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mLayoutInflater;
        boolean[] checkBoxState;
        List<CardDetail> arrCardDetail;

        public CardListAdapter(Context c, ArrayList<CardDetail> arrCardDetail) {
            mContext = c;

            mLayoutInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.arrCardDetail = arrCardDetail;

        }

        public void setList(ArrayList<CardDetail> arrCardDetail) {
            this.arrCardDetail = arrCardDetail;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {

            if (arrCardDetail != null) {
                return arrCardDetail.size();
            } else {
                return 0;
            }

        }

        @Override
        public Object getItem(int position) {
            return arrCardDetail.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {

            public TextView txtCardNo, txtCardSelect;

        }

        ViewHolder viewHolder;
        View vi;

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final int position_is = position;
            vi = convertView;

            if (convertView == null) {

                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.row_card_list, null);
                viewHolder = new ViewHolder();

                viewHolder.txtCardNo = (TextView) vi.findViewById(R.id.txtCardNo);
                viewHolder.txtCardSelect = (TextView) vi.findViewById(R.id.txtCardSelect);

                vi.setTag(viewHolder);
            } else
                viewHolder = (ViewHolder) vi.getTag();


            viewHolder.txtCardSelect.setTag(position);
            String cardLastfour = "";
            cardLastfour = arrCardDetail.get(position).cardLast4;
            viewHolder.txtCardNo.setText("XXXX XXXX XXXX " + cardLastfour);

            viewHolder.txtCardSelect.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    existCardSetup(pos, arrCardDetail);
                }
            });

            return vi;
        }

    }

    private void getStripeKey() {

        if (!haveNetworkConnection(mainActivity)) {
            hideProgress1();
            Toast.makeText(mainActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        String URL = URL_Utils.STRIPEPUBLISHABLEKEY;

        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Log.d(TAG, error.toString());
                } catch (RuntimeException e) {
                }
                hideProgress1();
                Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        String stripePublishableKey = response.getString("stripePublishableKey");
                        saveCreditCard(stripePublishableKey);

                    } else {
                        hideProgress1();
                        String message = response.getString("message");
                        dialog_info("", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void existCardSetup(int pos, List<CardDetail> jarray_receipt_details) {
        // TODO Auto-generated method stub
        noZipForm.clearForm();
        CardDetail jobj_receipt_details = jarray_receipt_details.get(pos);
        noZipForm.setCardNumber(jobj_receipt_details.cardNumber, true);
        CreditCardEntry.textFourDigits.setEnabled(false);
        CreditCardEntry.textFourDigits.setFocusable(false);
        CreditCardEntry.textFourDigits.setFocusableInTouchMode(false);

        String updatedString = String.valueOf(jobj_receipt_details.expire_month);
        String updateMonth = "";

        if (updatedString.length() == 2) {
            updateMonth = updatedString;
        } else {
            updateMonth = "0" + updatedString;
        }

        noZipForm.setExpDate(updateMonth + "/" + String.valueOf(jobj_receipt_details.expire_year).substring(jobj_receipt_details.expire_year.length() - 2), true);
        postCodeEditText.setText(jobj_receipt_details.postalCode);

    }

    public void saveCreditCard(String stripePublishableKey) {
        if (!haveNetworkConnection(mainActivity)) {
            hideProgress1();
            Toast.makeText(mainActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        Card card = new Card(noZipForm.getCreditCard().getCardNumber(), noZipForm.getCreditCard().getExpMonth(), noZipForm.getCreditCard().getExpYear(), noZipForm.getCreditCard().getSecurityCode(), "", "", "", "", "", postCodeEditText.getText().toString(), "");
        boolean validation = card.validateCard();
        if (validation) {
            showProgress1();
            new Stripe().createToken(
                    card,
                    stripePublishableKey,
                    new TokenCallback() {
                        public void onSuccess(Token token) {

                            token_id = token.getId();

                            cardLast4 = token.getCard().getLast4();
                            cardToken = token.getId();
                            cardExpMonth = token.getCard().getExpMonth();
                            cardExpYear = token.getCard().getExpYear();
                            cardType = getIntent().getIntExtra("PaymentTypeNo", 0);
                            cardNumber = noZipForm.getCreditCard().getCardNumber();

                            if (token.getCard().getCountry().equals("GB") || token.getCard().getCountry().equals("GBP")) {
                                card_region = "Domestic";
                            } else {
                                card_region = "International";
                            }
                            validateOrder();
                            if (isSaveCard) {
                                if (preDrinkOrderDAO.iscardsExist(mUserId, cardLast4))
                                    return;

                                String postCode = postCodeEditText.getText().toString();
                                preDrinkOrderDAO.insertOffers(mUserId, cardLast4, cardToken, cardType, cardNumber, cardExpMonth, cardExpYear, postCode);
                            }

                        }

                        public void onError(Exception error) {
                            paymentCount = sharedPrefPayment.getInt("PaymentCount", 0) + 1;
                            editorPayment.putInt("PaymentCount", paymentCount);
                            editorPayment.commit();

                            String message = "Your card has been declined. Please check your card details and try again.";
                            //dialog_info("ERROR..!!!", message);
                            dialog_info(Constant.CONFIDENTIALS, message);
                            hideProgress1();
                        }
                    });
        } else if (!card.validateNumber()) {
            handleError("The card number that you entered is invalid");
            hideProgress1();
        }
    }

    private void handleError(String error) {
        DialogFragment fragment = ErrorDialogFragment.newInstance(R.string.validationErrors, error);
        fragment.show(getSupportFragmentManager(), "error");
    }

    @Override
    public String getCardNumber() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getCvc() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer getExpMonth() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer getExpYear() {
        // TODO Auto-generated method stub
        return null;
    }

    private void validateOrder() {
        OfferPayment();
    }

    private void OfferPayment() {
        String mUserId = "";
        mUserId = getLoadedData(Constant.USERID);

        JSONObject js = new JSONObject();

        try {
            js.put(Constant.ORDERID, orderId);
            js.put(URL_Utils.USER_ID, Long.parseLong(mUserId));
            js.put(Constant.NETAMOUNT, netAmount);
            js.put(Constant.TRANSACTION_FEE, mTransactionFee);
            js.put(Constant.STRIPETOKENTOBEPROCESSED, token_id);
            js.put(Constant.CARDREGION, card_region);
            js.put(Constant.CARTITEMS, cartItemsArray);
            js.put(Constant.ISADDEDTOCART, IsAddedToCart);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String URL = URL_Utils.PLACE_ORDER_NEW;

        Log.d(TAG, "OfferPayment: url => " + URL);
        Log.d(TAG, "OfferPayment: object => " + js);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress1();
                        Log.d(TAG, response.toString());
                        try {
                            String success = response.getString("success");
                            String message = response.getString("message");
                            if (success.equals("1")) {
                                if (orderId != 0) {
                                    Utils.removeAlarmBroadcastReceiver(StripePaymentActivity.this, 1111);
                                    Utils.removeAlarmBroadcastReceiver(StripePaymentActivity.this, 1113);
                                    mPrefs.edit().putLong("alarmExpireTime", 0).apply();
                                    saveLoadedData("numberOfAddToCart", "" + 0);
                                }

                                editorOfferPayment.putBoolean("OfferPayment", true);
                                editorOfferPayment.apply();
                                editorPayment.putBoolean("OfferPayment", true);
                                editorPayment.apply();
                                Intent intent = new Intent(StripePaymentActivity.this, PaymentSuccessActivity.class);
                                intent.putExtra("", "");
                                startActivityForResult(intent, 100);
                            } else {
                                message = "Your card has been declined. Please check your card details and try again.";
                                //dialog_info("ERROR..!!!", message);
                                dialog_info(Constant.CONFIDENTIALS, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress1();
                try {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    NetworkResponse response = error.networkResponse;
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        Response.error(new ParseError(je));
                    }
                } catch (NullPointerException e) {
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null
                        && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(
                            volleyError.networkResponse.data));
                    volleyError = error;
                }

                return volleyError;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SCAN_CARD) {
            if (resultCode == Activity.RESULT_OK) {
                cards.pay.paycardsrecognizer.sdk.Card card = data.getParcelableExtra(ScanCardIntent.RESULT_PAYCARDS_CARD);
                String cardData = "Card number: " + card.getCardNumber() + "\n"
                        + "Card holder: " + card.getCardHolderName() + "\n"
                        + "Card expiration date: " + card.getExpirationDate();
                Log.i(TAG, "Card info: " + cardData);

                noZipForm.setCardNumber(card.getCardNumber(), true);
                noZipForm.setExpDate(card.getExpirationDate(), true);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "Scan canceled");
            } else {
                Log.i(TAG, "Scan failed");
            }
        } else {
            setResult(resultCode, data);
            finish();
        }
    }

    private ProgressDialog progDialog;

    protected void showProgress1() {
        if (progDialog == null || !progDialog.isShowing()) {
            progDialog = ProgressDialog.show(StripePaymentActivity.this, "", "Loading...");
        }
    }

    protected void hideProgress1() {
        if (progDialog != null && progDialog.isShowing()) {
            progDialog.dismiss();
        }
    }

}

package com.predrinks.app.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;

import com.predrinks.app.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GiftSendSuccessActivity extends AppCompatActivity {

    @Bind(R.id.tvEmail)
    TextView tvEmail;

    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_send_success);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("email")) {
            email = getIntent().getStringExtra("email");
        }

        if (!TextUtils.isEmpty(email)) {
            tvEmail.setText(email);
        }
    }

    @OnClick(R.id.lnrOk)
    public void onClick() {
        onBackPressed();
    }
}

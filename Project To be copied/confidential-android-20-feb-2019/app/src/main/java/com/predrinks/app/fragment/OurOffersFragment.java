package com.predrinks.app.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.app.R;
import com.predrinks.app.activity.MainActivity;
import com.predrinks.app.adapter.OurOfferAdapter;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.SimpleDividerItemDecoration;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.widget.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OurOffersFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "OurOffersFragment";
    @Bind(R.id.recycler_view_offer)
    RecyclerView recyclerViewOffer;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;

    private String mVenueId;
    private Activity mActivity;
    private ProgressDialog pDialog;
    String successResponse = "";

    public static OurOffersFragment newInstance(String param1, String param2) {
        OurOffersFragment fragment = new OurOffersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = getActivity();
        if (getArguments() != null) {
            mVenueId = getArguments().getString(ARG_PARAM1);
        }

        pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage("Loading...");
        pDialog.show();

        String URL = URL_Utils.OFFER + "/" + mVenueId + "/event/list_new";
        Log.d(TAG, "onCreate: "+URL);
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_our_offers, container, false);
        ButterKnife.bind(this, view);


        if (!TextUtils.isEmpty(successResponse))
            setOfferList(successResponse);

        return view;
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            pDialog.dismiss();
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            pDialog.dismiss();
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    setOfferList(response.toString());
                    successResponse = response.toString();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        MainActivity.setBackButton(true);
        MainActivity.setVisibleHeaderRightMap(false);
        MainActivity.setHeaderTitle("OUR OFFERS");
    }

    OurOfferAdapter offerListAdapter;
    JSONArray offerList = null;

    private void setOfferList(String response) {
        recyclerViewOffer.setVerticalScrollBarEnabled(false);
        try {
            JSONObject responseObject = new JSONObject(response);
            JSONArray jArray = responseObject.getJSONArray(Constant.RESULT);
            if (jArray != null) {
                offerList = new JSONArray();
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = jArray.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
                    if (totalRemainingOffers.contains("SOLD OUT") || totalRemainingOffers.contains("sold out") || totalRemainingOffers.contains("0 Remaining")) {
                    } else {
                        offerList.put(jsonObject);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (offerList == null) {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerViewOffer.setVisibility(View.GONE);
            return;
        }
        offerListAdapter = new OurOfferAdapter(mActivity, offerList);

        recyclerViewOffer.setAdapter(offerListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false) {
            @Override
            public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
            }
        };
        recyclerViewOffer.setLayoutManager(linearLayoutManager);
        recyclerViewOffer.addItemDecoration(new SimpleDividerItemDecoration(mActivity, R.drawable.search_divider));

        recyclerViewOffer.addOnItemTouchListener(new RecyclerItemClickListener(mActivity, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                MainActivity.hideKeyboard(recyclerViewOffer);
                if (offerList == null)
                    return;
                offerListAdapter.setSelection(position);

                JSONObject object;
                String eventId = "";
                try {
                    object = offerList.getJSONObject(position);
                    eventId = object.getString("eventId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                OfferDetailFragment fragment = OfferDetailFragment.newInstance(eventId, "");
                MainActivity.loadContentFragmentWithBack(fragment, false);

            }
        }));

        if (offerList.length() == 0) {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerViewOffer.setVisibility(View.GONE);
        } else {
            lnrEmpty.setVisibility(View.GONE);
            recyclerViewOffer.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}

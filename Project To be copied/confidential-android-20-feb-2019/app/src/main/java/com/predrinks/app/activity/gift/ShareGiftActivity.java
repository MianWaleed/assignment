package com.predrinks.app.activity.gift;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.app.R;
import com.predrinks.app.model.GiftSendModel;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ShareGiftActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backIv;
    private TextView firstNameTv;
    private TextView lastNameTv;
    private TextView emailTv;
    private Button sendBt;
    private TextView messageTv;
    private EditText messageEt;
    private TextView offerDetailTv;
    String myOfferID, offerTitle, venueTitle;
    GiftSendModel.User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_gift);
        initView();
    }

    private void initView() {
        backIv = (ImageView) findViewById(R.id.back_iv);
        firstNameTv = (TextView) findViewById(R.id.first_name_tv);
        lastNameTv = (TextView) findViewById(R.id.last_name_tv);
        emailTv = (TextView) findViewById(R.id.email_tv);
        sendBt = (Button) findViewById(R.id.send_bt);
        messageTv = (TextView) findViewById(R.id.message_tv);
        messageEt = (EditText) findViewById(R.id.message_et);
        offerDetailTv = (TextView) findViewById(R.id.offer_detail_tv);

        if (getIntent().hasExtra("myOfferID")) {
            myOfferID = getIntent().getStringExtra("myOfferID");
            offerTitle = getIntent().getStringExtra("offerTitle");
            venueTitle = getIntent().getStringExtra("venueTitle");
            user = (GiftSendModel.User) getIntent().getSerializableExtra("model");
        }

        firstNameTv.setText(user.getFirstname());
        lastNameTv.setText(user.getLastname());
        emailTv.setText(user.getEmailID());

        offerDetailTv.setText("Item: " + offerTitle);

        sendBt.setOnClickListener(this);
        backIv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_iv: {
                finish();
                break;
            }
            case R.id.send_bt: {
                sendData();
                break;
            }

        }
    }

    private void sendData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("MyOfferID", myOfferID);
            jsonObject.put("userId", user.getUserID());
            jsonObject.put("Message", messageEt.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL_Utils.USER + "/" + "498" + "/share/in/app", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}

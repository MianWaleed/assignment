package com.predrinks.app.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.app.R;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.widget.GiftOnClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RedeemedListAdapter extends BaseAdapter {

    private GiftOnClickListener giftOnClickListener;
    private JSONArray myOfferList;
    Context mContext;
    LayoutInflater inflater;


    public RedeemedListAdapter(Context context, GiftOnClickListener giftOnClickListener, JSONArray myOfferList) {
        this.mContext = context;
        this.myOfferList = myOfferList;
        this.giftOnClickListener = giftOnClickListener;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return myOfferList.length();
    }

    @Override
    public JSONObject getItem(int i) {
        try {
            return myOfferList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            view = inflater.inflate(R.layout.row_redeemed_list, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        JSONObject jsonObject = null;
        try {
            jsonObject = myOfferList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String qRCodeImage = getJsonString(jsonObject, "qRCodeImage");
        String qRCode = getJsonString(jsonObject, "qRCode");
        String expiresOn = getJsonString(jsonObject, "expiresOn");
        String isOfferExpired = getJsonString(jsonObject, "isOfferExpired");
        String isOfferActive = getJsonString(jsonObject, "isOfferActive");
        String orderNo = getJsonString(jsonObject, "orderNo");

        boolean isRefunded = false;
        try {
            isRefunded = jsonObject.getBoolean("isRefunded");
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Toast.makeText(mContext, ""+isRefunded, Toast.LENGTH_SHORT).show();


        boolean isRedeemed = false;
        String offerExpiryMode = getJsonString(jsonObject, "offerExpiryMode");
        boolean isGifted = false;
        try {
            isGifted = jsonObject.getBoolean("isGifted");
            isRedeemed = jsonObject.getBoolean("isRedeemed");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.tvCode.setText(qRCode);
        holder.tvOffTitle.setText(getJsonString(jsonObject, "offerTitle"));
        holder.tvClubTitle.setText(getJsonString(jsonObject, "venueTitle"));

        holder.tvOrderNumber.setText("Order No.: " + orderNo);
        if (isRedeemed) {
            holder.lnrRedeemed.setVisibility(View.VISIBLE);
            String dateTime = getJsonString(jsonObject, "redeemedOn");
            holder.tvDateTime.setText(dateTime);
            holder.tvExpiresDate.setVisibility(View.GONE);
        } else {
            holder.lnrRedeemed.setVisibility(View.GONE);
            holder.tvExpiresDate.setVisibility(View.VISIBLE);
            if (!isOfferActive.equals("true") && !isOfferExpired.equals("true")) {
                holder.tvExpiresDate.setText("Voucher Inactive");
                holder.tvExpiresDate.setTextColor(Color.parseColor("#9E9E9E"));

            } else if (isOfferExpired.equals("true") || !isOfferActive.equals("true")) {
                holder.tvExpiresDate.setText("(Voucher Expired on " + expiresOn + ")");
                holder.tvExpiresDate.setTextColor(Color.parseColor("#C92A2A"));
            } else {
                String expireMessage = "";
                if (offerExpiryMode.equals("OnDate")) {
                    expireMessage = "(Voucher Expires on " + expiresOn + ")";
                } else if (offerExpiryMode.equals("AtEventExpiry")) {
                    expireMessage = "(Voucher Expires on " + expiresOn + ")";
                } else if (offerExpiryMode.equals("AfterDays")) {
                    expireMessage = "(Voucher Expired on " + expiresOn + ")";
                }
                holder.tvExpiresDate.setText(expireMessage);
                holder.tvExpiresDate.setTextColor(Color.parseColor("#9E9E9E"));
            }
        }


        if (isGifted) {
            holder.imgRedeemed.setImageResource(R.mipmap.gifted);
            holder.lnrCode.setVisibility(View.GONE);
        } else {
            holder.lnrCode.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(URL_Utils.URL_IMAGE_DNLOAD + qRCodeImage)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .error(R.mipmap.default_event_venue_image)
                    .override(500, 500)
                    .into(holder.imgRedeemed);
        }


        if (isRedeemed)
            holder.ivGift.setVisibility(View.GONE);
        else
            holder.ivGift.setVisibility(View.VISIBLE);

        holder.ivGift.setTag(i);
        holder.ivGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmatiomDialog(v);
            }
        });

        if (isRefunded) {
            holder.lnrRedeemed.setVisibility(View.VISIBLE);
            holder.isRed.setVisibility(View.VISIBLE);
            holder.isRed.setImageResource(R.drawable.ic_refunded);
        }

        return view;
    }

    private void showConfirmatiomDialog(View view) {
        Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView okTv = dialog.findViewById(R.id.ok_tv);
        TextView cancelTv = dialog.findViewById(R.id.cancel_tv);

        okTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View df) {
                int pos = (int) view.getTag();
                JSONObject jsonObject = null;
                boolean isRedeemed = false;
                try {
                    jsonObject = myOfferList.getJSONObject(pos);
                    isRedeemed = jsonObject.getBoolean("isRedeemed");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isRedeemed)
                    return;
                String myOfferID = getJsonString(jsonObject, "myOfferID");
                String offerTitle = getJsonString(jsonObject, "offerTitle");
                String venueTitle = getJsonString(jsonObject, "venueTitle");
                giftOnClickListener.onGiftClick(offerTitle, venueTitle, myOfferID);
            }
        });

        cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }


    static class ViewHolder {
        @Bind(R.id.imgRedeemed)
        ImageView imgRedeemed;
        @Bind(R.id.tvCode)
        TextView tvCode;
        @Bind(R.id.tvOffTitle)
        TextView tvOffTitle;
        @Bind(R.id.tvClubTitle)
        TextView tvClubTitle;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.tvNumberX)
        TextView tvNumberX;
        @Bind(R.id.lnrRedeemed)
        LinearLayout lnrRedeemed;
        @Bind(R.id.lnrCode)
        LinearLayout lnrCode;
        @Bind(R.id.tvExpiresDate)
        TextView tvExpiresDate;
        @Bind(R.id.tvOrderNumber)
        TextView tvOrderNumber;
        @Bind(R.id.ivGift)
        ImageView ivGift;

        @Bind(R.id.is_refunded_iv)
        ImageView isRed;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (jsonObject == null || !jsonObject.has(strKey))
                return str;

            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

}

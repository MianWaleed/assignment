package com.predrinks.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.stripe.StripePayment.Main.StripePaymentActivity;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.PaymentsUtil;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Vivek on 12-10-2016.
 */

public class CartMainActivity extends BaseFragmentActivity {

    private static final String TAG = "CartMainActivity";
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;
    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.relative_cancel)
    RelativeLayout relativeCancel;
    @Bind(R.id.relative_header)
    RelativeLayout relativeHeader;
    @Bind(R.id.relative_your_savings)
    RelativeLayout relativeYourSavings;
    @Bind(R.id.txt_confirm_checkout)
    TextView txtConfirmCheckout;
    @Bind(R.id.txt_total_price)
    TextView txtTotalPrice;
    @Bind(R.id.tvSaving)
    TextView tvSaving;
    //  @Bind(R.id.tvAddMore)
    TextView tvAddMore;
    @Bind(R.id.tvSearchOffers)
    TextView tvSearchOffers;
    @Bind(R.id.relative_footer)
    RelativeLayout relativeFooter;
    @Bind(R.id.lnrCheckOut)
    LinearLayout lnrCheckOut;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;
    @Bind(R.id.lnrListView)
    LinearLayout lnrListView;
    //LinearLayout lnrFooter;
    @Bind(R.id.activity_main_cart)
    LinearLayout activity_main_cart;
    @Bind(R.id.iconInfo)
    ImageView iconInfo;
    @Bind(R.id.tvTransactionFees)
    TextView tvTransactionFees;

    Paint paint;
    String mUserId = "";
    String mResponse = "";
    FragmentActivity mActivity;

    String offerIDstring = "";
    String deviceID = "0", deviceUDID;
    View footerView;
    private SharedPreferences mPrefs;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cart);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait!!");

        if (getIntent().getBooleanExtra("buyNow", false)) {
            progressDialog.show();
        }

        mActivity = this;
        mResponse = "";
        mUserId = getLoadedData(Constant.USERID);
        mPrefs = getSharedPreferences(Confidentials.NAME_PREFERENCE, 0);

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

        tvSearchOffers.setPaintFlags(tvSearchOffers.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        txtTotalPrice.setTypeface(MainActivity.getTypeFaceRegular());
        txtConfirmCheckout.setTypeface(MainActivity.getTypeFaceRegular());
        tvSaving.setTypeface(MainActivity.getTypeFaceRegular());

        footerView = ((LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout, null, false);
        listView.addFooterView(footerView);
        tvAddMore = footerView.findViewById(R.id.tvAddMore);
        tvAddMore.setPaintFlags(paint.getFlags());
        tvAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iconInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_info_Fees("Transaction Fees",
                        "When you make a purchase through our services, we may charge you a transaction fee. These fees go towards improving our products and services in the future.",
                        "Terms and Conditions apply, see the “More” section for details");
            }
        });

        lnrCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartListAdapter == null) {
                    dialog_info(mActivity, Constant.CONFIDENTIALS, "Please select an offer first");
                    return;
                }
                String userdetail = getLoadedData(Constant.USER_DETAIL);
                if (TextUtils.isEmpty(userdetail)) {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(userdetail);
                        boolean isActive = object.getBoolean("isActive");
                        boolean emailId = object.getBoolean("emailId");
                        if (!isActive) {
                            dialog_Login1(mActivity, "Sorry, " + emailId + "does not appear to be an active user. Please contact Confidentials support from Home > More(...) > Support from within this app or email us at support@predrinks.com", "OK");
                            return;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                refreshCartItemPaymentTime();
            }
        });

        if (!isLoggedIn()) {
            dialog_Login(mActivity);
        }
        if (!haveNetworkConnection()) {
            Toast.makeText(mActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
        }

    }

    private void checkOutCartItems() {
        JSONArray array = cartListAdapter.getDataList();
        if (array != null && array.length() > 0) {
            JSONObject jsonObject = null;
            JSONArray paramCartList = new JSONArray();
            try {
                jsonObject = array.getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int orderId = Integer.parseInt(getJsonString(jsonObject, "orderId"));
            offerIDstring = "";
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = null;
                try {
                    object = array.getJSONObject(i);
                    JSONObject objectNew = new JSONObject();

                    int quantity = Integer.parseInt(getJsonString(object, "qunatity"));
                    double discountedPrice = Double.parseDouble(getJsonString(object, "discountedPrice"));
                    double tvSubTotalValue = quantity * discountedPrice;

                    objectNew.put("OfferID", object.getString("offerId"));
                    objectNew.put("Quantity", object.getString("qunatity"));
                    objectNew.put("Price", object.getString("cartItemPrice"));
                    objectNew.put("TotalPrice", tvSubTotalValue);
                    objectNew.put("VenueID", object.getString("venueId"));
                    objectNew.put("eventId", object.getString("eventId"));

                    paramCartList.put(objectNew);

                    if (TextUtils.isEmpty(offerIDstring))
                        offerIDstring += object.getString("offerId");
                    else {
                        offerIDstring += "," + object.getString("offerId");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            checkCartValues(orderId, paramCartList);
        } else {

        }
    }

    private void checkCartValues(final int orderId, final JSONArray paramCartList) {
        final Response.ErrorListener ErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                Toast.makeText(mActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        };

        final Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    String success = response.getString("status");
                    if (success.equals("1")) {
                        String responses = response.toString();
                        JSONArray jsonArray = new JSONObject(responses).getJSONArray("availabeOfferQty");

                        boolean isOrederValid = true;
                        if (jsonArray.length() == paramCartList.length()) {
                            for (int i = 0; i < paramCartList.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String qut = object.optString("availableQty");
                                boolean isExpired = object.getBoolean("isExpired");
                                boolean isActive = object.getBoolean("isActive");
                                if (!isActive || isExpired) {
                                    isOrederValid = false;
                                    break;
                                } else if (!TextUtils.isEmpty(qut)) {
                                    int qty = Integer.parseInt(qut);
                                    int userQty = paramCartList.getJSONObject(i).getInt("Quantity");
                                    if (qty < 1 && qty < userQty) {
                                        isOrederValid = false;
                                        break;
                                    }
                                }
                            }
                            if (isOrederValid) {
                                getRemainingOfferQty(orderId, paramCartList);
                            } else {
                                dialog_info(Constant.CONFIDENTIALS, "Please review your cart, one or more offers are no longer available");
                            }
                        }
                    } else {
                        Toast.makeText(mActivity, R.string.error_backend, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        if (!haveNetworkConnection(this)) {
            Toast.makeText(mActivity, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String URL = URL_Utils.AVAILABLEOFFERQUANTITY + "/" + offerIDstring;
                Log.d(TAG, "run: URL => " + URL);
                CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
            }
        }, 1000);

    }

    private void placeOrder(int orderId, JSONArray paramCartList) {
        String pr = txtTotalPrice.getText().toString();
        String tp = tvTransactionFees.getText().toString();
        pr = pr.replace("£", "");
        tp = tp.replace("Transaction fee: £", "");
        double total = Double.parseDouble(pr);
        double transactionPrice = Double.parseDouble(tp);
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        double finalAmt = total - transactionPrice;
        if (finalAmt >= 3) {
            showDifferentPaymentMethods(total, orderId, paramCartList, transactionPrice);
        } else {
            Snackbar.make(activity_main_cart, "Minimum spend £3.00", Snackbar.LENGTH_SHORT).show();
        }
    }

    private PaymentsClient mPaymentsClient;
    TextView googlePayTv;

    private void showDifferentPaymentMethods(double total, int orderId, JSONArray paramCartList, double transactionPrice) {
        Log.d(TAG, "showDifferentPaymentMethods: total => " + total + "  transactionFee => " + transactionPrice);

        mPaymentsClient = PaymentsUtil.createPaymentsClient(this);

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.show_payment_method_dialog, null);

        TextView confidentialsPayTv = view.findViewById(R.id.pay_with_confidentials_tv);
        googlePayTv = view.findViewById(R.id.pay_with_google_tv);
        googlePayTv.setVisibility(View.GONE);
//        possiblyShowGooglePayButton(googlePayTv);

        confidentialsPayTv.setOnClickListener(v -> {
            Intent intent = new Intent(CartMainActivity.this, StripePaymentActivity.class);
            intent.putExtra("NetAmount", total);
            intent.putExtra("orderId", orderId);
            intent.putExtra("CartItems", paramCartList.toString());
            intent.putExtra("IsAddedToCart", true);
            intent.putExtra("transactionFee", transactionPrice);
            startActivityForResult(intent, 100);
        });

        googlePayTv.setOnClickListener(v -> {
            requestPayment(v, total, transactionPrice);
        });

        dialog.setContentView(view);
        dialog.show();

    }

    public void requestPayment(View view, double total, double transactionPrice) {
        total = total * 1000000;
        transactionPrice = transactionPrice * 1000000;

        double grandTotal = total + transactionPrice;

        // Disables the button to prevent multiple clicks.
        view.setClickable(false);

        Log.d(TAG, "requestPayment: "+grandTotal);
        Log.d(TAG, "requestPayment: "+(long) grandTotal);
        // The price provided to the API should include taxes and shipping.
        // This price is not displayed to the user.
        String price = PaymentsUtil.microsToString((long) grandTotal);

        // TransactionInfo transaction = PaymentsUtil.createTransaction(price);
        JSONObject paymentDataRequestJson = PaymentsUtil.getPaymentDataRequest(price);
        if (paymentDataRequestJson == null) {
            return;
        }
        PaymentDataRequest request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString());

        // Since loadPaymentData may show the UI asking the user to select a payment method, we use
        // AutoResolveHelper to wait for the user interacting with it. Once completed,
        // onActivityResult will be called with the result.
        if (request != null) {
            AutoResolveHelper.resolveTask(mPaymentsClient.loadPaymentData(request), this, LOAD_PAYMENT_DATA_REQUEST_CODE);
        }
    }


    private void possiblyShowGooglePayButton(TextView googlePayTv) {
        final JSONObject isReadyToPayJson = PaymentsUtil.getIsReadyToPayRequest();
        if (isReadyToPayJson == null) {
            return;
        }
        IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(isReadyToPayJson.toString());
        if (request == null) {
            return;
        }

        // The call to isReadyToPay is asynchronous and returns a Task. We need to provide an
        // OnCompleteListener to be triggered when the result of the call is known.
        Task<Boolean> task = mPaymentsClient.isReadyToPay(request);
        task.addOnCompleteListener(this,
                new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult()) {
//                                googlePayTv.setVisibility(View.GONE);
                                googlePayTv.setVisibility(View.VISIBLE);
                            } else {
                                googlePayTv.setVisibility(View.GONE);
                            }
                        } else {
                            Log.w("isReadyToPay failed", task.getException());
                        }
                    }
                });
    }

    JSONArray leftOutOffers;

    private void getRemainingOfferQty(final int orderId, final JSONArray paramCartList) {
        String pr = txtTotalPrice.getText().toString();
        pr = pr.replace("£", "");
        double total = Double.parseDouble(pr);
        JSONObject js = new JSONObject();
        try {
            js.put(Constant.ORDERID, orderId);
            js.put(URL_Utils.USER_ID, Long.parseLong(mUserId));
            js.put(Constant.NETAMOUNT, total);
            js.put(Constant.STRIPETOKENTOBEPROCESSED, "");
            js.put(Constant.CARDREGION, "");
            js.put(Constant.CARTITEMS, paramCartList);
            js.put(Constant.ISADDEDTOCART, true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String URL = URL_Utils.GET_REMAINING_OFFER_QTY;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            boolean isBack = false;
                            if (status.equals("1")) {
                                leftOutOffers = new JSONArray(response.getString("leftOutOffers"));
                                if (leftOutOffers.length() > 0) {
                                    for (int i = 0; i < leftOutOffers.length(); i++) {
                                        JSONObject object = (JSONObject) leftOutOffers.get(i);
                                        if (object != null) {
                                            long remainingOfferQty = object.getLong("remainingOfferQty");
                                            if (remainingOfferQty < 0) {
                                                isBack = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!isBack) {
                                        placeOrder(orderId, paramCartList);
                                    } else {
                                        final Dialog dialog = new Dialog(CartMainActivity.this, R.style.DialogSlideAnim);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setContentView(R.layout.dialog_info);
                                        dialog.setCancelable(false);

                                        View.OnClickListener dialogClick = new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        };

                                        ((TextView) dialog.findViewById(R.id.title)).setText(Constant.CONFIDENTIALS);
                                        ((TextView) dialog.findViewById(R.id.message)).setText("We cannot proceed further, Please review your order, one or more offers are no longer available.");

                                        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

                                        dialog.show();
                                    }
                                } else {
                                    getUserDetail(mUserId);
                                }
                            } else {
                                getUserDetail(mUserId);
                                dialog_info("ERROR..!!!", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                getUserDetail(mUserId);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                // headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                // return super.parseNetworkError(volleyError);
                if (volleyError.networkResponse != null
                        && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(
                            volleyError.networkResponse.data));
                    volleyError = error;
                }

                return volleyError;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                //  String result = data.getStringExtra("result");
                MainActivity.ITEM_ADD_TO_CART = 0;
                MainActivity.setHeaderNotificationNumber(MainActivity.ITEM_ADD_TO_CART);
                saveLoadedData("numberOfAddToCart", "" + MainActivity.ITEM_ADD_TO_CART);
                finish();
            } else if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == LOAD_PAYMENT_DATA_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    PaymentData paymentData = PaymentData.getFromIntent(data);
                    handlePaymentSuccess(paymentData);
                    break;
                case Activity.RESULT_CANCELED:
                    // Nothing to here normally - the user simply cancelled without selecting a
                    // payment method.
                    break;
                case AutoResolveHelper.RESULT_ERROR:
                    Status status = AutoResolveHelper.getStatusFromIntent(data);
                    handleError(status.getStatusCode());
                    break;
                default:
                    // Do nothing.
            }

            // Re-enables the Google Pay payment button.
            googlePayTv.setClickable(true);
        }
    }

    private void handlePaymentSuccess(PaymentData paymentData) {
        String paymentInformation = paymentData.toJson();

        // Token will be null if PaymentDataRequest was not constructed using fromJson(String).
        if (paymentInformation == null) {
            return;
        }
        JSONObject paymentMethodData;

        try {
            paymentMethodData = new JSONObject(paymentInformation).getJSONObject("paymentMethodData");
            // If the gateway is set to "example", no payment information is returned - instead, the
            // token will only consist of "examplePaymentMethodToken".
            if (paymentMethodData
                    .getJSONObject("tokenizationData")
                    .getString("type")
                    .equals("PAYMENT_GATEWAY")
                    && paymentMethodData
                    .getJSONObject("tokenizationData")
                    .getString("token")
                    .equals("examplePaymentMethodToken")) {
                AlertDialog alertDialog =
                        new AlertDialog.Builder(this)
                                .setTitle("Warning")
                                .setMessage(
                                        "Gateway name set to \"example\" - please modify "
                                                + "Constants.java and replace it with your own gateway.")
                                .setPositiveButton("OK", null)
                                .create();
                alertDialog.show();
            }

            String billingName =
                    paymentMethodData.getJSONObject("info").getJSONObject("billingAddress").getString("name");
            Log.d("BillingName", billingName);
            Toast.makeText(this, "asd", Toast.LENGTH_LONG)
                    .show();

            // Logging token string.
            Log.d("GooglePaymentToken", paymentMethodData.getJSONObject("tokenizationData").getString("token"));
        } catch (JSONException e) {
            Log.e("handlePaymentSuccess", "Error: " + e.toString());
            return;
        }
    }

    private void handleError(int statusCode) {
        Log.w("loadPaymentData failed", String.format("Error code: %d", statusCode));
    }

    Handler handler = new Handler();

    @Override
    protected void onResume() {
        super.onResume();
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (isLoggedIn()) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getCartList();
                }
            }, 500);
        }
    }

    private void getCartList() {
        Response.ErrorListener ErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                getUserDetail(mUserId);
                setOfferList(mResponse);
            }
        };

        Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                getUserDetail(mUserId);
                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        mResponse = response.toString();
                        setOfferList(mResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String URL = URL_Utils.USER + "/" + mUserId + "/cart/offer/List";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }


    JSONArray offerCartList = null;
    CartAdapter cartListAdapter;
    boolean mIsActive = false;

    private void setOfferList(String response) {
        mIsActive = false;
        offerCartList = null;
        try {
            JSONObject responseObject = new JSONObject(response);
            offerCartList = responseObject.getJSONArray(Constant.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (offerCartList != null && offerCartList.length() > 0) {
            for (int i = 0; i < offerCartList.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = offerCartList.getJSONObject(i);
                    boolean isActive = jsonObject.getBoolean("isActive");
                    mIsActive = isActive;
                    boolean isExpired = jsonObject.getBoolean("isExpired");
                    if (isExpired)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            offerCartList.remove(i);
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        if (offerCartList != null && offerCartList.length() > 0) {
            lnrEmpty.setVisibility(View.GONE);
            lnrListView.setVisibility(View.VISIBLE);
            tvTitleHeader.setText("MY CART (" + offerCartList.length() + ")");
            saveLoadedData("numberOfAddToCart", "" + offerCartList.length());
            MainActivity.setHeaderNotificationNumber(offerCartList.length());
            saveLoadedData("numberOfAddToCart", String.valueOf(offerCartList.length()));
            double totalAmount = 0;

            for (int i = 0; i < offerCartList.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = offerCartList.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int quantity = Integer.parseInt(getJsonString(jsonObject, "qunatity"));
                double discountedPrice = Double.parseDouble(getJsonString(jsonObject, "discountedPrice"));
                double transactionFee = Double.parseDouble(getJsonString(jsonObject, "transactionFee"));
                double tvSubTotalValue = quantity * discountedPrice;
                //double discountAmount = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));
                totalAmount += tvSubTotalValue;
            }
            tvSaving.setVisibility(View.VISIBLE);
            /*try {
                00tvTransactionFees.setText("Transaction fee: £" + getJsonString(offerCartList.getJSONObject(0), "transactionFee"));
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            // txtTotalPrice.setText("£" + String.valueOf(totalAmount));
        } else {
            tvTitleHeader.setText("MY CART (" + 0 + ")");
            saveLoadedData("numberOfAddToCart", String.valueOf(0));
            lnrEmpty.setVisibility(View.VISIBLE);
            tvTransactionFees.setText("Transaction fee: £" + String.format("%.2f", 0.00));
            txtTotalPrice.setText("£" + String.format("%.2f", 0.00));
            tvSaving.setVisibility(View.GONE);
            lnrListView.setVisibility(View.GONE);
            Utils.removeAlarmBroadcastReceiver(CartMainActivity.this, 1111);
            Utils.removeAlarmBroadcastReceiver(CartMainActivity.this, 1113);
            mPrefs.edit().putLong("alarmExpireTime", 0).apply();
            return;
        }

        for (int i = 0; i < offerCartList.length(); i++) {
            int quantity = 0, remaining = 0;
            JSONObject jsonObject = null;
            try {
                jsonObject = offerCartList.getJSONObject(i);
                quantity = jsonObject.getInt("qunatity");
                remaining = jsonObject.getInt("offerForSale");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (quantity >= 20) {
                quantity = 20;
                if (remaining <= quantity) {
                    try {
                        jsonObject.put("qunatity", remaining);
                        offerCartList.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        jsonObject.put("qunatity", quantity);
                        offerCartList.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (remaining <= quantity) {
                    try {
                        jsonObject.put("qunatity", remaining);
                        offerCartList.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        jsonObject.put("qunatity", quantity);
                        offerCartList.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }

        setTotalPrice(offerCartList);
        cartListAdapter = new CartAdapter(mActivity, offerCartList);

        listView.setAdapter(cartListAdapter);
        //listView.addFooterView(footerView);
        //  setListViewHeightBasedOnChildren(listView);


        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                if (getIntent().getBooleanExtra("buyNow", false)) {
                    progressDialog.show();
                    if (cartListAdapter == null) {
                        dialog_info(mActivity, Constant.CONFIDENTIALS, "Please select an offer first");
                        return;
                    }
                    String userdetail = getLoadedData(Constant.USER_DETAIL);
                    if (TextUtils.isEmpty(userdetail)) {
                        JSONObject object = null;
                        try {
                            object = new JSONObject(userdetail);
                            boolean isActive = object.getBoolean("isActive");
                            boolean emailId = object.getBoolean("emailId");
                            if (!isActive) {
                                dialog_Login1(mActivity, "Sorry, " + emailId + "does not appear to be an active user. Please contact Confidentials support from Home > More(...) > Support from within this app or email us at support@predrinks.com", "OK");
                                return;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    refreshCartItemPaymentTime();
                }
            }
        }.start();

    }

    private void refreshCartItemPaymentTime() {

        Response.ErrorListener ErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        };

        Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        mResponse = response.toString();
                        mIsActive = false;
                        offerCartList = null;
                        try {
                            JSONObject responseObject = new JSONObject(mResponse);
                            offerCartList = responseObject.getJSONArray(Constant.RESULT);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (offerCartList != null && offerCartList.length() > 0) {
                            for (int i = 0; i < offerCartList.length(); i++) {
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = offerCartList.getJSONObject(i);
                                    boolean isActive = jsonObject.getBoolean("isActive");
                                    mIsActive = isActive;
                                    boolean isExpired = jsonObject.getBoolean("isExpired");
                                    if (isExpired)
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                            offerCartList.remove(i);
                                        }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        if (offerCartList != null && offerCartList.length() > 0) {
                            lnrEmpty.setVisibility(View.GONE);
                            lnrListView.setVisibility(View.VISIBLE);
                            tvTitleHeader.setText("MY CART (" + offerCartList.length() + ")");
                            saveLoadedData("numberOfAddToCart", "" + offerCartList.length());
                            MainActivity.setHeaderNotificationNumber(offerCartList.length());
                            saveLoadedData("numberOfAddToCart", String.valueOf(offerCartList.length()));
                        } else {
                            tvTitleHeader.setText("MY CART (" + 0 + ")");
                            saveLoadedData("numberOfAddToCart", String.valueOf(0));
                            lnrEmpty.setVisibility(View.VISIBLE);
                            tvTransactionFees.setText("Transaction fee: £" + String.format("%.2f", 0.00));
                            txtTotalPrice.setText("£" + String.format("%.2f", 0.00));
                            tvSaving.setVisibility(View.GONE);
                            lnrListView.setVisibility(View.GONE);
                            Utils.removeAlarmBroadcastReceiver(CartMainActivity.this, 1111);
                            Utils.removeAlarmBroadcastReceiver(CartMainActivity.this, 1113);
                            mPrefs.edit().putLong("alarmExpireTime", 0).apply();
                            return;
                        }

                        for (int i = 0; i < offerCartList.length(); i++) {
                            int quantity = 0, remaining = 0;
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = offerCartList.getJSONObject(i);
                                quantity = jsonObject.getInt("qunatity");
                                remaining = jsonObject.getInt("offerForSale");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (quantity >= 20) {
                                quantity = 20;
                                if (remaining <= quantity) {
                                    try {
                                        jsonObject.put("qunatity", remaining);
                                        offerCartList.put(i, jsonObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        jsonObject.put("qunatity", quantity);
                                        offerCartList.put(i, jsonObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                if (remaining <= quantity) {
                                    try {
                                        jsonObject.put("qunatity", remaining);
                                        offerCartList.put(i, jsonObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        jsonObject.put("qunatity", quantity);
                                        offerCartList.put(i, jsonObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }

                        double mDiscountAmount = 0, totalAmount = 0, transactionAmount = 0;
                        for (int i = 0; i < offerCartList.length(); i++) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = offerCartList.getJSONObject(i);
                            } catch (JSONException e) {
                            }
                            String discountType = getJsonString(jsonObject, "discountType");
                            String originalPrice = getJsonString(jsonObject, "originalPrice");

                            int quantity = Integer.parseInt(getJsonString(jsonObject, "qunatity"));
                            double discountedPrice = Double.parseDouble(getJsonString(jsonObject, "discountedPrice"));
                            double transactionFee = Double.parseDouble(getJsonString(jsonObject, "transactionFee"));
                            double tvSubTotalValue = quantity * discountedPrice;

                            double discountAmount = 0;
                            if (discountType.equals("Percentage")) {
                                double original = Double.parseDouble(originalPrice);
                                double discountPer = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));

                                discountAmount = ((original * discountPer) / 100);
                            } else {
                                discountAmount = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));
                            }
                            mDiscountAmount += discountAmount * quantity;
                            totalAmount += tvSubTotalValue;

                            String userDetail = getLoadedData(Constant.USER_DETAIL);
                            JSONObject userData = new JSONObject(userDetail);
                            boolean isSubscribed = userData.getBoolean("isSubscribed");
                            if (!isSubscribed) {
                                if (transactionFee > transactionAmount) {
                                    transactionAmount = transactionFee;
                                }
                            }


                        }

                        String priceS = txtTotalPrice.getText().toString();
                        priceS = priceS.replace("£", "");
                        double price = Double.parseDouble(priceS) - transactionAmount;
                        if (totalAmount < price) {
                            Toast.makeText(mActivity, "Please review your cart, one or more offers are no longer available", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        /*try {
                            tvTransactionFees.setText("Transaction fee: £" + getJsonString(offerCartList.getJSONObject(0), "transactionFee"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        // String TotalPrice1 = String.format("%.2f", totalAmount);
                        // txtTotalPrice.setText("£" + TotalPrice1);

//                        tvSaving.setText("YOUR SAVING: £" + String.format("%.2f", mDiscountAmount));
//                        tvSaving.setVisibility(View.VISIBLE);

                        setTotalPrice(offerCartList);
                        cartListAdapter = new CartAdapter(mActivity, offerCartList);

                        listView.setAdapter(cartListAdapter);
                        // setListViewHeightBasedOnChildren(listView);
                        checkOutCartItems();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String URL = URL_Utils.USER + "/" + mUserId + "/cart/offer/List";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    @OnClick({R.id.relative_cancel, R.id.relative_footer, R.id.tvSearchOffers})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_cancel:
                onBackPressed();
                break;
            case R.id.relative_footer:
                break;
            case R.id.tvSearchOffers:
                Intent intent = new Intent();
                intent.putExtra("SearchOffers", false);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /****************************************************************/

    private void updateQuantity(String orderDetailID, String quantity) {
        String URL = URL_Utils.UPDATE_OFFER_QUANTITY_INCART + "/order/" + orderDetailID + "/quantity/" + quantity;
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, successListener, errorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
        }
    };

    Response.Listener<JSONObject> successListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    getCartList();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    double mTransactionFee = 0.00;

    private void setTotalPrice(JSONArray offerCartList) {
        mTransactionFee = 0.00;
        double mDiscountAmount = 0, totalAmount = 0;
        for (int i = 0; i < offerCartList.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = offerCartList.getJSONObject(i);
            } catch (JSONException e) {
            }
            String discountType = getJsonString(jsonObject, "discountType");
            String originalPrice = getJsonString(jsonObject, "originalPrice");

            int quantity = Integer.parseInt(getJsonString(jsonObject, "qunatity"));
            double discountedPrice = Double.parseDouble(getJsonString(jsonObject, "discountedPrice"));
            double tvSubTotalValue = quantity * discountedPrice;
            double transactionFee = Double.parseDouble(getJsonString(jsonObject, "transactionFee"));

            double discountAmount = 0;
            if (discountType.equals("Percentage")) {
                double original = Double.parseDouble(originalPrice);
                double discountPer = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));

                discountAmount = ((original * discountPer) / 100);
            } else {
                discountAmount = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));
            }
            mDiscountAmount += discountAmount * quantity;
            totalAmount += tvSubTotalValue;

            boolean isSubscribed = false;
            try {
                String userDetail = getLoadedData(Constant.USER_DETAIL);
                JSONObject userData = new JSONObject(userDetail);
                isSubscribed = userData.getBoolean("isSubscribed");
                if (!isSubscribed) {
                    if (transactionFee > mTransactionFee) {
                        mTransactionFee = transactionFee;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            tvTransactionFees.setText("Transaction fee: £" + String.format("%.2f", mTransactionFee));

            String TotalPrice1 = String.format("%.2f", totalAmount + mTransactionFee);
            // String TotalPrice1 = String.format("%.2f", totalAmount + 1.00);
            txtTotalPrice.setText("£" + TotalPrice1);

            // String TotalPrice = String.format("%.2f", tvSubTotalValue);
            if (mDiscountAmount == 0.0) {
                tvSaving.setText("YOUR SAVING: £" + String.format("%.2f", mDiscountAmount));
                tvSaving.setVisibility(View.GONE);
//                Toast.makeText(mActivity, "Jero", Toast.LENGTH_SHORT).show();
            } else {
                tvSaving.setText("YOUR SAVING: £" + String.format("%.2f", mDiscountAmount));
                tvSaving.setVisibility(View.VISIBLE);
//                Toast.makeText(mActivity, "Not Jero", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public class CartAdapter extends BaseAdapter {

        private static final String TAG = "CartAdapter";
        private JSONArray offerCartList;
        Context mContext;
        private int mPosition = -1;
        public int mSelectedPos;
        public HashMap<Integer, Integer> mQtyList;
        public HashMap<Integer, Double> mSavingList, mNetPriceList;
        double mDiscountAmount = 0;
        public double totalAmount = 0;
        String[] qty = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
        private static final int TYPE_ITEM = 1;
        private static final int TYPE_FOOTER = 2;
        LayoutInflater inflater;

        boolean isSelected;
        int check = 0;

        public CartAdapter(Context context, JSONArray clubLists) {
            this.mContext = context;
            this.offerCartList = clubLists;
            mSelectedPos = -1;
            mQtyList = new HashMap<>();
            mSavingList = new HashMap<>();
            mNetPriceList = new HashMap<>();
            mDiscountAmount = 0;
            totalAmount = 0;
            isSelected = false;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public JSONArray getDataList() {
            return offerCartList;
        }

        @Override
        public int getCount() {
            return offerCartList.length();
        }

        @Override
        public JSONObject getItem(int i) {
            try {
                return offerCartList.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            final ViewHolder holder;
            if (view != null) {
                holder = (ViewHolder) view.getTag();
            } else {
                view = inflater.inflate(R.layout.raw_cart_list, parent, false);
                holder = new ViewHolder(view);
                view.setTag(holder);
            }

            holder.spinner.setTag(position);
            holder.imgCancel.setTag(position);

            JSONObject jsonObject = null;
            try {
                jsonObject = offerCartList.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String offerId = getJsonString(jsonObject, "offerId");
            int offerIdL = Integer.parseInt(offerId);
            String eventImage = getJsonString(jsonObject, "eventImage");
            String title = getJsonString(jsonObject, "title");
            String venueName = getJsonString(jsonObject, "venueName");
            String discountType = getJsonString(jsonObject, "discountType");
            String originalPrice = getJsonString(jsonObject, "originalPrice");

            String eventEndDateTime = getJsonString(jsonObject, "eventEndDateTime");
            String expiresOn = getJsonString(jsonObject, "expiresOn");
            String offerExpiryMode = getJsonString(jsonObject, "offerExpiryMode");
            String expiresAfterDays = getJsonString(jsonObject, "expiresAfterDays");

            int quantity = Integer.parseInt(getJsonString(jsonObject, "qunatity"));
            double discountedPrice = Double.parseDouble(getJsonString(jsonObject, "discountedPrice"));
            double tvSubTotalValue = quantity * discountedPrice;

            double discountAmount = 0;
            if (discountType.equals("Percentage")) {
                double original = Double.parseDouble(originalPrice);
                double discountPer = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));

                discountAmount = ((original * discountPer) / 100);
            } else {
                discountAmount = Double.parseDouble(getJsonString(jsonObject, "discountAmount"));
            }

            if (!mSavingList.containsKey(offerIdL)) {
                mSavingList.put(offerIdL, discountAmount * quantity);
            } else {
            }

            totalAmount += tvSubTotalValue;

            String TotalPrice = String.format("%.2f", tvSubTotalValue);

            holder.tvVenueName.setText(venueName);
            holder.tvName.setText(title);
            holder.tvDateTime.setText(eventEndDateTime);
            holder.tvDateTime.setVisibility(View.GONE);
            holder.tvSubTotalValue.setText("£" + TotalPrice);

            holder.tvPrice.setText("£" + String.format("%.2f", discountedPrice));

            String expireMessage = "";


            if (offerExpiryMode.equals("OnDate")) {
                expireMessage = "(Voucher Expires on " + expiresOn + ")";
            } else if (offerExpiryMode.equals("AtEventExpiry")) {
                expireMessage = "(Voucher Expires on " + expiresOn + ")";
            } else if (offerExpiryMode.equals("AfterDays")) {
                if (TextUtils.isEmpty(expiresAfterDays)) {
                    expireMessage = "(Voucher Expires at midnight on " + expiresOn + ")";
                } else {
                    expireMessage = "(Voucher Expires " + expiresAfterDays + " days from purchase)";
                    holder.tvExpiresDate.setText(expireMessage);
                }
            }
            holder.tvExpiresDate.setText(expireMessage);


            ImageLoader mImageLoader = VolleySingleton.getInstance().getImageLoader();
            mImageLoader.get(URL_Utils.URL_IMAGE_DNLOAD + eventImage, ImageLoader.getImageListener(holder.imgMain, 0, R.mipmap.default_club_logo));
            String totalRemainingOffers = getJsonString(jsonObject, "offerForSale");
            int remaining = Integer.parseInt(totalRemainingOffers);
            ArrayList<String> arrayList = new ArrayList<String>();
            if (remaining < 20) {
                for (int i = 0; i < remaining; i++) {
                    arrayList.add(qty[i]);
                }
            } else {
                for (int i = 0; i < 20; i++) {
                    arrayList.add(qty[i]);
                }
            }
            ArrayAdapter<String> adp = new ArrayAdapter<String>(mContext, R.layout.simple_spinner_item, arrayList);
            holder.spinner.setAdapter(adp);

            if (quantity > 20) {
                if (remaining < quantity) {
                    holder.spinner.setSelection(remaining - 1);
                } else {
                    holder.spinner.setSelection(19);
                }
            } else {
                if (remaining < quantity) {
                    holder.spinner.setSelection(remaining - 1);
                } else {
                    holder.spinner.setSelection(quantity - 1);
                }
            }

            //check = 0;
            final AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    int posAdapter = (int) parent.getTag();
                    JSONObject jsonObject = null;
                    if (isSelected) {
                        try {
                            jsonObject = offerCartList.getJSONObject(posAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String totalRemainingOffers = getJsonString(jsonObject, "offerForSale");
                        String orderDetailID = getJsonString(jsonObject, "orderDetailId");
                        if (Integer.parseInt(totalRemainingOffers) >= (pos + 1)) {
                            mQtyList.put(posAdapter, pos);
                            try {

                                jsonObject.put("qunatity", String.valueOf(pos + 1));
                                offerCartList.put(posAdapter, jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            updateQuantity(orderDetailID, String.valueOf(pos + 1));
                        } else {
                            if (Integer.parseInt(totalRemainingOffers) > 0) {
                                mQtyList.put(posAdapter, Integer.parseInt(totalRemainingOffers));
                                try {
                                    jsonObject.put("qunatity", totalRemainingOffers);
                                    offerCartList.put(posAdapter, jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                updateQuantity(orderDetailID, totalRemainingOffers);
                            }
                        }
                        setTotalPrice(offerCartList);
                        mDiscountAmount = 0;
                        totalAmount = 0;
                        isSelected = false;

                        notifyDataSetChanged();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            };
            holder.spinner.setOnItemSelectedListener(listener);
            holder.spinner.setFocusable(false);
            holder.rlySpinner.setFocusable(true);
            holder.rlySpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isSelected = true;

                }
            });
            holder.spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    isSelected = true;
                    return false;
                }
            });


            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (int) view.getTag();
                    mSelectedPos = position;
                    cancelDialog();
                }
            });

            return view;
        }

        private void cancelDialog() {
            if (!haveNetworkConnection(mContext)) {
                Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
                return;
            }

            String itemTitle = "";
            try {
                itemTitle = offerCartList.getJSONObject(mSelectedPos).getString("title");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
            //alertDialogBuilder.setTitle("OOPS");
            alertDialogBuilder.setTitle("Confidentials");
            alertDialogBuilder.setMessage("Are you sure you want to remove " + itemTitle + " from your cart?");
            alertDialogBuilder.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            String orderDetailId = "";
                            try {
                                orderDetailId = offerCartList.getJSONObject(mSelectedPos).getString("orderDetailId");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            totalAmount = 0;
                            mDiscountAmount = 0;

                            String URL = URL_Utils.USER + "/" + mUserId + "/cartitem/" + orderDetailId + "/delete";
                            CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, null, createRequestSuccessListener, createRequestErrorListener);
                            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
                        }
                    });

            alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }


        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    String success = response.getString("success");
                    if (success.equals("1")) {
                        ;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            offerCartList.remove(mSelectedPos);
                        }
                        String cartItemCount = response.getString("cartItemCount");
                        saveLoadedData("numberOfAddToCart", "" + cartItemCount);
                        if (offerCartList.length() == 0) {
                            tvTransactionFees.setText("Transaction fee: £" + String.format("%.2f", 0.00));
                            txtTotalPrice.setText("£" + String.format("%.2f", 0.00));
                            tvSaving.setText("YOUR SAVING: £" + String.format("%.2f", 0.00));
                            MainActivity.ITEM_ADD_TO_CART = 0;
                            MainActivity.setHeaderNotificationNumber(MainActivity.ITEM_ADD_TO_CART);
                            saveLoadedData("numberOfAddToCart", "" + MainActivity.ITEM_ADD_TO_CART);
                            tvTitleHeader.setText("MY CART (" + 0 + ")");
                            tvSaving.setVisibility(View.VISIBLE);
                            Utils.removeAlarmBroadcastReceiver(CartMainActivity.this, 1111);
                            Utils.removeAlarmBroadcastReceiver(CartMainActivity.this, 1113);
                            mPrefs.edit().putLong("alarmExpireTime", 0).apply();
                        } else {
                            setTotalPrice(offerCartList);
                        }
                        notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        class ViewHolder {
            @Bind(R.id.imgMain)
            ImageView imgMain;
            @Bind(R.id.tvVenueName)
            TextView tvVenueName;
            @Bind(R.id.tvName)
            TextView tvName;
            @Bind(R.id.tvPrice)
            TextView tvPrice;
            @Bind(R.id.tvDateTime)
            TextView tvDateTime;
            @Bind(R.id.imgCancel)
            ImageView imgCancel;
            @Bind(R.id.spinner)
            Spinner spinner;
            @Bind(R.id.rlySpinner)
            RelativeLayout rlySpinner;
            @Bind(R.id.tvSubTotalValue)
            TextView tvSubTotalValue;
            @Bind(R.id.tvExpiresDate)
            TextView tvExpiresDate;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);

                tvName.setSelected(true);
                tvName.setFocusable(true);
            }
        }

    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }

}

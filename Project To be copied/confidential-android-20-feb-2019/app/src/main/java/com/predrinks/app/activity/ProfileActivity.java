package com.predrinks.app.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.app.Confidentials;
import com.predrinks.app.R;
import com.predrinks.app.fragment.SubscriptionsFragment;
import com.predrinks.app.network.CustomRequest;
import com.predrinks.app.network.VolleySingleton;
import com.predrinks.app.utils.Constant;
import com.predrinks.app.utils.URL_Utils;
import com.predrinks.app.utils.Utils;
import com.predrinks.app.widget.WheelView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.predrinks.app.utils.URL_Utils.PARAM_MOBILE_NO;
import static com.predrinks.app.utils.URL_Utils.RESET_PASSWORD;

public class ProfileActivity extends BaseFragmentActivity {

    private static final String TAG = "ProfileActivity";
    @Bind(R.id.tvEdit)
    TextView tvEdit;

    @Bind(R.id.etFName)
    EditText etFName;
    @Bind(R.id.etLName)
    EditText etLName;
    @Bind(R.id.etBDay)
    TextView etBDay;
    @Bind(R.id.tvSubscription)
    TextView tvSubscription;
    @Bind(R.id.etGender)
    TextView tvGender;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etMobile)
    EditText etMobile;
    @Bind(R.id.etPostcode)
    EditText etPostcode;
    @Bind(R.id.activity_profile)
    RelativeLayout activityProfile;

    private boolean isEditMode = false;
    private boolean isSubscribed = false;

    private static final String[] MONTH = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final int[] MONTH_INT = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    private static final List<String> DATES = new ArrayList<>();
    private static final List<String> YEAR = new ArrayList<>();

    Context mContext;
    Activity mActivity;

    //CBCBCB
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        ButterKnife.bind(this);
        mContext = this;
        mActivity = this;

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        isEditMode = false;
        setDefaultUI();
        getUserDetail1(getLoadedData(Constant.USERID));
        for (int i = 0; i <= 30; i++) {
            DATES.add(String.valueOf(i + 1));
        }
        for (int i = 1900; i < 2050; i++) {
            YEAR.add(String.valueOf(i + 1));
        }


        etFName.setTypeface(MainActivity.getTypeFaceRegular());
        etLName.setTypeface(MainActivity.getTypeFaceRegular());
        etBDay.setTypeface(MainActivity.getTypeFaceRegular());
        tvGender.setTypeface(MainActivity.getTypeFaceRegular());
        etEmail.setTypeface(MainActivity.getTypeFaceRegular());
        etPostcode.setTypeface(MainActivity.getTypeFaceRegular());
        etMobile.setTypeface(MainActivity.getTypeFaceRegular());

        etPostcode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

    }

    private void setDefaultUI() {
        etFName.setEnabled(false);
        etLName.setEnabled(false);
        etBDay.setEnabled(false);
        tvGender.setEnabled(false);
        etEmail.setEnabled(false);
        etPostcode.setEnabled(false);
        etMobile.setEnabled(false);
    }

    private void setEnableUI() {
        etFName.setEnabled(true);
        etLName.setEnabled(true);
        etBDay.setEnabled(true);
        tvGender.setEnabled(true);
        etPostcode.setEnabled(true);
        etMobile.setEnabled(true);
    }


    @OnClick({R.id.lnrBack, R.id.llHeaderRight, R.id.etBDay, R.id.etGender, R.id.lnrSubscription})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnrBack:
                finish();
                break;
            case R.id.llHeaderRight:
                hideKeyBoard(etFName);
                if (!isEditMode) {
                    isEditMode = true;
                    setEnableUI();
                    tvEdit.setText("SAVE");
                } else {
                    updateUserDetail();
                }
                break;
            case R.id.etBDay:
                setDateTimeField();
                hideKeyBoard(etFName);
                break;
            case R.id.etGender:
                hideKeyBoard(etFName);
                firstExecution = true;
                selectGender();
                break;
            case R.id.lnrSubscription:
                if (isSubscribed)
                    break;

                hideKeyBoard(etFName);
                SubscriptionsFragment fragment = SubscriptionsFragment.newInstance("", "");
                MainActivity.loadContentFragmentWithBack(fragment, false);
                finish();
                break;
        }
    }

    private boolean firstExecution = true;
    private android.app.AlertDialog dialog;
    String genderArray[] = {"Select", "Male", "Female", "Other"};
    Spinner spinner;
    private ContextWrapper context;

    private void selectGender() {
        context = mActivity;
        if (Utils.isBrokenSamsungDevice()) {
            context = new ContextThemeWrapper(mActivity, android.R.style.Theme_Holo_Light_Dialog);
        }
        if (context == null)
            return;

        spinner = new Spinner(context);
        firstExecution = false;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, genderArray);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                if (firstExecution) {
                    firstExecution = false;
                    return;
                }
                if (tvGender != null && arg2 != 0) {
                    tvGender.setText(genderArray[arg2]);
                    tvGender.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (dialog != null && dialog.isShowing() && arg2 != 0) {
                                dialog.dismiss();
                            }
                        }
                    }, 1000);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spinner.setAdapter(adapter);
        spinner.setPadding(40, 10, 10, 10);
        if (dialog == null) {
            dialog = new android.app.AlertDialog.Builder(mActivity)
                    .setView(spinner)
                    .setTitle("Gender")
                    .create();
        }
        dialog.show();
    }


    private SimpleDateFormat dateFormatter;
    int monthOfYear;
    int year;
    int dayOfMonth;

    private void setDateTimeField() {
        monthOfYear = -1;
        year = -1;
        dayOfMonth = -1;

        final int yearThis, month, date, posYear, posDate;
        if (TextUtils.isEmpty(birthDay)) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            yearThis = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH) + 1;
            date = calendar.get(Calendar.DAY_OF_MONTH);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date));
        } else {
            birthDay = birthDay.replace(".", "/");
            String[] fulldate = birthDay.split("/");
            yearThis = Integer.parseInt(fulldate[2]);
            month = Integer.parseInt(fulldate[1]);
            date = Integer.parseInt(fulldate[0]);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date)) + 1;
        }

        View outerView = LayoutInflater.from(this).inflate(R.layout.wheel_view, null);
        WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);

        wv.setItems(Arrays.asList(MONTH));
        wv.setSeletion(month - 1);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                monthOfYear = selectedIndex;
            }
        });
        WheelView wv1 = (WheelView) outerView.findViewById(R.id.wheel_view_wv1);
        wv1.setItems(DATES);

        wv1.setSeletion(posDate - 1);
        wv1.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                dayOfMonth = Integer.parseInt(item);
            }
        });
        WheelView wv2 = (WheelView) outerView.findViewById(R.id.wheel_view_wv2);
        wv2.setItems(YEAR);
        wv2.setSeletion(posYear);
        wv2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                year = Integer.parseInt(item);
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });

        new AlertDialog.Builder(this)
                .setTitle("Date of Birth")
                .setView(outerView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (monthOfYear == -1) {
                            monthOfYear = month - 1;
                        } else {
                            monthOfYear -= 1;
                        }
                        if (dayOfMonth == -1) {
                            dayOfMonth = posDate;
                        }
                        if (year == -1) {
                            year = Integer.parseInt(YEAR.get(posYear));
                        }
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        Date birthDate = new Date(newDate.getTimeInMillis());

                        onDateSet(year, monthOfYear, dayOfMonth, birthDate);
                    }
                })
                .show();
    }

    public void onDateSet(int year, int month, int day, Date newDate) {
        Calendar userAge = new GregorianCalendar(year, month, day);
        Calendar minAdultAge = new GregorianCalendar();
        Calendar minAdultAge1 = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        minAdultAge1.set(1901, 1, 1);
        if (minAdultAge.before(userAge) || minAdultAge1.after(userAge)) {
            Toast.makeText(mContext, "Sorry. Date of birth is invalid. Must be over 18 years old", Toast.LENGTH_SHORT).show();
        } else {
            String date = dateFormatter.format(newDate.getTime());
            date = date.replace("/", ".");
            birthDay = date;
            etBDay.setText("Birthday: " + date);
        }
    }


    private void updateUI(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        JSONObject userDetail = jsonObject.getJSONObject("result");

        personFirstName = checkString(userDetail.getString("firstName"));
        personLastName = checkString(userDetail.getString("lastName"));
        if (userDetail.has("postCode"))
            personPostcode = checkString(userDetail.getString("postCode"));
        if (userDetail.has("mobileNo"))
            personMobile = checkString(userDetail.getString("mobileNo"));
        personEmail = checkString(userDetail.getString("emailId"));
        birthDay = checkString(userDetail.getString("dOB"));
        gender = checkString(userDetail.getString("gender"));
        registrationMode = checkString(userDetail.getString("registrationMode"));
        isSubscribed = userDetail.getBoolean("isSubscribed");
        subscriptionMode = checkString(userDetail.getString("subscriptionMode"));
        subscriptionPeriod = checkString(userDetail.getString("subscriptionPeriod"));

        String fName = "", lName = "";
        try {
            fName = personFirstName.substring(0, 1).toUpperCase() + personFirstName.substring(1);
            lName = personLastName.substring(0, 1).toUpperCase() + personLastName.substring(1);
        } catch (IndexOutOfBoundsException e) {
        }

        etFName.setText(fName);
        etLName.setText(lName);

        etPostcode.setText(personPostcode);
        etMobile.setText(personMobile);

        birthDay = birthDay.replace("/", ".");
        etBDay.setText("Birthday: " + birthDay);
        tvGender.setText(gender);
        etEmail.setText(personEmail);

        if (isSubscribed) {
            if (subscriptionMode.equals("MonthlyFromCMS")) {
                tvSubscription.setText("Subscription - " + subscriptionPeriod + " Month(s)");
            } else {
                tvSubscription.setText("Subscription - " + subscriptionMode);
            }
        } else {
            tvSubscription.setText("Subscription - None");
        }

    }

    ProgressDialog pDialog;

    private void getUserDetail1(String userId) {
        Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                setDefaultUI();
                //setEnableUI();
                Log.d(TAG, error.toString());
                Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        };

        Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                try {
                    String message = response.getString("success");
                    if (message.equals("1")) {
                        updateUI(response.toString());
                        JSONObject result = null;
                        try {
                            result = response.getJSONObject("result");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        saveLoadedData(Constant.USER_DETAIL, result.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        };

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = URL_Utils.USER + "/" + userId + "/" + "detail";
        CustomRequest jsObjRequest = new CustomRequest(url, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

    }

    String personFirstName, personLastName, personMobile = "", personPostcode = "", personEmail, gender, birthDay, registrationMode;
    String deviceID = "", subscriptionMode, subscriptionPeriod;

    private void updateUserDetail() {
        Response.Listener<JSONObject> updateSuccessListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                try {
                    String message = response.getString("success");
                    if (message.equals("1")) {
                        getUserDetail1(getLoadedData(Constant.USERID));
                        tvEdit.setText("EDIT");
                        isEditMode = false;
                        setDefaultUI();
                        updateUI(response.toString());

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener updateErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                setDefaultUI();
                //setEnableUI();
                Log.d(TAG, error.toString());
            }
        };

        if (!haveNetworkConnection(this)) {
            Toast.makeText(mContext, R.string.error_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        deviceID = getLoadedData(Confidentials.REG_ID);

        personFirstName = etFName.getText().toString();
        personLastName = etLName.getText().toString();
        personPostcode = etPostcode.getText().toString();
        personMobile = etMobile.getText().toString();
        gender = tvGender.getText().toString();
        birthDay = etBDay.getText().toString();
        personEmail = etEmail.getText().toString();
        birthDay = birthDay.replace("Birthday: ", "");
        birthDay = birthDay.replace(".", "/");
        etBDay.setError(null);

        /*if (TextUtils.isEmpty(personFirstName)) {
            Snackbar.make(activityProfile, "Please enter first name", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(personLastName)) {
            Snackbar.make(activityProfile, "Please enter last name", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(birthDay)) {
            Snackbar.make(activityProfile, "Please select date of birth", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(personMobile)) {
            Snackbar.make(activityProfile, "Please enter mobile number", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Utils.isValidMobileNumber(personMobile)) {
            Snackbar.make(activityProfile, "Mobile number should be at least 10 digits", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(personPostcode)) {
            Snackbar.make(activityProfile, "Please enter postcode", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (!Utils.isValidPostcodeForUK(personPostcode)) {
            Snackbar.make(activityProfile, "Please enter a valid postcode", Snackbar.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(gender) || gender.equals("Select")) {
            Snackbar.make(activityProfile, "Please select gender", Snackbar.LENGTH_LONG).show();
            return;
        }*/


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        String fName = personFirstName.substring(0, 1).toUpperCase() + personFirstName.substring(1);
        String lName = personLastName.substring(0, 1).toUpperCase() + personLastName.substring(1);
        etFName.setText(fName);
        etPostcode.setText(personPostcode);
        etMobile.setText(personMobile);
        etLName.setText(lName);

        Map<String, String> params = new HashMap<String, String>();
        params.put("UserId", getLoadedData(Constant.USERID));
        params.put(URL_Utils.PARAM_DOB, birthDay);
        params.put(URL_Utils.PARAM_FIRST_NAME, fName);
        params.put(URL_Utils.PARAM_LAST_NAME, lName);
        params.put(URL_Utils.PARAM_POSTCODE, personPostcode);
        params.put(PARAM_MOBILE_NO, personMobile);
        params.put(URL_Utils.PARAM_EMAIL_ID, personEmail);
        params.put(URL_Utils.PARAM_GENDER, gender);
        params.put(URL_Utils.PARAM_PROFILE_PIC, "");
        params.put(URL_Utils.PARAM_REGISTRATION_MODE, registrationMode);
        params.put(URL_Utils.PARAM_DEVICE_ID, deviceID);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL_Utils.EDIT_PROFILE, params, updateSuccessListener, updateErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void showChangePasswordDialog(View view) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.change_password_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setCancelable(false);

        EditText oldPasswordEt = dialog.findViewById(R.id.old_pass_et);
        EditText newPasswordEt = dialog.findViewById(R.id.new_pass_et);
        EditText newCnfrmPasswordEt = dialog.findViewById(R.id.confirm_new_pass_et);

        CardView updateBt = dialog.findViewById(R.id.update_bt);
        CardView cancelBt = dialog.findViewById(R.id.cancel_bt);

        updateBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPasswordEt.getText().toString().trim().length() == 0) {
                    Toast.makeText(mContext, "Enter old Password", Toast.LENGTH_SHORT).show();
                } else if (newPasswordEt.getText().toString().trim().length() == 0) {
                    Toast.makeText(mContext, "Enter new Password", Toast.LENGTH_SHORT).show();
                } else if (newCnfrmPasswordEt.getText().toString().trim().length() == 0) {
                    Toast.makeText(mContext, "Enter new Confirm Password", Toast.LENGTH_SHORT).show();
                } else {
                    if (newPasswordEt.getText().toString().equals(newCnfrmPasswordEt.getText().toString())) {
                        hitApi(dialog, oldPasswordEt.getText().toString(), newPasswordEt.getText().toString(), newCnfrmPasswordEt.getText().toString());
                    } else {
                        Toast.makeText(mContext, "New passwords should be same", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        cancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void hitApi(Dialog dialog, String oldPassword, String newPassword, String confirmNewPassword) {
        JSONObject j = new JSONObject();
        try {
            j.put("userId", getLoadedData(Constant.USERID));
            j.put("password", newPassword);
            j.put("old_password", oldPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, RESET_PASSWORD, j, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getInt("success") == 1){
                        dialog.cancel();
                        Toast.makeText(mContext, "Password Updated", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "Old password not correct", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}

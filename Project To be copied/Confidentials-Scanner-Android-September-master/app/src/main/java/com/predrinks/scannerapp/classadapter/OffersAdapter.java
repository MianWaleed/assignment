package com.predrinks.scannerapp.classadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyViewHolder> {

    private JSONArray pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imgMainBg)
        ImageView imgMainBg;
        @Bind(R.id.tvRemaining)
        TextView tvRemaining;
        @Bind(R.id.tvOfferTitle)
        TextView tvOfferTitle;
        @Bind(R.id.tvOfferDetail)
        TextView tvOfferDetail;
        @Bind(R.id.tvClubTitle)
        TextView tvClubTitle;
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.tvExpired)
        TextView tvExpired;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvOfferTitle.setSelected(true);
            tvOfferDetail.setSelected(true);
            tvOfferTitle.setFocusable(true);
            tvOfferDetail.setFocusable(true);
        }
    }


    public OffersAdapter(Context context, JSONArray moviesList) {
        this.mContext = context;
        this.pogoList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offer_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        JSONObject jsonObject = null;
        try {
            jsonObject = pogoList.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonObject == null)
            return;

        String imagename = getJsonString(jsonObject, "eventImage");
        String totalRemainingOffers = getJsonString(jsonObject, "totalRemainingOffers");
        String title = getJsonString(jsonObject, "title");
        String subTitle = getJsonString(jsonObject, "subTitle");
        String venueName = getJsonString(jsonObject, "venueName");
        String eventEndDateTime = getJsonString(jsonObject, "eventEndDateTime");
        String isActive = getJsonString(jsonObject, "isActive");
        String IsExpired = getJsonString(jsonObject, "isExpired");

        holder.tvOfferTitle.setText(title);
        holder.tvOfferDetail.setText(subTitle);
        holder.tvClubTitle.setText(venueName);
        holder.tvDateTime.setText(eventEndDateTime);

        String isEventDateDisabled = getJsonString(jsonObject, "isEventDateDisabled");
        if (isEventDateDisabled.equals("true")){
            holder.tvDateTime.setVisibility(View.INVISIBLE);
        }else {
            holder.tvDateTime.setVisibility(View.VISIBLE);
        }

        if (totalRemainingOffers.contains("-1")) {
            holder.tvRemaining.setVisibility(View.GONE);
        }else {
            holder.tvRemaining.setText(totalRemainingOffers.toUpperCase());
            holder.tvRemaining.setVisibility(View.VISIBLE);
        }


        holder.tvExpired.setVisibility(View.INVISIBLE);
        if (IsExpired.equals("true")){
            holder.tvExpired.setText("Expired");
            holder.tvExpired.setVisibility(View.VISIBLE);
        }else {
            if (isActive.equals("true")){
                holder.tvExpired.setVisibility(View.INVISIBLE);
            }else {
                holder.tvExpired.setText("EVENT CANCELLED");
                holder.tvExpired.setVisibility(View.VISIBLE);
            }
        }


        String imgUrl = "";

        if (TextUtils.isEmpty(imagename)) {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + "images/default_con.png";
        } else {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + imagename;
        }

        Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_event_venue_image)
                .into(holder.imgMainBg);
    }

    @Override
    public int getItemCount() {
        return pogoList.length();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }
}

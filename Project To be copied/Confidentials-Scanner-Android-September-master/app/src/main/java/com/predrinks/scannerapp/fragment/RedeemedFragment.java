package com.predrinks.scannerapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.predrinks.scannerapp.BaseFragment;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.activity.ScannerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.predrinks.scannerapp.database.ConfidentialsOrderDAO.KEY_EVENT_NAME;
import static com.predrinks.scannerapp.database.ConfidentialsOrderDAO.KEY_OFFER_TITLE;


public class RedeemedFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    TextView tvQrCode, tvOfferTitle, tvClubTitle;

    Activity mActivity;
    private Context mContext;

    public RedeemedFragment() {
    }

    public static RedeemedFragment newInstance(String param1, String param2) {
        RedeemedFragment fragment = new RedeemedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext = this.getContext();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setActonBar(mContext);

    }

    @Override
    public void onResume() {
        super.onResume();
        ScannerActivity.layout_bottom.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        ScannerActivity.layout_bottom.setVisibility(View.VISIBLE);
    }

    View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_redeemed, container, false);
        mView = view;

        tvQrCode = (TextView) view.findViewById(R.id.tvQrCode);
        tvOfferTitle = (TextView) view.findViewById(R.id.tvOfferTitle);
        tvClubTitle = (TextView) view.findViewById(R.id.tvClubTitle);

        ScannerActivity.showHeaderRight("CLOSE");
        ScannerActivity.setHeaderRightClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });
        if (!TextUtils.isEmpty(mParam2)) {
            String qrcode = mParam1;

            JSONObject object = null;
            try {
                object = new JSONObject(mParam2);
                String offerTitle = object.getString(KEY_OFFER_TITLE);
                String venueTitle = object.getString(KEY_EVENT_NAME);

                tvQrCode.setText(qrcode);
                tvOfferTitle.setText(offerTitle);
                tvClubTitle.setText(venueTitle);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        ScannerFragment.isStartedRedeem = false;
        updateToServer();
        ScannerActivity.setBackVisible(false);
        return view;
    }


}

package com.predrinks.scannerapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.fragment.ScannerFragment;
import com.predrinks.scannerapp.network.CustomRequest;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class BaseFragment extends Fragment {


    private static final String TAG = "BaseFragment";
    private SharedPreferences mPrefs;


    private String ScannedByTeamMemberID = "";
    private ConfidentialsOrderDAO confidentialsOrderDAO;
    private Context mContext;
    private String loginResponse;

    public void setActonBar(Context context) {
        mContext = context;
        confidentialsOrderDAO = new ConfidentialsOrderDAO(mContext);
        loginResponse = getLoadedData(mContext, "loginResponse");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(loginResponse);
            ScannedByTeamMemberID = jsonObject.getJSONObject("teammember").getString("teamMemberID");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    protected boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    protected void saveLoadedData(Context context, String TAG, String data) {
        mPrefs = context.getSharedPreferences(ConfidentialsScanner.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(TAG, data);
        editor.apply();
    }

    protected String getLoadedData(Context context, String TAG) {
        mPrefs = context.getSharedPreferences(ConfidentialsScanner.NAME_PREFERENCE, 0);
        String data = mPrefs.getString(TAG, "");
        return data;
    }

    protected void setLoggedIn(Context context, boolean isLogin) {
        mPrefs = context.getSharedPreferences(ConfidentialsScanner.NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean("isLoggedIn", isLogin);
        editor.apply();
    }

    protected String getJsonString(JSONObject jsonObject, String strKey) {
        String str = "";
        try {
            if (jsonObject == null || !jsonObject.has(strKey))
                return str;

            str = jsonObject.getString(strKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str.equals("null")) {
            return "";
        }
        return str;
    }


    protected void dialog_info(Activity activity, String title, String message) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    protected void updateToServer() {
        if (ScannerFragment.isStartedRedeem)
            return;

        if (haveNetworkConnection(mContext)) {
            confidentialsOrderDAO.open();
            if (confidentialsOrderDAO.isSyncedData("true")) {
                JSONArray jsonArray = confidentialsOrderDAO.getUnSyncedData("true", ScannedByTeamMemberID);
                if (jsonArray != null && jsonArray.length() > 0) {
                    postDataToServer(jsonArray);
                }
            }
            confidentialsOrderDAO.close();
        }
    }

    private void postDataToServer(JSONArray jsonArray) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("RedeemedOffers", jsonArray);
            String URL = URL_Utils.REDEEM_OFFER;

            Log.d(TAG, "postDataToServer: object => " + jsonObject);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            try {
                                String success = response.getString("success");
                                if (success.equals("1")) {
                                    confidentialsOrderDAO.open();
                                    confidentialsOrderDAO.upDateIsSynced("true");
                                    confidentialsOrderDAO.close();
                                    getDataFromWeb();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse response = error.networkResponse;
                    VolleyLog.d(TAG, "Error: " + response);

                    try {
                        byte[] bytes = response.data;
                        String str = new String(bytes, "UTF-8");
                        Log.i("str", str);
                    } catch (Exception e) {

                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
        } catch (Exception e) {

        }
    }

    private void getDataFromWeb() {
        if (haveNetworkConnection(mContext)) {
            String venueID = "";
            try {
                JSONObject loginResponse = new JSONObject(getLoadedData(mContext, "loginResponse"));
                JSONObject memberObject = loginResponse.getJSONObject("teammember");
                venueID = memberObject.getString("venueID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(venueID))
                return;

            String URL = URL_Utils.VENUE + "/" + venueID + "/purchasedoffers";
            CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
        }
    }

    Response.ErrorListener ErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());

        }
    };

    Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());

            try {
                String success = response.getString("message");
                if (success.equals("success")) {
                    JSONArray resultArray = response.getJSONArray("result");

                    if (resultArray != null && resultArray.length() > 0) {
                        if (!ScannerFragment.isStartedRedeem)
                            insertUpdateData(resultArray);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void insertUpdateData(JSONArray resultArray) {
        try {
            int size = resultArray.length();
            if (size > 0) {
                confidentialsOrderDAO.open();
                if (!confidentialsOrderDAO.isSyncedData("true")) {
                    for (int i = 0; i < size; i++) {
                        int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                        String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                                isRedeemed = "", isExpired = "", userName = "", eventTime = "", redeemedOn = "",
                                offerExpiryMode = "", expiresOn = "", expiryTime = "", isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";
                        JSONObject object = resultArray.getJSONObject(i);

                        myOfferID = object.getInt(ConfidentialsOrderDAO.KEY_MY_OFFER_ID);
                        //   orderID = object.getInt(ConfidentialsOrderDAO.KEY_ORDER_ID);
                        eventID = object.getInt(ConfidentialsOrderDAO.KEY_EVENT_ID);
                        offerID = object.getInt(ConfidentialsOrderDAO.KEY_OFFER_ID);
                        userID = object.getInt(ConfidentialsOrderDAO.KEY_USER_ID);
                        quantity = object.getInt(ConfidentialsOrderDAO.KEY_QUANTITY);
                        offerTitle = object.getString(ConfidentialsOrderDAO.KEY_OFFER_TITLE);
                        eventName = object.getString(ConfidentialsOrderDAO.KEY_EVENT_NAME);
                        venueTitle = object.getString(ConfidentialsOrderDAO.KEY_VENUE_TITLE);
                        qRCode = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE);
                        qRCodeImage = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE_IMAGE);
                        isRedeemed = object.getString(ConfidentialsOrderDAO.KEY_IS_REDEEMED);
                        isExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_EXPIRED);
                        userName = object.getString(ConfidentialsOrderDAO.KEY_USER_NAME);
                        eventTime = object.getString(ConfidentialsOrderDAO.KEY_EVENT_TIME);
                        redeemedOn = object.getString(ConfidentialsOrderDAO.KEY_REDEEMED_ON);
                        offerExpiryMode = object.getString(ConfidentialsOrderDAO.KEY_OFFER_EXPIRY_MODE);
                        expiresOn = object.getString(ConfidentialsOrderDAO.KEY_EXPIRES_ON);
                        expiryTime = object.getString(ConfidentialsOrderDAO.KEY_EXPIRY_TIME);
                        isOfferExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_EXPIRED);
                        isOfferActive = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_ACTIVE);
                        isEventDateDisabled = object.getString(ConfidentialsOrderDAO.KEY_IS_EVENT_DATE_DISABLED);
                        dateUpdated = object.getString(ConfidentialsOrderDAO.KEY_DATE_UPDATED);


                        if (confidentialsOrderDAO.getTotalOffersCount() == 0) {
                            confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                        } else {
                            if (confidentialsOrderDAO.isOffersExist(myOfferID, orderID, offerID, userID)) {
                                confidentialsOrderDAO.updateOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            } else {
                                confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            }
                        }
                    }
                }
                confidentialsOrderDAO.close();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

package com.predrinks.scannerapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.predrinks.scannerapp.activity.LoginActivity;
import com.predrinks.scannerapp.activity.ScannerActivity;


public class SplashActivity extends BaseFragmentActivity {

    private int progressStatus = 0;
    private static int progress;
    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus = doSomeWork();
                    handler.post(new Runnable() {
                        public void run() {
                        }
                    });
                }
                handler.post(new Runnable() {
                    public void run() {
                        Intent intent;
                        if (!isLoggedIn())
                            intent = new Intent(SplashActivity.this, LoginActivity.class);
                        else
                            intent = new Intent(SplashActivity.this, ScannerActivity.class);

                        startActivity(intent);
                        SplashActivity.this.finish();
                        handler.removeCallbacks(null);
                    }
                });
            }

            private int doSomeWork() {
                try {
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return ++progress;
            }
        }).start();
    }
}

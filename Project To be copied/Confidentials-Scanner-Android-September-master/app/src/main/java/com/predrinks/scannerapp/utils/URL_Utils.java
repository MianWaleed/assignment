package com.predrinks.scannerapp.utils;

public class URL_Utils {
    //    public static final String URL_IMAGE_DNLOAD						= "https://app.confidentials.com/";
//    public static final String BASE_URL								= "https://app.confidentials.com/api/";
//    http://predrinks.azurewebsites.net
//    public static final String BASE_URL = "http://178.62.60.182/api/";
//    public static final String URL_IMAGE_DNLOAD = "http://178.62.60.182/"; // sandbox

    public static final String BASE_URL = "http://confidentials.akkastechdemo.com/api/";
    public static final String URL_IMAGE_DNLOAD = "http://confidentials.akkastechdemo.com/"; // sandbox
//
//    public static final    String BASE_URL = "http://predrinks.azurewebsites.net/api/";
//    public static final String URL_IMAGE_DNLOAD = "http://predrinks.azurewebsites.net/"; //

//    public static final String URL_IMAGE_DNLOAD = "http://192.168.0.67/confidentials/public/";
//    public static final String BASE_URL                             = "http://192.168.0.67/confidentials/public/api/";

    public static final String LOGIN = BASE_URL + "teammember/login";

    public static final String REDEEM_OFFER = BASE_URL + "redeem/offer";
    public static final String VENUE = BASE_URL + "venue";
    public static final String VENUE_DETAIL = BASE_URL + "venue";

    public static final String AVAIL = BASE_URL + "confidentials/pass/redeem";

    public static final String CONFIDENTIALS_PASS = BASE_URL + "user/";
}

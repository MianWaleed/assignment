package com.predrinks.scannerapp.classadapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.models.HeroVoucherModel;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.MyPref;
import com.predrinks.scannerapp.utils.URL_Utils;
import com.predrinks.scannerapp.widget.ExpandableTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.android.volley.Request.Method.POST;

public class HeroVouchersAdapter extends RecyclerView.Adapter<HeroVouchersAdapter.MyViewHolder> {

    private List<HeroVoucherModel.Datum> pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView offerIv;
        TextView voucherTv, discountTv;
        TextView notAvailedTv, availedTv;
        ExpandableTextView descTv;

        public MyViewHolder(View view) {
            super(view);

            descTv = view.findViewById(R.id.desc_tv);
            notAvailedTv = view.findViewById(R.id.avail_and_not_tv);
            offerIv = view.findViewById(R.id.imgMainBg);
            voucherTv = view.findViewById(R.id.tvOfferTitle);
            discountTv = view.findViewById(R.id.tvOfferDetail);
            availedTv = view.findViewById(R.id.avail_not_tv);
        }
    }


    public HeroVouchersAdapter(Context context, List<HeroVoucherModel.Datum> moviesList) {
        this.mContext = context;
        this.pogoList = moviesList;
    }

    @Override
    public HeroVouchersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hero_voucher_item, parent, false);
        return new HeroVouchersAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HeroVouchersAdapter.MyViewHolder holder, int position) {
        final HeroVoucherModel.Datum pogo = pogoList.get(position);


        holder.discountTv.setText(pogo.getPrice());
        holder.voucherTv.setText(pogo.getTitle());

        holder.descTv.setText(pogo.getMessage());

        String imgUrl = "";

        if (TextUtils.isEmpty(pogo.getImage())) {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + "images/default_con.png";
        } else {
            imgUrl = URL_Utils.URL_IMAGE_DNLOAD + pogo.getImage();
        }

        if (pogo.getAvailed()) {
            holder.availedTv.setVisibility(View.VISIBLE);
            holder.notAvailedTv.setVisibility(View.GONE);
        } else {
            holder.notAvailedTv.setVisibility(View.VISIBLE);
            holder.availedTv.setVisibility(View.GONE);
        }

        Glide.with(mContext)
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.default_event_venue_image)
                .into(holder.offerIv);

        holder.notAvailedTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(mContext)
                        .setMessage("Are you sure?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                availTheOffer(pogo, holder);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();
            }
        });
    }

    private void availTheOffer(final HeroVoucherModel.Datum pogo, final MyViewHolder holder) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", MyPref.getId(mContext));
            jsonObject.put("passId", pogo.getId());
            jsonObject.put("teamMemberId", MyPref.getTeamMemberId(mContext));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, URL_Utils.AVAIL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pogo.setAvailed(true);
                if (pogo.getAvailed()) {
                    holder.availedTv.setVisibility(View.VISIBLE);
                    holder.notAvailedTv.setVisibility(View.GONE);
                } else {
                    holder.notAvailedTv.setVisibility(View.VISIBLE);
                    holder.availedTv.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public int getItemCount() {
        return pogoList.size();
    }
}

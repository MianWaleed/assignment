package com.predrinks.scannerapp.utils;

public class Constant {

    public static final String PARAM_EMAIL_ID = "EmailId";
    public static final String PARAM_PASSWORD = "Password";
    public static final String USERID = "userId";
    public static final String MESSAGE_TITLE = "Confidentials Scanner";

}

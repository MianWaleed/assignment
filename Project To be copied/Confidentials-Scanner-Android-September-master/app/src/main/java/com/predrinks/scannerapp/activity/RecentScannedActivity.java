package com.predrinks.scannerapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.predrinks.scannerapp.BaseFragmentActivity;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.classadapter.Items;
import com.predrinks.scannerapp.classadapter.ItemsListAdapter;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.utils.SimpleDividerItemDecoration;

import java.util.List;

public class RecentScannedActivity extends BaseFragmentActivity implements View.OnClickListener {

    private static final String TAG = "RecentScannedActivity";
    RelativeLayout rlTab1, rlTab2, rlTab3;
    TextView rlTabText1, rlTabText2, rlTabText3;
    TextView tvTitleHeader;

    LinearLayout lnrEmpty;
    RecyclerView recyclerView;
    ImageView imgRecentScanned;
    ConfidentialsOrderDAO confidentialsOrderDAO;
    EditText etSearch;
    String eventId, eventName;

    int mSelectedTab = 2;
    String isRedeem;
    String mQuery = "";
    private Context mContext;
    private String ScannedByTeamMemberID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_scanned);
        mContext = this.getApplicationContext();

        confidentialsOrderDAO = new ConfidentialsOrderDAO(this);

        Intent intent = getIntent();
        eventId = intent.getStringExtra("eventId");
        eventName = intent.getStringExtra("eventName");

        etSearch = (EditText) findViewById(R.id.etSearch);
        lnrEmpty = (LinearLayout) findViewById(R.id.lnrEmpty);

        rlTab1 = (RelativeLayout) findViewById(R.id.rlTab1);
        rlTab2 = (RelativeLayout) findViewById(R.id.rlTab2);
        rlTab3 = (RelativeLayout) findViewById(R.id.rlTab3);

        rlTabText1 = (TextView) findViewById(R.id.rlTabText1);
        rlTabText2 = (TextView) findViewById(R.id.rlTabText2);
        rlTabText3 = (TextView) findViewById(R.id.rlTabText3);

        tvTitleHeader = (TextView) findViewById(R.id.tvTitleHeader);

        imgRecentScanned = (ImageView) findViewById(R.id.imgRecentScanned);


        tvTitleHeader.setText(eventName);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setVerticalScrollBarEnabled(false);

        rlTab1.setOnClickListener(this);
        rlTab2.setOnClickListener(this);
        rlTab3.setOnClickListener(this);
        imgRecentScanned.setOnClickListener(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence)) {
                    mQuery = charSequence.toString();
                    setSearchData(mSelectedTab, isRedeem);
                } else {
                    mQuery = "";
                    setUIData(mSelectedTab, isRedeem);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        setUI();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setUIData(2, "true");
            }
        }, 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateToServer();
    }

    private void setUIData(int pos, String isTrue) {
        mSelectedTab = pos;
        isRedeem = isTrue;

        ItemsListAdapter adapter;
        confidentialsOrderDAO.open();
        List<Items> pogoList = confidentialsOrderDAO.getAllItemList(pos, eventId, isTrue);
        confidentialsOrderDAO.close();
        if (pogoList != null && pogoList.size() > 0) {
            adapter = new ItemsListAdapter(this, pogoList);
            recyclerView.setAdapter(adapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

            lnrEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void setSearchData(int pos, String isTrue) {
        ItemsListAdapter adapter;
        confidentialsOrderDAO.open();
        List<Items> pogoList = confidentialsOrderDAO.getSearchItemList(pos, eventId, isTrue, mQuery);
        confidentialsOrderDAO.close();

        if (pogoList != null && pogoList.size() > 0) {
            adapter = new ItemsListAdapter(this, pogoList);
            recyclerView.setAdapter(adapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
            lnrEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void setUI() {
        rlTab1.setBackground(getResources().getDrawable(R.drawable.bg_w_rounded_corner));
        rlTab2.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));
        rlTab3.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));

        rlTabText1.setTextColor(Color.WHITE);
        rlTabText2.setTextColor(Color.BLACK);
        rlTabText3.setTextColor(Color.BLACK);
    }

    @Override
    public void onClick(View view) {
       // etSearch.setText("");
        switch (view.getId()) {
            case R.id.rlTab1:
                rlTab1.setBackground(getResources().getDrawable(R.drawable.bg_w_rounded_corner));
                rlTab2.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));
                rlTab3.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));

                rlTabText1.setTextColor(Color.WHITE);
                rlTabText2.setTextColor(Color.BLACK);
                rlTabText3.setTextColor(Color.BLACK);

                if (!TextUtils.isEmpty(mQuery)) {
                    setSearchData(2, "true");
                } else {
                    setUIData(2, "true");
                }
                break;
            case R.id.rlTab2:
                rlTab1.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));
                rlTab2.setBackground(getResources().getDrawable(R.drawable.bg_w_rounded_corner));
                rlTab3.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));

                rlTabText1.setTextColor(Color.BLACK);
                rlTabText2.setTextColor(Color.WHITE);
                rlTabText3.setTextColor(Color.BLACK);

                if (!TextUtils.isEmpty(mQuery)) {
                    setSearchData(1, "true");
                } else {
                    setUIData(1, "true");
                }
                break;
            case R.id.rlTab3:
                rlTab1.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));
                rlTab2.setBackground(getResources().getDrawable(R.drawable.bg_white_rounded_corner));
                rlTab3.setBackground(getResources().getDrawable(R.drawable.bg_w_rounded_corner));

                rlTabText1.setTextColor(Color.BLACK);
                rlTabText2.setTextColor(Color.BLACK);
                rlTabText3.setTextColor(Color.WHITE);

                if (!TextUtils.isEmpty(mQuery)) {
                    setSearchData(0, "false");
                } else {
                    setUIData(0, "false");
                }
                break;
            case R.id.imgRecentScanned:
                onBackPressed();
                break;
        }
    }



}

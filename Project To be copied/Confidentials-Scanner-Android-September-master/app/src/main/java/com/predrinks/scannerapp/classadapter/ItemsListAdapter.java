package com.predrinks.scannerapp.classadapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.predrinks.scannerapp.R;

import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;


public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.MyViewHolder> {

    private List<Items> pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout isRedeem;
        TextView tvTitle;
        TextView tvName;
        TextView tvNumber;
        TextView tvTime;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvNumber = (TextView) view.findViewById(R.id.tvNumber);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            isRedeem = (RelativeLayout) view.findViewById(R.id.isRedeem);

            tvTitle.setSelected(true);
            tvTitle.setFocusable(true);
        }
    }


    public ItemsListAdapter(Context context, List<Items> moviesList) {
        this.mContext = context;
        this.pogoList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Items pogo = pogoList.get(position);
        holder.tvTitle.setText(pogo.getOfferTitle());
        holder.tvName.setText(pogo.getUserName());
        holder.tvNumber.setText(pogo.getqRCode());
        holder.tvTime.setVisibility(View.VISIBLE);
        holder.tvTime.setSelected(true);
        holder.tvTime.setFocusable(true);

        holder.tvNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label", pogo.getqRCode());
                    clipboard.setPrimaryClip(clip);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (!pogo.getIsRedeemed().equals("true")) {
            holder.isRedeem.setBackgroundColor(Color.RED);
            String eventTime = pogo.getDateTime();

            if (pogo.getIsEventDateDisabled().equals("true")) {
                holder.tvTime.setVisibility(View.GONE);
            } else {
                holder.tvTime.setVisibility(View.VISIBLE);
                eventTime = eventTime.replace("@", " ");
                holder.tvTime.setText(eventTime);
            }
        } else {
            holder.isRedeem.setBackgroundColor(Color.GREEN);
            holder.tvTime.setText(pogo.getRedeemedOn());
        }

    }

    @Override
    public int getItemCount() {
        return pogoList.size();
    }


}

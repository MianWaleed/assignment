package com.predrinks.scannerapp.activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.predrinks.scannerapp.classadapter.Items;
import com.predrinks.scannerapp.classadapter.ItemsRecentlyListAdapter;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.utils.SimpleDividerItemDecoration;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;

import java.util.List;

public class RecentlyRedeemedActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    LinearLayout lnrEmpty;
    ImageView imgCancel, imgBack;


    ConfidentialsOrderDAO confidentialsOrderDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recently_redeemed);

        confidentialsOrderDAO = new ConfidentialsOrderDAO(this);


        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        lnrEmpty = (LinearLayout) findViewById(R.id.lnrEmpty);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setVerticalScrollBarEnabled(false);

        imgBack.setOnClickListener(this);
        imgCancel.setOnClickListener(this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setUIData();
            }
        }, 1);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgCancel:
                onBackPressed();
                break;
            case R.id.imgBack:
                onBackPressed();
                break;

        }
    }

    private void setUIData() {
        ItemsRecentlyListAdapter adapter;
        confidentialsOrderDAO.open();
        List<Items> pogoList = confidentialsOrderDAO.getRedeemedList();
        confidentialsOrderDAO.close();
        if (pogoList != null && pogoList.size() > 0) {
            adapter = new ItemsRecentlyListAdapter(this, pogoList);
            recyclerView.setAdapter(adapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

            lnrEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            lnrEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

}

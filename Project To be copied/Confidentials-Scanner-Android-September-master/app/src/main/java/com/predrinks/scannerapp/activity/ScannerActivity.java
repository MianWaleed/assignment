package com.predrinks.scannerapp.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.scannerapp.BaseFragmentActivity;
import com.predrinks.scannerapp.Controller;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.fragment.ScannerFragment;
import com.predrinks.scannerapp.network.CustomRequest;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.Constant;
import com.predrinks.scannerapp.utils.MyPref;
import com.predrinks.scannerapp.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission;

public class ScannerActivity extends BaseFragmentActivity implements View.OnClickListener {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    private static final String TAG = "ScannerActivity";
    public static FragmentManager manager;
    public static ImageView back_btn;
    public static ImageView imgRecentScanned;
    public static LinearLayout llHeaderRight, llRecentlyRedeemed;
    public static TextView tvHeaderRight;
    String loginResponse = "";
    String venueId = "";
    String ScannedByTeamMemberID = "";
    ConfidentialsOrderDAO confidentialsOrderDAO;
    TextView versionCode;
    public static RelativeLayout layout_bottom;
    LinearLayout llSynchOffers;

    ProgressDialog progressDialog;
    ImageButton flashIb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        if (!isLoggedIn()) {
            Intent intent = new Intent(ScannerActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        confidentialsOrderDAO = new ConfidentialsOrderDAO(ScannerActivity.this);
        manager = getSupportFragmentManager();
        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);

        if (permissionCheck == 0) {
            ScannerFragment fragment = new ScannerFragment();
            loadContentFragment(fragment, (Bundle) null);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
        }

        loginResponse = getLoadedData("loginResponse");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(loginResponse);
            venueId = jsonObject.getJSONObject("teammember").getString("venueID");
            ScannedByTeamMemberID = jsonObject.getJSONObject("teammember").getString("teamMemberID");
            MyPref.setTeamMemberId(this, ScannedByTeamMemberID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        llSynchOffers = findViewById(R.id.llSynchOffers);
        llSynchOffers.setOnClickListener(this);
        versionCode = findViewById(R.id.versionCode);
        back_btn = findViewById(R.id.imgBack);
        imgRecentScanned = findViewById(R.id.imgRecentScanned);
        back_btn.setOnClickListener(backClick);
        llHeaderRight = findViewById(R.id.llHeaderRight);
        llRecentlyRedeemed = findViewById(R.id.llRecentlyRedeemed);
        layout_bottom = findViewById(R.id.layout_bottom);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Syncing offers please wait");
        progressDialog.setCancelable(true);
        tvHeaderRight = findViewById(R.id.tvHeaderRight);

        imgRecentScanned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScannerActivity.this, EventListActivity.class);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                intent.putExtra("venueId", venueId);
                startActivity(intent);
            }
        });
        llRecentlyRedeemed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScannerActivity.this, RecentlyRedeemedActivity.class);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                startActivity(intent);
            }
        });
        versionCode.setText("V" + Controller.APP_VERSION_NAME);
//        getWebAppVersion();

        every15Min();
    }

    private void every15Min() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.show();
            }
        });

        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            updateServer();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 900000);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        updateToServer();
    }

    Camera mCamera;

    public void flash() {
        if (isFlash) {
            turnFlashlightOff();
            isFlash = false;
            flashIb.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));
        } else {
            turnFlashlightOn();
            isFlash = true;
            flashIb.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off));
        }
    }

    boolean isFlash = false;
    CameraManager camManager;

    private void turnFlashlightOff() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                String cameraId;
                camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                if (camManager != null) {
                    cameraId = camManager.getCameraIdList()[0];
                    camManager.setTorchMode(cameraId, false);
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        } else {
            Camera mCamera = Camera.open();
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(parameters);
            mCamera.stopPreview();
        }
    }

    private void turnFlashlightOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                String cameraId = null;
                if (camManager != null) {
                    cameraId = camManager.getCameraIdList()[0];
                    camManager.setTorchMode(cameraId, true);
                }
            } catch (CameraAccessException e) {
                Log.e(TAG, e.toString());
            }
        } else {
            Camera mCamera = Camera.open();
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            mCamera.setParameters(parameters);
            mCamera.startPreview();
        }
    }

    public static void loadContentFragment(Fragment fragment, Bundle... bundles) {
        String backStateName = fragment.getClass().getName();
        if (bundles != null && bundles.length > 0) {
            fragment.setArguments(bundles[0]);
        }
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.content_frame, fragment, backStateName);
        ft.commitAllowingStateLoss();
    }

    public static void loadContentFragment(Fragment fragment, boolean flag, Bundle... bundles) {

        String backStateName = fragment.getClass().getName();
        if (bundles != null && bundles.length > 0) {
            // If any data is present
            fragment.setArguments(bundles[0]);
        }
        FragmentTransaction ft = manager.beginTransaction();
        if (flag) {
            ft.add(R.id.content_frame, fragment);
        } else {
            ft.replace(R.id.content_frame, fragment, backStateName);
            ft.addToBackStack(backStateName);
        }
        ft.commitAllowingStateLoss();
    }


    public static Fragment getVisibleFragment() {
        Fragment f = manager.findFragmentById(R.id.content_frame);
        return f;
    }

    public void manageBack() {
        if (manager.getBackStackEntryCount() > 0) {
            String fragmentClassName = getVisibleFragment().getClass().getName();
            fragmentClassName = fragmentClassName.substring(fragmentClassName.lastIndexOf(".") + 1);
            switch (fragmentClassName) {
                default:
                    try {
                        manager.popBackStackImmediate();
                    } catch (IllegalStateException ignored) {
                        // There's no way to avoid getting this if saveInstanceState has already been called.
                        ignored.printStackTrace();
                    }
            }
        } else
            finish();
    }


    public static void showMoreMenu() {
//        ScannerActivity.back_btn.setVisibility(View.VISIBLE);
        ScannerActivity.llHeaderRight.setVisibility(View.VISIBLE);
        ScannerActivity.tvHeaderRight.setVisibility(View.VISIBLE);
    }

    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            manageBack();
        }
    };


    public static void setBackVisible(boolean isBack) {
        if (isBack) {
//            ScannerActivity.back_btn.setVisibility(View.VISIBLE);
        } else {
            ScannerActivity.back_btn.setVisibility(View.GONE);
        }
    }

    public static void showHeaderRight(String tittle) {
        ScannerActivity.llHeaderRight.setVisibility(View.VISIBLE);
        ScannerActivity.tvHeaderRight.setVisibility(View.VISIBLE);
        ScannerActivity.tvHeaderRight.setText(tittle);
    }

    public static void setHeaderRightClick(View.OnClickListener onClickListener) {
        ScannerActivity.llHeaderRight.setOnClickListener(onClickListener);
    }

    public static void setBackClick(View.OnClickListener onClickListener) {
        ScannerActivity.back_btn.setOnClickListener(onClickListener);
    }

    @Override
    public void onBackPressed() {
        manageBack();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    ScannerFragment fragment = new ScannerFragment();
                    loadContentFragment(fragment, (Bundle) null);
                } else {
                    finish();
                }
                return;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llSynchOffers: {
                updateServer();
                break;
            }
        }
    }

    private void updateServer() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.show();
            }
        });

        String venueID = "";
        try {
            JSONObject loginResponse = new JSONObject(getLoadedData("loginResponse"));
            JSONObject memberObject = loginResponse.getJSONObject("teammember");
            venueID = memberObject.getString("venueID");
            ScannedByTeamMemberID = memberObject.getString("teamMemberID");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        confidentialsOrderDAO = new ConfidentialsOrderDAO(this);
        if (haveNetworkConnection(this)) {
            confidentialsOrderDAO.open();
            if (confidentialsOrderDAO.isSyncedData("true")) {
                final JSONArray jsonArray = confidentialsOrderDAO.getUnSyncedData("true", ScannedByTeamMemberID);
                if (jsonArray != null && jsonArray.length() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            postDataToServer1(jsonArray);
                        }
                    }, 100);
                }
            } else {
                getDataFromWebOffers();
            }
            confidentialsOrderDAO.close();
        }

//        getDataFromWebOffers();
    }

    private void postDataToServer1(JSONArray jsonArray) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("RedeemedOffers", jsonArray);
            String URL = URL_Utils.REDEEM_OFFER;

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            try {
                                String success = response.getString("status");
                                if (success.equals("1")) {
                                    confidentialsOrderDAO.open();
                                    confidentialsOrderDAO.upDateIsSynced("true");
                                    confidentialsOrderDAO.close();
                                    getDataFromWebOffers();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse response = error.networkResponse;
                    VolleyLog.d(TAG, "Error: " + response);

                    try {
                        byte[] bytes = response.data;
                        String str = new String(bytes, "UTF-8");
                        Log.i("str", str);
                    } catch (Exception e) {

                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
        } catch (Exception e) {

        }
    }

    private void getDataFromWebOffers() {
        if (haveNetworkConnection(this)) {
            String venueID = "";
            try {
                JSONObject loginResponse = new JSONObject(getLoadedData("loginResponse"));
                JSONObject memberObject = loginResponse.getJSONObject("teammember");
                venueID = memberObject.getString("venueID");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (MyPref.getIsSynced(this)) {
                Log.d(TAG, "getDataFromWebOffers: new " + URL_Utils.VENUE + "/" + venueID + "/purchasedoffers/new?dateUpdated=" + confidentialsOrderDAO.getLastId());
                CustomRequest jsObjRequest = new CustomRequest(URL_Utils.VENUE + "/" + venueID + "/purchasedoffers/new?dateUpdated=" + confidentialsOrderDAO.getLastId(), null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("1")) {
                                String success = response.getString("message");
                                if (success.equals("success")) {
                                    final JSONArray resultArray = response.getJSONArray("result");

                                    if (resultArray != null && resultArray.length() > 0) {
//                                        if (!ScannerFragment.isStartedRedeem) {
                                        new InsertionAsync().execute(resultArray);
//                                        }
                                    } else {
                                        progressDialog.cancel();
                                        dialog_info(ScannerActivity.this, Constant.MESSAGE_TITLE, "No vouchers available");
                                    }
                                }
                            } else {
                                progressDialog.cancel();
                                Toast.makeText(ScannerActivity.this, "No new vouchers available", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
            } else {
                String URL = URL_Utils.VENUE + "/" + venueID + "/purchasedoffers";
                Log.d(TAG, "getDataFromWebOffers: new " + URL);
                CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
            }
        }
    }

    Response.ErrorListener ErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, "getDataFromWebOffers onErrorResponse: " + error);
            Log.d(TAG, error.toString());

        }
    };

    Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, "getDataFromWebOffers: onREs " + response);
            try {
                String success = response.getString("message");
                if (success.equals("success")) {
                    final JSONArray resultArray = response.getJSONArray("result");

                    if (resultArray != null && resultArray.length() > 0) {
                        if (!ScannerFragment.isStartedRedeem) {
                            new InsertionAsync().execute(resultArray);
                        }
                    } else {
                        progressDialog.cancel();
                        dialog_info(ScannerActivity.this, Constant.MESSAGE_TITLE, "No vouchers available");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private class InsertionAsync extends AsyncTask<JSONArray, Void, Boolean> {
        String mTAG = "myAsyncTask";

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(JSONArray... arg) {

            try {
                JSONArray resultArray = arg[0];

                int size = resultArray.length();
                if (size > 0) {
                    confidentialsOrderDAO.open();
                    if (!confidentialsOrderDAO.isSyncedData("true")) {
                        for (int i = 0; i < size; i++) {
                            int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                            String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "", isRedeemed = "", isExpired = "", userName = "", eventTime = "", redeemedOn = "", offerExpiryMode = "", expiresOn = "", expiryTime = "", isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";
                            JSONObject object = resultArray.getJSONObject(i);
                            myOfferID = object.getInt(ConfidentialsOrderDAO.KEY_MY_OFFER_ID);
                            eventID = object.getInt(ConfidentialsOrderDAO.KEY_EVENT_ID);
                            offerID = object.getInt(ConfidentialsOrderDAO.KEY_OFFER_ID);
                            userID = object.getInt(ConfidentialsOrderDAO.KEY_USER_ID);
                            quantity = object.getInt(ConfidentialsOrderDAO.KEY_QUANTITY);
                            offerTitle = object.getString(ConfidentialsOrderDAO.KEY_OFFER_TITLE);
                            eventName = object.getString(ConfidentialsOrderDAO.KEY_EVENT_NAME);
                            venueTitle = object.getString(ConfidentialsOrderDAO.KEY_VENUE_TITLE);
                            qRCode = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE);
                            qRCodeImage = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE_IMAGE);
                            isRedeemed = object.getString(ConfidentialsOrderDAO.KEY_IS_REDEEMED);
                            isExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_EXPIRED);
                            userName = object.getString(ConfidentialsOrderDAO.KEY_USER_NAME);
                            eventTime = object.getString(ConfidentialsOrderDAO.KEY_EVENT_TIME);
                            redeemedOn = object.getString(ConfidentialsOrderDAO.KEY_REDEEMED_ON);
                            offerExpiryMode = object.getString(ConfidentialsOrderDAO.KEY_OFFER_EXPIRY_MODE);
                            expiresOn = object.getString(ConfidentialsOrderDAO.KEY_EXPIRES_ON);
                            expiryTime = object.getString(ConfidentialsOrderDAO.KEY_EXPIRY_TIME);
                            isOfferExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_EXPIRED);
                            isOfferActive = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_ACTIVE);
                            isEventDateDisabled = object.getString(ConfidentialsOrderDAO.KEY_IS_EVENT_DATE_DISABLED);
                            dateUpdated = object.getString(ConfidentialsOrderDAO.KEY_DATE_UPDATED);

                            if (confidentialsOrderDAO.getTotalOffersCount() == 0) {
                                confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            } else {
                                if (confidentialsOrderDAO.isOffersExist(myOfferID, orderID, offerID, userID)) {
                                    confidentialsOrderDAO.updateOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
//                                    return false;
                                } else {
                                    confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                                }
                            }
                        }
                    }
                    confidentialsOrderDAO.close();

                    return true;
                }
            } catch (JSONException e) {
                Log.e(TAG, "insertUpdateData1: ", e);
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            MyPref.setIsSynced(ScannerActivity.this, true);

            if (s) {
                progressDialog.cancel();
                dialog_info(ScannerActivity.this, Constant.MESSAGE_TITLE, "Offers successfully synced");
            } else {
                progressDialog.cancel();
                Toast.makeText(ScannerActivity.this, "No new vouchers available", Toast.LENGTH_SHORT).show();
//                dialog_info(ScannerActivity.this, Constant.MESSAGE_TITLE, "Error ");
            }
        }

    }


    private void insertUpdateData1(JSONArray resultArray) {
        try {
            int size = resultArray.length();
            if (size > 0) {
                confidentialsOrderDAO.open();
                if (!confidentialsOrderDAO.isSyncedData("true")) {
                    for (int i = 0; i < size; i++) {
                        int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                        String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                                isRedeemed = "", isExpired = "", userName = "", eventTime = "", redeemedOn = "",
                                offerExpiryMode = "", expiresOn = "", expiryTime = "", isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";
                        JSONObject object = resultArray.getJSONObject(i);

                        myOfferID = object.getInt(ConfidentialsOrderDAO.KEY_MY_OFFER_ID);
                        eventID = object.getInt(ConfidentialsOrderDAO.KEY_EVENT_ID);
                        offerID = object.getInt(ConfidentialsOrderDAO.KEY_OFFER_ID);
                        userID = object.getInt(ConfidentialsOrderDAO.KEY_USER_ID);
                        quantity = object.getInt(ConfidentialsOrderDAO.KEY_QUANTITY);
                        offerTitle = object.getString(ConfidentialsOrderDAO.KEY_OFFER_TITLE);
                        eventName = object.getString(ConfidentialsOrderDAO.KEY_EVENT_NAME);
                        venueTitle = object.getString(ConfidentialsOrderDAO.KEY_VENUE_TITLE);
                        qRCode = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE);
                        qRCodeImage = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE_IMAGE);
                        isRedeemed = object.getString(ConfidentialsOrderDAO.KEY_IS_REDEEMED);
                        isExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_EXPIRED);
                        userName = object.getString(ConfidentialsOrderDAO.KEY_USER_NAME);
                        eventTime = object.getString(ConfidentialsOrderDAO.KEY_EVENT_TIME);
                        redeemedOn = object.getString(ConfidentialsOrderDAO.KEY_REDEEMED_ON);
                        offerExpiryMode = object.getString(ConfidentialsOrderDAO.KEY_OFFER_EXPIRY_MODE);
                        expiresOn = object.getString(ConfidentialsOrderDAO.KEY_EXPIRES_ON);
                        expiryTime = object.getString(ConfidentialsOrderDAO.KEY_EXPIRY_TIME);
                        isOfferExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_EXPIRED);
                        isOfferActive = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_ACTIVE);
                        isEventDateDisabled = object.getString(ConfidentialsOrderDAO.KEY_IS_EVENT_DATE_DISABLED);
                        dateUpdated = object.getString(ConfidentialsOrderDAO.KEY_DATE_UPDATED);

                        if (confidentialsOrderDAO.getTotalOffersCount() == 0) {
                            confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                        } else {
                            if (confidentialsOrderDAO.isOffersExist(myOfferID, orderID, offerID, userID)) {
                                confidentialsOrderDAO.updateOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            } else {
                                confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            }
                        }
                    }
                }
                confidentialsOrderDAO.close();
                dialog_info(this, Constant.MESSAGE_TITLE, "Offers successfully synced");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.cancel();

                    }
                });
            }
        } catch (JSONException e) {
            Log.e(TAG, "insertUpdateData1: ", e);
            e.printStackTrace();
        }
    }

    protected void dialog_info(final Activity activity, final String title, final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_info);
                    dialog.setCancelable(false);

                    View.OnClickListener dialogClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    };

                    ((TextView) dialog.findViewById(R.id.title)).setText(title);
                    ((TextView) dialog.findViewById(R.id.message)).setText(message);
                    ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
                    dialog.show();
                }
            }
        });

    }

}

package com.predrinks.scannerapp.classadapter;

public class Items {

    private String userName;
    private String dateTime;

    private int orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
    private String offerTitle = "", venueTitle = "", qRCode = "", qRCodeImage = "",
            isRedeemed = "", isExpired = "", isSynced = "", redeemedOn = "", eventName,
            offerExpiryMode = "", expiresOn = "", expiryTime = ""
            , isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";

    public Items(String userName, String eventTime, int orderID, int eventID, int offerID, int userID, String offerTitle, String eventName, String venueTitle, String qRCode, String qRCodeImage, int quantity, String isRedeemed, String isExpired, String isSynced, String offerExpiryMode, String expiresOn, String expiryTime, String redeemedOn, String isOfferExpired, String isOfferActive, String isEventDateDisabled, String dateUpdated ) {
        this.userName = userName;
        this.dateTime = eventTime;

        this.orderID = orderID;
        this.eventID = eventID;
        this.offerID = offerID;
        this.userID = userID;
        this.quantity = quantity;
        this.offerTitle = offerTitle;
        this.eventName = eventName;
        this.venueTitle = venueTitle;
        this.qRCode = qRCode;
        this.qRCodeImage = qRCodeImage;
        this.isRedeemed = isRedeemed;
        this.isExpired = isExpired;
        this.isSynced = isSynced;
        this.offerExpiryMode = offerExpiryMode;
        this.expiresOn = expiresOn;
        this.expiryTime = expiryTime;
        this.redeemedOn = redeemedOn;
        this.isOfferExpired = isOfferExpired;
        this.isOfferActive = isOfferActive;
        this.isEventDateDisabled = isEventDateDisabled;
        this.dateUpdated = dateUpdated;
    }


    public String getEventName() {
        return eventName;
    }

    public String getOfferExpiryMode() {
        return offerExpiryMode;
    }

    public String getExpiresOn() {
        return expiresOn;
    }

    public String getExpiryTime() {
        return expiryTime;
    }


    public String getRedeemedOn() {
        return redeemedOn;
    }

    public String getUserName() {
        return userName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public int getOrderID() {
        return orderID;
    }

    public int getEventID() {
        return eventID;
    }

    public int getOfferID() {
        return offerID;
    }

    public int getUserID() {
        return userID;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public String getVenueTitle() {
        return venueTitle;
    }

    public String getqRCode() {
        return qRCode;
    }

    public String getqRCodeImage() {
        return qRCodeImage;
    }

    public String getIsRedeemed() {
        return isRedeemed;
    }

    public String getIsExpired() {
        return isExpired;
    }

    public String getIsSynced() {
        return isSynced;
    }

    public String getIsOfferExpired() {
        return isOfferExpired;
    }

    public String getIsOfferActive() {
        return isOfferActive;
    }

    public String getIsEventDateDisabled() {
        return isEventDateDisabled;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public String toString() {
        return "Items{" +
                "userName='" + userName + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", orderID=" + orderID +
                ", eventID=" + eventID +
                ", offerID=" + offerID +
                ", userID=" + userID +
                ", quantity=" + quantity +
                ", offerTitle='" + offerTitle + '\'' +
                ", venueTitle='" + venueTitle + '\'' +
                ", qRCode='" + qRCode + '\'' +
                ", qRCodeImage='" + qRCodeImage + '\'' +
                ", isRedeemed='" + isRedeemed + '\'' +
                ", isExpired='" + isExpired + '\'' +
                ", isSynced='" + isSynced + '\'' +
                ", redeemedOn='" + redeemedOn + '\'' +
                ", eventName='" + eventName + '\'' +
                ", offerExpiryMode='" + offerExpiryMode + '\'' +
                ", expiresOn='" + expiresOn + '\'' +
                ", expiryTime='" + expiryTime + '\'' +
                ", isOfferExpired='" + isOfferExpired + '\'' +
                ", isOfferActive='" + isOfferActive + '\'' +
                ", isEventDateDisabled='" + isEventDateDisabled + '\'' +
                ", dateUpdated='" + dateUpdated + '\'' +
                '}';
    }
}

package com.predrinks.scannerapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by usman
 */
public class MyPref {
    private static final String PREFS_NAME = "com.usmankhawar.sp1";

    public static void setIsSynced(Context context, Boolean isSynced) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putBoolean("isSynced", isSynced).apply();
    }

    public static Boolean getIsSynced(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getBoolean("isSynced", false);
    }

    public static void setId(Context context, String id) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("user", id).apply();
    }

    public static String getId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("user", "");
    }

    public static void setTeamMemberId(Context context, String memberId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("setTeamMemberId", memberId).apply();
    }

    public static String getTeamMemberId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("setTeamMemberId", "");
    }
}

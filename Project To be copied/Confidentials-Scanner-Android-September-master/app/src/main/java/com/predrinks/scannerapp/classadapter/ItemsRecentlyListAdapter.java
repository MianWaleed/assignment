package com.predrinks.scannerapp.classadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.predrinks.scannerapp.R;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class ItemsRecentlyListAdapter extends RecyclerView.Adapter<ItemsRecentlyListAdapter.MyViewHolder> {

    private List<Items> pogoList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout isRedeem;
        TextView tvTitle;
        TextView tvName;
        TextView tvNumber;
        TextView tvTime;
        TextView tvEventName;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvName = view.findViewById(R.id.tvName);
            tvNumber = view.findViewById(R.id.tvNumber);
            tvTime = view.findViewById(R.id.tvTime);
            tvEventName = (TextView) view.findViewById(R.id.tvEventName);
            isRedeem = (RelativeLayout) view.findViewById(R.id.isRedeem);
        }
    }

    public ItemsRecentlyListAdapter(Context context, List<Items> moviesList) {
        this.mContext = context;
        this.pogoList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_recently_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Items pogo = pogoList.get(position);
        holder.tvTitle.setText(pogo.getOfferTitle());
        holder.tvName.setText(pogo.getUserName());
        holder.tvNumber.setText("Voucher code: " + pogo.getqRCode());
        holder.tvEventName.setText(pogo.getEventName());

        if (!pogo.getIsRedeemed().equals("true")) {
            holder.isRedeem.setBackgroundColor(Color.RED);
            holder.tvTime.setText( pogo.getDateTime());
        } else {
            holder.isRedeem.setBackgroundColor(Color.GREEN);
            holder.tvTime.setText(pogo.getRedeemedOn());
        }
    }

    @Override
    public int getItemCount() {
        return pogoList.size();
    }

}

package com.predrinks.scannerapp.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.scannerapp.BaseFragmentActivity;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.classadapter.OffersAdapter;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.fragment.ScannerFragment;
import com.predrinks.scannerapp.network.CustomRequest;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.Constant;
import com.predrinks.scannerapp.utils.URL_Utils;
import com.predrinks.scannerapp.widget.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventListActivity extends BaseFragmentActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "HomeFragment";

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.lnrEmpty)
    LinearLayout lnrEmpty;
    @Bind(R.id.llSynchOffers)
    LinearLayout llSynchOffers;
    @Bind(R.id.lnrTop)
    LinearLayout lnrTop;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.imgBack)
    ImageView imgBack;
    @Bind(R.id.tvTitleHeader)
    TextView tvTitleHeader;
    @Bind(R.id.tvSynchOffers)
    TextView tvSynchOffers;
    @Bind(R.id.imgCancel)
    ImageView imgCancel;

    private String mParam1;
    private String mParam2;

    Activity mActivity;
    Context mContext;
    private ConfidentialsOrderDAO confidentialsOrderDAO;

    String successResponse = "", venueId = "";

    @Bind(R.id.ib_log_out)
    ImageView ibLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        ButterKnife.bind(this);

        mActivity = this;
        mContext = this;

        Intent intent = getIntent();
        venueId = intent.getExtras().getString("venueId");

        recyclerView.setVerticalScrollBarEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (haveNetworkConnection(this))
            getDataFromWeb();
    }

    private void getDataFromWeb() {
        swipeRefreshLayout.setRefreshing(true);
        String URL = URL_Utils.VENUE + "/" + venueId + "/event/list_new";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        successResponse = getLoadedData("responseEventList");
        if (!TextUtils.isEmpty(successResponse))
            setAdapterEvents(successResponse);
//        updateToServer();
    }


    JSONArray jsonArray = null;

    private void setAdapterEvents(String response) {
        try {
            JSONObject object = new JSONObject(response);
            jsonArray = object.getJSONArray("result");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonArray = new JSONArray();
        }

        if (jsonArray == null || jsonArray.length() == 0) {
            lnrEmpty.setVisibility(View.VISIBLE);
            lnrTop.setVisibility(View.GONE);
            return;
        } else {
            lnrEmpty.setVisibility(View.GONE);
            lnrTop.setVisibility(View.VISIBLE);
        }
        OffersAdapter clubListAdapter = new OffersAdapter(mContext, jsonArray);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setAdapter(clubListAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mActivity, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                JSONObject object;
                String eventId = "", eventName = "";
                try {
                    object = jsonArray.getJSONObject(position);
                    eventId = object.getString("eventId");
                    eventName = object.getString("title");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(mActivity, RecentScannedActivity.class);
                intent.putExtra("eventId", eventId);
                intent.putExtra("eventName", eventName);
                startActivity(intent);
            }
        }));
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
            try {
                String success = response.getString("success");
                if (success.equals("1")) {
                    successResponse = response.toString();
                    saveLoadedData("responseEventList", successResponse);
                    setAdapterEvents(response.toString());
                    updateToServer();
                } else {
                    String message = response.getString("message");
                    saveLoadedData("responseEventList", "");
                    setAdapterEvents("");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onRefresh() {
        getDataFromWeb();
    }

    @OnClick({R.id.llSynchOffers, R.id.imgBack})
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.llSynchOffers:
//                updateServer();
//                break;
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    @OnClick(R.id.ib_log_out)
    public void logOut() {
        final Dialog dialog = new Dialog(mActivity, R.style.Dialog_No_Border);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater m_inflater = LayoutInflater.from(mActivity);
        View m_view = m_inflater.inflate(R.layout.dialog_use_code, null);

        EditText m_etCode = (EditText) m_view.findViewById(R.id.etCode);
        m_etCode.setVisibility(View.GONE);

        TextView m_tvTitle = (TextView) m_view.findViewById(R.id.tvTitle);
        Button m_btnOk = (Button) m_view.findViewById(R.id.btnRedeem);
        m_tvTitle.setText("Are you sure you\nwant to log out?");
        m_btnOk.setText("Log Out");
        Button m_btnCancel = (Button) m_view.findViewById(R.id.btnCancel);
        View.OnClickListener m_clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLoggedIn(false);
                saveLoadedData("responseEventList", "");
                Intent intent = new Intent(mActivity, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finishAffinity();

                startActivity(intent);
                mActivity.deleteDatabase(ConfidentialsOrderDAO.DATABASE_NAME);
                mActivity.finish();
            }
        };
        m_btnOk.setOnClickListener(m_clickListener);
        m_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(m_view);
        dialog.show();
    }

    private String ScannedByTeamMemberID = "";

    private void updateServer() {
        String venueID = "";
        try {
            JSONObject loginResponse = new JSONObject(getLoadedData("loginResponse"));
            JSONObject memberObject = loginResponse.getJSONObject("teammember");
            venueID = memberObject.getString("venueID");
            ScannedByTeamMemberID = memberObject.getString("teamMemberID");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        confidentialsOrderDAO = new ConfidentialsOrderDAO(this);
        if (haveNetworkConnection(this)) {
            confidentialsOrderDAO.open();
            if (confidentialsOrderDAO.isSyncedData("true")) {
                final JSONArray jsonArray = confidentialsOrderDAO.getUnSyncedData("true", ScannedByTeamMemberID);
                if (jsonArray != null && jsonArray.length() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            postDataToServer1(jsonArray);
                        }
                    }, 100);
                }
            } else {
                getDataFromWebOffers();
            }
            confidentialsOrderDAO.close();
        }
    }

    private void getDataFromWebOffers() {
        if (haveNetworkConnection(this)) {
            String venueID = "";
            try {
                JSONObject loginResponse = new JSONObject(getLoadedData("loginResponse"));
                JSONObject memberObject = loginResponse.getJSONObject("teammember");
                venueID = memberObject.getString("venueID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String URL = URL_Utils.VENUE + "/" + venueID + "/purchasedoffers";
            CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
        }
    }

    Response.ErrorListener ErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());

        }
    };

    Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());

            try {
                String success = response.getString("message");
                if (success.equals("success")) {
                    final JSONArray resultArray = response.getJSONArray("result");

                    if (resultArray != null && resultArray.length() > 0) {
                        if (!ScannerFragment.isStartedRedeem) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    insertUpdateData1(resultArray);
                                }
                            }, 100);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    private void postDataToServer1(JSONArray jsonArray) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("RedeemedOffers", jsonArray);
            String URL = URL_Utils.REDEEM_OFFER;

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            try {
                                String success = response.getString("success");
                                if (success.equals("1")) {
                                    confidentialsOrderDAO.open();
                                    confidentialsOrderDAO.upDateIsSynced("true");
                                    confidentialsOrderDAO.close();
                                    getDataFromWebOffers();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse response = error.networkResponse;
                    VolleyLog.d(TAG, "Error: " + response);

                    try {
                        byte[] bytes = response.data;
                        String str = new String(bytes, "UTF-8");
                        Log.i("str", str);
                    } catch (Exception e) {

                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
        } catch (Exception e) {

        }
    }

    private void insertUpdateData1(JSONArray resultArray) {
        try {
            int size = resultArray.length();
            if (size > 0) {
                confidentialsOrderDAO.open();
                if (!confidentialsOrderDAO.isSyncedData("true")) {
                    for (int i = 0; i < size; i++) {
                        int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                        String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                                isRedeemed = "", isExpired = "", userName = "", eventTime = "", redeemedOn = "",
                                offerExpiryMode = "", expiresOn = "", expiryTime = "", isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";
                        JSONObject object = resultArray.getJSONObject(i);

                        myOfferID = object.getInt(ConfidentialsOrderDAO.KEY_MY_OFFER_ID);
                        eventID = object.getInt(ConfidentialsOrderDAO.KEY_EVENT_ID);
                        offerID = object.getInt(ConfidentialsOrderDAO.KEY_OFFER_ID);
                        userID = object.getInt(ConfidentialsOrderDAO.KEY_USER_ID);
                        quantity = object.getInt(ConfidentialsOrderDAO.KEY_QUANTITY);
                        offerTitle = object.getString(ConfidentialsOrderDAO.KEY_OFFER_TITLE);
                        eventName = object.getString(ConfidentialsOrderDAO.KEY_EVENT_NAME);
                        venueTitle = object.getString(ConfidentialsOrderDAO.KEY_VENUE_TITLE);
                        qRCode = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE);
                        qRCodeImage = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE_IMAGE);
                        isRedeemed = object.getString(ConfidentialsOrderDAO.KEY_IS_REDEEMED);
                        isExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_EXPIRED);
                        userName = object.getString(ConfidentialsOrderDAO.KEY_USER_NAME);
                        eventTime = object.getString(ConfidentialsOrderDAO.KEY_EVENT_TIME);
                        redeemedOn = object.getString(ConfidentialsOrderDAO.KEY_REDEEMED_ON);
                        offerExpiryMode = object.getString(ConfidentialsOrderDAO.KEY_OFFER_EXPIRY_MODE);
                        expiresOn = object.getString(ConfidentialsOrderDAO.KEY_EXPIRES_ON);
                        expiryTime = object.getString(ConfidentialsOrderDAO.KEY_EXPIRY_TIME);
                        isOfferExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_EXPIRED);
                        isOfferActive = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_ACTIVE);
                        isEventDateDisabled = object.getString(ConfidentialsOrderDAO.KEY_IS_EVENT_DATE_DISABLED);
                        dateUpdated = object.getString(ConfidentialsOrderDAO.KEY_DATE_UPDATED);

                        if (confidentialsOrderDAO.getTotalOffersCount() == 0) {
                            confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                        } else {
                            if (confidentialsOrderDAO.isOffersExist(myOfferID, orderID, offerID, userID)) {
                                confidentialsOrderDAO.updateOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            } else {
                                confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                            }
                        }
                    }
                }
                confidentialsOrderDAO.close();
                dialog_info(mActivity, Constant.MESSAGE_TITLE, "Offers successfully synced");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void dialog_info(final Activity activity, final String title, final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_info);
                    dialog.setCancelable(false);

                    View.OnClickListener dialogClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    };

                    ((TextView) dialog.findViewById(R.id.title)).setText(title);
                    ((TextView) dialog.findViewById(R.id.message)).setText(message);
                    ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);
                    dialog.show();
                }
            }
        });

    }

}

package com.predrinks.scannerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.predrinks.scannerapp.activity.ScannerActivity;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.fragment.ScannerFragment;
import com.predrinks.scannerapp.network.CustomRequest;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.Constant;
import com.predrinks.scannerapp.utils.MyPref;
import com.predrinks.scannerapp.utils.URL_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.predrinks.scannerapp.ConfidentialsScanner.NAME_PREFERENCE;

public class BaseFragmentActivity extends AppCompatActivity {

    private static final String TAG = "BaseFragmentActivity";
    private SharedPreferences mPrefs;
   private ConfidentialsOrderDAO confidentialsOrderDAO;


    protected void saveLoadedData(String TAG, String data) {
        mPrefs =  BaseFragmentActivity.this.getSharedPreferences(NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(TAG, data);
        editor.apply();
    }

    protected String getLoadedData(String TAG) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(NAME_PREFERENCE, 0);
        String data = mPrefs.getString(TAG, "");
        return data;
    }

    protected boolean isLoggedIn() {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(NAME_PREFERENCE, 0);
        boolean isLoggedIn = mPrefs.getBoolean("isLoggedIn", false);
        return isLoggedIn;
    }
    protected void setLoggedIn(boolean isLogin) {
        mPrefs = BaseFragmentActivity.this.getSharedPreferences(NAME_PREFERENCE, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean("isLoggedIn",isLogin);
        editor.apply();
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private String ScannedByTeamMemberID = "";
    protected void updateToServer() {
        Log.d(TAG, "updateToServer: ");
        String venueID = "";
        try {
            JSONObject loginResponse = new JSONObject(getLoadedData("loginResponse"));
            JSONObject memberObject = loginResponse.getJSONObject("teammember");
            venueID = memberObject.getString("venueID");
            ScannedByTeamMemberID = memberObject.getString("teamMemberID");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        confidentialsOrderDAO = new ConfidentialsOrderDAO(this);
        if (haveNetworkConnection(this)) {
            confidentialsOrderDAO.open();
            if (confidentialsOrderDAO.isSyncedData("true")) {
                final JSONArray jsonArray = confidentialsOrderDAO.getUnSyncedData("true", ScannedByTeamMemberID);
                if (jsonArray != null && jsonArray.length() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            postDataToServer(jsonArray);
                        }
                    }, 100);
                }
            } else {
                getDataFromWeb();
            }
            confidentialsOrderDAO.close();
        }
    }

    private void getDataFromWeb() {
        //String venueId = "";
        Log.d(TAG, "getDataFromWeb: ");
        if (haveNetworkConnection(this)) {
            String venueID = "";
            try {
                JSONObject loginResponse = new JSONObject(getLoadedData("loginResponse"));
                JSONObject memberObject = loginResponse.getJSONObject("teammember");
                venueID = memberObject.getString("venueID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String URL = URL_Utils.VENUE + "/" + venueID + "/purchasedoffers";
            CustomRequest jsObjRequest = new CustomRequest(URL, null, SuccessListener, ErrorListener);
            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
        }
    }

    Response.ErrorListener ErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(TAG, error.toString());

        }
    };

    Response.Listener<JSONObject> SuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());

            try {
                String success = response.getString("message");
                if (success.equals("success")) {
                    final JSONArray resultArray = response.getJSONArray("result");

                    if (resultArray != null && resultArray.length() > 0) {
                        if (!ScannerFragment.isStartedRedeem) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    insertUpdateData(resultArray);
                                    new InsertionAsync().execute(resultArray);
                                }
                            }, 100);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private class InsertionAsync extends AsyncTask<JSONArray, Void, Boolean> {
        String mTAG = "myAsyncTask";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(JSONArray... arg) {

            try {
                JSONArray resultArray = arg[0];

                int size = resultArray.length();
                if (size > 0) {
                    confidentialsOrderDAO.open();
                    if (!confidentialsOrderDAO.isSyncedData("true")) {
                        for (int i = 0; i < size; i++) {
                            int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                            String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "", isRedeemed = "", isExpired = "", userName = "", eventTime = "", redeemedOn = "", offerExpiryMode = "", expiresOn = "", expiryTime = "", isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";
                            JSONObject object = resultArray.getJSONObject(i);
                            myOfferID = object.getInt(ConfidentialsOrderDAO.KEY_MY_OFFER_ID);
                            eventID = object.getInt(ConfidentialsOrderDAO.KEY_EVENT_ID);
                            offerID = object.getInt(ConfidentialsOrderDAO.KEY_OFFER_ID);
                            userID = object.getInt(ConfidentialsOrderDAO.KEY_USER_ID);
                            quantity = object.getInt(ConfidentialsOrderDAO.KEY_QUANTITY);
                            offerTitle = object.getString(ConfidentialsOrderDAO.KEY_OFFER_TITLE);
                            eventName = object.getString(ConfidentialsOrderDAO.KEY_EVENT_NAME);
                            venueTitle = object.getString(ConfidentialsOrderDAO.KEY_VENUE_TITLE);
                            qRCode = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE);
                            qRCodeImage = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE_IMAGE);
                            isRedeemed = object.getString(ConfidentialsOrderDAO.KEY_IS_REDEEMED);
                            isExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_EXPIRED);
                            userName = object.getString(ConfidentialsOrderDAO.KEY_USER_NAME);
                            eventTime = object.getString(ConfidentialsOrderDAO.KEY_EVENT_TIME);
                            redeemedOn = object.getString(ConfidentialsOrderDAO.KEY_REDEEMED_ON);
                            offerExpiryMode = object.getString(ConfidentialsOrderDAO.KEY_OFFER_EXPIRY_MODE);
                            expiresOn = object.getString(ConfidentialsOrderDAO.KEY_EXPIRES_ON);
                            expiryTime = object.getString(ConfidentialsOrderDAO.KEY_EXPIRY_TIME);
                            isOfferExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_EXPIRED);
                            isOfferActive = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_ACTIVE);
                            isEventDateDisabled = object.getString(ConfidentialsOrderDAO.KEY_IS_EVENT_DATE_DISABLED);
                            dateUpdated = object.getString(ConfidentialsOrderDAO.KEY_DATE_UPDATED);

                            if (confidentialsOrderDAO.getTotalOffersCount() == 0) {
                                confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated );
                            } else {
                                if (confidentialsOrderDAO.isOffersExist(myOfferID, orderID, offerID, userID)) {
                                    confidentialsOrderDAO.updateOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated );
                                    return false;
                                } else {
                                    confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated );
                                }
                            }
                        }
                    }
                    confidentialsOrderDAO.close();

                    return true;
                }
            } catch (JSONException e) {
                Log.e(TAG, "insertUpdateData1: ", e);
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            MyPref.setIsSynced(getApplicationContext(), true);

        }
    }


    private void postDataToServer(JSONArray jsonArray) {
        Log.d(TAG, "postDataToServer: ");
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("RedeemedOffers", jsonArray);
            String URL = URL_Utils.REDEEM_OFFER;

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            try {
                                String success = response.getString("success");
                                if (success.equals("1")) {
                                    confidentialsOrderDAO.open();
                                    confidentialsOrderDAO.upDateIsSynced("true");
                                    confidentialsOrderDAO.close();
                                    getDataFromWeb();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse response = error.networkResponse;
                    VolleyLog.d(TAG, "Error: " + response);

                    try {
                        byte[] bytes = response.data;
                        String str = new String(bytes, "UTF-8");
                        Log.i("str", str);
                    } catch (Exception e) {

                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
        } catch (Exception e) {

        }
    }

    private void insertUpdateData(JSONArray resultArray) {
        Log.d(TAG, "insertUpdateData: ");
        try {
            int size = resultArray.length();
            if (size > 0) {
                confidentialsOrderDAO.open();
                if (!confidentialsOrderDAO.isSyncedData("true")) {
                    for (int i = 0; i < size; i++) {
                        int myOfferID = 0,orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                        String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                                isRedeemed = "", isExpired = "", userName = "", eventTime = "", redeemedOn = "",
                                offerExpiryMode = "", expiresOn = "", expiryTime = ""
                                , isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated ="";
                        JSONObject object = resultArray.getJSONObject(i);

                        myOfferID = object.getInt(ConfidentialsOrderDAO.KEY_MY_OFFER_ID);
                        //   orderID = object.getInt(ConfidentialsOrderDAO.KEY_ORDER_ID);
                        eventID = object.getInt(ConfidentialsOrderDAO.KEY_EVENT_ID);
                        offerID = object.getInt(ConfidentialsOrderDAO.KEY_OFFER_ID);
                        userID = object.getInt(ConfidentialsOrderDAO.KEY_USER_ID);
                        quantity = object.getInt(ConfidentialsOrderDAO.KEY_QUANTITY);
                        offerTitle = object.getString(ConfidentialsOrderDAO.KEY_OFFER_TITLE);
                        eventName = object.getString(ConfidentialsOrderDAO.KEY_EVENT_NAME);
                        venueTitle = object.getString(ConfidentialsOrderDAO.KEY_VENUE_TITLE);
                        qRCode = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE);
                        qRCodeImage = object.getString(ConfidentialsOrderDAO.KEY_QR_CODE_IMAGE);
                        isRedeemed = object.getString(ConfidentialsOrderDAO.KEY_IS_REDEEMED);
                        isExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_EXPIRED);
                        userName = object.getString(ConfidentialsOrderDAO.KEY_USER_NAME);
                        eventTime = object.getString(ConfidentialsOrderDAO.KEY_EVENT_TIME);
                        redeemedOn = object.getString(ConfidentialsOrderDAO.KEY_REDEEMED_ON);
                        offerExpiryMode = object.getString(ConfidentialsOrderDAO.KEY_OFFER_EXPIRY_MODE);
                        expiresOn = object.getString(ConfidentialsOrderDAO.KEY_EXPIRES_ON);
                        expiryTime = object.getString(ConfidentialsOrderDAO.KEY_EXPIRY_TIME);
                        isOfferExpired = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_EXPIRED);
                        isOfferActive = object.getString(ConfidentialsOrderDAO.KEY_IS_OFFER_ACTIVE);
                        isEventDateDisabled = object.getString(ConfidentialsOrderDAO.KEY_IS_EVENT_DATE_DISABLED);
                        dateUpdated = object.getString(ConfidentialsOrderDAO.KEY_DATE_UPDATED);

                        if (confidentialsOrderDAO.getTotalOffersCount() == 0) {
                            confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated );
                        } else {
                            if (confidentialsOrderDAO.isOffersExist(myOfferID, orderID, offerID, userID)) {
                                confidentialsOrderDAO.updateOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated );
                            } else {
                                confidentialsOrderDAO.insertOffers(myOfferID, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, userName, eventTime, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated );
                            }
                        }
                    }
                }
                confidentialsOrderDAO.close();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void getWebAppVersion() {
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        };

        Response.Listener<JSONObject> successListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    String status = response.getString("success");
                    if (status.equals("1")) {
                        String storeAppVersion = response.getString("storeAppVersion");
                        saveLoadedData("WebAppVersion", storeAppVersion);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String URL = URL_Utils.VENUE + "/android/forceUpdate";
        CustomRequest jsObjRequest = new CustomRequest(URL, null, successListener, errorListener);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void checkAppVersion(final Activity activity){
        SharedPreferences preferences = activity.getSharedPreferences(NAME_PREFERENCE, MODE_PRIVATE);

        String webVersion = "0.0";
        webVersion = preferences.getString("WebAppVersion", "");
        if (TextUtils.isEmpty(webVersion)) {
            webVersion = "0.0";
        }
        double webAppVersion = Double.parseDouble(webVersion);
        double appVersion = Double.parseDouble(Controller.APP_VERSION_NAME);

        if (webAppVersion > appVersion) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.version_title);
            builder.setMessage(R.string.version_msg);
            builder.setCancelable(false);

            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getApplicationContext().getPackageName())));
                }
            });

            builder.show();
            return;
        }
    }


}

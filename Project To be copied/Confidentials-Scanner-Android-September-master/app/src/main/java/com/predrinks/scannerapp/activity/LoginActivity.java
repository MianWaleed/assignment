package com.predrinks.scannerapp.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.predrinks.scannerapp.BaseFragmentActivity;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.network.CustomRequest;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.Constant;
import com.predrinks.scannerapp.utils.MyPref;
import com.predrinks.scannerapp.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseFragmentActivity {

    private static final String TAG = "LoginActivity";
    EditText etEmailId, etPassword;
    LinearLayout llLogin;
    private AlertDialog pDialog;
    RelativeLayout activityLogin;
    ConfidentialsOrderDAO confidentialsOrderDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activityLogin = (RelativeLayout) findViewById(R.id.activity_login);
        confidentialsOrderDAO = new ConfidentialsOrderDAO(this);

        Log.d(TAG, "URL's => \n Login URL=>" + URL_Utils.LOGIN + "\n" +
                "Redeem URL=>" + URL_Utils.REDEEM_OFFER + "\n" +
                "Venue URL=>" + URL_Utils.VENUE + "\n" +
                "Venue Details URL=>" + URL_Utils.VENUE_DETAIL + "\n");

        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etPassword = (EditText) findViewById(R.id.etPassword);
        llLogin = (LinearLayout) findViewById(R.id.llLogin);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
//        etEmailId.setText("jack@predrinks.com");
//        etPassword.setText("123456");

//        etEmailId.setText("enquiries@thechurchgreen.co.uk");
//        etPassword.setText("123456");

//        etEmailId.setText("operations@dongiovanni.uk.com");
//        etPassword.setText("Letmeinplease");
//        llLogin.performClick();

//        etEmailId.setText("braddan@recroombars.co.uk");
//        etPassword.setText("123456");

//        manchester@theveenocompany.com Letmeinplease

//        etEmailId.setText("manchester@theveenocompany.com");
//        etPassword.setText("Letmeinplease");

        llLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confidentialsOrderDAO.burnThemAll();
                MyPref.setIsSynced(LoginActivity.this, false);
                String personEmail = etEmailId.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if (TextUtils.isEmpty(personEmail)) {
                    Snackbar.make(activityLogin, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(personEmail).matches()) {
                    Snackbar.make(activityLogin, "Please enter a valid email address", Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (password.isEmpty()) {
                    Snackbar.make(activityLogin, "Please enter a password", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                Map<String, String> params = new HashMap<String, String>();

                pDialog.show();
                String URL = URL_Utils.LOGIN + "?" + Constant.PARAM_EMAIL_ID + "=" + personEmail + "&" + Constant.PARAM_PASSWORD + "=" + password;

                Log.d(TAG, "onErrorResponse: url" + URL);


                CustomRequest jsObjRequest = new CustomRequest(URL, params, createRequestSuccessListener, createRequestErrorListener);
                VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);

            }
        });
    }

    Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            try {
                pDialog.dismiss();
                Log.d(TAG, error.toString());
                Toast.makeText(LoginActivity.this, R.string.error_internet, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                String success = response.getString("success");
                if (success.equals("1")) {
                    saveLoadedData("loginResponse", response.toString());
                    setLoggedIn(true);
                    Intent intent = new Intent(LoginActivity.this, ScannerActivity.class);
                    intent.putExtra("loginResponse", response.toString());
                    startActivity(intent);
                    finish();
                } else {
                    String message = response.getString("message");
                    dialog_info("", message);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    protected void dialog_info(String title, String message) {
        final Dialog dialog = new Dialog(LoginActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }
}

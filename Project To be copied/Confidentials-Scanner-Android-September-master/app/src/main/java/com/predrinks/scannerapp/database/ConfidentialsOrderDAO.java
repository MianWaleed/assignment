package com.predrinks.scannerapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.predrinks.scannerapp.classadapter.Items;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.support.constraint.Constraints.TAG;

public class ConfidentialsOrderDAO {

    public static String DATABASE_NAME = "ConfidentialsOffer.db";
    final int DATABASE_VERSION = 8;

    public static String KEY_PRE_DRINK_OFFERS_ID = "_id";


    public static String KEY_ORDER_ID = "orderID";
    public static String KEY_EVENT_ID = "eventID";
    public static String KEY_OFFER_ID = "offerID";
    public static String KEY_MY_OFFER_ID = "myOfferID";
    public static String KEY_USER_ID = "userID";
    public static String KEY_OFFER_TITLE = "offerTitle";
    public static String KEY_EVENT_NAME = "eventName";
    public static String KEY_VENUE_TITLE = "venueTitle";
    public static String KEY_QR_CODE = "qRCode";
    public static String KEY_QR_CODE_IMAGE = "qRCodeImage";
    public static String KEY_QUANTITY = "quantity";
    public static String KEY_IS_REDEEMED = "isRedeemed";
    public static String KEY_IS_EXPIRED = "isExpired";
    public static String KEY_USER_NAME = "userName";
    public static String KEY_EVENT_TIME = "eventTime";
    public static String KEY_IS_SYNCED = "isSynced";

    public static String KEY_OFFER_EXPIRY_MODE = "offerExpiryMode";
    public static String KEY_EXPIRY_TIME = "expiryTime";
    public static String KEY_EXPIRES_ON = "expiresOn";
    public static String KEY_IS_OFFER_EXPIRED = "isOfferExpired";
    public static String KEY_IS_OFFER_ACTIVE = "isOfferActive";
    public static String KEY_REDEEMED_ON = "redeemedOn";
    public static String KEY_IS_EVENT_DATE_DISABLED = "isEventDateDisabled";
    public static String KEY_DATE_UPDATED = "DateUpdated";

    public static String TABLE_CONFIDENTIALS_OFFER = "ConfidentialsOffer";


    public static String TRUE = "true";
    public static String FALSE = "false";

    private String CREATE_TABLE_CONFIDENTIALS_ITEM = "CREATE TABLE " + TABLE_CONFIDENTIALS_OFFER
            + "("
            + KEY_PRE_DRINK_OFFERS_ID + " integer PRIMARY KEY autoincrement,"
            + KEY_MY_OFFER_ID + " integer DEFAULT '0', "
            + KEY_ORDER_ID + " integer DEFAULT '0', "
            + KEY_EVENT_ID + " integer DEFAULT '0', "
            + KEY_OFFER_ID + " integer DEFAULT '0', "
            + KEY_USER_ID + " integer DEFAULT '', "
            + KEY_OFFER_TITLE + " text DEFAULT '', "
            + KEY_EVENT_NAME + " text DEFAULT '', "
            + KEY_VENUE_TITLE + " text DEFAULT '', "
            + KEY_QR_CODE + " text DEFAULT '', "
            + KEY_QR_CODE_IMAGE + " text DEFAULT '', "
            + KEY_QUANTITY + " integer DEFAULT '', "
            + KEY_IS_REDEEMED + " text DEFAULT '', "
            + KEY_IS_EXPIRED + " text DEFAULT '', "
            + KEY_USER_NAME + " text DEFAULT '', "
            + KEY_EVENT_TIME + " text DEFAULT '', "
            + KEY_IS_SYNCED + " text DEFAULT '', "
            + KEY_OFFER_EXPIRY_MODE + " text DEFAULT '', "
            + KEY_EXPIRES_ON + " text DEFAULT '', "
            + KEY_EXPIRY_TIME + " text DEFAULT '', "
            + KEY_REDEEMED_ON + " DATETIME DEFAULT '', "
            + KEY_IS_OFFER_EXPIRED + " text DEFAULT '', "
            + KEY_IS_OFFER_ACTIVE + " text DEFAULT '', "
            + KEY_IS_EVENT_DATE_DISABLED + " text DEFAULT '', "
            + KEY_DATE_UPDATED + " text DEFAULT '' "
            + ");";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase sqLiteDatabase;

    public ConfidentialsOrderDAO(Context c) {
        this.context = c;
        DBHelper = new DatabaseHelper(context);
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_CONFIDENTIALS_ITEM);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIDENTIALS_OFFER);
            // Create tables again
            onCreate(db);
        }
    }

    // ---opens the database---
    public ConfidentialsOrderDAO open() throws SQLException {
        sqLiteDatabase = DBHelper.getWritableDatabase();
        return this;
    }

    // close the database
    public void close() throws SQLException {
        DBHelper.close();
    }


    public void insertOffers(int myOfferID, int orderID, int eventID, int offerID, int userID, String offerTitle,
                             String eventName, String venueTitle, String qRCode, String qRCodeImage, int quantity,
                             String isRedeemed, String isExpired, String userName, String eventTime, String offerExpiryMode, String expiresOn,
                             String expiryTime, String redeemedOn, String isOfferExpired, String isOfferActive,
                             String isEventDateDisabled, String dateUpdated) {

        ContentValues values = new ContentValues();
        values.put(KEY_MY_OFFER_ID, myOfferID);
        values.put(KEY_ORDER_ID, orderID);
        values.put(KEY_EVENT_ID, eventID);
        values.put(KEY_OFFER_ID, offerID);
        values.put(KEY_USER_ID, userID);
        values.put(KEY_OFFER_TITLE, offerTitle);
        values.put(KEY_EVENT_NAME, eventName);
        values.put(KEY_VENUE_TITLE, venueTitle);
        values.put(KEY_QR_CODE, qRCode);
        values.put(KEY_QR_CODE_IMAGE, qRCodeImage);
        values.put(KEY_QUANTITY, quantity);
        values.put(KEY_IS_REDEEMED, isRedeemed);
        values.put(KEY_IS_EXPIRED, isExpired);
        values.put(KEY_USER_NAME, userName);
        values.put(KEY_EVENT_TIME, eventTime);
        values.put(KEY_IS_SYNCED, "false");
        values.put(KEY_OFFER_EXPIRY_MODE, offerExpiryMode);
        values.put(KEY_EXPIRES_ON, expiresOn);
        values.put(KEY_EXPIRY_TIME, expiryTime);
        values.put(KEY_REDEEMED_ON, redeemedOn);
        values.put(KEY_IS_OFFER_EXPIRED, isOfferExpired);
        values.put(KEY_IS_OFFER_ACTIVE, isOfferActive);
        values.put(KEY_IS_EVENT_DATE_DISABLED, isEventDateDisabled);
        values.put(KEY_DATE_UPDATED, dateUpdated);

        long id = sqLiteDatabase.insert(TABLE_CONFIDENTIALS_OFFER, null, values);

        Log.e("Insert id :", String.valueOf(id));
    }

    public void updateOffers(int myOfferID, int orderID, int eventID, int offerID, int userID, String offerTitle, String eventName,
                             String venueTitle, String qRCode, String qRCodeImage, int quantity, String isRedeemed, String isExpired,
                             String userName, String eventTime, String offerExpiryMode, String expiresOn, String expiryTime,
                             String redeemedOn, String isOfferExpired, String isOfferActive, String isEventDateDisabled, String dateUpdated) {

        ContentValues values = new ContentValues();
        values.put(KEY_MY_OFFER_ID, myOfferID);
        values.put(KEY_ORDER_ID, orderID);
        values.put(KEY_EVENT_ID, eventID);
        values.put(KEY_OFFER_ID, offerID);
        values.put(KEY_USER_ID, userID);
        values.put(KEY_OFFER_TITLE, offerTitle);
        values.put(KEY_EVENT_NAME, eventName);
        values.put(KEY_VENUE_TITLE, venueTitle);
        values.put(KEY_QR_CODE, qRCode);
        values.put(KEY_QR_CODE_IMAGE, qRCodeImage);
        values.put(KEY_QUANTITY, quantity);
        values.put(KEY_IS_REDEEMED, isRedeemed);
        values.put(KEY_IS_EXPIRED, isExpired);
        values.put(KEY_USER_NAME, userName);
        values.put(KEY_EVENT_TIME, eventTime);
        values.put(KEY_IS_SYNCED, "false");
        values.put(KEY_OFFER_EXPIRY_MODE, offerExpiryMode);
        values.put(KEY_EXPIRES_ON, expiresOn);
        values.put(KEY_EXPIRY_TIME, expiryTime);
        values.put(KEY_REDEEMED_ON, redeemedOn);
        values.put(KEY_IS_OFFER_EXPIRED, isOfferExpired);
        values.put(KEY_IS_OFFER_ACTIVE, isOfferActive);
        values.put(KEY_IS_EVENT_DATE_DISABLED, isEventDateDisabled);
        values.put(KEY_IS_EVENT_DATE_DISABLED, isEventDateDisabled);
        values.put(KEY_DATE_UPDATED, dateUpdated);

        String where = "" + KEY_MY_OFFER_ID + "=?";
        String[] whereArgs = {String.valueOf(myOfferID)};

        try {
            sqLiteDatabase.update(TABLE_CONFIDENTIALS_OFFER, values, where, whereArgs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isOffersExist(int myOfferID, int orderID, int offerID, int userID) {
        Cursor cursor = null;
        try {
            // cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_ORDER_ID + " = '" + orderID + "' and " + KEY_OFFER_ID + " = '" + offerID + "' and " + KEY_USER_ID + " = '" + userID + "'", null);
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_MY_OFFER_ID + " = '" + myOfferID + "'", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (cursor == null)
            return false;

        return (cursor.getCount() > 0 ? true : false);
    }

    public int getTotalOffersCount() {
        int total = 0;
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (cursor != null)
                cursor.close();
        }
        if (cursor != null && !cursor.isClosed()) {
            total = cursor.getCount();
            cursor.close();
        }
        return total;
    }

    public JSONObject getObjectByQScarnner(String qRCodeParam) {
        JSONObject jsonObject = new JSONObject();
        Cursor cursor = null;
        // String query = "select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_QR_CODE + " = '" + qRCodeParam + "' and " + KEY_IS_REDEEMED + " = " + "'false'";
        String query = "select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_QR_CODE + " = '" + qRCodeParam + "'";
        try {
            cursor = sqLiteDatabase.rawQuery(query, null);
            if (cursor != null) {
                int orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                        isRedeemed = "", isExpired = "", userName = "", eventTime = "", isSynced = "",
                        redeemedOn = "", offerExpiryMode = "", expiresOn = "", expiryTime = "",
                        isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";

                int size = cursor.getCount();

                if (size == 0)
                    return jsonObject;

                cursor.moveToFirst();

                orderID = cursor.getInt(2);
                eventID = cursor.getInt(3);
                offerID = cursor.getInt(4);
                userID = cursor.getInt(5);
                offerTitle = cursor.getString(6);
                eventName = cursor.getString(7);
                venueTitle = cursor.getString(8);
                qRCode = cursor.getString(9);
                qRCodeImage = cursor.getString(10);
                quantity = cursor.getInt(11);
                isRedeemed = cursor.getString(12);
                isExpired = cursor.getString(13);
                userName = cursor.getString(14);
                eventTime = cursor.getString(15);
                isSynced = cursor.getString(16);
                offerExpiryMode = cursor.getString(17);
                expiresOn = cursor.getString(18);
                expiryTime = cursor.getString(19);
                redeemedOn = cursor.getString(20);
                isOfferExpired = cursor.getString(21);
                isOfferActive = cursor.getString(22);
                isEventDateDisabled = cursor.getString(23);
                dateUpdated = cursor.getString(24);

                try {
                    jsonObject.put(KEY_ORDER_ID, orderID);
                    jsonObject.put(KEY_EVENT_ID, eventID);
                    jsonObject.put(KEY_OFFER_ID, offerID);
                    jsonObject.put(KEY_USER_ID, userID);
                    jsonObject.put(KEY_OFFER_TITLE, offerTitle);
                    jsonObject.put(KEY_EVENT_NAME, eventName);
                    jsonObject.put(KEY_VENUE_TITLE, venueTitle);
                    jsonObject.put(KEY_QR_CODE, qRCode);
                    jsonObject.put(KEY_QR_CODE_IMAGE, qRCodeImage);
                    jsonObject.put(KEY_QUANTITY, quantity);
                    jsonObject.put(KEY_IS_REDEEMED, isRedeemed);
                    jsonObject.put(KEY_IS_EXPIRED, isExpired);
                    jsonObject.put(KEY_USER_NAME, userName);
                    jsonObject.put(KEY_EVENT_TIME, eventTime);
                    jsonObject.put(KEY_IS_SYNCED, isSynced);
                    jsonObject.put(KEY_OFFER_EXPIRY_MODE, offerExpiryMode);
                    jsonObject.put(KEY_EXPIRES_ON, expiresOn);
                    jsonObject.put(KEY_EXPIRY_TIME, expiryTime);
                    jsonObject.put(KEY_REDEEMED_ON, redeemedOn);
                    jsonObject.put(KEY_IS_OFFER_EXPIRED, isOfferExpired);
                    jsonObject.put(KEY_IS_OFFER_ACTIVE, isOfferActive);
                    jsonObject.put(KEY_IS_EVENT_DATE_DISABLED, isEventDateDisabled);
                    jsonObject.put(KEY_DATE_UPDATED, dateUpdated);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public ArrayList<Items> getRedeemedList() {
        ArrayList<Items> itemsArrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_REDEEMED + " = '" + "true" + "' ORDER BY " + KEY_DATE_UPDATED + " DESC limit 10", null);


            if (cursor != null && cursor.getCount() > 0) {
                int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                        isRedeemed = "", isExpired = "", userName = "", eventTime = "", isSynced = "", redeemedOn = "",
                        offerExpiryMode = "", expiresOn = "", expiryTime = "",
                        isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";

                if (cursor.moveToFirst()) {
                    do {
                        myOfferID = cursor.getInt(1);
                        orderID = cursor.getInt(2);
                        eventID = cursor.getInt(3);
                        offerID = cursor.getInt(4);
                        userID = cursor.getInt(5);
                        offerTitle = cursor.getString(6);
                        eventName = cursor.getString(7);
                        venueTitle = cursor.getString(8);
                        qRCode = cursor.getString(9);
                        qRCodeImage = cursor.getString(10);
                        quantity = cursor.getInt(11);
                        isRedeemed = cursor.getString(12);
                        isExpired = cursor.getString(13);
                        userName = cursor.getString(14);
                        eventTime = cursor.getString(15);
                        isSynced = cursor.getString(16);
                        offerExpiryMode = cursor.getString(17);
                        expiresOn = cursor.getString(18);
                        expiryTime = cursor.getString(19);
                        redeemedOn = cursor.getString(20);
                        isOfferExpired = cursor.getString(21);
                        isOfferActive = cursor.getString(22);
                        isEventDateDisabled = cursor.getString(23);
                        dateUpdated = cursor.getString(24);

                        Items items = new Items(userName, eventTime, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, isSynced, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                        Log.d(TAG, "updateOffers: " + items.toString());
                        itemsArrayList.add(items);
                    } while (cursor.moveToNext());
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (cursor != null)
                cursor.close();
        }
        Log.e("Date vise Sorting", itemsArrayList.toString());
        return itemsArrayList;
    }

    public ArrayList<Items> getAllItemList(int pos, String eventId, String isRedeem) {
        ArrayList<Items> itemsArrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (pos == 0) {
                cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_REDEEMED + " = '" + isRedeem + "' and " + KEY_EVENT_ID + " = " + "'" + eventId + "' ORDER BY " + KEY_REDEEMED_ON + " DESC", null);
            } else if (pos == 1) {
                cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_REDEEMED + " = '" + isRedeem + "' and " + KEY_EVENT_ID + " = " + "'" + eventId + "' ORDER BY " + KEY_REDEEMED_ON + " DESC", null);
            } else {
                cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_EVENT_ID + " = '" + eventId + "'", null);
            }


            if (cursor != null && cursor.getCount() > 0) {
                int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                String offerTitle = "", eventName = "", venueTitle = "", qRCode = "", qRCodeImage = "",
                        isRedeemed = "", isExpired = "", userName = "", eventTime = "", isSynced = "", redeemedOn = "",
                        offerExpiryMode = "", expiresOn = "", expiryTime = "",
                        isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";

                if (cursor.moveToFirst()) {
                    do {
                        myOfferID = cursor.getInt(1);
                        orderID = cursor.getInt(2);
                        eventID = cursor.getInt(3);
                        offerID = cursor.getInt(4);
                        userID = cursor.getInt(5);
                        offerTitle = cursor.getString(6);
                        eventName = cursor.getString(7);
                        venueTitle = cursor.getString(8);
                        qRCode = cursor.getString(9);
                        qRCodeImage = cursor.getString(10);
                        quantity = cursor.getInt(11);
                        isRedeemed = cursor.getString(12);
                        isExpired = cursor.getString(13);
                        userName = cursor.getString(14);
                        eventTime = cursor.getString(15);
                        isSynced = cursor.getString(16);
                        offerExpiryMode = cursor.getString(17);
                        expiresOn = cursor.getString(18);
                        expiryTime = cursor.getString(19);
                        redeemedOn = cursor.getString(20);
                        isOfferExpired = cursor.getString(21);
                        isOfferActive = cursor.getString(22);
                        isEventDateDisabled = cursor.getString(23);
                        dateUpdated = cursor.getString(24);

                        Items items = new Items(userName, eventTime, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, isSynced, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                        itemsArrayList.add(items);
                    } while (cursor.moveToNext());
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (cursor != null)
                cursor.close();
        }
        Log.e("Date vise Sorting", itemsArrayList.toString());
        return itemsArrayList;
    }

    public ArrayList<Items> getSearchItemList(int pos, String eventId, String isRedeem, String query) {
        ArrayList<Items> itemsArrayList = new ArrayList<>();
        Cursor cursor = null;
        String qr = "select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_EVENT_ID + " = '" + eventId + "' and " + KEY_OFFER_TITLE + " like %" + query + "%";
        try {
            if (pos == 0) {
                cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_REDEEMED + " = '" + isRedeem + "' and " + KEY_EVENT_ID + " = " + eventId + " and (" + KEY_OFFER_TITLE + " like '%" + query + "%' or " + KEY_QR_CODE + " like '%" + query + "%' or " + KEY_USER_NAME + " like '%" + query + "%') ORDER BY " + KEY_REDEEMED_ON + " DESC", null);
            } else if (pos == 1) {
                cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_REDEEMED + " = '" + isRedeem + "' and " + KEY_EVENT_ID + " = " + eventId + " and (" + KEY_OFFER_TITLE + " like '%" + query + "%' or " + KEY_QR_CODE + " like '%" + query + "%' or " + KEY_USER_NAME + " like '%" + query + "%') ORDER BY " + KEY_REDEEMED_ON + " DESC", null);
            } else {
                cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_EVENT_ID + " = " + eventId + " and (" + KEY_OFFER_TITLE + " like '%" + query + "%' or " + KEY_QR_CODE + " like '%" + query + "%' or " + KEY_USER_NAME + " like '%" + query + "%')", null);
            }

            if (cursor != null && cursor.getCount() > 0) {
                int orderID = 0, eventID = 0, offerID = 0, userID = 0, quantity = 0;
                String offerTitle = "", venueTitle = "", eventName = "", qRCode = "", qRCodeImage = "", isRedeemed = "",
                        isExpired = "", userName = "", eventTime = "", isSynced = "", redeemedOn = "", offerExpiryMode = "",
                        expiresOn = "", expiryTime = "", isOfferExpired = "", isOfferActive = "", isEventDateDisabled = "", dateUpdated = "";

                if (cursor.moveToFirst()) {
                    do {
                        orderID = cursor.getInt(2);
                        eventID = cursor.getInt(3);
                        offerID = cursor.getInt(4);
                        userID = cursor.getInt(5);
                        offerTitle = cursor.getString(6);
                        eventName = cursor.getString(7);
                        venueTitle = cursor.getString(8);
                        qRCode = cursor.getString(9);
                        qRCodeImage = cursor.getString(10);
                        quantity = cursor.getInt(11);
                        isRedeemed = cursor.getString(12);
                        isExpired = cursor.getString(13);
                        userName = cursor.getString(14);
                        eventTime = cursor.getString(15);
                        isSynced = cursor.getString(16);
                        offerExpiryMode = cursor.getString(17);
                        expiresOn = cursor.getString(18);
                        expiryTime = cursor.getString(19);
                        redeemedOn = cursor.getString(20);
                        isOfferExpired = cursor.getString(21);
                        isOfferActive = cursor.getString(22);
                        isEventDateDisabled = cursor.getString(23);
                        dateUpdated = cursor.getString(24);

                        Items items = new Items(userName, eventTime, orderID, eventID, offerID, userID, offerTitle, eventName, venueTitle, qRCode, qRCodeImage, quantity, isRedeemed, isExpired, isSynced, offerExpiryMode, expiresOn, expiryTime, redeemedOn, isOfferExpired, isOfferActive, isEventDateDisabled, dateUpdated);
                        itemsArrayList.add(items);
                    } while (cursor.moveToNext());
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.e("Date vise Sorting", itemsArrayList.toString());
        return itemsArrayList;
    }

    public void upDateIsReedem(String qRCode, String isRedeemed, String isSynced) {
        ContentValues values = new ContentValues();
        values.put(KEY_IS_REDEEMED, isRedeemed);
        values.put(KEY_IS_SYNCED, isSynced);
        values.put(KEY_REDEEMED_ON, getDateTime());

        String where = "" + KEY_QR_CODE + "=? AND " + KEY_IS_REDEEMED + "=?";
        String[] whereArgs = {qRCode, "false"};

        int pos = sqLiteDatabase.update(TABLE_CONFIDENTIALS_OFFER, values, where, whereArgs);
    }

    public boolean isQRCodeRedeem(String qRCode) {
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_QR_CODE + " = '" + qRCode + "' and " + KEY_IS_REDEEMED + " = " + "'true'", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            if (cursor != null)
                cursor.close();
        }
        if (cursor == null)
            return false;

        return cursor.getCount() > 0;
    }

    public boolean isSyncedData(String isSynced) {
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery("select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_SYNCED + " = '" + isSynced + "'", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor != null && (cursor.getCount() > 0 ? true : false);

    }

//    db.execSQL("delete from "+ TABLE_NAME);

    public void burnThemAll() {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CONFIDENTIALS_OFFER); //delete all rows in a table
        db.close();
    }

    public String getLastId() {
//        SELECT * FROM tablename ORDER BY column DESC LIMIT 1; KEY_OFFER_ID
        String myOfferID = "";
        int orderID = 0, eventID = 0, offerID = 0, userID = 0;

        Cursor cursor = null;
        String query = "select * from " + TABLE_CONFIDENTIALS_OFFER + " ORDER BY " + KEY_DATE_UPDATED + " DESC LIMIT 1;";

        try {
            cursor = DBHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor != null) {
                String dateTime = "";
                Log.d(TAG, "getLastId: " + cursor.toString());
                while (cursor.moveToNext()) {

                    JSONObject jsonObject = new JSONObject();
                    myOfferID = cursor.getString(24);
                    offerID = cursor.getInt(4);
                    userID = cursor.getInt(5);
                    dateTime = cursor.getString(20);
                }
            }

        } catch (SQLException e) {
            Log.e(TAG, "getLastId: " + e.getMessage(), e);
            e.printStackTrace();
        }
        return myOfferID;
    }

    public JSONArray getUnSyncedData(String isSyncedParam, String ScannedByTeamMemberID) {
        JSONArray jsonArray = null;
        Cursor cursor = null;
        String query = "select * from " + TABLE_CONFIDENTIALS_OFFER + " where " + KEY_IS_SYNCED + " = '" + isSyncedParam + "'";
        try {
            cursor = sqLiteDatabase.rawQuery(query, null);
            if (cursor != null) {
                int myOfferID = 0, orderID = 0, eventID = 0, offerID = 0, userID = 0;
                String dateTime = "";
                int size = cursor.getCount();
                jsonArray = new JSONArray();
                if (size == 0)
                    return jsonArray;

                while (cursor.moveToNext()) {

                    JSONObject jsonObject = new JSONObject();
                    myOfferID = cursor.getInt(1);
                    offerID = cursor.getInt(4);
                    userID = cursor.getInt(5);
                    dateTime = cursor.getString(20);
                    try {
                        jsonObject.put("myOfferID", myOfferID);
                        jsonObject.put("OrderID", orderID);
                        jsonObject.put("OfferID", offerID);
                        jsonObject.put("UserID", userID);
                        jsonObject.put("ScannedByTeamMemberID", Integer.parseInt(ScannedByTeamMemberID));
                        jsonObject.put("RedeemedOn", dateTime);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray.put(jsonObject);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void upDateIsSynced(String isSynced) {
        ContentValues values = new ContentValues();
        values.put(KEY_IS_SYNCED, "false");

        String where = "" + KEY_IS_SYNCED + "=?";
        String[] whereArgs = {isSynced};

        sqLiteDatabase.update(TABLE_CONFIDENTIALS_OFFER, values, where, whereArgs);
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}

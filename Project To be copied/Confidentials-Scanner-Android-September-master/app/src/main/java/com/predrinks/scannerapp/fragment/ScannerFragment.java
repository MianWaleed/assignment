package com.predrinks.scannerapp.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.predrinks.scannerapp.BaseFragment;
import com.predrinks.scannerapp.BaseFragmentActivity;
import com.predrinks.scannerapp.R;
import com.predrinks.scannerapp.activity.LoginActivity;
import com.predrinks.scannerapp.activity.ScannerActivity;
import com.predrinks.scannerapp.classadapter.HeroVouchersAdapter;
import com.predrinks.scannerapp.database.ConfidentialsOrderDAO;
import com.predrinks.scannerapp.models.HeroVoucherModel;
import com.predrinks.scannerapp.network.CustomRequest;
import com.predrinks.scannerapp.network.VolleySingleton;
import com.predrinks.scannerapp.utils.MyPref;
import com.predrinks.scannerapp.utils.URL_Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.android.volley.Request.Method.GET;

public class ScannerFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ScannerFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    SurfaceView cameraView;
    TextView barcodeInfo;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;
    Activity mActivity;
    ConfidentialsOrderDAO confidentialsOrderDAO;
    public static boolean isStartedRedeem;
    public boolean isConfidentialPass = false;

    public ScannerFragment() {
    }

    boolean isScannedDone;

    public static ScannerFragment newInstance(String param1, String param2) {
        ScannerFragment fragment = new ScannerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        confidentialsOrderDAO = new ConfidentialsOrderDAO(mActivity);
    }


    View mView;
    Switch confidentialsSt;
    ImageView flashIb;
    TextView scanTv;
    Gson gson;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scanner, container, false);
        confidentialsSt = view.findViewById(R.id.switch1);
        confidentialsSt.setOnCheckedChangeListener(this);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        scanTv = view.findViewById(R.id.scan_tv);

        mView = view;
        checkPermission();
        isStartedRedeem = false;
        ScannerActivity.showMoreMenu();
        ScannerActivity.showHeaderRight("USE CODE");
        ScannerActivity.setHeaderRightClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        ScannerActivity.setBackVisible(true);
        ScannerActivity.setBackClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogOutDialog();
            }
        });

        flashIb = mView.findViewById(R.id.ib_flash);

        if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            flashIb.setVisibility(View.VISIBLE);
        } else {
            flashIb.setVisibility(View.GONE);
        }

        flashIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flash();
            }
        });


//        showDialogForHero(new JSONArray());
//        showBottomSheetForHero(new JSONArray());

        view.findViewById(R.id.scan_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showBottomSheetForHero(new JSONArray());
                hitApiForHeroPass("http://confidentials.akkastechdemo.com/api/user/498/venue/267/hero/discount");
            }
        });

        return view;
    }

    public void hitApiForHeroPass(String url) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                HeroVoucherModel heroVoucherModel = gson.fromJson(response.toString(), HeroVoucherModel.class);
                showBottomSheetForHero(heroVoucherModel.getData());
                Log.d(TAG, "onResponse: " + heroVoucherModel.getData().toString());

//                for (int j = 0; j < heroVoucherModel.getData().size(); j++) {
//                    Log.d(TAG, "onResponse: "+heroVoucherModel.getData().get(i));
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private static Camera getCamera(@NonNull CameraSource cameraSource) {
        Field[] declaredFields = CameraSource.class.getDeclaredFields();

        for (Field field : declaredFields) {
            if (field.getType() == Camera.class) {
                field.setAccessible(true);
                try {
                    Camera camera = (Camera) field.get(cameraSource);
                    if (camera != null) {
                        return camera;
                    }
                    return null;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return null;
    }

    private Camera camera = null;
    boolean flashmode = false;

    public void flash() {
        camera = getCamera(cameraSource);
        if (camera != null) {
            try {
                Camera.Parameters param = camera.getParameters();
                param.setFlashMode(!flashmode ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                flashmode = !flashmode;
                if (flashmode) {
                    flashIb.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off));
                } else {
                    flashIb.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isScanned = false;
        isScannedDone = false;
        OpenCamera();
        getVenueDetail();
        BaseFragmentActivity.checkAppVersion(mActivity);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    OpenCamera();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            requestPermissions(new String[]{android.Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

            // MY_PERMISSIONS_REQUEST_CAMERA is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            return;
        }
        OpenCamera();
    }

    public void flashOn() {
        Camera camera = Camera.open();
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(parameters);
        camera.startPreview();
    }


    public void flashOff() {
        Camera camera2 = Camera.open();
        Camera.Parameters parameters2 = camera2.getParameters();
        parameters2.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera2.setParameters(parameters2);
        camera2.stopPreview();
    }

    private void OpenCamera() {
        cameraView = (SurfaceView) mView.findViewById(R.id.camera_view);
        barcodeInfo = (TextView) mView.findViewById(R.id.code_info);


        cameraView.invalidate();
        barcodeDetector = new BarcodeDetector.Builder(mActivity)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
//                Log.d(TAG, "receiveDetections: "+barcodes);
                if (barcodes.size() != 0) {
                    barcodeInfo.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            String scannedString = barcodes.valueAt(0).displayValue;

                            if (isConfidentialPass) {
                                if (isShowing) {
                                    hitApiForConfidentialsPass(scannedString);
                                    isShowing = false;
                                }
                            } else {
                                scannedString = scannedString.toUpperCase();
                                barcodeInfo.setText(scannedString);

                                confidentialsOrderDAO.open();
                                JSONObject jsonObject = confidentialsOrderDAO.getObjectByQScarnner(scannedString);
                                String barcode = "";
                                try {
                                    barcode = jsonObject.getString("qRCode");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                if (!isScanned) {
                                    if (TextUtils.isEmpty(barcode)) {
                                        isScanned = true;
                                        dialog_info(mActivity, "Unable to Redeem Code", "Invalid code");
                                    } else {
                                        String expiresOn = getJsonString(jsonObject, "expiresOn");
                                        String offerExpiryMode = getJsonString(jsonObject, "offerExpiryMode");
                                        String eventTime = getJsonString(jsonObject, "eventTime");
                                        String expiryTime = getJsonString(jsonObject, "expiryTime");
                                        String isOfferExpired = getJsonString(jsonObject, "isOfferExpired");
                                        String isOfferActive = getJsonString(jsonObject, "isOfferActive");

                                        if (TextUtils.isEmpty(barcode)) {
                                            isScanned = true;
                                            dialog_info(mActivity, "Unable to Redeem Code", "Invalid code");
                                        } else {

                                            String expireMessage = "";
                                            if (offerExpiryMode.equals("OnDate") || offerExpiryMode.equals("AfterDays")) {
                                                expireMessage = "(Voucher Expires on " + expiresOn + ")";
                                                expireMessage = "(Voucher Expires at midnight on " + expiresOn + ")";

                                                try {
                                                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                                    Date date = new Date();
                                                    Date expiryDatetime = null;
                                                    try {
                                                        if (expiryTime.equals("00:00"))
                                                            expiryDatetime = dateformat.parse(expiresOn + " 23:59");
                                                        else
                                                            expiryDatetime = dateformat.parse(expiresOn + " " + expiryTime);

                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                    boolean isBefore = false;
                                                    if (date.before(expiryDatetime)) {
                                                        isBefore = true;
                                                    }
                                                    if (!isBefore) {
                                                        isScanned = true;
                                                        dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                                    } else {
                                                        boolean isredeemCode = confidentialsOrderDAO.isQRCodeRedeem(barcode);
                                                        if (isredeemCode) {
                                                            isScanned = true;
                                                            dialog_info(mActivity, "Unable to Redeem Code", "You have already used this offer");
                                                        } else if (isOfferExpired.equals("true") || !isOfferActive.equals("true")) {
                                                            isScanned = true;
                                                            dialog_info(mActivity, "Unable to Redeem Code", "Invalid code");
                                                        } else if (scannedString.equals(barcode) && !isScannedDone) {
                                                            isScannedDone = true;
                                                            isStartedRedeem = true;
                                                            isScanned = true;
                                                            Bundle bundle = new Bundle();
                                                            bundle.putString("jsonObject", jsonObject.toString());
                                                            bundle.putString("barcode", barcode);

                                                            confidentialsOrderDAO.upDateIsReedem(barcode, "true", "true");

                                                            RedeemedFragment fragment = RedeemedFragment.newInstance(barcode, jsonObject.toString());
                                                            ScannerActivity.loadContentFragment(fragment, false);
                                                        }
                                                    }
                                                } catch (NullPointerException e) {
                                                    e.printStackTrace();
                                                    isScanned = true;
                                                    dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                                }
                                            } else if (offerExpiryMode.equals("AtEventExpiry")) {
                                                try {
                                                    String date1 = eventTime.split("-")[1];
                                                    date1 = date1.replace("@", " ");
                                                    String[] time = date1.split(" ");
                                                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                                                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                                                    Date _24HourDt = null;
                                                    try {
                                                        time[2] = time[2].replace("am", " AM");
                                                        time[2] = time[2].replace("pm", " PM");
                                                        Date d1 = _12HourSDF.parse(time[2]);
                                                        System.out.println(_24HourSDF.format(d1));
                                                        _24HourDt = _12HourSDF.parse(time[2]);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                                    Date date = new Date();
                                                    Date datetime = null;
                                                    try {
                                                        datetime = dateformat.parse(time[1] + " " + _24HourSDF.format(_24HourDt));
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                    boolean isBefore = false;
                                                    if (date.before(datetime)) {
                                                        isBefore = true;
                                                    }
                                                    if (!isBefore) {
                                                        isScanned = true;
                                                        boolean isredeemCode = confidentialsOrderDAO.isQRCodeRedeem(barcode);
                                                        if (isredeemCode) {
                                                            isScanned = true;
                                                            dialog_info(mActivity, "Unable to Redeem Code", "You have already used this offer");
                                                        } else {
                                                            dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                                        }
                                                    } else {
                                                        boolean isredeemCode = confidentialsOrderDAO.isQRCodeRedeem(barcode);
                                                        if (isredeemCode) {
                                                            isScanned = true;
                                                            dialog_info(mActivity, "Unable to Redeem Code", "You have already used this offer");
                                                        } else if (isOfferExpired.equals("true") || !isOfferActive.equals("true")) {
                                                            isScanned = true;
                                                            dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                                        } else if (scannedString.equals(barcode) && !isScannedDone) {
                                                            isScannedDone = true;
                                                            isStartedRedeem = true;
                                                            isScanned = true;
                                                            Bundle bundle = new Bundle();
                                                            bundle.putString("jsonObject", jsonObject.toString());
                                                            bundle.putString("barcode", barcode);

                                                            confidentialsOrderDAO.upDateIsReedem(barcode, "true", "true");

                                                            RedeemedFragment fragment = RedeemedFragment.newInstance(barcode, jsonObject.toString());
                                                            ScannerActivity.loadContentFragment(fragment, false);
                                                        }
                                                    }
                                                } catch (NullPointerException e) {
                                                    e.printStackTrace();
                                                    isScanned = true;
                                                    dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                                }
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    });
                }
            }
        });

        cameraSource = new CameraSource
                .Builder(mActivity, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(30.0f)
                .setAutoFocusEnabled(true)
                .build();

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(mActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
    }


    public static Dialog m_dialog;

    public void showDialog() {
        isScanned = true;
        m_dialog = new Dialog(mActivity, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater m_inflater = LayoutInflater.from(mActivity);
        View m_view = m_inflater.inflate(R.layout.dialog_use_code, null);

        final EditText m_etCode = (EditText) m_view.findViewById(R.id.etCode);
        Button m_btnOk = (Button) m_view.findViewById(R.id.btnRedeem);
        Button m_btnCancel = (Button) m_view.findViewById(R.id.btnCancel);

        m_etCode.requestFocus();

        View.OnClickListener m_clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = m_etCode.getText().toString().trim();
                if (TextUtils.isEmpty(code)) {
                    Toast.makeText(mActivity, "Please enter code", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    code = code.toUpperCase();

                    confidentialsOrderDAO.open();
                    JSONObject jsonObject = confidentialsOrderDAO.getObjectByQScarnner(code);
                    String barcode = "";
                    try {
                        barcode = jsonObject.getString("qRCode");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String expiresOn = getJsonString(jsonObject, "expiresOn");
                    String offerExpiryMode = getJsonString(jsonObject, "offerExpiryMode");
                    String eventTime = getJsonString(jsonObject, "eventTime");
                    String expiryTime = getJsonString(jsonObject, "expiryTime");
                    String isOfferExpired = getJsonString(jsonObject, "isOfferExpired");
                    String isOfferActive = getJsonString(jsonObject, "isOfferActive");


                    if (TextUtils.isEmpty(barcode)) {
                        dialog_info(mActivity, "Unable to Redeem Code", "Invalid code");
                    } else {

                        String expireMessage = "";
                        if (offerExpiryMode.equals("OnDate") || offerExpiryMode.equals("AfterDays")) {
                            expireMessage = "(Voucher Expires on " + expiresOn + ")";
                            expireMessage = "(Voucher Expires at midnight on " + expiresOn + ")";

                            try {
                                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                Date date = new Date();
                                Date expiryDatetime = null;
                                try {
                                    if (expiryTime.equals("00:00"))
                                        expiryDatetime = dateformat.parse(expiresOn + " 23:59");
                                    else
                                        expiryDatetime = dateformat.parse(expiresOn + " " + expiryTime);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                boolean isBefore = false;
                                if (date.before(expiryDatetime)) {
                                    isBefore = true;
                                }
                                if (!isBefore) {
                                    dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                } else if (isOfferExpired.equals("true") || !isOfferActive.equals("true")) {
                                    dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                } else {
                                    boolean isredeemCode = confidentialsOrderDAO.isQRCodeRedeem(barcode);
                                    if (isredeemCode) {
                                        dialog_info(mActivity, "Unable to Redeem Code", "You have already used this offer");
                                    } else if (code.equals(barcode) && !isScannedDone) {
                                        isStartedRedeem = true;
                                        Bundle bundle = new Bundle();
                                        bundle.putString("jsonObject", jsonObject.toString());
                                        bundle.putString("barcode", barcode);

                                        confidentialsOrderDAO.upDateIsReedem(barcode, "true", "true");

                                        RedeemedFragment fragment = RedeemedFragment.newInstance(barcode, jsonObject.toString());
                                        ScannerActivity.loadContentFragment(fragment, false);
                                    }
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                            }
                        } else if (offerExpiryMode.equals("AtEventExpiry")) {
                            try {
                                String date1 = eventTime.split("-")[1];
                                date1 = date1.replace("@", " ");
                                String[] time = date1.split(" ");
                                SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                                SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                                Date _24HourDt = null;
                                try {
                                    time[2] = time[2].replace("am", " AM");
                                    time[2] = time[2].replace("pm", " PM");
                                    Date d1 = _12HourSDF.parse(time[2]);
                                    System.out.println(_24HourSDF.format(d1));
                                    _24HourDt = _12HourSDF.parse(time[2]);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                Date date = new Date();
                                Date datetime = null;
                                try {
                                    datetime = dateformat.parse(time[1] + " " + _24HourSDF.format(_24HourDt));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                boolean isBefore = false;
                                if (date.before(datetime)) {
                                    isBefore = true;
                                }
                                if (!isBefore) {
                                    // return jsonObject;
                                    dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                } else if (isOfferExpired.equals("true") || !isOfferActive.equals("true")) {
                                    dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                                } else {
                                    boolean isredeemCode = confidentialsOrderDAO.isQRCodeRedeem(barcode);
                                    if (isredeemCode) {
                                        dialog_info(mActivity, "Unable to Redeem Code", "You have already used this offer");
                                    } else if (code.equals(barcode) && !isScannedDone) {
                                        isStartedRedeem = true;
                                        Bundle bundle = new Bundle();
                                        bundle.putString("jsonObject", jsonObject.toString());
                                        bundle.putString("barcode", barcode);

                                        confidentialsOrderDAO.upDateIsReedem(barcode, "true", "true");

                                        RedeemedFragment fragment = RedeemedFragment.newInstance(barcode, jsonObject.toString());
                                        ScannerActivity.loadContentFragment(fragment, false);
                                    }
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                dialog_info(mActivity, "Unable to Redeem Code", "Voucher Expired");
                            }
                        }
                    }
                }
                isScanned = false;
                m_dialog.dismiss();
            }
        };
        m_btnOk.setOnClickListener(m_clickListener);
        m_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isScanned = false;
                m_dialog.dismiss();
            }
        });
        m_dialog.setContentView(m_view);
        m_dialog.show();
    }

    public void showLogOutDialog() {
        m_dialog = new Dialog(mActivity, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater m_inflater = LayoutInflater.from(mActivity);
        View m_view = m_inflater.inflate(R.layout.dialog_use_code, null);

        EditText m_etCode = (EditText) m_view.findViewById(R.id.etCode);
        m_etCode.setVisibility(View.GONE);

        TextView m_tvTitle = (TextView) m_view.findViewById(R.id.tvTitle);
        Button m_btnOk = (Button) m_view.findViewById(R.id.btnRedeem);
        m_tvTitle.setText("Are you sure you\nwant to log out?");
        m_btnOk.setText("Log Out");
        Button m_btnCancel = (Button) m_view.findViewById(R.id.btnCancel);
        View.OnClickListener m_clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLoggedIn(mActivity, false);
                saveLoadedData(mActivity, "responseEventList", "");
                Intent intent = new Intent(mActivity, LoginActivity.class);
                startActivity(intent);
                mActivity.deleteDatabase(ConfidentialsOrderDAO.DATABASE_NAME);
                mActivity.finish();
            }
        };
        m_btnOk.setOnClickListener(m_clickListener);
        m_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });
        m_dialog.setContentView(m_view);
        m_dialog.show();
    }

    boolean isScanned = false;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String barcode = result.getContents();
        Toast.makeText(getActivity(), barcode, Toast.LENGTH_LONG).show();
    }

    protected void dialog_info(Activity activity, String title, String message) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(false);

        View.OnClickListener dialogClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                isScanned = false;
            }
        };

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.message)).setText(message);

        ((View) dialog.findViewById(R.id.okBtn)).setOnClickListener(dialogClick);

        dialog.show();
    }

    String venueID = "";

    private void getVenueDetail() {
        if (haveNetworkConnection(mActivity)) {
            try {
                JSONObject loginResponse = new JSONObject(getLoadedData(mActivity, "loginResponse"));
                JSONObject memberObject = loginResponse.getJSONObject("teammember");
                venueID = memberObject.getString("venueID");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Response.ErrorListener createRequestErrorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                }
            };

            Response.Listener<JSONObject> createRequestSuccessListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, response.toString());
                    try {
                        String success = response.getString("success");
                        if (success.equals("1")) {
                            boolean isActive = response.getJSONObject("result").getBoolean("isActive");
                            isActive = true;
                            if (!isActive) {
                                setLoggedIn(mActivity, false);
                                saveLoadedData(mActivity, "responseEventList", "");
                                Intent intent = new Intent(mActivity, LoginActivity.class);
                                startActivity(intent);
                                mActivity.deleteDatabase(ConfidentialsOrderDAO.DATABASE_NAME);
                                mActivity.finish();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            String URL = URL_Utils.VENUE_DETAIL + "/" + venueID + "/user/" + 0 + "/detail_New ";
            CustomRequest jsObjRequest = new CustomRequest(URL, null, createRequestSuccessListener, createRequestErrorListener);
            VolleySingleton.getInstance().addToRequestQueue(jsObjRequest);
        }


    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            isConfidentialPass = true;
            scanTv.setText("Scan Confidentials pass");
//            Toast.makeText(mActivity, "Scan Confidentials pass!", Toast.LENGTH_SHORT).show();
        } else {
            scanTv.setText("Scan vouchers");
            isConfidentialPass = false;
//            Toast.makeText(mActivity, "Scan vouchers!", Toast.LENGTH_SHORT).show();
        }
    }

    boolean isShowing = true;

    private void hitApiForConfidentialsPass(final String scannedString) {
        String url = URL_Utils.CONFIDENTIALS_PASS + scannedString + "/venue/" + venueID + "/hero/discount";

        MyPref.setId(getActivity(), scannedString);
        hitApiForHeroPass(url);
    }

    int count = 0;

//    private void showDialogForHero(final JSONArray data) {
//        Log.d(TAG, "showDialogForHero: " + count);
//        count++;
//
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        dialog.setContentView(R.layout.fragment_dialog_offer_hero);
//
//        ImageView cancalIv = dialog.findViewById(R.id.imageView);
//        cancalIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.cancel();
//                isShowing = true;
//            }
//        });
//
//        RecyclerView recyclerView = dialog.findViewById(R.id.hero_offers_reccycler);
//
//        List<HeroVoucherModel> list = new ArrayList<>();
//
//        if (data.length() == 0) {
//            for (int i = 0; i < 10; i++) {
//                HeroVoucherModel heroVoucherModel = new HeroVoucherModel();
////                heroVoucherModel.setDiscount("24 ");
////                heroVoucherModel.setTitle("title");
//                list.add(heroVoucherModel);
//            }
//        } else {
//            for (int i = 0; i < data.length(); i++) {
//                try {
//                    JSONObject jsonObject = data.getJSONObject(i);
//
//                    HeroVoucherModel heroVoucherModel = new HeroVoucherModel();
////                    heroVoucherModel.setDiscount(jsonObject.getString("discount"));
////                    heroVoucherModel.setTitle(jsonObject.getString("title"));
////                    heroVoucherModel.setImag(jsonObject.getString("imag"));
//
//                    list.add(heroVoucherModel);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        HeroVouchersAdapter heroVouchersAdapter = new HeroVouchersAdapter(getActivity(), list);
//
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        recyclerView.setAdapter(heroVouchersAdapter);
//
//        if (!dialog.isShowing()) {
//            dialog.show();
//        }
//    }

    public Drawable getRoundRect() {
        RoundRectShape rectShape = new RoundRectShape(new float[]{
                50, 50, 50, 50,
                0, 0, 0, 0
        }, null, null);

        ShapeDrawable shapeDrawable = new ShapeDrawable(rectShape);
        shapeDrawable.getPaint().setColor(Color.parseColor("#F0F0F0"));
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        shapeDrawable.getPaint().setAntiAlias(true);
        shapeDrawable.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
        return shapeDrawable;
    }

    int i = 50;

    public void showBottomSheetForHero(List<HeroVoucherModel.Datum> data) {
        Log.d(TAG, "showDialogForHero: " + count);
        count++;

        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity(), DialogFragment.STYLE_NO_FRAME);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        View child = getLayoutInflater().inflate(R.layout.fragment_bottomsheet_offer_hero, null);
        dialog.setContentView(child);
        final RelativeLayout container = dialog.findViewById(R.id.container_rl);

        container.setBackground(getRoundRect());

        final ImageView topBarIv = dialog.findViewById(R.id.top_bar_iv);


        RecyclerView recyclerView = dialog.findViewById(R.id.hero_offers_reccycler);

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) child.getParent());
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    Log.d(TAG, "State Expanded");
                    i = 0;
                    topBarIv.setVisibility(View.GONE);
                    roundedCornerDrawable(0, container);
                } else if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dialog.dismiss();
                } else {
                    topBarIv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.d(TAG, "onSlide: " + slideOffset);
                if (slideOffset == 0.0) {
                    i = 50;
                    roundedCornerDrawable(i, container);
                } else if (slideOffset > 0.5) {
                    i--;
                    roundedCornerDrawable(i >= 0 ? i : 0, container);
                } else if (slideOffset < 0.5) {
                    i++;
                    roundedCornerDrawable(i <= 50 ? i : 50, container);
                }
            }
        });

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        HeroVouchersAdapter heroVouchersAdapter = new HeroVouchersAdapter(getActivity(), data);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(heroVouchersAdapter);

        if (!dialog.isShowing()) {
            dialog.show();
        }

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isShowing = true;
            }
        });
    }

    private void roundedCornerDrawable(int i, RelativeLayout container) {
        RoundRectShape rectShape = new RoundRectShape(new float[]{
                i, i, i, i,
                0, 0, 0, 0
        }, null, null);
        ShapeDrawable shapeDrawable = new ShapeDrawable(rectShape);
        shapeDrawable.getPaint().setColor(Color.parseColor("#F0F0F0"));
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        shapeDrawable.getPaint().setAntiAlias(true);
        shapeDrawable.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
        container.setBackground(shapeDrawable);
    }
}

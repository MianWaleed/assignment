package com.akkastech.loyaltyscannerapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akkastech.loyaltyscannerapp.R;
import com.akkastech.loyaltyscannerapp.model.BeaconLogsModel;
import com.akkastech.loyaltyscannerapp.utils.FontChangeCrawler;

import java.util.ArrayList;

public class AwardedAdapter extends RecyclerView.Adapter<AwardedAdapter.MyViewHolder> {

    ArrayList<BeaconLogsModel> arrayList;
    Context context;

    public AwardedAdapter(ArrayList<BeaconLogsModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AwardedAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.awarded_item, parent, false);
        return new AwardedAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AwardedAdapter.MyViewHolder myViewHolder, int i) {
        BeaconLogsModel beaconLogsModel = arrayList.get(i);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(View itemView) {
            super(itemView);
            setFont(itemView);
        }
    }

    public void setFont(View itemView) {
        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts((ViewGroup) itemView);
    }
}

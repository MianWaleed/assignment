package com.akkastech.loyaltyscannerapp.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akkastech.loyaltyscannerapp.R;
import com.akkastech.loyaltyscannerapp.utils.AnimationsUtil;
import com.akkastech.loyaltyscannerapp.utils.FontChangeCrawler;
import com.gigamole.library.ShadowLayout;

import static com.akkastech.loyaltyscannerapp.utils.Utils.hideKeyboard;
import static com.akkastech.loyaltyscannerapp.utils.Utils.isValidEmail;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {
    Dialog dialogProgressBar;
    TextView backTv;
    TextView toSignUpTv;
    EditText emailEt, passwordEt;
    ShadowLayout toHomeBt;
    CoordinatorLayout viewGroup;
    RelativeLayout loginCard;
    ImageView loyaltyLogoIv, signInLogoIv, linesIv, giftIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        setFont();
        init();
    }

    private void init() {
        giftIv = findViewById(R.id.imageView7);
        linesIv = findViewById(R.id.imageView6);

        signInLogoIv = findViewById(R.id.imageView8);

        loginCard = findViewById(R.id.login_card);
        AnimationsUtil.cardSlideUp(this, loginCard);

        loyaltyLogoIv = findViewById(R.id.loyalty_logo_iv);
        AnimationsUtil.signInLogoZoomIn(this, loyaltyLogoIv);

        AnimationsUtil.slideInForLeftGift(this, linesIv);
        AnimationsUtil.slideInForRightLines(this, giftIv);
        AnimationsUtil.slideDownLoginSignLogo(ForgotActivity.this, signInLogoIv);

        viewGroup = findViewById(R.id.view_group_login);

        emailEt = findViewById(R.id.email_login_et);
        passwordEt = findViewById(R.id.password_login_et);

        toHomeBt = findViewById(R.id.to_home_login_bt);
        toHomeBt.setOnClickListener(this);

        toSignUpTv = findViewById(R.id.to_sign_up_tv);
        toSignUpTv.setOnClickListener(this);

        backTv = findViewById(R.id.back_tv);
        backTv.setOnClickListener(this);

        AnimationsUtil.slideRightBack(this, backTv);

        showProgressDialog();
    }

    private void setFont() {
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.to_home_login_bt: {
                hideKeyboard(ForgotActivity.this);
                validateTheEditText();
                break;
            }
            case R.id.back_tv: {
                hideKeyboard(ForgotActivity.this);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
            }
            case R.id.to_sign_up_tv: {
                hideKeyboard(ForgotActivity.this);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
            }
        }
    }

    private void validateTheEditText() {
        if (emailEt.getText().toString().trim().length() == 0) {
            showSnackBar("Please enter email!");
        } else if (!isValidEmail(emailEt.getText().toString())) {
            showSnackBar("Please enter a valid email!");
        } else if (passwordEt.getText().toString().trim().length() == 0) {
            showSnackBar("Please enter password!");
        } else if (passwordEt.getText().toString().trim().length() <= 5) {
            showSnackBar("Password should be at-least 6 characters!");
        } else {
            dialogProgressBar.show();
//            hitApiForLogin();
            hitApiViaVolleyForLogin();
        }
    }

    private void hitApiViaVolleyForLogin() {
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void showProgressDialog() {
        dialogProgressBar = new Dialog(ForgotActivity.this);
        dialogProgressBar.setContentView(R.layout.progress_dialog);
        dialogProgressBar.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogProgressBar.setCancelable(false);
    }

    private void showSnackBar(String s) {
        Snackbar.make(viewGroup, s, Snackbar.LENGTH_LONG).show();
    }

    public void toForgotPasswordActivity(View view) {
        try {
            startActivity(new Intent(this, ForgotActivity.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

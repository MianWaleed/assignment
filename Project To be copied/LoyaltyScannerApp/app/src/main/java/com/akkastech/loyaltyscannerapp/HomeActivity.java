package com.akkastech.loyaltyscannerapp;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akkastech.loyaltyscannerapp.fragments.AwardedFragment;
import com.akkastech.loyaltyscannerapp.fragments.HealthFragment;
import com.akkastech.loyaltyscannerapp.utils.FontChangeCrawler;
import com.akkastech.loyaltyscannerapp.utils.KotlinExampleKt;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.io.IOException;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    TextView awardedTv, scannerTv, healthTv;
    ImageButton scannerFab;
    LinearLayout manualCv;
    FrameLayout frameLayout;
    RelativeLayout scannerLayout;

    BarcodeDetector barcodeDetector;
    SurfaceView cameraView;
    CameraSource cameraSource;
    SurfaceHolder holder;
    private String TAG = "HOME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();
        scannerLayout.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
        setFont();


//        KotlinExampleKt.rana();
    }

    private void init() {
        frameLayout = findViewById(R.id.frameLayout);
        scannerLayout = findViewById(R.id.scanner_layout);

        manualCv = findViewById(R.id.manual_cv);
        manualCv.setOnClickListener(this);

        awardedTv = findViewById(R.id.textView4);
        scannerFab = findViewById(R.id.floatingActionButton);
        scannerTv = findViewById(R.id.textView);
        healthTv = findViewById(R.id.textView3);

        awardedTv.setOnClickListener(this);
        scannerFab.setOnClickListener(this);
        scannerTv.setOnClickListener(this);
        healthTv.setOnClickListener(this);

        scannerTv.setTextColor(getResources().getColor(R.color.black));

        allTheScannerThings();
    }

    private void allTheScannerThings() {
        final String[] permissions = {Manifest.permission.CAMERA};
        final String rationale = "Need permission so you can scan QR code!";
        final Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning");


        cameraView = findViewById(R.id.camera_view);
        cameraView.setZOrderMediaOverlay(true);
        holder = cameraView.getHolder();

//        cameraView.invalidate();
        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        if (!barcodeDetector.isOperational()) {
            Toast.makeText(getApplicationContext(), "Sorry, Couldn't setup the detector", Toast.LENGTH_LONG).show();
            this.finish();
        }
        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(24)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1920, 1024)
                .build();

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcode = detections.getDetectedItems();
                if (barcode.size() != 0) {
                    Log.d(TAG, "receiveDetections: "+barcode.valueAt(0).displayValue);
                    Toast.makeText(HomeActivity.this, "" + barcode.valueAt(0).displayValue, Toast.LENGTH_SHORT).show(); }
            }
        });

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(cameraView.getHolder());
                    } else {
                        Permissions.check(HomeActivity.this, permissions, rationale, options, new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                try {
                                    cameraSource.start(cameraView.getHolder());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                                Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });

    }

    private void loadFragment(Fragment fragment) {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingActionButton: {
                //scanner_fab
                scannerTv.setTextColor(getResources().getColor(R.color.black));
                healthTv.setTextColor(getResources().getColor(R.color.default_color_tv));
                awardedTv.setTextColor(getResources().getColor(R.color.default_color_tv));

                awardedTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));
                healthTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));
                scannerTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));

                manualCv.setVisibility(View.VISIBLE);
                scannerLayout.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
                break;
            }
            case R.id.textView: {
                //scanner_tv
                break;
            }
            case R.id.textView4: {
                //awarded_tv
                scannerTv.setTextColor(getResources().getColor(R.color.default_color_tv));
                healthTv.setTextColor(getResources().getColor(R.color.default_color_tv));
                awardedTv.setTextColor(getResources().getColor(R.color.black));

                awardedTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                healthTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));
                scannerTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));

                manualCv.setVisibility(View.GONE);
                scannerLayout.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                loadFragment(new AwardedFragment());

                break;
            }
            case R.id.textView3: {
                //health_tv
                scannerTv.setTextColor(getResources().getColor(R.color.default_color_tv));
                healthTv.setTextColor(getResources().getColor(R.color.black));
                awardedTv.setTextColor(getResources().getColor(R.color.default_color_tv));

                awardedTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));
                healthTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                scannerTv.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.default_color_tv)));

                manualCv.setVisibility(View.GONE);
                scannerLayout.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                loadFragment(new HealthFragment());

//                MyPref.setLogin(HomeActivity.this, false);
//                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
//                finish();
                break;
            }
            case R.id.manual_cv: {
                loadManualQrDialog();
                break;
            }
        }
    }

    private void loadManualQrDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_manual_qr_code);

        CardView viewGroup = dialog.findViewById(R.id.view_group);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts(viewGroup);
        dialog.show();
    }

    private void setFont() {
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }
}

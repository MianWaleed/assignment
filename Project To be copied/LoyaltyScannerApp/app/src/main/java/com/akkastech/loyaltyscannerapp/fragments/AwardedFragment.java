package com.akkastech.loyaltyscannerapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akkastech.loyaltyscannerapp.R;
import com.akkastech.loyaltyscannerapp.adapter.AwardedAdapter;
import com.akkastech.loyaltyscannerapp.model.BeaconLogsModel;

import java.util.ArrayList;

public class AwardedFragment extends Fragment {

    View view;
    ConstraintLayout viewGroup;
    RecyclerView awardedRecycler;
    ArrayList<BeaconLogsModel> arrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.awarded_fragment, container, false);

        init();

        return view;
    }

    private void init() {
        awardedRecycler = view.findViewById(R.id.recyclerView);
        arrayList = new ArrayList<>();
        loadDummyData();
    }


    private void loadDummyData() {
        for (int p = 0; p < 10; p++) {
            BeaconLogsModel beaconLogsModel = new BeaconLogsModel();
            beaconLogsModel.setName("Name " + p);
            arrayList.add(beaconLogsModel);
        }

        AwardedAdapter awardedAdapter = new AwardedAdapter(arrayList, getActivity());
        awardedRecycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        awardedRecycler.setAdapter(awardedAdapter);
    }
}

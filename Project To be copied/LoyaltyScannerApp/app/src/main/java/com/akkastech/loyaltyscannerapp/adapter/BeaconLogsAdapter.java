package com.akkastech.loyaltyscannerapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akkastech.loyaltyscannerapp.R;
import com.akkastech.loyaltyscannerapp.model.BeaconLogsModel;
import com.akkastech.loyaltyscannerapp.utils.FontChangeCrawler;

import java.util.ArrayList;

public class BeaconLogsAdapter extends RecyclerView.Adapter<BeaconLogsAdapter.MyViewHolder> {

    ArrayList<BeaconLogsModel> arrayList;
    Context context;

    public BeaconLogsAdapter(ArrayList<BeaconLogsModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public BeaconLogsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.beacon_logs_item, parent, false);
        return new BeaconLogsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BeaconLogsAdapter.MyViewHolder myViewHolder, int i) {
        BeaconLogsModel beaconLogsModel = arrayList.get(i);

        myViewHolder.nameTv.setText(beaconLogsModel.getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTv;

        public MyViewHolder(View itemView) {
            super(itemView);

            nameTv = itemView.findViewById(R.id.textView14);
            setFont(itemView);
        }
    }

    public void setFont(View itemView) {
        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts((ViewGroup) itemView);
    }
}

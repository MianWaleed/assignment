package com.akkastech.loyaltyscannerapp.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.akkastech.loyaltyscannerapp.R;


/**
 * Created by Muzzamil Saleem on 4/5/2018.
 */

public class AnimationsUtil {

    public static void slideDown(final View view) {
        view.animate()
                .translationY(view.getHeight())
                .alpha(0.f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // superfluous restoration
                        view.setVisibility(View.GONE);
                        view.setAlpha(1.f);
                        view.setTranslationY(0.f);
                    }
                });
    }

    public static void slideUp(final View view) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.f);

        if (view.getHeight() > 0) {
            slideUpNow(view);
        } else {
            // wait till height is measured
            view.post(new Runnable() {
                @Override
                public void run() {
                    slideUpNow(view);
                }
            });
        }
    }

    public static void slideFromRight(final View view) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.f);

        if (view.getHeight() > 0) {
            slideRightToLeft(view);
        } else {
            // wait till height is measured
            view.post(new Runnable() {
                @Override
                public void run() {
                    slideRightToLeft(view);
                }
            });
        }
    }

    public static void slideFromLeft(final View view) {
//        view.setVisibility(View.VISIBLE);
        view.setAlpha(1.f);

        if (view.getHeight() > 0) {
            slideLeftToRight(view);
        } else {
            // wait till height is measured
            view.post(new Runnable() {
                @Override
                public void run() {
                    slideLeftToRight(view);
                }
            });
        }
    }

    private static void slideLeftToRight(final View view) {
        view.setTranslationX(view.getHeight());
        view.animate()
                .translationX(5)
                .alpha(-1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
//                        view.setVisibility(View.GONE);
                        view.setAlpha(0f);
                    }
                });
    }


    private static void slideUpNow(final View view) {
        view.setTranslationY(view.getHeight());
        view.animate()
                .translationY(0)
                .alpha(1.f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                        view.setAlpha(1.f);
                    }
                });
    }

    private static void slideRightToLeft(final View view) {
        view.setTranslationX(view.getHeight());
        view.animate()
                .translationX(0)
                .alpha(1.f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                        view.setAlpha(1.f);
                    }
                });
    }

    public static void slideRightBack(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_right);
        viewToAnimate.startAnimation(animation);
    }

    public static void slideDownLoginSignLogo(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slidedown_out);
        viewToAnimate.startAnimation(animation);
    }


    public static void animateItemFromLeft(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.left_in_with_right);
        viewToAnimate.startAnimation(animation);
    }

    public static void animateItemFromRight(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.right_in_with_left);
        viewToAnimate.startAnimation(animation);
    }

    public static void slideInUp(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slidedown_in);
        viewToAnimate.startAnimation(animation);
    }

    public static void slideInForLeftGift(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_from_left);
        viewToAnimate.startAnimation(animation);
    }

    public static void slideInForRightLines(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.from_the_other_sode);
        viewToAnimate.startAnimation(animation);
    }

    public static void wobbleAnimation(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.wobble_aniamtion);
        viewToAnimate.startAnimation(animation);
    }

    public static void animateZoomIn(final Context mContext, final View zoomIn) {
//        zoomIn.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                zoomIn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in);
//                zoomIn.startAnimation(animation);
//            }
//        });
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in);
        zoomIn.startAnimation(animation);
    }

    public static void signInLogoZoomIn(final Context mContext, final View zoomIn) {
//        zoomIn.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                zoomIn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in);
//                zoomIn.startAnimation(animation);
//            }
//        });
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.login_zoom_in);
        zoomIn.startAnimation(animation);
    }


    public static void animateItemFromBottom(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.up_from_bottom);
        viewToAnimate.startAnimation(animation);
    }

    public static void animateItemShake(Context mContext, View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.shake);
        int random = (int) (Math.random() / 70f);
        animation.setStartOffset(random);
        animation.setDuration(350 + random);
        viewToAnimate.startAnimation(animation);
    }

    @SuppressLint("NewApi")
    public static void animateViewFromRight(final Context mContext, final View v) {
        v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.right_in_with_left);
                v.startAnimation(animation);
            }
        });
    }

    @SuppressLint("NewApi")
    public static void arrowFromRight(final Context mContext, final View v) {
        v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.arrow_from_right);
                v.startAnimation(animation);
            }
        });
    }

    @SuppressLint("NewApi")
    public static void arrowFromLeft(final Context mContext, final View v) {
        v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.arrow_from_left);
                v.startAnimation(animation);
            }
        });
    }


    @SuppressLint("NewApi")
    public static void animateViewFromRightLines(final Context mContext, final View v) {
        v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.lines_from_right);
                animation.setStartOffset(1000);
                v.startAnimation(animation);
            }
        });
    }


    @SuppressLint("NewApi")
    public static void animateViewFromLeft(final Context mContext, final View v) {
        v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.left_in_with_right);
                v.startAnimation(animation);
            }
        });
    }

    public static void cardSlideUp(final Context mContext, final View v) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.card_slide_up);
        v.startAnimation(animation);
    }


    public static void animateBounce(final View v, int delay) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(v, "scaleX", 0f, 1.2f).setDuration(150)
        );
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(v, "scaleY", 0f, 1.2f).setDuration(150)
        );

        AnimatorSet animatorSet1 = new AnimatorSet();

        animatorSet1.playTogether(
                ObjectAnimator.ofFloat(v, "scaleX", 1.2f, 1.0f).setDuration(150)
        );
        animatorSet1.playTogether(
                ObjectAnimator.ofFloat(v, "scaleY", 1.2f, 1f).setDuration(150)
        );

        AnimatorSet bounce = new AnimatorSet();
        bounce.playSequentially(animatorSet, animatorSet1);
        bounce.setStartDelay(delay);
        bounce.start();
    }

    public static void expandWithAnimator(final View view) {
        //set Visible
        view.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(0, view.getMeasuredHeight(), view);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                view.setLayoutParams(layoutParams);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        mAnimator.start();
    }

    public static void collapseWithAnimator(final View view) {
        int finalHeight = view.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0, view);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimator.start();
    }

    private static ValueAnimator slideAnimator(int start, int end, final View view) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = value;
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }


    @SuppressLint("NewApi")
    public static void animateBounceFromBottom(final View v, long delay) {
        v.setTranslationY(1090);
        v.animate().translationY(-50).setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(500).setStartDelay(delay)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        v.animate().translationY(0).setDuration(200)
                                .setInterpolator(new AccelerateDecelerateInterpolator())
                                .setStartDelay(0)
                                .start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }


    public static void animateDownWithAlpha(final View v) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(v, "translationY", 20).setDuration(1000)
        );

        animatorSet.playTogether(ObjectAnimator.ofFloat(v, View.ALPHA, 0, 0.8f).setDuration(800));

        // animatorSet.setDuration(800);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                AnimatorSet scaleUp = new AnimatorSet();
                scaleUp.playTogether(
                        ObjectAnimator.ofFloat(v, "translationY", 0)
                );
                scaleUp.playTogether(ObjectAnimator.ofFloat(v, View.ALPHA, 0.8f, 1.0f));

                scaleUp.setDuration(200);
                scaleUp.setInterpolator(new AccelerateDecelerateInterpolator());
                scaleUp.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorSet.start();

    }


    @SuppressLint("NewApi")
    public static void animateBounceFromLeft(final View v, int duration, long delay) {
        v.animate().translationX(20).setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(duration).setStartDelay(delay)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        v.animate().translationX(0).setDuration(200)
                                .setInterpolator(new AccelerateDecelerateInterpolator())
                                .setStartDelay(0)
                                .start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    @SuppressLint("NewApi")
    public static void animateBounceFromRight(final View v, int duration, long delay) {
        v.animate().translationX(-20).setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(duration).setStartDelay(delay)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        v.animate().translationX(0).setDuration(200)
                                .setInterpolator(new AccelerateDecelerateInterpolator())
                                .setStartDelay(0)
                                .start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    public static void animateWithAlpha(final View v, long duration, int delay, float alpha) {
        AnimatorSet animatorSet = new AnimatorSet();
        v.setAlpha(0f);
        animatorSet.playTogether(ObjectAnimator.ofFloat(v, View.ALPHA, 0, alpha));
        animatorSet.setDuration(duration).setStartDelay(delay);
        // animatorSet.setDuration(800);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());

        animatorSet.start();

    }

    @SuppressLint("NewApi")
    public static void animateBounceFromTop(final View v, int duration, long delay) {
        v.animate().translationY(30).setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(duration).setStartDelay(delay)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        v.animate().translationY(0).setDuration(200)
                                .setInterpolator(new DecelerateInterpolator())
                                .setStartDelay(0)
                                .start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }


}

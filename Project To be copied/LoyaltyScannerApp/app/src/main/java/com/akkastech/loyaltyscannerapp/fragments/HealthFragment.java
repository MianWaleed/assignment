package com.akkastech.loyaltyscannerapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akkastech.loyaltyscannerapp.R;
import com.akkastech.loyaltyscannerapp.adapter.BeaconLogsAdapter;
import com.akkastech.loyaltyscannerapp.model.BeaconLogsModel;
import com.akkastech.loyaltyscannerapp.utils.FontChangeCrawler;

import java.util.ArrayList;

public class HealthFragment extends Fragment {
    View view;
    ConstraintLayout viewGroup;
    ArrayList<BeaconLogsModel> arrayList;
    RecyclerView beaconLogRecycler;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.health_fragment, container, false);

        init();
        setFont();
        return view;
    }

    private void init() {
        arrayList = new ArrayList<>();
        viewGroup = view.findViewById(R.id.view_group);

        beaconLogRecycler = view.findViewById(R.id.beacon_log_recycler);

        loadDummyData();
    }

    private void loadDummyData() {
        for (int p = 0; p < 10; p++) {
            BeaconLogsModel beaconLogsModel = new BeaconLogsModel();
            beaconLogsModel.setName("Name " + p);
            arrayList.add(beaconLogsModel);
        }

        BeaconLogsAdapter beaconLogsAdapter = new BeaconLogsAdapter(arrayList, getActivity());
        beaconLogRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        beaconLogRecycler.setAdapter(beaconLogsAdapter);
        beaconLogRecycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    private void setFont() {
        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts(viewGroup);
    }
}
package com.akkastech.loyaltyscannerapp.network;


import com.akkastech.loyaltyscannerapp.model.LoginSignUpModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("login/vendor")
    Call<LoginSignUpModel> loginWithEmailAndPassword(@Field("email") String email,
                                                     @Field("password") String password);

}

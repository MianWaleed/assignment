package com.akkastech.loyaltyscannerapp.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.akkastech.loyaltyscannerapp.HomeActivity;
import com.akkastech.loyaltyscannerapp.R;
import com.akkastech.loyaltyscannerapp.model.LoginSignUpModel;
import com.akkastech.loyaltyscannerapp.network.ApiClient;
import com.akkastech.loyaltyscannerapp.network.ApiInterface;
import com.akkastech.loyaltyscannerapp.utils.AnimationsUtil;
import com.akkastech.loyaltyscannerapp.utils.FontChangeCrawler;
import com.akkastech.loyaltyscannerapp.utils.MyPref;
import com.gigamole.library.ShadowLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.akkastech.loyaltyscannerapp.utils.Utils.hideKeyboard;
import static com.akkastech.loyaltyscannerapp.utils.Utils.isValidEmail;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Dialog dialogProgressBar;
    EditText emailEt, passwordEt;
    ShadowLayout toHomeBt;
    CoordinatorLayout viewGroup;

    RelativeLayout loginCard;
    ImageView loyaltyLogoIv, signInLogoIv, linesIv, giftIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setFont();
        init();
        isLogin();
    }

    private void isLogin() {
        if (MyPref.getLogin(this)) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        }
    }

    private void init() {
        giftIv = findViewById(R.id.imageView7);
        linesIv = findViewById(R.id.imageView6);
        signInLogoIv = findViewById(R.id.imageView8);
        loginCard = findViewById(R.id.login_card);
        loyaltyLogoIv = findViewById(R.id.loyalty_logo_iv);

        AnimationsUtil.cardSlideUp(this, loginCard);
        AnimationsUtil.signInLogoZoomIn(this, loyaltyLogoIv);
        AnimationsUtil.slideInForLeftGift(this, linesIv);
        AnimationsUtil.slideInForRightLines(this, giftIv);
        AnimationsUtil.slideDownLoginSignLogo(LoginActivity.this, signInLogoIv);

        viewGroup = findViewById(R.id.view_group_login);

        emailEt = findViewById(R.id.email_login_et);
        passwordEt = findViewById(R.id.password_login_et);

        toHomeBt = findViewById(R.id.to_home_login_bt);
        toHomeBt.setOnClickListener(this);

        showProgressDialog();
    }

    private void setFont() {
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "prox_reg.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.to_home_login_bt: {
                hideKeyboard(LoginActivity.this);
                validateTheEditText();
                break;
            }
        }
    }

    private void validateTheEditText() {
        if (emailEt.getText().toString().trim().length() == 0) {
            showSnackBar("Please enter email!");
        } else if (!isValidEmail(emailEt.getText().toString())) {
            showSnackBar("Please enter a valid email!");
        } else if (passwordEt.getText().toString().trim().length() == 0) {
            showSnackBar("Please enter password!");
        } else if (passwordEt.getText().toString().trim().length() <= 5) {
            showSnackBar("Password should be at-least 6 characters!");
        } else {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
//            dialogProgressBar.show();
//            hitApiForLogin();
        }
    }

    private void hitApiForLogin() {
        ApiInterface apiInterface = ApiClient.getApiClientGsonConverter().create(ApiInterface.class);
        Call<LoginSignUpModel> call = apiInterface.loginWithEmailAndPassword(emailEt.getText().toString(), passwordEt.getText().toString());
        call.enqueue(new Callback<LoginSignUpModel>() {
            @Override
            public void onResponse(Call<LoginSignUpModel> call, Response<LoginSignUpModel> response) {
                try {
                    if (response.body().getStatusCode() == 200) {
                        if (response.body().getMessage().equals("success")) {
                            showSnackBar("Successful. Welcome back " + response.body().getUser().getName());
                            dialogProgressBar.dismiss();
                            MyPref.setLoginDetails(LoginActivity.this, response.body());
                            MyPref.setLogin(LoginActivity.this, true);
                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            finish();
                        } else {
                            dialogProgressBar.dismiss();
                            showSnackBar("An error occurred!");
                        }
                    } else if (response.body().getStatusCode() == 301) {
                        dialogProgressBar.dismiss();
                        showSnackBar("Invalid user or password!");
                    }
                } catch (Exception e) {
                    showSnackBar("Access denied. Email or password is wrong!");
                    dialogProgressBar.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginSignUpModel> call, Throwable t) {

            }
        });
    }

    private void showProgressDialog() {
        dialogProgressBar = new Dialog(LoginActivity.this);
        dialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProgressBar.setContentView(R.layout.progress_dialog);
        dialogProgressBar.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogProgressBar.setCancelable(false);
    }

    private void showSnackBar(String s) {
        Snackbar.make(viewGroup, s, Snackbar.LENGTH_LONG).show();
    }

    public void toHome(View view) {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    public void toForgotPasswordActivity(View view) {
        startActivity(new Intent(this, ForgotActivity.class));
        finish();
    }
}

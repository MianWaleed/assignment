package com.akkastech.loyaltyscannerapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.akkastech.loyaltyscannerapp.model.LoginSignUpModel;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by saif on 9/21/2016.
 */
public class MyPref {
    private static final String PREFS_NAME = "com.usmankhawar.sp1";

    public static void setFav(Context context, String[] favList) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("fav", String.valueOf(favList)).apply();
    }

    public static String getFav(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("fav", "");
    }

    public static void setLoginDetails(Context context, LoginSignUpModel loginSignUpModel) {
        SharedPreferences mPrefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(loginSignUpModel);
        prefsEditor.putString("MyObject", json);
        prefsEditor.commit();
    }

    public static LoginSignUpModel getLoginDetails(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("MyObject", "");
        LoginSignUpModel obj = gson.fromJson(json, LoginSignUpModel.class);
        return obj;
    }

    public static void removeAll(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().clear().apply();
    }

    public static void setId(Context context, int id) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putInt("user_id", id).apply();
    }

    public static int getId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getInt("user_id", 0);
    }

    public static void setWidth(Context context, int width) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putInt("width", width).apply();
    }

    public static int getWidth(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getInt("width", 0);
    }

    public static void setUsername(Context context, String username) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("username", username).apply();
    }

    public static String getUsername(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("username", "");
    }

    public static void setLogin(Context context, boolean login) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putBoolean("isUserlogin", login).apply();
    }

    public static Boolean getLogin(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getBoolean("isUserlogin", false);
    }

    public static void setPhoto(Context context, String photo) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("photo", photo).apply();
    }

    public static String getPhoto(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("photo", "");
    }

    public static void setPhone(Context context, String phone) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("phone", phone).apply();
    }

    public static String getPhone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("phone", "");
    }

    public static void setCountry(Context context, String country) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("country", country).apply();
    }

    public static String getCountry(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("country", "");
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("email", email).apply();
    }

    public static String getEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("email", "");
    }

    public static void setCategoryJson(Context context, String category) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("category", category).apply();
    }

    public static String getCategoryJson(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("category", "");
    }

    public static void setMaxPrice(Context context, String price) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("maxPrice", price).apply();
    }

    public static String getMaxPrice(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("maxPrice", "");
    }

    public static void setURL(Context context, String url) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("url", url).apply();
    }

    public static String getURL(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("url", "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("token", token).apply();
    }

    public static String getToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("token", "");
    }

    public static void setSocialLogin(Context context, String social) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit().putString("social", social).apply();
    }

    public static String getSocialLogin(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("social", "");
    }
}

package screen.green.chroma;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static screen.green.chroma.RealPathUtil.getFilePath;

public class Main extends AppCompatActivity {

    final int  STORAGE_PERMISSION = 2;
    ImageView getStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);

        Appodeal.disableNetwork(getApplicationContext(), "unity_ads");
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(Main.this, constants.API_KEY_ADS, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO | Appodeal.BANNER | Appodeal.NATIVE | Appodeal.MREC);

        Appodeal.show(Main.this, Appodeal.BANNER_BOTTOM);

        setContentView(R.layout.activity_main);

        getStarted = (ImageView) findViewById(R.id.select);



        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askPermission(Manifest.permission.READ_EXTERNAL_STORAGE, STORAGE_PERMISSION);// Delete
//                Appodeal.show(Main.this, Appodeal.INTERSTITIAL);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.settings:
                Intent intent = new Intent(this, Settings.class);
                startActivity(intent);
                overridePendingTransitionExit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void pickImageFromGallery() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),1);
    }

    public void askPermission(String permission, int requestCode){
        if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        } else {
            pickImageFromGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case STORAGE_PERMISSION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.d("Granted", "Granted");
                } else {
                    Log.d("Not Granted", "Not Granted");
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {
                Uri currImageURI = data.getData();

                try {
                    Intent intent = new Intent(this, Magic.class);
                    intent.putExtra("path", getFilePath(this, currImageURI));
                    startActivity(intent);
                    overridePendingTransitionExit();
                    Log.d("Path", getFilePath(this, currImageURI));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

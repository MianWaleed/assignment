package screen.green.chroma;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Magic extends AppCompatActivity {

    ImageView imageView;
    int PICK_PHOTO_FOR_AVATAR = 1,  PICK_GALLERY = 2;
    GridView listView;
    private static final int CAMERA_REQUEST = 1888;
    Bitmap icon, foreground;
    int[] images = {R.drawable.beaches, R.drawable.space, R.drawable.feilds, R.drawable.gallery};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.setTitle("Magic happens here!!");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magic);
        listView = (GridView) findViewById(R.id.listView);
        imageView = (ImageView) findViewById(R.id.image);

        Appodeal.disableNetwork(getApplicationContext(), "unity_ads");
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(Magic.this, constants.API_KEY_ADS, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO | Appodeal.BANNER | Appodeal.NATIVE | Appodeal.MREC);

        Appodeal.show(Magic.this, Appodeal.BANNER_BOTTOM);
        Appodeal.show(Magic.this, Appodeal.INTERSTITIAL);
        String path = getIntent().getStringExtra("path");

        File imgFile = new  File(path);
        if(imgFile.exists()) {
            imageView.setImageURI(Uri.fromFile(imgFile));
        }

        icon = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        this.foreground = (icon);

        CustomAdaper customAdaper = new CustomAdaper();
        listView.setAdapter(customAdaper);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 3){
                    pickImage();
                } else {
                    if(Settings.selected){
                        final ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView);
                        final BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView1.getDrawable();
                        final Bitmap yourBitmap = bitmapDrawable.getBitmap();
                        Bitmap resized = ThumbnailUtils.extractThumbnail(yourBitmap, foreground.getWidth(), foreground.getHeight());
                        imageView.setImageBitmap(overlay( resized, replaceGreenByTransparent(icon) ));
                        Log.d("TAG", "green");
                    } else {
                        final ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView);
                        final BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView1.getDrawable();
                        final Bitmap yourBitmap = bitmapDrawable.getBitmap();
                        Bitmap resized = ThumbnailUtils.extractThumbnail(yourBitmap, foreground.getWidth(), foreground.getHeight());
                        imageView.setImageBitmap(overlay( resized, replaceBlueByTransparent(icon) ));
                        Log.d("TAG", "blue");
                    }
                }
            }
        });

    }

    private Bitmap replaceGreenByTransparent(Bitmap bitmap) {
        if (bitmap == null) {
            throw new IllegalStateException("Bitmap must not be null");

        }
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        int[] pix = new int[(bitmapWidth * bitmapHeight)];
        bitmap.getPixels(pix, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        for (int y = 0; y < bitmapHeight; y++) {
            for (int x = 0; x < bitmapWidth; x++) {
                int index = (y * bitmapWidth) + x;
                int r = (pix[index] >> 16) & 255;
                int g = (pix[index] >> 8) & 255;
                int b = pix[index] & 255;
                if (g > 32 && ((double) g) > ((double) r) * 1.1d && ((double) g) > ((double) b) * 1.1d) {
                    pix[index] = 0;
                }
            }
        }
        return Bitmap.createBitmap(pix, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    private Bitmap replaceBlueByTransparent(Bitmap bitmap) {
        if (bitmap == null) {
            throw new IllegalStateException("Bitmap must not be null");

        }
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        int[] pix = new int[(bitmapWidth * bitmapHeight)];
        bitmap.getPixels(pix, 0, bitmapWidth, 0, 0, bitmapWidth, bitmapHeight);
        for (int y = 0; y < bitmapHeight; y++) {
            for (int x = 0; x < bitmapWidth; x++) {
                int index = (y * bitmapWidth) + x;
                int r = (pix[index] >> 16) & 255;
                int g = (pix[index] >> 8) & 255;
                int b = pix[index] & 255;
                if (b > 32 && ((double) b) > ((double) r) * 1.1d && ((double) b) > ((double) g) * 1.1d) {
                    pix[index] = 0;
                }
            }
        }
        return Bitmap.createBitmap(pix, bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
    }

    private Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0.0f, 0.0f, null);
        return bmOverlay;
    }

    class CustomAdaper extends BaseAdapter {

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.list_row, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            imageView.setImageResource(images[position]);
            return view;
        }
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                Bitmap resized = ThumbnailUtils.extractThumbnail(bitmap, foreground.getWidth(), foreground.getHeight());

                if(Settings.selected){
                    imageView.setImageBitmap(overlay( resized, replaceGreenByTransparent(icon) ));
                    Log.d("TAG", "green");
                } else {
                    imageView.setImageBitmap(overlay( resized, replaceBlueByTransparent(icon) ));
                    Log.d("TAG", "blue");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_GALLERY) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                icon = bitmap;
                imageView.setImageBitmap(bitmap);
                this.foreground = (icon);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap picture = (Bitmap) data.getExtras().get("data");//this is your bitmap image and now you can do whatever you want with this
            imageView.setImageBitmap(picture); //for example I put bmp in an ImageView
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.save:
                if(null!=imageView.getDrawable()){
                    Appodeal.show(Magic.this, Appodeal.INTERSTITIAL);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    String currentDateandTime = sdf.format(new Date());
                    Bitmap bm = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
                    saveImage(bm, currentDateandTime);

                } else {
                    Toast.makeText(Magic.this, "Select any image!", Toast.LENGTH_SHORT).show();
                }
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    private void saveImage(Bitmap finalBitmap, String image_name) {

        String root = Environment.getExternalStorageDirectory().toString()+"/Chroma";
        File myDir = new File(root);
        myDir.mkdirs();
        String fname = "Image-" + image_name+ ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        Log.i("LOAD", root + fname);
        Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
        scanFile(file.getAbsolutePath());
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scanFile(String path) {

        MediaScannerConnection.scanFile(Magic.this,
                new String[] { path }, null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("TAG", "Finished scanning " + path);
                    }
                });
    }
}


package screen.green.chroma;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Settings extends AppCompatActivity {

    CheckBox green, blue;
    static public boolean selected = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Settings");
        setContentView(R.layout.activity_settings);

        green = (CheckBox) findViewById(R.id.green);
        blue = (CheckBox) findViewById(R.id.blue);

        Appodeal.disableNetwork(getApplicationContext(), "unity_ads");
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(Settings.this, constants.API_KEY_ADS, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO | Appodeal.BANNER | Appodeal.NATIVE | Appodeal.MREC);

        Appodeal.show(Settings.this, Appodeal.BANNER_BOTTOM);


        green.setChecked(true);

        green.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    selected = true;
                    blue.setChecked(false);
                }
            }
        });
        blue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    selected = false;
                    green.setChecked(false);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Appodeal.show(Settings.this, Appodeal.INTERSTITIAL);
        overridePendingTransitionExit();

    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}

package bhatti.muhammadirfan.emghealthapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Activity_patientSignUp extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_sign_up);
    }


    public void onButtonClick(View v) {

        //buttonDoc = (Button) findViewById(R.id.buttonDoc);
        // buttonPatient = (Button)findViewById(R.id.buttonPatient);

        if (v.getId() == R.id.buttonEnterPSUP) {

            EditText name = (EditText) findViewById(R.id.eTNPSUP);
            EditText age = (EditText) findViewById(R.id.eTAPSUP);
            EditText sex = (EditText) findViewById(R.id.eTSPSUP);
            EditText weight = (EditText) findViewById(R.id.eTWPSUP);
            EditText pass1 = (EditText) findViewById(R.id.eTPPSUP);
            EditText pass2 = (EditText) findViewById(R.id.eTCPPSUP);


            String namestr = name.getText().toString();
            String agestr = age.getText().toString();
            String sexstr = sex.getText().toString();
            String weightstr = weight.getText().toString();
            String pass1str = pass1.getText().toString();
            String pass2str = pass2.getText().toString();

            if (!pass1str.equals(pass2str)) {
                //ppoup mesg

                Toast pass = Toast.makeText(Activity_patientSignUp.this, "Password don´t match", Toast.LENGTH_SHORT);
                pass.show();
            }

            else {
                //insert the details in database
                Contact c = new Contact();
                c.setName(namestr);
                c.setAge(agestr);
                c.setSex(sexstr);
                c.setWeight(weightstr);
                c.setPass(pass1str);


                helper.insertContact(c);


                Intent i = new Intent(Activity_patientSignUp.this, ActivityPatient1.class);
                startActivity(i);
            }


            //Intent i = new Intent(Activity_patientSignUp.this, ActivityPatient1.class);
            //startActivity(i);
        }


    }
}

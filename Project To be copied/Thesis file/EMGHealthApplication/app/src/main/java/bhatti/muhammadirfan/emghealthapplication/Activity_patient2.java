package bhatti.muhammadirfan.emghealthapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity_patient2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient2);
    }

    public void onButtonClick(View v) {

       // buttonDoc = (Button) findViewById(R.id.buttonDoc);
        //buttonPatient = (Button)findViewById(R.id.buttonPatient);

        if (v.getId() == R.id.buttonOldgraphP2) {

            Intent i = new Intent(Activity_patient2.this, Activitypatient21.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonOldtableP2) {

            Intent i = new Intent(Activity_patient2.this, Activitypatient22.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonNewgraphP2) {

            Intent i = new Intent(Activity_patient2.this, Activitypatient23.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonNewtableP2) {

            Intent i = new Intent(Activity_patient2.this, Activitypatient24.class);
            startActivity(i);
        }

    }
}

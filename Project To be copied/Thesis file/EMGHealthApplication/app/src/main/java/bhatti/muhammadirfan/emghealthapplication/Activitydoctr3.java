package bhatti.muhammadirfan.emghealthapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Activitydoctr3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitydoctr3);
    }


    public void onButtonClick(View v) {

        //buttonDoc = (Button) findViewById(R.id.buttonDoc);
        //  buttonPatient = (Button)findViewById(R.id.buttonPatient);

        if (v.getId() == R.id.buttonOldgraphD3) {

            Intent i = new Intent(Activitydoctr3.this, Activitydoctr31.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonOldtableD3) {

            Intent i = new Intent(Activitydoctr3.this, Activitydoctr32.class);
            startActivity(i);
        }
        if (v.getId() == R.id.buttonNewgraphD3) {

            Intent i = new Intent(Activitydoctr3.this, Activitydoctr33.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonNewtableD3) {

            Intent i = new Intent(Activitydoctr3.this, Activitydoctr34.class);
            startActivity(i);
        }
        if (v.getId() == R.id.buttonSettingD3) {

            Intent i = new Intent(Activitydoctr3.this, Activitydoctr35.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonResetD3) {

            Intent i = new Intent(Activitydoctr3.this, Activitydoctr36.class);
            startActivity(i);
        }

    }
}

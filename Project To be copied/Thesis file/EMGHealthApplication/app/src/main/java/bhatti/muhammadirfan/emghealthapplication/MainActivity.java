package bhatti.muhammadirfan.emghealthapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
private Button buttonDoc;
private Button buttonPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //buttonDoc = (Button) findViewById(R.id.buttonDoc);
          //buttonPatient = (Button)findViewById(R.id.buttonPatient);
    }
    public void onButtonClick(View v) {

        buttonDoc = (Button) findViewById(R.id.buttonDoc);
        buttonPatient = (Button)findViewById(R.id.buttonPatient);

        if (v.getId() == R.id.buttonDoc) {

            Intent i = new Intent(MainActivity.this, ActivityDoctr1.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonPatient) {

            Intent i = new Intent(MainActivity.this, ActivityPatient1.class);
            startActivity(i);
        }

    }
}

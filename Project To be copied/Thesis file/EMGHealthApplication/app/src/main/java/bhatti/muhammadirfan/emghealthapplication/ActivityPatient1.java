package bhatti.muhammadirfan.emghealthapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityPatient1 extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient1);
    }


    public void onButtonClick(View v) {

        //buttonDoc = (Button) findViewById(R.id.buttonDoc);
       // buttonPatient = (Button)findViewById(R.id.buttonPatient);

        if (v.getId() == R.id.buttonLoginP1) {

            EditText a = (EditText) findViewById(R.id.eTNP1);
            String str = a.getText().toString();

            EditText b = (EditText) findViewById(R.id.eTPP1);
            String pass = b.getText().toString();


            String password = helper.searchPass(str);
            if (pass.equals(password)) {

                Intent i = new Intent(ActivityPatient1.this, Activity_patient2.class);
                i.putExtra("Username", str);
                startActivity(i);
            }
            else


            {

                Toast temp = Toast.makeText(ActivityPatient1.this, " Username and Password don´t match", Toast.LENGTH_SHORT);
                temp.show();

            }




        }

        if (v.getId() == R.id.buttonSignUpP1) {

            Intent i = new Intent(ActivityPatient1.this, Activity_patientSignUp.class);
            startActivity(i);
        }

    }
}

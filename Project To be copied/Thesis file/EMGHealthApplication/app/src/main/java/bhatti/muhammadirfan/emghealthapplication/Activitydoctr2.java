package bhatti.muhammadirfan.emghealthapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activitydoctr2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitydoctr2);
    }


    public void onButtonClick(View v) {

        //buttonDoc = (Button) findViewById(R.id.buttonDoc);
      //  buttonPatient = (Button)findViewById(R.id.buttonPatient);

        if (v.getId() == R.id.buttonEnterD2) {

            Intent i = new Intent(Activitydoctr2.this, Activitydoctr3.class);
            startActivity(i);
        }

        if (v.getId() == R.id.buttonSkipD2) {

            Intent i = new Intent(Activitydoctr2.this, Activitydoctr3.class);
            startActivity(i);
        }

    }
}

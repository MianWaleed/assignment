package bhatti.muhammadirfan.emghealthapplication;

import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Activitydoctr33 extends AppCompatActivity {


    Button button_sent;
    EditText smessage;
    TextView chat, display_status;
    String str, msg = "";
    int serverport = 5454;
    ServerSocket serverSocket;
    //Socket client;
    Handler handler = new Handler();
    WifiManager wmanager;

    private LineGraphSeries<DataPoint> series;


    public static final int SERVERPORT = 1336;
    public static final String SERVERIP = "192.168.4.1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitydoctr33);


        // we get graph view instant
        GraphView graph = (GraphView) findViewById(R.id.graph);

        // data
        series = new LineGraphSeries<DataPoint>();
        graph.addSeries(series);

        // customize a littel bit viewport
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(0);
        viewport.setMaxY(1100);
        viewport.setScrollable(true);
        viewport.setScalable(true);
        //  viewport.setScalableY(true);
        //  viewport.setScrollableY(true);
        series.setBackgroundColor(Color.BLACK);


        Thread serverThread = new Thread(new serverThread());
        serverThread.start();
    }

    StringBuilder c;

    public class serverThread implements Runnable {
        public int lastX = 0;
        Socket socket = null;

        @Override
        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(SERVERIP);
                socket = new Socket(serverAddr, SERVERPORT);

                while (true) {
                    c = new StringBuilder();


                    /*******************************************s
                     setup i/p streams
                     ******************************************/
                    // DataInputStream in = new DataInputStream(client.getInputStream());
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    int line;


                    while ((line = in.read()) != -1) {


                        // new code add by me
                        char l = (char) line; // Ascii int into char conversion
                        String s = String.valueOf(l); // char into string conversion
                        // msg = msg  + (char)line   ;
                        if (s.equals(",")) {
                            //  final int k = Integer.valueOf(c.toString());
                            // float  f = 5* k/1024;
                            //String g =String.valueOf(f);


                            msg = msg + c;


                            String str = c.toString();    //converting string biulder into string
                            final int value = Integer.parseInt(str);
                            //final float f = 5* value/1024 ; its worke for
                            //msg = msg  + c +"\n" ;
                            c.setLength(0); // for clerar the string builder next time use
                            handler.post(new Runnable() {


                                @Override
                                public void run() {

                                    series.appendData(new DataPoint(lastX++, value), true, 10000);


                                }
                            });

                        } else {

                            c.append(s); // store in string bulider


                        }
                    }

                    Thread.sleep(100); // till here new code


                }
            } catch (Exception e) {
            }
        }
    }
}

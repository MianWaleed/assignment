package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Show extends AppCompatActivity {

    TextView fName,lName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        fName = findViewById(R.id.first_name_tv);
        lName = findViewById(R.id.last_name_tv);
        Intent intent = getIntent();
        String first = intent.getExtras().getString("fName");
        String last = intent.getExtras().getString("lName");
        fName.setText(first);
        lName.setText(last);

    }
}

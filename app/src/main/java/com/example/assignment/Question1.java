package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class Question1 extends AppCompatActivity {
    private TextView colorTV;
    private Button colorButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question1);
        colorButton = findViewById(R.id.color_button);
        colorTV = findViewById(R.id.color_tv);


        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random rnd = new Random();
                int a,b,c;
                a = rnd.nextInt(256);
                b = rnd.nextInt(256);
                c = rnd.nextInt(256);
                int color = Color.argb(255,a ,b ,c );
                colorTV.setTextColor(color);
                colorTV.setText("Color : "+ a +"r "+b+"g " +c+"b ");
            }
        });
    }
}

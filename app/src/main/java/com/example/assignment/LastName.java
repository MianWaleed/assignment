package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LastName extends AppCompatActivity {
    private  EditText lastName;
    private  Button  nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_name);

        Intent intent = getIntent();
        lastName = findViewById(R.id.last_name_et);
        nextButton = findViewById(R.id.next_2_button);
        final String fname = intent.getExtras().getString("fName");

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showIntent = new Intent(LastName.this,Show.class);
                showIntent.putExtra("lName",lastName.getText().toString());
                showIntent.putExtra("fName",fname);
                startActivity(showIntent);
            }
        });
    }
}
